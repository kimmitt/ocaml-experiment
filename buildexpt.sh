export PREFIX=/usr/local/expt
./configure -prefix $PREFIX -cc 'gcc -m32 -g' -host `./config/gnu/config.guess|sed s=x86_64=i386=` -as 'as -arch i386' -aspp 'gcc -c -m32 -g'
make coldstart TOP=$PWD
find asmcomp -type l|xargs rm
rm -f asmcomp/*.cmo
rm -f asmcomp/*.cmi
rm -f asmcomp/emit.ml
rm -f utils/config.ml
make ocamlopt TOP=$PWD ARCH=expt
mkdir -p $PREFIX/bin
cp ocamlopt $PREFIX/bin
cp byterun/ocamlrun $PREFIX/bin
make -C asmrun
make -C stdlib stdlib.cmxa std_exit.cmx COMPILER=../ocamlopt
rm -rf `./ocamlopt -where`
mkdir -p `./ocamlopt -where`
cp -p asmrun/libasmrun.a stdlib/stdlib.cmxa stdlib/*.{o,cm[ix]} `./ocamlopt -where`
