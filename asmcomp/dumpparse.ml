open Dumpgram
open Dumplex
open Arch

let make_symbol s = Compilenv.make_symbol ~unitname:state.symbol_prefix s

let cnv = function
  | IDSTR id -> "\""^id^"\""
  | oth -> Ord.getstr oth

let parse verbose str =
  try
  let includes = open_in str in
  if verbose then print_endline str;
  Array.iteri (fun i x -> history.(i) <- hist_init()) history;
  histcnt := 0;
  let lexbuf = Lexing.from_channel includes in
  let (id,q,p) = Dumpgram.start Dumplex.token lexbuf in
  ((id,str,q,p):marshal_object)
  with Parsing.Parse_error ->
    failwith (
	      "Parsing error after "^
	      string_of_int (history.(!histcnt).strt)^
	      ":"^string_of_int (history.(!histcnt).stop)^
	      " characters, last token: "^
	      String.concat ";" (List.map (fun ix -> cnv (history.(ix).tok))
					  (Array.to_list 
					   (let len = min 20 !histcnt in Array.init len
						(fun ix -> !histcnt-len+ix+1)))))

let stem str = if String.contains str '/' then
    let base = String.rindex str '/' in
    String.sub str (base+1) (String.length str - base - 1) else
    str

let marshalling_yard = Hashtbl.create 256

let dump_code fil p =
  state.symbol_prefix <- fil;
(*
  let strm = open_out (fil^".dump") in
  if true then output_string strm (String.concat "\n" (List.map cnv_dump p));
  close_out strm
*)
  ()

let hash_startup_data (id,fil,q,p) =
  state.symbol_prefix <- fil;
  List.iter (fun ((str:string),(itmlst:emit_data_item list)) ->
    let itmlst' = List.rev itmlst in
    List.iter (function
      | Esymbol_address s -> Hashtbl.replace state.used s ()
      | _ -> ()) itmlst';
    Hashtbl.add state.global str fil;
    hash_add_dat str itmlst') q

let hash_startup (id,fil,q,p) =
  state.symbol_prefix <- fil;
  dump_code fil p;
  List.iter (fun arg -> emit_prg state 45 arg; match arg with
  | Elabdef(Elab str) -> Hashtbl.add state.global str fil;
  | Efundecl(n,str,_) -> Hashtbl.add state.global str fil;
  | Ecall(Econstsym(true, s), _) -> Hashtbl.replace state.used s ()
  | oth -> ()) p

let hash_data verbose (id,fil,q,p) =
  state.symbol_prefix <- fil;
  if verbose then print_endline (fil^" data");
  List.iter (fun ((str:string),(itmlst:emit_data_item list)) ->
  Hashtbl.add state.global str fil;
  hash_add_dat str (List.rev itmlst)) q

let hash_code verbose (id,fil,q,p) =
  state.symbol_prefix <- fil;
  if verbose then print_endline (fil^" code");
  dump_code fil p;
  let key = ref "default_code" in List.iter (function
    | Elabdef(Elab lbl) as lab ->
      key := lbl;
      repl state.lib !key lab;
      Hashtbl.add state.global lbl fil;
    | Efundecl(n,lbl,_) as lab ->
      key := lbl;
      repl state.lib !key lab;
      Hashtbl.add state.global lbl fil;
    | oth ->
      repl state.lib !key oth) p

let unmarshal' verbose arg =
  let inchan = open_in arg in
  if verbose then print_endline ("Unmarshal: "^arg);
  try
    let rslt = Marshal.from_channel inchan in
    close_in inchan;
    rslt
  with Failure _ | End_of_file ->
    close_in inchan;
    raise (Failure "unmarshal")
 
let hashtbl_marshalling_yard verbose id (id,b,c,d) =
  if verbose then print_endline ("Hashtbl add: "^id);
  if Hashtbl.mem marshalling_yard id then failwith (id^": "^b);
  Hashtbl.replace marshalling_yard id (id,b,c,d)

let unmarshal verbose arg =
  try
    let (id,b,c,d:marshal_object) = unmarshal' verbose arg in
    hashtbl_marshalling_yard verbose id (id,b,c,d);
    id
  with
    Failure "unmarshal" -> print_endline ("Skipping: "^arg); ""

let dump verbose exec asm arg libpath liblst =
  let (assoc:(string*marshal_object) list ref) = ref [] in
  if verbose then List.iter (fun asm -> print_endline ("Asm "^asm)) asm;
  if verbose then List.iter (fun arg -> print_endline ("Object "^arg)) arg;
  if verbose then List.iter (fun lib -> print_endline ("Path "^lib)) libpath;
  if verbose then List.iter (fun (_,lib) -> print_endline ("Lib "^lib)) liblst;
(*
  let myglobals = "globals" in
  hashtbl_marshalling_yard verbose myglobals (myglobals,myglobals,fst caml_globals__dump, snd caml_globals__dump);
  let myenviron = "environ" in
  hashtbl_marshalling_yard verbose myenviron (myenviron,myenviron,fst caml_environ__dump, snd caml_environ__dump);
*)
  let startup = unmarshal verbose (List.hd arg) in
  List.iteri (fun ix arg -> let (id,b,c,d) = parse verbose arg in hashtbl_marshalling_yard verbose id (id,b,c,d)) asm;

  List.iter (fun arg -> ignore (unmarshal verbose arg)) (List.tl arg);
  
  List.iter (fun (lib',lib) ->
    let abs = lib.[0] = '/' in
    let rel = String.contains lib '/' in
    List.iter (fun path -> 
    let src = if abs then lib else if rel then lib else path^"/"^lib in
    if verbose then print_endline ("Trying library path "^src);
    if Sys.file_exists src then
      begin
	if verbose then print_endline ("Parsing "^src);
	try
	  let (rslt:(string*marshal_object) list) = unmarshal' verbose src in
	  assoc := List.map (fun (str,obj) -> 
	    let ridx = if String.contains str '/' then
		String.rindex str '/'
	      else
		0
	    in
	    (String.sub str (ridx) (String.length str - ridx - 2), obj)) rslt @ !assoc
	with
	  Failure "unmarshal" -> print_endline ("Skipping: "^src^" (invalid format)")
      end
    else if verbose then print_endline ("Not parsing non-existent path "^src)) (if abs || rel then ["/"] else libpath)
  ) liblst;

  state.prg <- [];
  Hashtbl.clear state.dat;
  Hashtbl.clear state.lib;
  Hashtbl.clear state.used;
  state.stack_offset <- 0;
(*
  Hashtbl.clear state.decl_lst;
*)
  state.symbol_prefix <- "symbol_prefix";
  Hashtbl.clear state.global;
  Hashtbl.clear state.extcall;

  Hashtbl.iter (fun k x -> if k = startup then hash_startup_data x else hash_data verbose x) marshalling_yard;

  if verbose then List.iter (fun (str,marshal_obj) -> print_endline ("Hashed: "^str)) !assoc;

  List.iter (function
    | Esymbol_address itm ->
      let lwr = String.make 1 (Char.lowercase(itm.[5])) ^ (String.sub itm 6 (String.length itm - 6)) in
      let localobj = Hashtbl.mem marshalling_yard lwr in
      let hashed = List.mem_assoc lwr !assoc in
      let chk (id,b,c,d:marshal_object) src = 
        if not (Hashtbl.mem marshalling_yard id) then
	  begin
	    hashtbl_marshalling_yard verbose id (id,b,c,d);
	    hash_data verbose (id,b,c,d)
	  end
        else print_endline ("Skipping existing module "^id^" (path "^src^")") in
      if verbose then print_endline ("Global "^itm^"("^lwr^"): (local="^string_of_bool localobj^", library="^string_of_bool hashed^")");
      Hashtbl.replace state.used itm ();
      if hashed then 
	  chk (List.assoc lwr !assoc) lwr
      else List.iter (fun path -> 
          let src = path^"/"^lwr^".o" in
          if verbose then print_endline ("Trying path "^src);
	  if Sys.file_exists src then
	    begin
	      if verbose then print_endline ("Parsing "^src);
	      try
		chk (unmarshal' verbose src) src
	      with
		Failure "unmarshal" -> print_endline ("Skipping: "^src^" (invalid format)")
	    end
          else if verbose then print_endline ("Not parsing non-existent path "^src)) libpath
    | _ -> ()) (Hashtbl.find state.dat "_caml_globals");
  Hashtbl.iter (fun k x -> if k <> startup then hash_code verbose x) marshalling_yard;
  emit_label_def 200 "caml_reset" 0;
  emit_prg state 201 (Ecopy (Eshiftconst32 (false, 4l, !memsiz), Eregstore 13));
  emit_prg state 201 (Ecopy (Earith(Esub, Eregload 13, Eintconst32 32l), Eregstore 13));
  if mygetenv "TIGHT_ALLOC" > 0 then
    emit_prg state 202 (Ecopy (Econstsym(true, "init_heap"), Eregstore 10))
  else
    emit_prg state 202 (Ecopy (Eshiftconst32 (false, 1l, 16), Eregstore 10));
  emit_prg state 203 (Ecopy (Eintconst32 0l, Eregstore 12));
  List.iteri (fun ix s ->
	let label_prefix = "caml" and lab = ix+1000001 in
        emit_prg state 206 (Ecall (Econstsym(true, emit_symbol s), []));
	emit_label_def 207 label_prefix lab
	  ) ["_caml_program"; "_caml_testheap"];
  emit_prg state 213 ( Elabdef(Elabel("caml", 100)) );
  emit_prg state 214 ( Ecaml_sys_exit() );
  emit_prg state 215 ( Econd(Enop, Egoto(Elabel("caml", 100))));
  List.iter (fun arg -> hash_startup (Hashtbl.find marshalling_yard arg)) [startup];
  if verbose then print_endline "dump";
  dump_code "combined_startup" state.prg;
  dump_code "debug_startup" (let (_,_,_,p) = (Hashtbl.find marshalling_yard startup) in p);
  if mygetenv "OCAML_SYMTAB" > 0 then
    begin
    let symtab = open_out "symtab" in
    Hashtbl.iter (fun k _ -> output_string symtab ("Used: "^k^"\n")) state.used;
    Hashtbl.iter (fun _ (id,fil,q,p) ->
      output_string symtab (id^" -> "^fil^"\n");
      ) marshalling_yard;
    Hashtbl.iter (fun str fil -> output_string symtab (str^" -> "^fil^"\n")) state.global;
    close_out symtab
    end
