open Emitaux
open Dumpparse
open Arch

let call_linker exec files extra =
  let arglst = (List.rev !Clflags.ccopts) @ files @ if extra <> "" then [extra] else [] in
  let libpath = List.filter ((<>) "") !Config.load_path in
        (* choose target architecture *)
        let objlst = ref [] and liblst = ref [] and asmlst = ref [] in
        List.iter (fun arg ->
          if arg.[0] = '-' then
            (match arg.[1] with
              | 'l' -> liblst := (arg,"lib"^String.sub arg 2 (String.length arg - 2)^".a") :: !liblst
              | _   -> failwith arg)
          else (match String.sub arg (String.length arg - 2) 2 with
              | ".a" -> liblst := (arg,arg) :: !liblst
              | ".o" -> objlst := arg :: !objlst
              | ".s" -> asmlst := arg :: !asmlst
              | _    -> objlst := arg :: !objlst)) arglst;
        let verbose = mygetenv "DUMP_VERBOSE" > 0 in
	let interm = (match !Arch.simulator with Cver | Verilator | Icarus -> exec ^ "_gen.v" | _ -> exec ^ "_gen.c") in
	output_channel := open_out interm;
        dump verbose interm (List.rev !asmlst) (List.rev !objlst) libpath !liblst;
	if verbose then print_endline "assembly";
	Hashtbl.remove state.dat "_caml_frametable";
	Hashtbl.remove state.dat "_caml_globals_map";
        Hashtbl.remove state.dat "_caml_code_segments";
        Hashtbl.remove state.dat "_caml_data_segments";
	state.linkphase <- Some (Sys.getcwd()^"/"^exec);
	Emit.end_assembly();
	close_out !output_channel;
	let wd = Sys.getcwd()^"/" in
        let cmdline = (match !Arch.simulator with
	| Cver -> 
          let fd = open_out_gen [Open_wronly; Open_creat; Open_trunc; Open_text] 0o770 exec in
          output_string fd ("exec /usr/local/bin/cver -q -l "^exec^".log "^(if !Clflags.debug then "+DBG_DUMPVARS " else " ")^"+define+state_mem "^wd^interm^" "^wd^exec^"_tb.v "^wd^exec^"_a23.v $@\n");
          close_out fd;
	  "echo script saved in: "^exec
	| Verilator ->
	  let fd = open_out (wd^interm^"_main.cpp") in
          output_string fd ("      #include \"V"^exec^"_gen.h\"\n");
          output_string fd ("      #include \"verilated.h\"\n");
          output_string fd ("      #include \"verilated_vcd_c.h\"\n");
	  output_string fd ("      vluint64_t main_time = 0; // Current simulation time\n");
          output_string fd ("      double sc_time_stamp () { // Called by $time in Verilog\n");
	  output_string fd ("      return main_time; } // converts to double, to match what SystemC does\n\n");
          output_string fd ("      int main(int argc, char **argv, char **env) {\n");
          output_string fd ("          Verilated::commandArgs(argc, argv);\n");
          output_string fd ("	  Verilated::traceEverOn(true);\n");
          output_string fd ("	  VerilatedVcdC* tfp = new VerilatedVcdC;\n");
          output_string fd ("          V"^exec^"_gen* top = new V"^exec^"_gen;\n");
          output_string fd ("	  tfp->open (\"obj_dir/simx.vcd\");\n");
          output_string fd ("          while (!Verilated::gotFinish()) {\n");
          output_string fd ("	    top->rst = main_time < 10;\n");
          output_string fd ("	    top->eval();\n");
          output_string fd ("	    tfp->dump(main_time);\n");
          output_string fd ("	    top->a23_clk = main_time++ & 1;\n");
          output_string fd ("	    if (top->writech & 128) putchar(top->writech&127);\n");
          output_string fd ("	  }\n");
          output_string fd ("	  tfp->close();\n");
          output_string fd ("          delete top;\n");
          output_string fd ("          exit(0);\n");
          output_string fd ("      }\n");
          close_out fd;
          "verilator --debug --gdbbt -I.. -I../verilog/amber23 -cc "^interm^" --exe "^interm^"_main.cpp -CFLAGS -g -CFLAGS -Wno-parentheses-equality -CFLAGS -fbracket-depth=99999 && make -C obj_dir -f V"^exec^"_gen.mk"
        | Icarus ->
          let fd = open_out_gen [Open_wronly; Open_creat; Open_trunc; Open_text] 0o770 exec in
          output_string fd ("ix=0\nset -o noglob\nargs=\"/usr/local/bin/vvp "^wd^exec^".vvp \"\n");
	  output_string fd ("for i in $0 $@; do args=\"$args +ARGV$ix=$i\"; ix=$(($ix + 1)); done\n");
	  output_string fd ("exec $args\n");
          close_out fd;
          "/usr/local/bin/iverilog -I .. -o "^exec^".vvp "^wd^interm
        | Cstandalone | Cstate_machine ->
	  "gcc -m32 "^(if !Clflags.debug then "-g -DDBG " else "-Os ")^"-o "^ exec ^" "^interm^" -DSTANDALONE -lm -ldl"
        | Csim | Cconst_seg_opt ->
	  "gcc -m32 "^(if !Clflags.debug then "-g -DDBG " else "-Os ")^"-o "^ exec ^" "^ interm ^(String.concat "" (List.map (fun arg -> " -I "^arg) libpath)^" "^String.concat "" (List.map (fun arg -> " -L "^arg) libpath)^" "^String.concat " " (List.filter (fun arg -> arg.[0]='-') (List.map fst !liblst))^" -lasmrun -lm -ldl")) in
        print_endline cmdline;
        Sys.command cmdline = 0

let call_linker_partial interm arg =
  let (id,fil,q,p) = unmarshal' false (List.hd arg) in
  let rcode = ref p and rdata = ref q in
  List.iter (fun arg -> let (id',fil',q',p') = unmarshal' false arg in
  rcode := p' @ !rcode;
  rdata := q' @ !rdata) (List.tl arg);
      
  let (rslt:marshal_object) = (id,fil,!rdata,!rcode) in
  let output_channel = open_out interm in
  Marshal.to_channel output_channel rslt [];
  close_out output_channel;
  true

let create_archive archive file_list =
  Misc.remove_file archive;
  let lst = ref [] in
  List.iter (fun arg ->
    let (id',fil',q',p') = unmarshal' false arg in
    if !Clflags.verbose then print_endline (arg^" -> "^id');
    lst := (arg, (id',fil',q',p')) :: !lst
  ) file_list;
  let (rslt:(string*marshal_object) list) = !lst in
  let output_channel = open_out archive in
  Marshal.to_channel output_channel rslt [];
  close_out output_channel;
  if !Clflags.verbose then print_endline (archive^" -> "^string_of_int (List.length !lst)^" member(s)");
  0
