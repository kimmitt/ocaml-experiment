(*
val arch_sel : unit -> Arch.arch
*)
val make_symbol : string option -> string
val cnv : Dumpgram.token -> string
val parse : bool -> string -> Arch.marshal_object
val stem : string -> string
(*
val caml_environ__dump :
  (string * Arch.emit_data_item list) list * Arch.emit_t list
val caml_globals__dump :
  (string * Arch.emit_data_item list) list * Arch.emit_t list
*)
val marshalling_yard :
  (string,
   string * string * (string * Arch.emit_data_item list) list *
   Arch.emit_t list)
  Hashtbl.t
val dump_code : string -> 'a -> unit
val hash_startup_data :
  'a * string * (string * Arch.emit_data_item list) list * 'b -> unit
val hash_startup : 'a * string * 'b * Arch.emit_t list -> unit
val hash_data :
  bool -> 'a * string * (string * Arch.emit_data_item list) list * 'b -> unit
val hash_code : bool -> 'a * string * 'b * Arch.emit_t list -> unit
val unmarshal' : bool -> string -> Arch.marshal_object
val hashtbl_marshalling_yard :
  bool ->
  'a ->
  string * string * (string * Arch.emit_data_item list) list *
  Arch.emit_t list -> unit
val unmarshal : bool -> string -> string
val dump :
  bool ->
  string ->
  string list -> string list -> string list -> ('a * string) list -> unit
