(*
    <vscr - Verilog converter to abc format.>
    Copyright (C) <2011,2012>  <Jonathan Richard Robert Kimmitt>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

{
open Lexing
open Dumpgram

type hist = {tok:token;strt:int;stop:int;key:bool};;

let ksymbols = Hashtbl.create 256;;
let hsiz = 1024;;
let hist_init () = {tok=UNKNOWN;strt=0;stop=0;key=false};;
let histcnt = ref 0;;
let history = Array.init hsiz (fun i -> hist_init())

let (tsymbols : (string, token) Hashtbl.t) = Hashtbl.create 256

let enter_keyword id keyword = 
if Hashtbl.mem ksymbols id then
  Printf.printf "Error: repeated keyword %s\n" id
else begin
(*  Printf.printf "Enter %s\n" id; *)
  Hashtbl.add ksymbols id keyword
  end

let hlog lexbuf ktok':token = begin
histcnt := (!histcnt+1)mod hsiz;
history.(!histcnt) <- {tok=ktok';strt=(Lexing.lexeme_start lexbuf);stop=(Lexing.lexeme_end lexbuf);key=true};
ktok'
end

let _ = List.iter (fun (str,key) -> enter_keyword str key)
[
( "Enop", ENOP );
( "Exit", EXIT );
( "Eabort", EABORT );
( "Efundecl", EFUNDECL );
( "Einit", EINIT );
( "Eblockcopy", EBLOCKCOPY );
( "Eregload", EREGLOAD );
( "Eregstore", EREGSTORE );
( "Elab", ELAB );
( "Eadj", EADJ );
( "Emove", EMOVE );
( "Ecopy", ECOPY );
( "Ecall", ECALL );
( "Eextcall", EEXTCALL );
( "Eexternalcall", EEXTERNALCALL );
( "Eresult", ERESULT );
( "Egoto", EGOTO );
( "Econd", ECOND );
(*
( "Etailcallind", ETAILCALLIND );
( "Etailcallimm", ETAILCALLIMM );
*)
( "Ereloadretaddr", ERELOADRETADDR );
( "Ereturn", ERETURN );
( "Econdbranch3", ECONDBRANCH3 );
( "Eswitch", ESWITCH );
( "Esetuptrap", ESETUPTRAP );
( "Epushtrap", EPUSHTRAP );
( "Epoptrap", EPOPTRAP );
( "Etrap", ETRAP );
( "Ecomment", ECOMMENT );
( "Estackloadref", ESTACKLOADREF );
( "Estackstoreref", ESTACKSTOREREF );
( "Eunknownstore", EUNKNOWNSTORE );
( "Eunknownload", EUNKNOWNLOAD );
( "Eoffset", EOFFSET );
( "Ebased", EBASED );
( "Estatecase", ESTATECASE );
( "Ereadaddrb", EREADADDRB );
( "Ewriteaddrb", EWRITEADDRB );
( "Ereadaddrd", EREADADDRD );
( "Ewriteaddrd", EWRITEADDRD );
( "Ereadaddrw", EREADADDRW );
( "Ewriteaddrw", EWRITEADDRW );
( "Earith", EARITH );
( "Efloatop", EFLOATOP );
( "Efloatfunc", EFLOATFUNC );
( "Efloatconst", EFLOATCONST );
( "Econstsym", ECONSTSYM );
( "Ealloc", EALLOC );
(*
( "Eallocate", EALLOCATE );
*)
( "Ecompare", ECOMPARE );
( "Ecompare_flt", ECOMPARE_FLT );
( "Ebool", EBOOL );
( "Elabel", ELABEL );
( "Elabdef", ELABDEF );
( "Elabelref", ELABELREF );
( "Eglobal", EGLOBAL );
( "Esigned", ISIGNED );
( "Eunsigned", IUNSIGNED );
( "Eeq", CEQ );
( "Ene", CNE );
( "Elt", CLT );
( "Ele", CLE );
( "Egt", CGT );
( "Ege", CGE );
( "Eglobal_symbol", CGLOBAL_SYMBOL );
( "Edefine_symbol", CDEFINE_SYMBOL );
( "Edefine_label", CDEFINE_LABEL );
( "Eint8", CINT8 );
( "Eint16", CINT16 );
( "Eint32", CINT32 );
( "Eintconst32", CINT32 );
( "Eint", CINT );
( "Esingle", CSINGLE );
( "Edouble", CDOUBLE );
( "Eshiftconst32", ESHIFTINT32 );
( "Esymbol_address", CSYMBOL_ADDRESS );
( "Esymbol_address_off", CSYMBOL_ADDRESS_OFF );
( "Elabel_address", CLABEL_ADDRESS );
( "Estring", CSTRING );
( "Eskip", CSKIP );
( "Ealign", CALIGN );
( "Emisc", CMISC );
( "Eadd", EADD );
( "Esub", ESUB );
( "Emul", EMUL );
( "Ediv", EDIV );
( "Emod", EMOD );
( "Eand", EAND );
( "Eor",  EOR );
( "Exor", EXOR );
( "Elsl", ELSL );
( "Easr", EASR );
( "Elsr", ELSR );
( "open", OPEN );
( "let",  LET );
( "Some", SOME );
( "None", NONE );
( "Edebug_char", EDEBUG_CHAR );
( "Edebug_int", EDEBUG_INT );
( "Edebug_ptr", EDEBUG_PTR );
( "Eio_read", EIO_READ );
( "Eio_write", EIO_WRITE );
( "Eml_array_bound_error", EML_ARRAY_BOUND_ERROR );
( "Eml_blit_string", EML_BLIT_STRING );
( "Eml_close_channel", EML_CLOSE_CHANNEL );
( "Eml_compare", EML_COMPARE );
( "Eml_create_raw", EML_CREATE_RAW );
( "Eml_enter_raw_mode", EML_ENTER_RAW_MODE );
( "Eml_equal", EML_EQUAL );
( "Eml_flush", EML_FLUSH );
( "Eml_leave_raw_mode", EML_LEAVE_RAW_MODE );
( "Eml_make_vect", EML_MAKE_VECT );
( "Eml_modify", EML_MODIFY );
( "Eml_notequal", EML_NOTEQUAL );
( "Eml_open_descriptor_in", EML_OPEN_DESCRIPTOR_IN );
( "Eml_open_descriptor_out", EML_OPEN_DESCRIPTOR_OUT );
( "Eml_open_term", EML_OPEN_TERM );
( "Eml_output_char", EML_OUTPUT_CHAR );
( "Eml_print_raw_char", EML_PRINT_RAW_CHAR );
( "Eml_read_raw_char", EML_READ_RAW_CHAR );
( "Eml_string_notequal", EML_STRING_NOTEQUAL );
( "Eprint_char", EPRINT_CHAR );
( "Eraise_exn", ERAISE_EXN );
( "Especific", ESPECIFIC );
( "Efloatspecial", EFLOATSPECIAL );
( "Eformatspecial", EFORMATSPECIAL );
( "Enegf", ENEGF );
( "Eabsf", EABSF );
( "Eaddf", EADDF );
( "Esubf", ESUBF );
( "Emulf", EMULF );
( "Edivf", EDIVF );
( "Efloatofint", EFLOATOFINT );
( "Eintoffloat", EINTOFFLOAT );
( "Ishiftarith", ISHIFTARITH );
( "Ishiftcheckbound", ISHIFTCHECKBOUND );
( "Irevsubimm", IREVSUBIMM );
( "Imuladd", IMULADD );
( "Imulsub", IMULSUB );
( "Inegmulf", INEGMULF );
( "Imuladdf", IMULADDF );
( "Inegmuladdf", INEGMULADDF );
( "Imulsubf  ", IMULSUBF   );
( "Inegmulsubf", INEGMULSUBF );
( "Isqrtf    ", ISQRTF     );
( "Ibswap", IBSWAP );
( "Ishiftadd", ISHIFTADD );
( "Ishiftsub", ISHIFTSUB );
( "Ishiftsubrev", ISHIFTSUBREV );
];;

}

let digit = ['0'-'9']
let ident = ['a'-'z' 'A'-'Z' '_' '$']
let ident_num = ['a'-'z' 'A'-'Z' '_' '0'-'9' '$']

rule token = parse
| "\"" { let verbose = (if false then print_endline else fun _ -> ()) in
         hlog lexbuf (ASCNUM (string1 verbose (Lexing.lexeme_start lexbuf) lexbuf)) }
| "/*" { comment1 (Lexing.lexeme_start lexbuf) lexbuf; token lexbuf }
| "(*" { comment2 (Lexing.lexeme_start lexbuf) lexbuf; token lexbuf }
| "&" { hlog lexbuf (P_AMPERSAND) }
| "@" { hlog lexbuf (AT) }
| "^" { hlog lexbuf (P_CARET) }
| ":" { hlog lexbuf (COLON) }
| "," { hlog lexbuf (COMMA) }
| "/" { hlog lexbuf (DIVIDE) }
| "=" { hlog lexbuf (EQUALS) }
| ">" { hlog lexbuf (GREATER) }
| "#" { hlog lexbuf (HASH) }
| "[" { hlog lexbuf (LBRACK) }
| "{" { hlog lexbuf (LCURLY) }
| "<" { hlog lexbuf (LESS) }
| "(" { hlog lexbuf (LPAREN) }
| "-" { hlog lexbuf (MINUS) }
| "%" { hlog lexbuf (MODULO) }
| "." { hlog lexbuf (DOT) }
| "!" { hlog lexbuf (PLING) }
| "+" { hlog lexbuf (PLUS) }
| "?" { hlog lexbuf (QUERY) }
| "]" { hlog lexbuf (RBRACK) }
| "}" { hlog lexbuf (RCURLY) }
| ")" { hlog lexbuf (RPAREN) }
| ";" { hlog lexbuf (SEMICOLON) }
| "~" { hlog lexbuf (P_TILDE) }
| "*" { hlog lexbuf (TIMES) }
| "|" { hlog lexbuf (P_VBAR) }
| digit+'.'digit* as floatnum { hlog lexbuf ( FLOATNUM ( float_of_string floatnum ) ) }
| '0'['b' 'B']['0' '1' 'x' 'X' 'z' 'Z' '?' '_']+ as bnum { hlog lexbuf (BINNUM bnum ) }
| '0'['o' 'O']['0'-'7' 'x' 'X' 'z' 'Z' '?' '_']+ as onum { hlog lexbuf (OCTNUM onum ) }
| '\"''0'['x' 'X']['0'-'9' 'A'-'F' 'a'-'f' 'x' 'X' 'z' 'Z' '?' '_']+'\"' as hnum { hlog lexbuf (HEXNUM (String.sub hnum 3 (String.length hnum - 4)) ) }
| '0'['x' 'X']['0'-'9' 'A'-'F' 'a'-'f' 'x' 'X' 'z' 'Z' '?' '_']+'n' as hnum { hlog lexbuf (NATNUM (try Nativeint.of_string (String.sub hnum 0 (String.length hnum - 1))
									    with Failure _ -> failwith hnum)) }
| '-'* digit+ 'n' as inum {
    hlog lexbuf (NATNUM (try Nativeint.of_string (String.sub inum 0 (String.length inum - 1))
    with Failure _ -> failwith inum)) }
| digit+ 'l' as inum { hlog lexbuf (INTNUM (String.sub inum 0 (String.length inum - 1))) }
| digit+ as inum { hlog lexbuf (INTNUM inum ) }
| ident ident_num* as word {
if Hashtbl.mem ksymbols word then hlog lexbuf (Hashtbl.find ksymbols word) else hlog lexbuf (IDSTR word)
}
  | [' ' '\t' ]		{token lexbuf }
  | ['\r' '\n' ]	{token lexbuf }
  | eof		{ hlog lexbuf (ENDOFFILE) }
  | _		{ hlog lexbuf (ILLEGAL ( lexeme_char lexbuf 0 ) ) }

and string1 print start = parse
| "\\\'"
    { let str = Lexing.lexeme lexbuf in print str; "\'"^string1 print start lexbuf }
| "\\\""
    { let str = Lexing.lexeme lexbuf in print str; "\""^string1 print start lexbuf }
| "\\\\"
    { let str = Lexing.lexeme lexbuf in print str; "\\"^string1 print start lexbuf }
| "\\n"
    { let str = Lexing.lexeme lexbuf in print str; "\n"^string1 print start lexbuf }
| "\\r"
    { let str = Lexing.lexeme lexbuf in print str; "\r"^string1 print start lexbuf }
| "\\t"
    { let str = Lexing.lexeme lexbuf in print str; "\t"^string1 print start lexbuf }
| "\\b"
    { let str = Lexing.lexeme lexbuf in print str; "\b"^string1 print start lexbuf }
| "\""
    { "" }
| "\n"
    { failwith (Printf.sprintf "Newline in string at offset %d." start) }
| eof
    { failwith (Printf.sprintf "Unterminated string at offset %d." start) }
| _ as oth
    { let str = String.make 1 oth in print str; str^string1 print start lexbuf }

and comment1 start = parse
  "/*"
    { comment1 (Lexing.lexeme_start lexbuf) lexbuf; comment1 start lexbuf }
| "*/"
    { () }
| eof
    { failwith (Printf.sprintf "Unterminated /* comment */ at offset %d." start) }
| _
    { comment1 start lexbuf }

and comment2 start = parse
  "(*"
    { comment2 (Lexing.lexeme_start lexbuf) lexbuf; comment2 start lexbuf }
| "*)"
    { () }
| eof
    { failwith (Printf.sprintf "Unterminated (* comment *) at offset %d." start) }
| _
    { comment2 start lexbuf }
