(***********************************************************************)
(*                                                                     *)
(*                                OCaml                                *)
(*                                                                     *)
(*                  Benedikt Meurer, University of Siegen              *)
(*                                                                     *)
(*    Copyright 1998 Institut National de Recherche en Informatique    *)
(*    et en Automatique. Copyright 2012 Benedikt Meurer. All rights    *)
(*    reserved.  This file is distributed  under the terms of the Q    *)
(*    Public License version 1.0.                                      *)
(*                                                                     *)
(***********************************************************************)

(* Specific operations for the ARM processor *)

open Format

type abi = EABI_unknown | EABI | EABI_HF
type arch = ARMv2a | ARMv4 | ARMv5 | ARMv5TE | ARMv6 | ARMv6T2 | ARMv7
type fpu = Soft | VFPv2 | VFPv3_D16 | VFPv3
type simulator = Csim | Cconst_seg_opt | Cstandalone | Cstate_machine | Cver | Verilator | Icarus
type memargs = bool
type simple = bool
type memsiz = int
type pclen = int

let dflt_abi =
  match Config.system with
    "linux_eabi"   -> EABI
  | "linux_eabihf" -> EABI_HF
  | _ -> EABI_unknown

let string_of_sim = function
  | Csim -> "csim"
  | Cconst_seg_opt -> "const-seg"
  | Cstandalone -> "standalone"
  | Cstate_machine -> "state-mach"
  | Cver -> "cver"
  | Verilator -> "verilator"
  | Icarus -> "icarus"

let string_of_arch = function
    ARMv2a  -> "armv2a"
  | ARMv4   -> "armv4"
  | ARMv5   -> "armv5"
  | ARMv5TE -> "armv5te"
  | ARMv6   -> "armv6"
  | ARMv6T2 -> "armv6t2"
  | ARMv7   -> "armv7"

let string_of_fpu = function
    Soft      -> "soft"
  | VFPv2     -> "vfpv2"
  | VFPv3_D16 -> "vfpv3-d16"
  | VFPv3     -> "vfpv3"

let string_of_abi = function
    EABI_unknown -> "(unknown)"
  | EABI         -> "eabi"
  | EABI_HF      -> "eabi_hf"

(* Machine-specific command-line options *)

let (arch, fpu, thumb, abi) =
  let (def_arch, def_fpu, def_thumb, def_abi) =
    begin match dflt_abi, Config.model with
    (* Defaults for architecture, FPU and Thumb *)
      EABI, "armv5"    -> ARMv5,   Soft,      false, EABI
    | EABI, "armv5te"  -> ARMv5TE, Soft,      false, EABI
    | EABI, "armv6"    -> ARMv6,   Soft,      false, EABI
    | EABI, "armv6t2"  -> ARMv6T2, Soft,      false, EABI
    | EABI, "armv7"    -> ARMv7,   Soft,      false, EABI
    | EABI, "armv4"    -> ARMv4,   Soft,      false, EABI
    | EABI_HF, "armv6" -> ARMv6,   VFPv2,     false, EABI_HF
    | EABI_HF, _       -> ARMv7,   VFPv3_D16, true,  EABI_HF
    | _, _             -> ARMv2a,  Soft,      false, EABI
    end in
  (ref def_arch, ref def_fpu, ref def_thumb, ref def_abi)

let simulator = ref Csim
let memargs = ref false
let simple = ref true
let memsiz = ref 14
let pclen = ref 16

let farch spec =
  arch := (match spec with
             "armv2a" when !abi <> EABI_HF  -> ARMv2a
           | "armv4" when !abi <> EABI_HF   -> ARMv4
           | "armv5" when !abi <> EABI_HF   -> ARMv5
           | "armv5te" when !abi <> EABI_HF -> ARMv5TE
           | "armv6"                       -> ARMv6
           | "armv6t2"                     -> ARMv6T2
           | "armv7"                       -> ARMv7
           | spec -> raise (Arg.Bad spec))

let ffpu spec =
  fpu := (match spec with
            "soft" when !abi <> EABI_HF     -> Soft
          | "vfpv2" when !abi = EABI_HF     -> VFPv2
          | "vfpv3-d16" when !abi = EABI_HF -> VFPv3_D16
          | "vfpv3" when !abi = EABI_HF     -> VFPv3
          | spec -> raise (Arg.Bad spec))

let fabi spec =
  abi := (match spec with
            "eabi"    -> EABI
          | "eabi_hf" -> EABI_HF
          | spec      -> raise (Arg.Bad spec))

let fsim spec =
  simulator := (match spec with
  | "csim"       -> Csim
  | "const-seg"  -> Cconst_seg_opt
  | "standalone" -> Cstandalone
  | "state-mach" -> Cstate_machine
  | "cver"       -> Cver
  | "verilator"  -> Verilator
  | "icarus"     -> Icarus
  | spec         -> raise (Arg.Bad spec))

let fmemargs spec =
  memargs := try bool_of_string spec with _ -> raise (Arg.Bad spec)

let fsimple spec =
  simple := try bool_of_string spec with _ -> raise (Arg.Bad spec)

let fmemsiz spec =
  memsiz := try int_of_string spec with _ -> raise (Arg.Bad spec)

let fpclen spec =
  pclen := try int_of_string spec with _ -> raise (Arg.Bad spec)

let command_line_options =
  [ "-farch", Arg.String farch,
      "<arch>  Select the ARM target architecture"
      ^ " (default: " ^ (string_of_arch !arch) ^ ")";
    "-fmemargs", Arg.String fmemargs,
      "<memargs>  Select passing arguments in memory"
      ^ " (default: " ^ (string_of_bool !memargs) ^ ")";
    "-fsimple", Arg.String fsimple,
      "<simple>  Select simplification of extcalls to hardware"
      ^ " (default: " ^ (string_of_bool !simple) ^ ")";
    "-fmemsiz", Arg.String fmemsiz,
      "<memsiz>  Select Verilog memory size (as a power of 2)"
      ^ " (default: " ^ (string_of_int !memsiz) ^ ")";
    "-fpclen", Arg.String fpclen,
      "<pclen>  Select Verilog program counter length (in bits)"
      ^ " (default: " ^ (string_of_int !pclen) ^ ")";
    "-fabi", Arg.String fabi,
      "<abi>  Select the ARM target ABI"
      ^ " (default: " ^ (string_of_abi dflt_abi) ^ ")";
    "-ffpu", Arg.String ffpu,
      "<fpu>  Select the floating-point hardware"
      ^ " (default: " ^ (string_of_fpu !fpu) ^ ")";
    "-fsim", Arg.String fsim,
      "<sim>  Select the target simulator"
      ^ " (default: " ^ (string_of_sim !simulator) ^ ")";
    "-fthumb", Arg.Set thumb,
      " Enable Thumb/Thumb-2 code generation"
      ^ (if !thumb then " (default)" else "");
    "-fno-thumb", Arg.Clear thumb,
      " Disable Thumb/Thumb-2 code generation"
      ^ (if not !thumb then " (default" else "")]

(* Addressing modes *)

type addressing_mode =
    Iindexed of int                     (* reg + displ *)

(* We do not support the reg + shifted reg addressing mode, because
   what we really need is reg + shifted reg + displ,
   and this is decomposed in two instructions (reg + shifted reg -> tmp,
   then addressing tmp + displ). *)

(* Specific operations *)

type specific_operation =
    Ishiftarith of arith_operation * int
  | Ishiftcheckbound of int
  | Irevsubimm of int
  | Imuladd       (* multiply and add *)
  | Imulsub       (* multiply and subtract *)
  | Inegmulf      (* floating-point negate and multiply *)
  | Imuladdf      (* floating-point multiply and add *)
  | Inegmuladdf   (* floating-point negate, multiply and add *)
  | Imulsubf      (* floating-point multiply and subtract *)
  | Inegmulsubf   (* floating-point negate, multiply and subtract *)
  | Isqrtf        (* floating-point square root *)
  | Ibswap of int (* endianess conversion *)
  | Iflush (* flush console stream *)

and arith_operation =
    Ishiftadd
  | Ishiftsub
  | Ishiftsubrev

(* Sizes, endianness *)

let big_endian = false

let size_addr = 4
let size_int = 4
let size_float = 8
let size_int64 = 8

let allow_unaligned_access = false

(* Behavior of division *)

let division_crashes_on_overflow = false

(* Operations on addressing modes *)

let identity_addressing = Iindexed 0

let offset_addressing (Iindexed n) delta = Iindexed(n + delta)

let num_args_addressing (Iindexed n) = 1

(* Printing operations and addressing modes *)

let print_addressing printreg addr ppf arg =
  match addr with
  | Iindexed n ->
      printreg ppf arg.(0);
      if n <> 0 then fprintf ppf " + %i" n

let print_specific_operation printreg op ppf arg =
  match op with
  | Ishiftarith(op, shift) ->
      let op_name = function
      | Ishiftadd -> "+"
      | Ishiftsub -> "-"
      | Ishiftsubrev -> "-rev" in
      let shift_mark =
       if shift >= 0
       then sprintf "<< %i" shift
       else sprintf ">> %i" (-shift) in
      fprintf ppf "%a %s %a %s"
       printreg arg.(0) (op_name op) printreg arg.(1) shift_mark
  | Ishiftcheckbound n ->
      fprintf ppf "check %a >> %i > %a" printreg arg.(0) n printreg arg.(1)
  | Irevsubimm n ->
      fprintf ppf "%i %s %a" n "-" printreg arg.(0)
  | Imuladd ->
      fprintf ppf "(%a * %a) + %a"
        printreg arg.(0)
        printreg arg.(1)
        printreg arg.(2)
  | Imulsub ->
      fprintf ppf "-(%a * %a) + %a"
        printreg arg.(0)
        printreg arg.(1)
        printreg arg.(2)
  | Inegmulf ->
      fprintf ppf "-f (%a *f %a)"
        printreg arg.(0)
        printreg arg.(1)
  | Imuladdf ->
      fprintf ppf "%a +f (%a *f %a)"
        printreg arg.(0)
        printreg arg.(1)
        printreg arg.(2)
  | Inegmuladdf ->
      fprintf ppf "%a -f (%a *f %a)"
        printreg arg.(0)
        printreg arg.(1)
        printreg arg.(2)
  | Imulsubf ->
      fprintf ppf "(-f %a) +f (%a *f %a)"
        printreg arg.(0)
        printreg arg.(1)
        printreg arg.(2)
  | Inegmulsubf ->
      fprintf ppf "(-f %a) -f (%a *f %a)"
        printreg arg.(0)
        printreg arg.(1)
        printreg arg.(2)
  | Isqrtf ->
      fprintf ppf "sqrtf %a"
        printreg arg.(0)
  | Ibswap n ->
      fprintf ppf "bswap%i %a" n
        printreg arg.(0)
  | Iflush ->
      fprintf ppf "flush"

(* Specific operations for the EXPT processor *)

let mygetenv str = try int_of_string(Sys.getenv str) with Not_found -> 0

(* Recognize immediate operands *)

let is_immediate n = ((n >= 0l) && (n <= 255l))

(* dump specific *)

open Printf

(* emit format *)

type emit_data_item =
  | Edefine_symbol of string
  | Edefine_label of string * int
  | Eglobal_symbol of string
  | Eint8 of int
  | Eint16 of int
  | Eint32 of nativeint
  | Eint of nativeint
  | Esingle of string
  | Edouble of string
  | Esymbol_address of string
  | Esymbol_address_off of string * int
  | Elabel_address of string * int
  | Estring of int list
  | Eskip of int
  | Ealign of int
  | Emisc of emit_t

and emit_comparison =
  | Eeq
  | Ene
  | Elt
  | Ele
  | Egt
  | Ege

and emit_integer_comparison =
  | Esigned of emit_comparison
  | Eunsigned of emit_comparison

and emit_integer_operation =
  | Eadd | Esub | Emul | Ediv | Emod
  | Eand | Eor | Exor | Elsl | Elsr | Easr
  | Ecomp of emit_integer_comparison
  | Echeckbound

and emit_test =
  | Etruetest
  | Efalsetest
  | Einttest of emit_integer_comparison
  | Einttest_imm of emit_integer_comparison * int
  | Efloattest of emit_comparison * bool
  | Eoddtest
  | Eeventest

and emit_memory_chunk =
  | Ebyte_unsigned
  | Ebyte_signed
  | Esixteen_unsigned
  | Esixteen_signed
  | Ethirtytwo_unsigned
  | Ethirtytwo_signed
  | Eword
  | ESingle
  | EDouble                              (* 64-bit-aligned 64-bit float *)
  | EDouble_u                            (* word-aligned 64-bit float *)

and emit_operation =
  | Emove
  | Espill
  | Ereload
  | Econst_int of nativeint
  | Econst_float of string
  | Econst_symbol of string
  | Ecall_ind
  | Ecall_imm of string
  | Etailcall_ind
  | Etailcall_imm of string
  | Eextcall of string * bool
  | Estackoffset of int
  | Eload of emit_memory_chunk * addressing_mode
  | Estore of emit_memory_chunk * addressing_mode
  | Ealloc of int
  | Eintop of emit_integer_operation
  | Eintop_imm of emit_integer_operation * int
  | Enegf | Eabsf | Eaddf | Esubf | Emulf | Edivf
  | Efloatofint | Eintoffloat
  | Especific_op of specific_operation

and emit_t = 
| Enop
| Ecaml_sys_abort of unit
| Ecaml_sys_exit of unit
| Eabort of string (* abort with optional information *)
| Etask of string (* special Verilog task *)
| Etask2 of string * emit_t (* special Verilog task 2 *)
| Eregload of int (* virtual reg *)
| Eregstore of int (* virtual reg *)
| Elab of string (*label*)
| Elabdef of emit_t (* label definition *)
| Eadj of int (* stack adjust *)
| Ecopy of emit_t * emit_t (*move*)
| Ecall of emit_t * emit_t list (* call immediate / indirect *)
| Eexternalcall of string * bool * emit_t list (* external call *)
| Eresult of string * emit_t list (* result of call *)
| Egoto of emit_t (* goto *)
| Econd of emit_t * emit_t (* if then *)
(*
| Etailcallind of bool * int * emit_t * emit_t list (* tailcall indirect *)
| Etailcallimm of bool * int * string * emit_t list (* tailcall indirect *)
*)
| Ereloadretaddr of int * emit_t (* reload return address *)
| Ereturn of int (* decrement stack and return from function *)
| Eswitch of string*emit_t * int array (* match statement *)
| Esetuptrap of string*int*int (* setup trap handler for try .. fail block *)
| Epushtrap of unit (* push previous trap handler prior to try .. fail *)
| Epoptrap of unit (* pop previous trap handler after fail (or not) *)
| Ecomment of string (*comment*)
| Estackloadref of int (* stack reference *)
| Estackstoreref of int (* stack reference *)
| Eunknownstore (* unsupported store *)
| Eunknownload (* unsupported load *)
| Eoffset of emit_t*int (* offset addressing *)
| Ebased of string*int (* based addressing *)
| Estatecase of emit_t*emit_t*int (* two reg addressing *)
| Ereadaddrb of emit_t (* address memory in bytes *)
| Ewriteaddrb of emit_t (* address memory in bytes *)
| Ereadaddrw of emit_t (* address memory in words *)
| Ewriteaddrw of emit_t (* address memory in words *)
| Ereadaddrd of emit_t (* address memory in doubles *)
| Ewriteaddrd of emit_t (* address memory in doubles *)
| Earith of emit_integer_operation*emit_t*emit_t (* arith operations *)
| Efloatop of emit_operation*emit_t*emit_t (* float operations *)
| Efloatfunc of emit_operation*emit_t (* float functions *)
| Eintconst32 of int32 (* an integer *)
| Eshiftconst32 of bool*int32*int (* an integer *)
| Efloatconst of string (* float const stored as string reference *)
| Econstsym of bool*string (* constant symbol *)
| Eextref of string (* external reference symbol *)
| Ecompare of emit_integer_comparison*emit_t*emit_t (* compare objects *)
| Ecompare_flt of emit_comparison*bool*emit_t*emit_t (* compare objects *)
| Ebool of emit_t (* convert argument to bool *)
| Elabel of string*int (* branch destination *)
| Elabelref of emit_t (* arithmetic label reference *)
| Ecaml_debug_reg of string * int (* debug int instruction *)
| Ecaml_debug_char of unit (* debug char instruction *)
| Ecaml_debug_int of unit (* debug int instruction *)
| Ecaml_debug_float of unit (* debug float instruction *)
| Ecaml_debug_int64 of unit (* debug int64 instruction *)
| Ecaml_debug_ptr of unit (* debug ptr instruction *)
| Ecaml_debug_contents of unit (* debug contents instruction *)
| Ecaml_debug_string of unit (* debug ptr instruction *)
| Eraise_exn of unit(* raise exception *)
| Ecaml_ml_array_bound_error of unit (* bounds error *)
| Espload of unit
| Espstore of unit
| Epcstore of unit
| Etrap of string
| Especific of specific_operation * int list * int list
| Efundecl of int * string * bool
| Einit of emit_t list * string
| Eblockcopy of string * int * emit_t * int
| Edelayslot of emit_t * bool
| Edefaults

type marshal_object = string (* *int *) * string * (string * emit_data_item list) list * emit_t list

type word6 = int
type ival = int

type ostream =
  | StrDump of string * int

(* Layout of the stack frame *)

type alloc_seg = { hdr: emit_data_item array; arr: emit_t array; }

type const_seg = {
     entry: string;
     entry_entry: string;
     regs: emit_t array;
     old_regs: emit_t array;
     mutable pendlst: emit_t list;
     mutable commentary: string list;
     alloctbl: (string,alloc_seg)Hashtbl.t
     }

type frame = {
  mutable prg: emit_t list;
  dat: (string,emit_data_item list)Hashtbl.t;
  lib: (string,emit_t list)Hashtbl.t;
  used: (string,unit)Hashtbl.t;
  mutable stack_offset: int;
  param: (string,int * int64 * int64 * int * string)Hashtbl.t;
  pseudos: int32;
  exnpcreg: int;
  exnspreg: int;
  pcreg: int;
  spreg: int;
  mutable linkphase: string option;
  mutable debug: bool;
  mutable verbose: bool;
  mutable symbol_prefix: string;
  mutable heapcum: int;
  global: (string,string)Hashtbl.t;
  prim: (string,int)Hashtbl.t;
  extcall: (string,unit)Hashtbl.t;
  local: (string,int)Hashtbl.t;
  width: (string,string*int)Hashtbl.t;
  mutable outbuf: string list;
  mutable delayed: string;
  mutable primcnt: int;
  const_seg: const_seg;
  }

let state = {prg = [];
	     dat = Hashtbl.create 256; 
	     lib = Hashtbl.create 256; 
	     used = Hashtbl.create 256;
	     stack_offset = 0; 
	     param = Hashtbl.create 256; 
	     pseudos = 0l; 
	     pcreg = 15; 
	     exnpcreg = 14; 
	     spreg = 13;
	     exnspreg = 12; (* 8,12 swapped relative to ARM port *)
	     linkphase = None; 
	     debug = false; 
	     verbose = false; 
	     symbol_prefix = "";
	     heapcum = 0;
	     global = Hashtbl.create 256; 
	     prim = Hashtbl.create 256; 
	     extcall = Hashtbl.create 256; 
	     local = Hashtbl.create 256; 
	     width = Hashtbl.create 256; 
	     outbuf = [];
	     delayed = "";
	     primcnt = 0;
	     const_seg = {
	    	      pendlst=[];
	    	      commentary=[];
	    	      entry=""; 
	    	      entry_entry="";
		      regs = Array.init 16 (fun ix -> Eabort ("entry_filter_regs"^string_of_int ix));
		      old_regs = Array.init 16 (fun ix -> Eabort ("entry_filter_regs"^string_of_int ix));
		      alloctbl = Hashtbl.create 256}
	     }

let rec emit_symbol'' = function
  | "" -> ""
  | s -> let nxt = emit_symbol'' (String.sub s 1 (String.length s - 1)) in
    match s.[0] with 
    | 'A'..'Z'
    | 'a'..'z'
    | '0'..'9'
    | '_' -> String.sub s 0 1^nxt
    | '\'' -> Printf.sprintf "$037d%s" nxt
    | _ -> Printf.sprintf "$%.3d%s" (Char.code s.[0]) nxt

let rec emit_symbol' s = match String.length s with
  | 0 -> ""
  | _ -> let nxt = emit_symbol' (String.sub s 1 (String.length s - 1)) in
    match s.[0] with 
    | 'A'..'Z'
    | 'a'..'z'
    | '0'..'9'
    | '_'
    | '$' -> String.sub s 0 1^nxt
    | '\'' -> "$$"^nxt
    | _ -> Printf.sprintf "$%.3d%s" (Char.code s.[0]) nxt

let emit_symbol s =
    let sym = emit_symbol' s in
    let mathfun = [
      "acos";
      "asin";
      "atan";
      "atan2";
      "cabsf";
      "ceil";
      "cos";
      "cosh";
      "dadd";
      "dcmpeq";
      "dcmpge";
      "dcmpgt";
      "dcmple";
      "dcmplt";
      "ddiv";
      "dmul";
      "dsub";
      "exp";
      "floor";
      "fmod";
      "log";
      "log10";
      "pow";
      "sin";
      "sinh";
      "sqrt";
      "tan";
      "tanh"] in
    if Hashtbl.mem state.lib sym then Hashtbl.replace state.used sym ();
    if List.mem sym mathfun then "_"^sym else sym

let repl dat key sym =
  if Hashtbl.mem dat key
  then Hashtbl.replace dat key (sym::Hashtbl.find dat key)
  else Hashtbl.add dat key [sym]

let hash_add_dat key arg =
  Hashtbl.add state.dat key arg

let dump_cmm_comparison = function
  | Eeq -> "Eeq"
  | Ene -> "Ene"
  | Elt -> "Elt"
  | Ele -> "Ele"
  | Egt -> "Egt"
  | Ege -> "Ege"

let dump_cmm_memory_chunk = function
  | Ebyte_unsigned -> "Byte_unsigned"
  | Ebyte_signed -> "Byte_signed"
  | Esixteen_unsigned -> "Sixteen_unsigned"
  | Esixteen_signed -> "Sixteen_signed"
  | Ethirtytwo_unsigned -> "Thirtytwo_unsigned"
  | Ethirtytwo_signed -> "Thirtytwo_signed"
  | Eword -> "Word"
  | ESingle -> "Single"
  | EDouble -> "Double"
  | EDouble_u -> "Double_u"

let dump_integer_comparison = function
  | Esigned (cmm_comparison) -> sprintf "Esigned (%s)" (dump_cmm_comparison cmm_comparison)
  | Eunsigned (cmm_comparison) -> sprintf "Eunsigned (%s)" (dump_cmm_comparison cmm_comparison)

let dump_integer_operation = function
  | Eadd -> "Eadd"
  | Esub -> "Esub"
  | Emul -> "Emul"
  | Ediv -> "Ediv"
  | Emod -> "Emod"
  | Eand -> "Eand"
  | Eor  -> "Eor"
  | Exor -> "Exor"
  | Elsl -> "Elsl"
  | Elsr -> "Elsr"
  | Easr -> "Easr"
  | Ecomp (integer_comparison) -> sprintf "Ecomp (%s)" (dump_integer_comparison integer_comparison)
  | Echeckbound -> "Echeckbound"

let dump_arch_addressing_mode = function
  | Iindexed (int1)               (* offset addressing *) -> sprintf "Iindexed (%d) (* offset addressing *)" int1

let dump_mach_test = function
  | Etruetest -> "Etruetest"
  | Efalsetest -> "Efalsetest"
  | Einttest (integer_comparison) -> sprintf "Einttest (%s)" (dump_integer_comparison integer_comparison)
  | Einttest_imm (integer_comparison, int1) -> sprintf "Einttest_imm (%s, %d)" (dump_integer_comparison integer_comparison) (int1)
  | Efloattest (cmm_comparison, bool) -> sprintf "Efloattest (%s,%s)" (dump_cmm_comparison cmm_comparison) (string_of_bool bool)
  | Eoddtest -> "Eoddtest"
  | Eeventest -> "Eeventest"

let cnv_arith_operation = function
  | Ishiftadd -> "Ishiftadd"
  | Ishiftsub -> "Ishiftsub"
  | Ishiftsubrev -> "Ishiftsubrev"

let cnv_specific = function
  | Ishiftarith(arith_op,int1) -> "Ishiftarith("^cnv_arith_operation arith_op^", "^string_of_int int1^")"
  | Ishiftcheckbound(int1) -> "Ishiftcheckbound("^string_of_int int1^")"
  | Irevsubimm(int1) -> "Irevsubimm("^string_of_int int1^")"
  | Imuladd       -> "Imuladd       " (* multiply and add *)
  | Imulsub       -> "Imulsub       " (* multiply and subtract *)
  | Inegmulf      -> "Inegmulf      " (* floating-point negate and multiply *)
  | Imuladdf      -> "Imuladdf      " (* floating-point multiply and add *)
  | Inegmuladdf   -> "Inegmuladdf   " (* floating-point negate, multiply and add *)
  | Imulsubf      -> "Imulsubf      " (* floating-point multiply and subtract *)
  | Inegmulsubf   -> "Inegmulsubf   " (* floating-point negate, multiply and subtract *)
  | Isqrtf        -> "Isqrtf        " (* floating-point square root *)
  | Ibswap(int1) -> "Ibswap("^string_of_int int1^")" (* endianess conversion *)
(*
  | Inegf -> "Inegf"
  | Iabsf -> "Iabsf"
  | Iaddf -> "Iaddf"
  | Isubf -> "Isubf"
  | Imulf -> "Imulf"
  | Idivf -> "Idivf"
  | Ifloatofint -> "Ifloatofint"
  | Iintoffloat -> "Iintoffloat"
*)
  | Iflush -> "Iflush"

let dump_mach_operation = function
  | Emove -> "Emove"
  | Espill -> "Espill"
  | Ereload -> "Ereload"
  | Econst_int (nativeint) -> sprintf "Econst_int (%d)" (Nativeint.to_int nativeint)
  | Econst_float (str) -> sprintf "Econst_float (%s)" str
  | Econst_symbol (str) -> sprintf "Econst_symbol (%s)" str
  | Ecall_ind -> "Ecall_ind"
  | Ecall_imm (str) -> sprintf "Ecall_imm (%s)" str
  | Etailcall_ind -> "Etailcall_ind"
  | Etailcall_imm str -> sprintf "Etailcall_imm (%s)" str
  | Eextcall (str, bool1) -> sprintf "Eextcall (%s, %s)" str (string_of_bool bool1)
  | Estackoffset (int1) -> sprintf "Estackoffset (%d)" int1
  | Eload (cmm_memory_chunk, arch_addressing_mode) -> sprintf "Eload (%s, %s)"
     (dump_cmm_memory_chunk cmm_memory_chunk)
     (dump_arch_addressing_mode arch_addressing_mode)
  | Estore (cmm_memory_chunk, arch_addressing_mode) -> sprintf "Estore (%s,%s)"
     (dump_cmm_memory_chunk cmm_memory_chunk)
     (dump_arch_addressing_mode arch_addressing_mode)
  | Ealloc (int1) -> sprintf "Ealloc (%d)" (int1)
  | Eintop (integer_operation) -> sprintf "Eintop (%s)" (dump_integer_operation integer_operation)
  | Eintop_imm (integer_operation, int1) -> sprintf "Eintop_imm (%s, %d)" (dump_integer_operation integer_operation) (int1)
  | Enegf -> "Enegf"
  | Eabsf -> "Eabsf"
  | Eaddf -> "Eaddf"
  | Esubf -> "Esubf"
  | Emulf -> "Emulf"
  | Edivf -> "Edivf"
  | Efloatofint -> "Efloatofint"
  | Eintoffloat -> "Eintoffloat"
  | Especific_op (arch_specific_operation) -> "Especific_op("^cnv_specific arch_specific_operation^")"

let cnv_some = function
  | None -> "None"
  | Some n -> "Some "^string_of_int n

(* Printing operations and addressing modes *)

let print_addressing printreg addr ppf arg = ()

let print_specific_operation printreg op ppf arg = 
  Format.fprintf ppf "%s " (cnv_specific op)

let rec cnv_dump = function
| Enop -> "Enop"
| Ecaml_sys_abort () -> "Ecaml_sys_abort()"
| Ecaml_sys_exit () -> "Ecaml_sys_exit()"
| Eregload n -> "Eregload("^string_of_int n^")"
| Eregstore n -> "Eregstore("^string_of_int n^")"
| Elab s -> "Elab(\"_"^s^"\")"
| Elabdef dest -> "Elabdef("^cnv_dump dest^")"
| Eadj (n) -> "Eadj("^string_of_int n^")"
| Ecopy(src, dst) -> "Ecopy("^cnv_dump src^", "^cnv_dump dst^")"
| Ecall(arg, arglst) -> "Ecall("^cnv_dump arg^", [" ^ String.concat ";" (List.map cnv_dump arglst) ^ "])"
| Eexternalcall(s, alloc, arglst) -> "Eexternalcall(\"_"^s^"\", "^string_of_bool alloc^", [" ^ String.concat ";" (List.map cnv_dump arglst) ^ "])"
| Eresult(label,reslst) -> "Eresult(\""^label^"\", [" ^ String.concat ";" (List.map cnv_dump reslst) ^ "])"
| Egoto dest -> "Egoto("^cnv_dump dest^")"
| Econd(cond, dest) -> "Econd("^cnv_dump cond^", "^cnv_dump dest^")"
(*
| Etailcallind(calls,int1,dst, arglst) -> "Etailcallind("^string_of_bool calls^", "^string_of_int int1^", "^cnv_dump dst^", [" ^ String.concat ";" (List.map cnv_dump arglst) ^ "])"
| Etailcallimm(calls,int1,dst, arglst) -> "Etailcallimm("^string_of_bool calls^", "^string_of_int int1^", _"^dst^", [" ^ String.concat ";" (List.map cnv_dump arglst) ^ "])"
*)
| Ereloadretaddr(int1,r) -> "Ereloadretaddr("^string_of_int int1^", "^cnv_dump r^")"
| Ereturn int1 -> "Ereturn("^string_of_int int1^")"
| Eswitch(func,arg0,argvec) -> "Eswitch(\"_"^func^"\", "^cnv_dump arg0^", [|"^String.concat ";" (List.map string_of_int (Array.to_list argvec))^"|])"
| Esetuptrap(func,lbl,lab) -> "Esetuptrap(\"_"^func^"\", "^string_of_int lbl^", "^string_of_int lab^")"
| Epushtrap() -> "Epushtrap()"
| Epoptrap() -> "Epoptrap()"
| Ecomment s -> "Ecomment(\""^s^"\")"
| Estackloadref ofs -> "Estackloadref("^string_of_int ofs^")"
| Estackstoreref ofs -> "Estackstoreref("^string_of_int ofs^")"
| Eunknownstore -> "Eunknownstore()"
| Eunknownload -> "Eunknownload()"
| Eoffset(r,d) -> "Eoffset("^cnv_dump r^", "^string_of_int d^")"
| Ebased(s, d) -> "Ebased(\"_"^s^"\", "^string_of_int d^")"
| Estatecase(r, r2, d) -> "Estatecase("^cnv_dump r^", "^cnv_dump r2^", "^string_of_int d^")"
| Ereadaddrb s -> "Ereadaddrb("^cnv_dump s^")"
| Ewriteaddrb s -> "Ewriteaddrb("^cnv_dump s^")"
| Ereadaddrw s -> "Ereadaddrw("^cnv_dump s^")"
| Ewriteaddrw s -> "Ewriteaddrw("^cnv_dump s^")"
| Ereadaddrd s -> "Ereadaddrd("^cnv_dump s^")"
| Ewriteaddrd s -> "Ewriteaddrd("^cnv_dump s^")"
| Earith(op,arg0,arg1) -> "Earith("^dump_integer_operation op^", "^cnv_dump arg0^", "^cnv_dump arg1^")"
| Efloatop(op,arg0,arg1) -> "Efloatop("^dump_mach_operation op^", "^cnv_dump arg0^", "^cnv_dump arg1^")"
| Efloatfunc(op,arg0) -> "Efloatfunc("^dump_mach_operation op^", "^cnv_dump arg0^")"
| Eintconst32 n -> "Eintconst32("^Int32.to_string n^"l)"
| Eshiftconst32(negate, n,s) -> "Eshiftconst32("^string_of_bool negate^", "^Int32.to_string n^"l, "^string_of_int s^")"
| Efloatconst f -> "Efloatconst(\"_"^f^"\")"
| Econstsym(force, s) -> "Econstsym("^string_of_bool force^", \"_"^s^"\")"
| Eextref s -> "Eextref(\""^s^"\")"
| Ecompare(op, lft, rght) -> "Ecompare ("^dump_integer_comparison op^", "^cnv_dump lft^", "^cnv_dump rght^")"
| Ecompare_flt(op, neg, lft, rght) -> "Ecompare_flt ("^dump_cmm_comparison op^", "^string_of_bool neg^", "^cnv_dump lft^", "^cnv_dump rght^")"
| Ebool str -> "Ebool("^cnv_dump str^")"
| Elabel(func, dest) -> "Elabel(\"_"^func^"\", "^string_of_int  dest^")"
| Elabelref dest -> "Elabelref("^cnv_dump dest^")"
| Ecaml_debug_reg(str,n) -> "Ecaml_debug_reg(\""^str^"\", "^string_of_int n^")"
| Ecaml_debug_char() -> "Ecaml_debug_char()"
| Ecaml_debug_int() -> "Ecaml_debug_int()"
| Ecaml_debug_float() -> "Ecaml_debug_float()"
| Ecaml_debug_int64() -> "Ecaml_debug_int64()"
| Ecaml_debug_ptr() -> "Ecaml_debug_ptr()"
| Ecaml_debug_contents() -> "Ecaml_debug_contents()"
| Ecaml_debug_string() -> "Ecaml_debug_string()"
| Eraise_exn() -> "Eraise_exn()"
| Ecaml_ml_array_bound_error() -> "Ecaml_ml_array_bound_error()"
| Eabort reason -> "Eabort(\""^reason^"\")"
| Etask nam -> "Etask(\""^nam^"\")"
| Etask2(nam,arg) -> "Etask2(\""^nam^"\", "^cnv_dump arg^")"
| Espload() -> "Espload()"
| Espstore() -> "Espstore()"
| Epcstore() -> "Epcstore()"
| Etrap str -> "Etrap(\""^str^"\")"
| Especific(op,arg,res) -> "Especific("^cnv_specific op^", [" ^ String.concat ";" (List.map string_of_int arg) ^ "], [" ^ String.concat ";" (List.map string_of_int res) ^ "])"
| Efundecl(n,s,calls) -> "Efundecl("^string_of_int n^", _"^s^", "^string_of_bool calls^")"
| Einit(arr,s) -> "Einit( { " ^ String.concat ";\n\t" (List.map cnv_dump arr) ^ " } , \"_"^s^"\")"
| Eblockcopy(str,off,dst,len) -> "Eblockcopy(\"_"^str^"\", "^string_of_int off^", "^cnv_dump dst^", "^string_of_int len^")"
| Edelayslot(r,byte) -> "Edelayslot("^cnv_dump r^", "^string_of_bool byte^")"
| Edefaults -> "Edefaults"

let debug_data = function
  | Eglobal_symbol s -> "Eglobal_symbol(\"_" ^ emit_symbol s ^ "\")"
  | Edefine_symbol s -> "Edefine_symbol(\"_" ^ emit_symbol s ^ "\")"
  | Edefine_label(func, lbl) -> "Edefine_label(\"_"^func^"\", "^string_of_int lbl^")"
  | Eint8 n -> "Eint8("^string_of_int n^")"
  | Eint16 n -> "Eint16("^ string_of_int n^")"
  | Eint32 n -> "Eint32("^(Nativeint.to_string n)^"l)"
  | Eint n -> sprintf "Eint(0x%.8nXn)" (Nativeint.logand n 0xFFFFFFFFn)
  | Esingle f -> "Esingle(\"" ^ f ^ "\")"
  | Edouble f -> "Edouble(\"" ^ f ^ "\")"
  | Esymbol_address s -> "Esymbol_address(\"_"^emit_symbol s^"\")"
  | Esymbol_address_off(s,off) -> "Esymbol_address_off(\"_"^emit_symbol s^"\", " ^ string_of_int off^")"
  | Elabel_address(func,lbl) -> "Elabel_address(\"_" ^ func ^ "\", " ^ string_of_int lbl^")"
  | Estring s -> "Estring([" ^ String.concat ";" (List.map string_of_int s) ^ "])"
  | Eskip n -> "Eskip("^string_of_int n ^ ")"
  | Ealign n -> "Ealign("^ string_of_int n ^ ")"
  | Emisc misc -> "Emisc("^ cnv_dump misc ^ ")"

let emit_prg state lin cod =
  state.prg <- cod :: state.prg
										     
let emit_lab lin s = emit_prg state lin (Elabdef (Elab (emit_symbol s)))

let emit_label_def lin func lbl = emit_prg state lin ( Elabdef (Elabel(func, lbl)))

let decompose_a23_intconst n =
      let lst = ref [] in
      let fn bits shft =
          lst := Eshiftconst32 (false, bits, shft) :: !lst in
      let i = ref n in
      let shift = ref 0 in
      let nr = Int32.lognot n in
      if is_immediate n then
	lst := [Eshiftconst32(false, n, 0)]
      else if is_immediate nr then
	lst := [Eshiftconst32(true, nr, 0)]
      else
      while !i <> 0l do
	if Int32.logand (Int32.shift_right !i !shift) 1l = 0l then
	  incr shift
	else begin
	  let bits = Int32.logand !i (Int32.shift_left 0xffl !shift) in
  	  fn (Int32.shift_right bits !shift) !shift;
	  i := Int32.sub !i bits;
	  shift := !shift + 8;
	end
      done;
      List.rev !lst

let emit_a23_intconst src dst n =
        let split = decompose_a23_intconst n in
	Ecopy(List.hd split, dst) ::
	List.map (fun arg -> Ecopy(Earith(Eadd, src, arg), dst)) (List.tl split)
