//*****************************************************************************
//
//    <vscr - Verilog converter to abc format.>
//    Copyright (C) <2011,2012>  <Jonathan Richard Robert Kimmitt>
//    Derived from verilator/src/verilog.y bison grammar code by Wilson Snyder
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//*****************************************************************************

// Generic lexer tokens, for example a number
// IEEE: real_number
%token<float>		FLOATNUM	// "FLOATING-POINT NUMBER"
// IEEE: identifier, class_identifier, class_variable_identifier,
// covergroup_variable_identifier, dynamic_array_variable_identifier,
// enum_identifier, interface_identifier, interface_instance_identifier,
// package_identifier, type_identifier, variable_identifier,
%token<string>		IDSTR	// "IDENTIFIER"
// IEEE: integral_number
%token<int>		INT	// "INTEGER NUMBER as int"
%token<Nativeint.t>	NATNUM	// "INTEGER NUMBER as natural int"
%token<string>		INTNUM	// "INTEGER NUMBER as string"
%token<string>		BINNUM	// "BINARY NUMBER"
%token<string>		OCTNUM	// "OCTAL NUMBER"
%token<string>		DECNUM	// "DECIMAL NUMBER"
%token<string>		HEXNUM	// "HEXADECIMAL NUMBER"
%token<int64>		INT64	// "INTEGER NUMBER as int64"
// IEEE: string_literal
%token<string>		ASCNUM	// "ASCII NUMBER / STRING"

%token<char> ILLEGAL
%token DOLLAR
%token UNKNOWN

// Specific keywords
%token P_AMPERSAND
%token AT
%token P_CARET
%token COLON
%token COMMA
%token DIVIDE
%token EQUALS
%token GREATER
%token HASH
%token LBRACK
%token LCURLY
%token LESS
%token LPAREN
%token MINUS
%token MODULO
%token DOT
%token PLING
%token PLUS
%token QUERY
%token RBRACK
%token RCURLY
%token RPAREN
%token SEMICOLON
%token P_TILDE
%token TIMES
%token P_VBAR
%token ENDOFFILE
%token ENOP
%token EXIT
%token EABORT
%token EFUNDECL
%token EINIT
%token EBLOCKCOPY
%token EREGLOAD
%token EREGSTORE
%token ELAB
%token EADJ
%token EMOVE
%token ECOPY
%token ECALL
%token EEXTCALL
%token EEXTERNALCALL
%token ERESULT
%token EGOTO
%token ECOND
/*
%token ETAILCALLIND
%token ETAILCALLIMM
*/
%token ERELOADRETADDR
%token ERETURN
%token ECONDBRANCH3
%token ESWITCH
%token ESETUPTRAP
%token EPUSH
%token EPUSHTRAP
%token EPOPTRAP
%token ETRAP
%token ECOMMENT
%token ESTACKLOADREF
%token ESTACKSTOREREF
%token EUNKNOWNSTORE
%token EUNKNOWNLOAD
%token EOFFSET
%token EBASED
%token ESTATECASE
%token EREADADDRB
%token EWRITEADDRB
%token EREADADDRD
%token EWRITEADDRD
%token EREADADDRW
%token EWRITEADDRW
%token EARITH
%token EFLOATOP
%token EFLOATFUNC
%token EINT32
%token ESHIFTINT32
%token EFLOATCONST
%token ECONSTSYM
%token EALLOC
/*
%token EALLOCATE
*/
%token ECOMPARE
%token ECOMPARE_FLT
%token EBOOL
%token ELABEL
%token ELABDEF
%token ELABELREF
%token EDEBUG_CHAR
%token EDEBUG_INT
%token EPRINT_CHAR
%token EML_OUTPUT_CHAR
%token EML_CLOSE_CHANNEL
%token EML_OPEN_DESCRIPTOR_IN
%token EML_OPEN_DESCRIPTOR_OUT
%token ERAISE_EXN
%token EML_ARRAY_BOUND_ERROR
%token EGLOBAL
%token ESPECIFIC
%token EFLOATSPECIAL
%token EFORMATSPECIAL
%token ISIGNED
%token IUNSIGNED
%token CEQ
%token CNE
%token CLT
%token CLE
%token CGT
%token CGE
%token CGLOBAL_SYMBOL
%token CDEFINE_SYMBOL
%token CDEFINE_LABEL
%token CINT8
%token CINT16
%token CINT32
%token CINT
%token CSINGLE
%token CDOUBLE
%token CSYMBOL_ADDRESS
%token CSYMBOL_ADDRESS_OFF
%token CLABEL_ADDRESS
%token CSTRING
%token CSKIP
%token CALIGN
%token CMISC
%token EADD
%token ESUB
%token EMUL
%token EDIV
%token EMOD
%token EAND
%token EOR
%token EXOR
%token ELSL
%token EASR
%token ELSR
%token OPEN
%token LET
%token SOME
%token NONE
%token EDEBUG_CHAR 
%token EDEBUG_INT 
%token EDEBUG_PTR 
%token EIO_READ 
%token EIO_WRITE 
%token EML_ARRAY_BOUND_ERROR 
%token EML_BLIT_STRING 
%token EML_CLOSE_CHANNEL 
%token EML_COMPARE 
%token EML_CREATE_RAW
%token EML_ENTER_RAW_MODE 
%token EML_EQUAL 
%token EML_FLUSH 
%token EML_LEAVE_RAW_MODE 
%token EML_MAKE_VECT 
%token EML_MODIFY 
%token EML_NOTEQUAL 
%token EML_OPEN_TERM 
%token EML_OUTPUT_CHAR 
%token EML_PRINT_RAW_CHAR 
%token EML_READ_RAW_CHAR 
%token EML_STRING_NOTEQUAL 
%token EPRINT_CHAR 
%token ERAISE_EXN 
%token ENEGF
%token EABSF
%token EADDF
%token ESUBF
%token EMULF
%token EDIVF
%token EFLOATOFINT
%token EINTOFFLOAT
%token ISHIFTARITH
%token ISHIFTCHECKBOUND
%token IREVSUBIMM
%token IMULADD
%token IMULSUB
%token INEGMULF
%token IMULADDF
%token INEGMULADDF
%token IMULSUBF  
%token INEGMULSUBF
%token ISQRTF    
%token IBSWAP
%token ISHIFTADD
%token ISHIFTSUB
%token ISHIFTSUBREV

%start start

%type<int> word
%type<string*(string*Arch.emit_data_item list) list*(Arch.emit_t list)> start
%type<Arch.emit_data_item> const
%type<Arch.emit_data_item list> constList

%{
open Arch
%}

%%

//**********************************************************************
//

start:
                OPEN str LET str EQUALS LPAREN LBRACK headerList RBRACK COMMA LPAREN LBRACK instList RBRACK RPAREN RPAREN SEMICOLON SEMICOLON              { $4,$8,$13 }
        |       OPEN str LET str EQUALS LPAREN LBRACK headerList RBRACK COMMA LPAREN LBRACK instList RBRACK RPAREN RPAREN SEMICOLON SEMICOLON ENDOFFILE    { $4,$8,$13 }
	;

headerList:
                                                     { [] }
        |       header SEMICOLON                     { [ $1 ] }
        |       headerList header SEMICOLON          { $1 @ [ $2 ] }
        ;

header:
	LPAREN str COMMA LBRACK constList RBRACK RPAREN { $2, $5 }

instList:
                inst SEMICOLON                       { [ $1 ] }
        |       instList inst SEMICOLON              { $1 @ [ $2 ] }
        ;

word:           INTNUM                               { try int_of_string $1 with Failure _ -> max_int }
        |       MINUS INTNUM                         { try -(int_of_string $2) with Failure _ -> min_int }
        ;

native:         NATNUM                               { $1 }
        |       MINUS NATNUM                         { Nativeint.neg ($2) }
        ;

str:            ASCNUM                               { $1 }
        |       IDSTR                                { $1 }
        |       HEXNUM                               { try Scanf.sscanf $1 "%x" (fun arg -> string_of_int arg) with Scanf.Scan_failure _ -> failwith $1 }
        ;

icomp_of_str:
| ISIGNED LPAREN comparison_of_str RPAREN { Esigned $3 }
| IUNSIGNED LPAREN comparison_of_str RPAREN { Eunsigned $3 }
;

comparison_of_str:
| CEQ { Eeq }
| CNE { Ene }
| CLT { Elt }
| CLE { Ele }
| CGT { Egt }
| CGE { Ege }
;

const:
    CGLOBAL_SYMBOL LPAREN str RPAREN { Eglobal_symbol $3 }
  | CDEFINE_SYMBOL LPAREN str RPAREN { Edefine_symbol $3 }
  | CDEFINE_LABEL LPAREN str COMMA word RPAREN { Edefine_label ($3, $5 ) }
  | CINT8 LPAREN word RPAREN { Eint8 $3 }
  | CINT16 LPAREN word RPAREN { Eint16 $3 }
  | CINT32 LPAREN word RPAREN { Eint32 (Nativeint.of_int $3) }
  | CINT LPAREN native RPAREN { Eint ($3) }
  | CSINGLE LPAREN str RPAREN { Esingle $3 }
  | CDOUBLE LPAREN str RPAREN { Edouble $3 }
  | CSYMBOL_ADDRESS LPAREN str RPAREN { Esymbol_address $3 }
  | CSYMBOL_ADDRESS_OFF LPAREN str COMMA word RPAREN { Esymbol_address_off($3,$5) }
  | CLABEL_ADDRESS LPAREN str COMMA word RPAREN { Elabel_address ($3, $5 ) }
  | CSTRING LPAREN LBRACK intList RBRACK RPAREN { Estring $4 }
  | CSTRING LPAREN LBRACK RBRACK RPAREN { Estring [] }
  | CSKIP LPAREN word RPAREN { Eskip $3 }
  | CALIGN LPAREN word RPAREN { Ealign $3 }
  | CMISC LPAREN init RPAREN { Emisc $3 }

constList:
                const                                  { [ $1 ] }
        |       constList SEMICOLON const              { $1 @ [ $3 ] }
        ;

int_operation:
       |  EADD  { Eadd }
       |  ESUB  { Esub }
       |  EMUL  { Emul }
       |  EDIV  { Ediv }
       |  EMOD  { Emod }
       |  EAND  { Eand }
       |  EOR   { Eor  }
       |  EXOR  { Exor }
       |  ELSL  { Elsl }
       |  EASR  { Easr }
       |  ELSR  { Elsr }
       ;

intList:
                word                                 { [ $1  ] }
        |       intList SEMICOLON word               { $1 @ [ $3  ] }
        ;

some:
                SOME word                            { Some $2 }
        |       SOME LPAREN word RPAREN              { Some $3 }
        |       NONE                                 { None }

brackinst:
	        LBRACK RBRACK                        { [] }
	|       LBRACK brackList RBRACK              { $2 }

brackList:
                inst                                 { [ $1 ] }
        |       brackList SEMICOLON inst             { $1 @ [ $3 ] }
        ;

inst:
|  CINT32 LPAREN word RPAREN { Eintconst32 (Int32.of_int $3) }
|  ENOP { Enop }
|  EXIT LPAREN RPAREN { Ecaml_sys_exit()  }
|  EABORT LPAREN str RPAREN { Eabort $3  }
|  EFUNDECL LPAREN word COMMA str COMMA str RPAREN { Efundecl($3, $5, bool_of_string $7) }
|  EREGLOAD LPAREN word RPAREN { Eregload $3 }
|  EREGSTORE LPAREN word RPAREN { Eregstore $3 }
|  ELAB LPAREN str RPAREN { Elab $3 }
|  EADJ LPAREN word RPAREN { Eadj $3 }
|  ECOPY LPAREN inst COMMA inst RPAREN { Ecopy($3, $5) }
|  ECALL LPAREN inst COMMA brackinst RPAREN { Ecall($3, $5) }
|  EEXTERNALCALL LPAREN str COMMA str COMMA brackinst RPAREN { Eexternalcall($3, bool_of_string $5, $7) }
|  ERESULT LPAREN str COMMA brackinst RPAREN { Eresult($3,$5) }
|  EGOTO LPAREN inst RPAREN { Egoto $3 }
|  ECOND LPAREN inst COMMA inst RPAREN { Econd($3, $5) }
/*
|  ETAILCALLIND LPAREN str COMMA word COMMA inst COMMA brackinst RPAREN { Etailcallind(bool_of_string $3,$5,$7,$9) }
|  ETAILCALLIMM LPAREN str COMMA word COMMA str COMMA brackinst RPAREN { Etailcallimm(bool_of_string $3,$5,$7,$9) }
*/
|  ERELOADRETADDR LPAREN word COMMA inst RPAREN { Ereloadretaddr($3,$5) }
|  ERETURN LPAREN word RPAREN { Ereturn $3 }
/*
|  ECONDBRANCH3 LPAREN inst COMMA str COMMA str COMMA str RPAREN { Econdbranch3($3, $5, $7, $9) }
*/
|  ESWITCH LPAREN str COMMA inst COMMA LBRACK P_VBAR intList P_VBAR RBRACK RPAREN { Eswitch($3, $5, Array.of_list $9) }
|  ESETUPTRAP LPAREN str COMMA word COMMA word RPAREN { Esetuptrap ($3, $5, $7) }
|  EPUSHTRAP LPAREN RPAREN { Epushtrap() }
|  EPOPTRAP LPAREN RPAREN { Epoptrap() }
|  ETRAP LPAREN str RPAREN { Etrap ($3) }
|  ECOMMENT LPAREN str RPAREN { Ecomment $3 }
|  ESTACKLOADREF LPAREN word RPAREN { Estackloadref $3 }
|  ESTACKSTOREREF LPAREN word RPAREN { Estackstoreref $3 }
|  EUNKNOWNSTORE LPAREN RPAREN { Eunknownstore }
|  EUNKNOWNLOAD LPAREN RPAREN { Eunknownload }
|  EOFFSET LPAREN inst COMMA word RPAREN { Eoffset($3, $5) }
|  EBASED LPAREN str COMMA word RPAREN { Ebased($3, $5) }
|  ESTATECASE LPAREN inst COMMA inst COMMA word RPAREN { Estatecase($3, $5, $7) }
|  EREADADDRB LPAREN inst RPAREN { Ereadaddrb $3 }
|  EWRITEADDRB LPAREN inst RPAREN { Ewriteaddrb $3 }
|  EREADADDRD LPAREN inst RPAREN { Ereadaddrd $3 }
|  EWRITEADDRD LPAREN inst RPAREN { Ewriteaddrd $3 }
|  EREADADDRW LPAREN inst RPAREN { Ereadaddrw $3 }
|  EWRITEADDRW LPAREN inst RPAREN { Ewriteaddrw $3 }
|  EARITH LPAREN int_operation COMMA inst COMMA inst RPAREN { Earith($3, $5, $7) }
|  EFLOATOP LPAREN mach_operation COMMA inst COMMA inst RPAREN { Efloatop($3, $5, $7) }
|  EFLOATFUNC LPAREN mach_operation COMMA inst RPAREN { Efloatfunc($3, $5) }
|  EINT32 LPAREN word RPAREN { Eintconst32 (Int32.of_int $3) }
|  ESHIFTINT32 LPAREN str COMMA word COMMA word RPAREN { Eshiftconst32 (bool_of_string $3, Int32.of_int $5, $7) }
|  EFLOATCONST LPAREN str RPAREN { Efloatconst $3 }
|  ECONSTSYM LPAREN str COMMA str RPAREN { Econstsym(bool_of_string $3, $5) }
/*
|  EALLOCATE LPAREN word COMMA word COMMA inst RPAREN { Eallocate($3, $5, $7) }
*/
|  ECOMPARE LPAREN icomp_of_str COMMA inst COMMA inst RPAREN { Ecompare($3, $5, $7) }
|  ECOMPARE_FLT LPAREN comparison_of_str COMMA str COMMA inst COMMA inst RPAREN { Ecompare_flt($3, bool_of_string $5, $7, $9) }
|  EBOOL LPAREN inst RPAREN { Ebool $3 }
|  ELABEL LPAREN str COMMA word RPAREN { Elabel ( $3, $5 ) }
|  ELABELREF LPAREN inst RPAREN { Elabelref $3 }
|  ELABDEF LPAREN inst RPAREN { Elabdef $3 }
|  EDEBUG_CHAR LPAREN RPAREN { Ecaml_debug_char() }
|  EDEBUG_INT LPAREN RPAREN { Ecaml_debug_int() }
|  ERAISE_EXN LPAREN RPAREN { Eraise_exn() }
|  ESPECIFIC LPAREN specific COMMA LBRACK intList RBRACK COMMA LBRACK intList RBRACK RPAREN { Especific($3,$6,$10) }
|  ESPECIFIC LPAREN specific COMMA LBRACK intList RBRACK COMMA LBRACK RBRACK RPAREN { Especific($3,$6,[]) }
|  EINIT LPAREN LCURLY initList RCURLY COMMA str RPAREN { Einit($4,$7) }
|  EBLOCKCOPY LPAREN str COMMA word COMMA init COMMA word RPAREN { Eblockcopy($3,$5,$7,$9) }

initList:
                init                                 { [ $1 ] }
        |       initList SEMICOLON init              { $1 @ [ $3 ] }
        ;

init:
	        EREGLOAD LPAREN word RPAREN { Eregload $3 }
	|       ECONSTSYM LPAREN str COMMA str RPAREN { Econstsym(bool_of_string $3, $5) }
	|       EARITH LPAREN EADD COMMA init COMMA init RPAREN { Earith(Eadd, $5, $7) }
	|       CINT32 LPAREN word RPAREN { Eintconst32 (Int32.of_int $3) }
	|       ESHIFTINT32 LPAREN str COMMA word COMMA word RPAREN { Eshiftconst32 (bool_of_string $3, Int32.of_int $5, $7) }
	|       EREADADDRW LPAREN inst RPAREN { Ereadaddrw $3 }
    
arith_operation:
  | ISHIFTADD { Ishiftadd }
  | ISHIFTSUB { Ishiftsub }
  | ISHIFTSUBREV { Ishiftsubrev }

specific:
  | ISHIFTARITH LPAREN arith_operation COMMA word RPAREN { Ishiftarith ($3, $5) }
  | ISHIFTCHECKBOUND LPAREN word RPAREN { Ishiftcheckbound($3) }
  | IREVSUBIMM LPAREN word RPAREN { Irevsubimm($3) }
  | IMULADD { Imuladd  }
  | IMULSUB { Imulsub  }
  | INEGMULF{ Inegmulf }
  | IMULADDF{ Imuladdf }
  | INEGMULADDF{ Inegmuladdf }
  | IMULSUBF   { Imulsubf    }
  | INEGMULSUBF{ Inegmulsubf }
  | ISQRTF     { Isqrtf      }
  | IBSWAP LPAREN word RPAREN { Ibswap($3) }

mach_operation:
|	ENEGF        { Enegf }
|	EABSF        { Eabsf }
|	EADDF        { Eaddf }
|	ESUBF        { Esubf }
|	EMULF        { Emulf }
|	EDIVF        { Edivf }
|	EFLOATOFINT        { Efloatofint }
|	EINTOFFLOAT        { Eintoffloat }

