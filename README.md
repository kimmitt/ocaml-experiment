This directory contains a native port of ocaml to FPGA technology.  
 
The build details are in buildexpt.sh

The output of the simulation version of the compiler needs gcc to
compile to a simulatable form.

The FPGA output needs Xilinx implementation tools to run on real
hardware.

+  An OCaml framework for logic synthesis and fault-tolerance

Written by Jonathan Richard Robert Kimmitt <jonathan.kimmitt AT anglia.ac.uk>
in part-credit for the degree of doctor of philosophy at
Anglia Ruskin University.

Supervisors:

+ George Wilson <George.Wilson AT anglia.ac.uk>
+ David Greaves <david.greaves.10 AT gmail.com>
+ Marcian Cirstea <marcian.cirstea AT anglia.ac.uk>

Examiners:

+ Anil Madhavapeddy <avsm2 AT cl.cam.ac.uk>
+ John O'Donnell <John.ODonnell AT glasgow.ac.uk>
