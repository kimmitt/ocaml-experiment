open Mylib

let length s = 
  print_string "length \"";
  print_string s; print_char '\"';
  let n = String.length s in
  print_int n; print_newline();
  n

let create n = 
  print_string "create ";
  print_int n; print_char '=';
  let s = String.create n in
  print_int (String.length s); print_newline();
  s

let unsafe_blit s1 i1 s2 i2 len =
  print_string "unsafe_blit ";
  print_int (String.length s1); print_char ':';
  print_int i1; print_char ' ';
  print_int (String.length s2); print_char ':';
  print_int i2; print_char ' ';
  print_int len; print_newline();
  String.unsafe_blit s1 i1 s2 i2 len;
  print_int (String.length s2); print_newline()

let str_concat sep l =
    match l with
      [] -> ""
    | hd :: tl ->
	let num = ref 0 and len = ref 0 in
	List.iter (fun s -> incr num; len := !len + length s) l;
	let r = create (!len + length sep * (!num - 1)) in
	unsafe_blit hd 0 r 0 (length hd);
	let pos = ref(length hd) in
	List.iter
	  (fun s ->
	    unsafe_blit sep 0 r !pos (length sep);
	    pos := !pos + length sep;
	    unsafe_blit s 0 r !pos (length s);
	    pos := !pos + length s)
	  tl;
	r

type recording = { mutable harvest: string array; }

let run () =
  try
    let tmp = BinPosDef.Pos.of_nat Datatypes.O in
    let (executable_name, argv) = Sys.get_argv() in
    Array.iter print_endline argv;
    let arglst = Array.to_list argv in
    List.iter print_endline arglst;
    let concat = str_concat " " (List.tl arglst) in
    print_endline concat;
    print_endline "Testing get_config()";
    let (os_type, word_size, big_endian) = Sys.get_config() in
    print_endline (os_type^" "^string_of_int word_size^" "^string_of_bool big_endian);
(*
    print_endline "Testing caml_modify";
    let myrec = { harvest=Array.make 1 "" } in
    myrec.harvest.(0) <- "hello";
    print_endline ("caml_modify result was: "^myrec.harvest.(0));
*)
    print_endline "Testing string overwrite";
    "bad".[0] <- 'B';
    print_endline "String overwrite apparently succeeded";
  with _ ->
    print_endline "Sorry, an exception occured"

let _ = run()
