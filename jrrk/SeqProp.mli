open Datatypes
open Peano
open Raxioms
open Rdefinitions
open Specif

val opp_seq : (nat -> coq_R) -> nat -> coq_R

val growing_cv : (nat -> coq_R) -> coq_R

val decreasing_cv : (nat -> coq_R) -> coq_R

val ub_to_lub : (nat -> coq_R) -> coq_R

val lb_to_glb : (nat -> coq_R) -> coq_R

val lub : (nat -> coq_R) -> coq_R

val glb : (nat -> coq_R) -> coq_R

val sequence_ub : (nat -> coq_R) -> nat -> coq_R

val sequence_lb : (nat -> coq_R) -> nat -> coq_R

val maj_cv : (nat -> coq_R) -> coq_R

val min_cv : (nat -> coq_R) -> coq_R

