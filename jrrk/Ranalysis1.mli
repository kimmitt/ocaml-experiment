open Datatypes
open Peano
open Raxioms
open Rdefinitions
open Rpow_def

val plus_fct : (coq_R -> coq_R) -> (coq_R -> coq_R) -> coq_R -> coq_R

val opp_fct : (coq_R -> coq_R) -> coq_R -> coq_R

val mult_fct : (coq_R -> coq_R) -> (coq_R -> coq_R) -> coq_R -> coq_R

val mult_real_fct : coq_R -> (coq_R -> coq_R) -> coq_R -> coq_R

val minus_fct : (coq_R -> coq_R) -> (coq_R -> coq_R) -> coq_R -> coq_R

val div_fct : (coq_R -> coq_R) -> (coq_R -> coq_R) -> coq_R -> coq_R

val div_real_fct : coq_R -> (coq_R -> coq_R) -> coq_R -> coq_R

val comp : (coq_R -> coq_R) -> (coq_R -> coq_R) -> coq_R -> coq_R

val inv_fct : (coq_R -> coq_R) -> coq_R -> coq_R

val fct_cte : coq_R -> coq_R -> coq_R

val id : coq_R -> coq_R

type derivable_pt = coq_R

type derivable = coq_R -> derivable_pt

val derive_pt : (coq_R -> coq_R) -> coq_R -> derivable_pt -> coq_R

val derive : (coq_R -> coq_R) -> derivable -> coq_R -> coq_R

type coq_Differential = { d1 : (coq_R -> coq_R); cond_diff : derivable }

val coq_Differential_rect :
  ((coq_R -> coq_R) -> derivable -> 'a1) -> coq_Differential -> 'a1

val coq_Differential_rec :
  ((coq_R -> coq_R) -> derivable -> 'a1) -> coq_Differential -> 'a1

val d1 : coq_Differential -> coq_R -> coq_R

val cond_diff : coq_Differential -> derivable

type coq_Differential_D2 = { d2 : (coq_R -> coq_R); cond_D1 : derivable;
                             cond_D2 : derivable }

val coq_Differential_D2_rect :
  ((coq_R -> coq_R) -> derivable -> derivable -> 'a1) -> coq_Differential_D2
  -> 'a1

val coq_Differential_D2_rec :
  ((coq_R -> coq_R) -> derivable -> derivable -> 'a1) -> coq_Differential_D2
  -> 'a1

val d2 : coq_Differential_D2 -> coq_R -> coq_R

val cond_D1 : coq_Differential_D2 -> derivable

val cond_D2 : coq_Differential_D2 -> derivable

val derivable_pt_plus :
  (coq_R -> coq_R) -> (coq_R -> coq_R) -> coq_R -> derivable_pt ->
  derivable_pt -> derivable_pt

val derivable_pt_opp :
  (coq_R -> coq_R) -> coq_R -> derivable_pt -> derivable_pt

val derivable_pt_minus :
  (coq_R -> coq_R) -> (coq_R -> coq_R) -> coq_R -> derivable_pt ->
  derivable_pt -> derivable_pt

val derivable_pt_mult :
  (coq_R -> coq_R) -> (coq_R -> coq_R) -> coq_R -> derivable_pt ->
  derivable_pt -> derivable_pt

val derivable_pt_const : coq_R -> coq_R -> derivable_pt

val derivable_pt_scal :
  (coq_R -> coq_R) -> coq_R -> coq_R -> derivable_pt -> derivable_pt

val derivable_pt_id : coq_R -> derivable_pt

val derivable_pt_Rsqr : coq_R -> derivable_pt

val derivable_pt_comp :
  (coq_R -> coq_R) -> (coq_R -> coq_R) -> coq_R -> derivable_pt ->
  derivable_pt -> derivable_pt

val derivable_plus :
  (coq_R -> coq_R) -> (coq_R -> coq_R) -> derivable -> derivable -> derivable

val derivable_opp : (coq_R -> coq_R) -> derivable -> derivable

val derivable_minus :
  (coq_R -> coq_R) -> (coq_R -> coq_R) -> derivable -> derivable -> derivable

val derivable_mult :
  (coq_R -> coq_R) -> (coq_R -> coq_R) -> derivable -> derivable -> derivable

val derivable_const : coq_R -> derivable

val derivable_scal : (coq_R -> coq_R) -> coq_R -> derivable -> derivable

val derivable_id : derivable

val derivable_Rsqr : derivable

val derivable_comp :
  (coq_R -> coq_R) -> (coq_R -> coq_R) -> derivable -> derivable -> derivable

val pow_fct : nat -> coq_R -> coq_R

val derivable_pt_pow : nat -> coq_R -> derivable_pt

val derivable_pow : nat -> derivable

