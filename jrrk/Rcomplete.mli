open Datatypes
open Rdefinitions
open SeqProp

val coq_R_complete : (nat -> coq_R) -> coq_R

