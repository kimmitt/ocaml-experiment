open Mylib

let _ =
    let (_, argv) = Sys.get_argv() in
    Array.iter print_endline argv;
