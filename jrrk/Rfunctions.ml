open BinNums
open BinPos
open Datatypes
open Peano
open Rbasic_fun
open Rdefinitions
open Rpow_def

(** val powerRZ : coq_R -> coq_Z -> coq_R **)

let powerRZ x = function
| Z0 -> coq_R1
| Zpos p -> pow x (Pos.to_nat p)
| Zneg p -> coq_Rinv (pow x (Pos.to_nat p))

(** val decimal_exp : coq_R -> coq_Z -> coq_R **)

let decimal_exp r z =
  coq_Rmult r
    (powerRZ
      (coq_Rmult (coq_Rplus coq_R1 coq_R1)
        (coq_Rplus coq_R1
          (coq_Rmult (coq_Rplus coq_R1 coq_R1) (coq_Rplus coq_R1 coq_R1))))
      z)

(** val sum_nat_f_O : (nat -> nat) -> nat -> nat **)

let rec sum_nat_f_O f = function
| O -> f O
| S n' -> plus (sum_nat_f_O f n') (f (S n'))

(** val sum_nat_f : nat -> nat -> (nat -> nat) -> nat **)

let sum_nat_f s n f =
  sum_nat_f_O (fun x -> f (plus x s)) (minus n s)

(** val sum_nat_O : nat -> nat **)

let sum_nat_O n =
  sum_nat_f_O (fun x -> x) n

(** val sum_nat : nat -> nat -> nat **)

let sum_nat s n =
  sum_nat_f s n (fun x -> x)

(** val sum_f_R0 : (nat -> coq_R) -> nat -> coq_R **)

let rec sum_f_R0 f = function
| O -> f O
| S i -> coq_Rplus (sum_f_R0 f i) (f (S i))

(** val sum_f : nat -> nat -> (nat -> coq_R) -> coq_R **)

let sum_f s n f =
  sum_f_R0 (fun x -> f (plus x s)) (minus n s)

(** val coq_R_dist : coq_R -> coq_R -> coq_R **)

let coq_R_dist x y =
  coq_Rabs (coq_Rminus x y)

