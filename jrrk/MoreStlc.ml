open Datatypes
open Peano
open SfLib

module STLCExtended = 
 struct 
  type ty =
  | Coq_ty_arrow of ty * ty
  | Coq_ty_prod of ty * ty
  | Coq_ty_sum of ty * ty
  | Coq_ty_List of ty
  | Coq_ty_Nat
  
  (** val ty_rect :
      (ty -> 'a1 -> ty -> 'a1 -> 'a1) -> (ty -> 'a1 -> ty -> 'a1 -> 'a1) ->
      (ty -> 'a1 -> ty -> 'a1 -> 'a1) -> (ty -> 'a1 -> 'a1) -> 'a1 -> ty ->
      'a1 **)
  
  let rec ty_rect f f0 f1 f2 f3 = function
  | Coq_ty_arrow (t0, t1) ->
    f t0 (ty_rect f f0 f1 f2 f3 t0) t1 (ty_rect f f0 f1 f2 f3 t1)
  | Coq_ty_prod (t0, t1) ->
    f0 t0 (ty_rect f f0 f1 f2 f3 t0) t1 (ty_rect f f0 f1 f2 f3 t1)
  | Coq_ty_sum (t0, t1) ->
    f1 t0 (ty_rect f f0 f1 f2 f3 t0) t1 (ty_rect f f0 f1 f2 f3 t1)
  | Coq_ty_List t0 -> f2 t0 (ty_rect f f0 f1 f2 f3 t0)
  | Coq_ty_Nat -> f3
  
  (** val ty_rec :
      (ty -> 'a1 -> ty -> 'a1 -> 'a1) -> (ty -> 'a1 -> ty -> 'a1 -> 'a1) ->
      (ty -> 'a1 -> ty -> 'a1 -> 'a1) -> (ty -> 'a1 -> 'a1) -> 'a1 -> ty ->
      'a1 **)
  
  let rec ty_rec f f0 f1 f2 f3 = function
  | Coq_ty_arrow (t0, t1) ->
    f t0 (ty_rec f f0 f1 f2 f3 t0) t1 (ty_rec f f0 f1 f2 f3 t1)
  | Coq_ty_prod (t0, t1) ->
    f0 t0 (ty_rec f f0 f1 f2 f3 t0) t1 (ty_rec f f0 f1 f2 f3 t1)
  | Coq_ty_sum (t0, t1) ->
    f1 t0 (ty_rec f f0 f1 f2 f3 t0) t1 (ty_rec f f0 f1 f2 f3 t1)
  | Coq_ty_List t0 -> f2 t0 (ty_rec f f0 f1 f2 f3 t0)
  | Coq_ty_Nat -> f3
  
  type tm =
  | Coq_tm_var of id
  | Coq_tm_app of tm * tm
  | Coq_tm_abs of id * ty * tm
  | Coq_tm_pair of tm * tm
  | Coq_tm_fst of tm
  | Coq_tm_snd of tm
  | Coq_tm_inl of ty * tm
  | Coq_tm_inr of ty * tm
  | Coq_tm_case of tm * id * tm * id * tm
  | Coq_tm_nil of ty
  | Coq_tm_cons of tm * tm
  | Coq_tm_lcase of tm * tm * id * id * tm
  | Coq_tm_nat of nat
  | Coq_tm_succ of tm
  | Coq_tm_pred of tm
  | Coq_tm_mult of tm * tm
  | Coq_tm_if0 of tm * tm * tm
  | Coq_tm_let of id * tm * tm
  | Coq_tm_fix of tm
  
  (** val tm_rect :
      (id -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (id -> ty -> tm ->
      'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> 'a1) ->
      (tm -> 'a1 -> 'a1) -> (ty -> tm -> 'a1 -> 'a1) -> (ty -> tm -> 'a1 ->
      'a1) -> (tm -> 'a1 -> id -> tm -> 'a1 -> id -> tm -> 'a1 -> 'a1) -> (ty
      -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1
      -> id -> id -> tm -> 'a1 -> 'a1) -> (nat -> 'a1) -> (tm -> 'a1 -> 'a1)
      -> (tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1
      -> tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (id -> tm -> 'a1 -> tm -> 'a1 ->
      'a1) -> (tm -> 'a1 -> 'a1) -> tm -> 'a1 **)
  
  let rec tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17 = function
  | Coq_tm_var i -> f i
  | Coq_tm_app (t0, t1) ->
    f0 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t0) t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t1)
  | Coq_tm_abs (i, t0, t1) ->
    f1 i t0 t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t1)
  | Coq_tm_pair (t0, t1) ->
    f2 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t0) t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t1)
  | Coq_tm_fst t0 ->
    f3 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t0)
  | Coq_tm_snd t0 ->
    f4 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t0)
  | Coq_tm_inl (t0, t1) ->
    f5 t0 t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t1)
  | Coq_tm_inr (t0, t1) ->
    f6 t0 t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t1)
  | Coq_tm_case (t0, i, t1, i0, t2) ->
    f7 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t0) i t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t1) i0 t2
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t2)
  | Coq_tm_nil t0 -> f8 t0
  | Coq_tm_cons (t0, t1) ->
    f9 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t0) t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t1)
  | Coq_tm_lcase (t0, t1, i, i0, t2) ->
    f10 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t0) t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t1) i i0 t2
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t2)
  | Coq_tm_nat n -> f11 n
  | Coq_tm_succ t0 ->
    f12 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t0)
  | Coq_tm_pred t0 ->
    f13 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t0)
  | Coq_tm_mult (t0, t1) ->
    f14 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t0) t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t1)
  | Coq_tm_if0 (t0, t1, t2) ->
    f15 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t0) t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t1) t2
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t2)
  | Coq_tm_let (i, t0, t1) ->
    f16 i t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t0) t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t1)
  | Coq_tm_fix t0 ->
    f17 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 t0)
  
  (** val tm_rec :
      (id -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (id -> ty -> tm ->
      'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> 'a1) ->
      (tm -> 'a1 -> 'a1) -> (ty -> tm -> 'a1 -> 'a1) -> (ty -> tm -> 'a1 ->
      'a1) -> (tm -> 'a1 -> id -> tm -> 'a1 -> id -> tm -> 'a1 -> 'a1) -> (ty
      -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1
      -> id -> id -> tm -> 'a1 -> 'a1) -> (nat -> 'a1) -> (tm -> 'a1 -> 'a1)
      -> (tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1
      -> tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (id -> tm -> 'a1 -> tm -> 'a1 ->
      'a1) -> (tm -> 'a1 -> 'a1) -> tm -> 'a1 **)
  
  let rec tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17 = function
  | Coq_tm_var i -> f i
  | Coq_tm_app (t0, t1) ->
    f0 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t0) t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t1)
  | Coq_tm_abs (i, t0, t1) ->
    f1 i t0 t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t1)
  | Coq_tm_pair (t0, t1) ->
    f2 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t0) t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t1)
  | Coq_tm_fst t0 ->
    f3 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t0)
  | Coq_tm_snd t0 ->
    f4 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t0)
  | Coq_tm_inl (t0, t1) ->
    f5 t0 t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t1)
  | Coq_tm_inr (t0, t1) ->
    f6 t0 t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t1)
  | Coq_tm_case (t0, i, t1, i0, t2) ->
    f7 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t0) i t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t1) i0 t2
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t2)
  | Coq_tm_nil t0 -> f8 t0
  | Coq_tm_cons (t0, t1) ->
    f9 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t0) t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t1)
  | Coq_tm_lcase (t0, t1, i, i0, t2) ->
    f10 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t0) t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t1) i i0 t2
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t2)
  | Coq_tm_nat n -> f11 n
  | Coq_tm_succ t0 ->
    f12 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t0)
  | Coq_tm_pred t0 ->
    f13 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t0)
  | Coq_tm_mult (t0, t1) ->
    f14 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t0) t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t1)
  | Coq_tm_if0 (t0, t1, t2) ->
    f15 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t0) t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t1) t2
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t2)
  | Coq_tm_let (i, t0, t1) ->
    f16 i t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t0) t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t1)
  | Coq_tm_fix t0 ->
    f17 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        t0)
  
  (** val subst : id -> tm -> tm -> tm **)
  
  let rec subst x s t = match t with
  | Coq_tm_var y -> if beq_id x y then s else t
  | Coq_tm_app (t1, t2) -> Coq_tm_app ((subst x s t1), (subst x s t2))
  | Coq_tm_abs (y, t0, t1) ->
    Coq_tm_abs (y, t0, (if beq_id x y then t1 else subst x s t1))
  | Coq_tm_pair (t1, t2) -> Coq_tm_pair ((subst x s t1), (subst x s t2))
  | Coq_tm_fst t0 -> Coq_tm_fst (subst x s t0)
  | Coq_tm_snd t0 -> Coq_tm_snd (subst x s t0)
  | Coq_tm_inl (t0, t1) -> Coq_tm_inl (t0, (subst x s t1))
  | Coq_tm_inr (t0, t1) -> Coq_tm_inr (t0, (subst x s t1))
  | Coq_tm_case (t0, y1, t1, y2, t2) ->
    Coq_tm_case ((subst x s t0), y1,
      (if beq_id x y1 then t1 else subst x s t1), y2,
      (if beq_id x y2 then t2 else subst x s t2))
  | Coq_tm_nil t0 -> t
  | Coq_tm_cons (t1, t2) -> Coq_tm_cons ((subst x s t1), (subst x s t2))
  | Coq_tm_lcase (t1, t2, y, z, t3) ->
    Coq_tm_lcase ((subst x s t1), (subst x s t2), y, z,
      (if if beq_id x y then true else beq_id x z then t3 else subst x s t3))
  | Coq_tm_nat n -> Coq_tm_nat n
  | Coq_tm_succ t0 -> Coq_tm_succ (subst x s t0)
  | Coq_tm_pred t0 -> Coq_tm_pred (subst x s t0)
  | Coq_tm_mult (t1, t2) -> Coq_tm_mult ((subst x s t1), (subst x s t2))
  | Coq_tm_if0 (t1, t2, t3) ->
    Coq_tm_if0 ((subst x s t1), (subst x s t2), (subst x s t3))
  | Coq_tm_let (y, t1, t2) ->
    Coq_tm_let (y, (subst x s t1), (if beq_id x y then t2 else subst x s t2))
  | Coq_tm_fix t0 -> Coq_tm_fix (subst x s t0)
  
  type context = ty partial_map
  
  (** val redvalue : tm -> bool **)
  
  let rec redvalue = function
  | Coq_tm_pair (v1, v2) -> if redvalue v1 then redvalue v2 else false
  | Coq_tm_inl (t0, v1) -> redvalue v1
  | Coq_tm_inr (t0, v2) -> redvalue v2
  | Coq_tm_cons (v1, v2) -> if redvalue v1 then redvalue v2 else false
  | Coq_tm_nat n -> true
  | _ -> false
  
  (** val reduce : tm -> tm **)
  
  let rec reduce = function
  | Coq_tm_app (a, b) ->
    (match a with
     | Coq_tm_abs (x, ty0, t12) ->
       if redvalue b
       then subst x b t12
       else Coq_tm_app ((reduce a), (reduce b))
     | _ -> Coq_tm_app ((reduce a), (reduce b)))
  | Coq_tm_abs (a, b, c) -> Coq_tm_abs (a, b, (reduce c))
  | Coq_tm_fst v ->
    (match v with
     | Coq_tm_pair (v1, v2) -> reduce v1
     | _ -> Coq_tm_fst (reduce v))
  | Coq_tm_snd v ->
    (match v with
     | Coq_tm_pair (v1, v2) -> reduce v2
     | _ -> Coq_tm_snd (reduce v))
  | Coq_tm_inl (a, b) -> Coq_tm_inl (a, (reduce b))
  | Coq_tm_inr (a, b) -> Coq_tm_inr (a, (reduce b))
  | Coq_tm_case (a, b, c, d, e) ->
    (match a with
     | Coq_tm_inl (t2, v0) -> subst b v0 c
     | Coq_tm_inr (t1, v0) -> subst d v0 e
     | _ -> Coq_tm_case ((reduce a), b, (reduce c), d, (reduce e)))
  | Coq_tm_cons (a, b) -> Coq_tm_cons ((reduce a), (reduce b))
  | Coq_tm_lcase (a, b, c, d, e) ->
    (match a with
     | Coq_tm_nil t0 -> reduce b
     | Coq_tm_cons (vh, vt) -> subst c vh (subst d vt e)
     | _ -> Coq_tm_lcase ((reduce a), (reduce b), c, d, (reduce e)))
  | Coq_tm_succ v ->
    (match v with
     | Coq_tm_nat n -> Coq_tm_nat (S n)
     | _ -> Coq_tm_succ (reduce v))
  | Coq_tm_pred v ->
    (match v with
     | Coq_tm_nat n0 ->
       (match n0 with
        | O -> Coq_tm_nat O
        | S n -> Coq_tm_nat n)
     | Coq_tm_succ t0 -> if redvalue t0 then reduce t0 else t0
     | _ -> Coq_tm_pred (reduce v))
  | Coq_tm_mult (a, b) ->
    (match a with
     | Coq_tm_nat n1 ->
       (match b with
        | Coq_tm_nat n2 -> Coq_tm_nat (mult n1 n2)
        | _ -> Coq_tm_mult ((reduce a), (reduce b)))
     | _ -> Coq_tm_mult ((reduce a), (reduce b)))
  | Coq_tm_if0 (a, b, c) ->
    (match a with
     | Coq_tm_nat n0 ->
       (match n0 with
        | O -> reduce b
        | S n -> reduce c)
     | _ -> Coq_tm_if0 ((reduce a), (reduce b), (reduce c)))
  | Coq_tm_let (x, v1, t2) -> subst x v1 t2
  | Coq_tm_fix v ->
    (match v with
     | Coq_tm_abs (x, t1, t2) ->
       subst x (Coq_tm_fix (Coq_tm_abs (x, t1, t2))) t2
     | _ -> Coq_tm_fix (reduce v))
  | x -> x
  
  module Examples = 
   struct 
    module Numtest = 
     struct 
      (** val test : tm **)
      
      let test =
        Coq_tm_if0 ((Coq_tm_pred (Coq_tm_succ (Coq_tm_pred (Coq_tm_mult
          ((Coq_tm_nat (S (S O))), (Coq_tm_nat O)))))), (Coq_tm_nat (S (S (S
          (S (S O)))))), (Coq_tm_nat (S (S (S (S (S (S O))))))))
     end
    
    module Prodtest = 
     struct 
      (** val test : tm **)
      
      let test =
        Coq_tm_snd (Coq_tm_fst (Coq_tm_pair ((Coq_tm_pair ((Coq_tm_nat (S (S
          (S (S (S O)))))), (Coq_tm_nat (S (S (S (S (S (S O))))))))),
          (Coq_tm_nat (S (S (S (S (S (S (S O)))))))))))
     end
    
    module LetTest = 
     struct 
      (** val test : tm **)
      
      let test =
        Coq_tm_let ((S (S (S (S (S (S (S (S (S O))))))))), (Coq_tm_pred
          (Coq_tm_nat (S (S (S (S (S (S O)))))))), (Coq_tm_succ (Coq_tm_var
          (S (S (S (S (S (S (S (S (S O))))))))))))
     end
    
    module Sumtest1 = 
     struct 
      (** val test : tm **)
      
      let test =
        Coq_tm_case ((Coq_tm_inl (Coq_ty_Nat, (Coq_tm_nat (S (S (S (S (S
          O)))))))), (S (S (S (S (S (S (S (S (S O))))))))), (Coq_tm_var (S (S
          (S (S (S (S (S (S (S O)))))))))), (S (S (S (S (S (S (S (S (S (S
          O)))))))))), (Coq_tm_var (S (S (S (S (S (S (S (S (S (S
          O))))))))))))
     end
    
    module Sumtest2 = 
     struct 
      (** val test : tm **)
      
      let test =
        Coq_tm_let ((S (S (S (S (S (S (S (S (S (S (S O))))))))))),
          (Coq_tm_abs ((S (S (S (S (S (S (S (S (S O))))))))), (Coq_ty_sum
          (Coq_ty_Nat, Coq_ty_Nat)), (Coq_tm_case ((Coq_tm_var (S (S (S (S (S
          (S (S (S (S O)))))))))), (S (S (S (S (S (S (S (S (S (S (S (S
          O)))))))))))), (Coq_tm_var (S (S (S (S (S (S (S (S (S (S (S (S
          O))))))))))))), (S (S (S (S (S (S (S (S (S (S (S (S O)))))))))))),
          (Coq_tm_if0 ((Coq_tm_var (S (S (S (S (S (S (S (S (S (S (S (S
          O))))))))))))), (Coq_tm_nat (S O)), (Coq_tm_nat O))))))),
          (Coq_tm_pair ((Coq_tm_app ((Coq_tm_var (S (S (S (S (S (S (S (S (S
          (S (S O)))))))))))), (Coq_tm_inl (Coq_ty_Nat, (Coq_tm_nat (S (S (S
          (S (S O)))))))))), (Coq_tm_app ((Coq_tm_var (S (S (S (S (S (S (S (S
          (S (S (S O)))))))))))), (Coq_tm_inr (Coq_ty_Nat, (Coq_tm_nat (S (S
          (S (S (S O)))))))))))))
     end
    
    module ListTest = 
     struct 
      (** val test : tm **)
      
      let test =
        Coq_tm_let ((S (S (S O))), (Coq_tm_cons ((Coq_tm_nat (S (S (S (S (S
          O)))))), (Coq_tm_cons ((Coq_tm_nat (S (S (S (S (S (S O))))))),
          (Coq_tm_nil Coq_ty_Nat))))), (Coq_tm_lcase ((Coq_tm_var (S (S (S
          O)))), (Coq_tm_nat O), (S (S (S (S (S (S (S (S (S O))))))))), (S (S
          (S (S (S (S (S (S (S (S O)))))))))), (Coq_tm_mult ((Coq_tm_var (S
          (S (S (S (S (S (S (S (S O)))))))))), (Coq_tm_var (S (S (S (S (S (S
          (S (S (S O)))))))))))))))
     end
    
    module FixTest1 = 
     struct 
      (** val fact : tm **)
      
      let fact =
        Coq_tm_fix (Coq_tm_abs ((S O), (Coq_ty_arrow (Coq_ty_Nat,
          Coq_ty_Nat)), (Coq_tm_abs (O, Coq_ty_Nat, (Coq_tm_if0 ((Coq_tm_var
          O), (Coq_tm_nat (S O)), (Coq_tm_mult ((Coq_tm_var O), (Coq_tm_app
          ((Coq_tm_var (S O)), (Coq_tm_pred (Coq_tm_var O))))))))))))
      
      (** val fact_calc : nat -> tm **)
      
      let fact_calc n =
        Coq_tm_app (fact, (Coq_tm_nat n))
     end
    
    module FixTest2 = 
     struct 
      (** val map : tm **)
      
      let map =
        Coq_tm_abs ((S (S O)), (Coq_ty_arrow (Coq_ty_Nat, Coq_ty_Nat)),
          (Coq_tm_fix (Coq_tm_abs ((S O), (Coq_ty_arrow ((Coq_ty_List
          Coq_ty_Nat), (Coq_ty_List Coq_ty_Nat))), (Coq_tm_abs ((S (S (S
          O))), (Coq_ty_List Coq_ty_Nat), (Coq_tm_lcase ((Coq_tm_var (S (S (S
          O)))), (Coq_tm_nil Coq_ty_Nat), O, (S (S (S O))), (Coq_tm_cons
          ((Coq_tm_app ((Coq_tm_var (S (S O))), (Coq_tm_var O))), (Coq_tm_app
          ((Coq_tm_var (S O)), (Coq_tm_var (S (S (S O))))))))))))))))
     end
    
    module FixTest3 = 
     struct 
      (** val equal : tm **)
      
      let equal =
        Coq_tm_fix (Coq_tm_abs ((S (S (S (S (S (S (S (S (S (S (S (S (S
          O))))))))))))), (Coq_ty_arrow (Coq_ty_Nat, (Coq_ty_arrow
          (Coq_ty_Nat, Coq_ty_Nat)))), (Coq_tm_abs ((S (S (S (S (S (S (S (S
          (S (S (S (S (S (S O)))))))))))))), Coq_ty_Nat, (Coq_tm_abs ((S (S
          (S (S (S (S (S (S (S (S (S (S O)))))))))))), Coq_ty_Nat,
          (Coq_tm_if0 ((Coq_tm_var (S (S (S (S (S (S (S (S (S (S (S (S (S (S
          O))))))))))))))), (Coq_tm_if0 ((Coq_tm_var (S (S (S (S (S (S (S (S
          (S (S (S (S O))))))))))))), (Coq_tm_nat (S O)), (Coq_tm_nat O))),
          (Coq_tm_if0 ((Coq_tm_var (S (S (S (S (S (S (S (S (S (S (S (S
          O))))))))))))), (Coq_tm_nat O), (Coq_tm_app ((Coq_tm_app
          ((Coq_tm_var (S (S (S (S (S (S (S (S (S (S (S (S (S
          O)))))))))))))), (Coq_tm_pred (Coq_tm_var (S (S (S (S (S (S (S (S
          (S (S (S (S (S (S O)))))))))))))))))), (Coq_tm_pred (Coq_tm_var (S
          (S (S (S (S (S (S (S (S (S (S (S O))))))))))))))))))))))))))
     end
    
    module FixTest4 = 
     struct 
      (** val eotest : tm **)
      
      let eotest =
        Coq_tm_let ((S (S (S (S (S (S (S (S (S (S (S (S (S (S (S
          O))))))))))))))), (Coq_tm_fix (Coq_tm_abs ((S (S (S (S (S (S (S (S
          (S (S (S (S (S (S (S (S (S (S O)))))))))))))))))), (Coq_ty_prod
          ((Coq_ty_arrow (Coq_ty_Nat, Coq_ty_Nat)), (Coq_ty_arrow
          (Coq_ty_Nat, Coq_ty_Nat)))), (Coq_tm_pair ((Coq_tm_abs ((S (S (S (S
          (S (S (S (S (S (S (S (S O)))))))))))), Coq_ty_Nat, (Coq_tm_if0
          ((Coq_tm_var (S (S (S (S (S (S (S (S (S (S (S (S O))))))))))))),
          (Coq_tm_nat (S O)), (Coq_tm_app ((Coq_tm_snd (Coq_tm_var (S (S (S
          (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S O)))))))))))))))))))),
          (Coq_tm_pred (Coq_tm_var (S (S (S (S (S (S (S (S (S (S (S (S
          O)))))))))))))))))))), (Coq_tm_abs ((S (S (S (S (S (S (S (S (S (S
          (S (S O)))))))))))), Coq_ty_Nat, (Coq_tm_if0 ((Coq_tm_var (S (S (S
          (S (S (S (S (S (S (S (S (S O))))))))))))), (Coq_tm_nat O),
          (Coq_tm_app ((Coq_tm_fst (Coq_tm_var (S (S (S (S (S (S (S (S (S (S
          (S (S (S (S (S (S (S (S O)))))))))))))))))))), (Coq_tm_pred
          (Coq_tm_var (S (S (S (S (S (S (S (S (S (S (S (S
          O))))))))))))))))))))))))), (Coq_tm_let ((S (S (S (S (S (S (S (S (S
          (S (S (S (S (S (S (S O)))))))))))))))), (Coq_tm_fst (Coq_tm_var (S
          (S (S (S (S (S (S (S (S (S (S (S (S (S (S O))))))))))))))))),
          (Coq_tm_let ((S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S
          O))))))))))))))))), (Coq_tm_snd (Coq_tm_var (S (S (S (S (S (S (S (S
          (S (S (S (S (S (S (S O))))))))))))))))), (Coq_tm_pair ((Coq_tm_app
          ((Coq_tm_var (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S
          O))))))))))))))))), (Coq_tm_nat (S (S (S O)))))), (Coq_tm_app
          ((Coq_tm_var (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S
          O))))))))))))))))), (Coq_tm_nat (S (S (S (S O))))))))))))))
     end
   end
 end

