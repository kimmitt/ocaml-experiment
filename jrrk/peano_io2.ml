open Mylib
open Datatypes
open Peano
open BinNums
open Factorial

let default_nan_pl64 =
  Coq_pair (false,
    (nat_iter (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S
      (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S
      (S (S (S (S (S (S O)))))))))))))))))))))))))))))))))))))))))))))))))))
      (fun x -> Coq_xO x) Coq_xH))

let rec asNat = function
  | 0 -> Datatypes.O
  | n -> Datatypes.S (asNat(n-1))

let rec fromNat = function
  | Datatypes.O -> 0
  | Datatypes.S num -> 1 + fromNat num

let (_,argv) = get_argv()
let _ = print_string (Array.get argv (0)); print_char ' '; print_string (Array.get argv (1)); print_char '='; let n = int_of_string (Array.get argv (1)) in print_int_nl (fromNat (fact (asNat n))) (* ; sys_exit 0 *)
