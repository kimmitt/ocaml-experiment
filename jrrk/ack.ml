open Mylib

let rec ack x y =
  if x <= 0 then y + 1 else
  if y <= 0 then ack (x - 1) 1 else
  ack (x - 1) (ack x (y - 1))

let acks () =
  for i = 2 to 10 do
    for j = 1 to 6 do
      print_string "ack("; print_int i; print_char ','; print_int j; print_string ")="; flush(stdout); print_int (ack i j); print_newline()
    done;
  done

let _ = acks()
