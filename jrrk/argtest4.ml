open Mylib

let run () =
  try
    print_endline "Testing exceptions";
    for i = 0 to 10 do
      let d = 10 - i in
      print_int d;
      print_char ' ';
      print_int (100000000 / (d*d));
      print_char '\n';
    done;
    raise (Failure "argtest4");
  with
  | Division_by_zero -> print_endline "Oops - divide by zero"
  | _ -> print_endline "Sorry, an exception occured"

let _ = run()
