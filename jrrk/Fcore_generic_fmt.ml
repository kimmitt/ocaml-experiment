open BinInt
open BinNums
open Datatypes
open Fcore_Raux
open Fcore_Zaux
open Fcore_defs
open Rdefinitions

type __ = Obj.t
let __ = let rec f _ = Obj.repr f in Obj.repr f

(** val canonic_exp : radix -> (coq_Z -> coq_Z) -> coq_R -> coq_Z **)

let canonic_exp beta fexp x =
  fexp (ln_beta_val beta x (ln_beta beta x))

(** val scaled_mantissa : radix -> (coq_Z -> coq_Z) -> coq_R -> coq_R **)

let scaled_mantissa beta fexp x =
  coq_Rmult x (bpow beta (Z.opp (canonic_exp beta fexp x)))

type coq_Valid_rnd =
| Build_Valid_rnd

(** val coq_Valid_rnd_rect : (coq_R -> coq_Z) -> (__ -> __ -> 'a1) -> 'a1 **)

let coq_Valid_rnd_rect rnd f =
  f __ __

(** val coq_Valid_rnd_rec : (coq_R -> coq_Z) -> (__ -> __ -> 'a1) -> 'a1 **)

let coq_Valid_rnd_rec rnd f =
  f __ __

(** val round :
    radix -> (coq_Z -> coq_Z) -> (coq_R -> coq_Z) -> coq_R -> coq_R **)

let round beta fexp rnd x =
  coq_F2R beta { coq_Fnum = (rnd (scaled_mantissa beta fexp x)); coq_Fexp =
    (canonic_exp beta fexp x) }

(** val coq_Zrnd_opp : (coq_R -> coq_Z) -> coq_R -> coq_Z **)

let coq_Zrnd_opp rnd x =
  Z.opp (rnd (coq_Ropp x))

(** val coq_Znearest : (coq_Z -> bool) -> coq_R -> coq_Z **)

let coq_Znearest choice x =
  match coq_Rcompare (coq_Rminus x (coq_Z2R (coq_Zfloor x)))
          (coq_Rinv (coq_Rplus coq_R1 coq_R1)) with
  | Eq -> if choice (coq_Zfloor x) then coq_Zceil x else coq_Zfloor x
  | Lt -> coq_Zfloor x
  | Gt -> coq_Zceil x

