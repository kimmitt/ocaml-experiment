open Datatypes
open Rcomplete
open Rdefinitions
open Rfunctions

val cv_cauchy_2 : (nat -> coq_R) -> coq_R

val coq_SP : (nat -> coq_R -> coq_R) -> nat -> coq_R -> coq_R

