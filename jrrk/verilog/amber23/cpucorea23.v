// 
//  Hardware ML processor core and testbench.
//  (C) 2012 DJ Greaves, M Myrene, University of Cambridge Computer Laboratory.
// 

module CORE0_TOP(
input clk,
input reset,
input [31:0] logging, 
input [31:0] read_data,
input [7:0] readch,
output     [31:0]           execute_write_data,
output     [31:0]           execute_write_data_nxt,
output     [31:0]           execute_address,      // registered version of execute_address to the ram
output     [31:0]           execute_address_nxt,  // un-registered version of execute_address to the ram
output     [3:0]            execute_byte_enable_nxt,
output                      execute_write_enable,
output                      execute_write_enable_nxt,
output [0:0] read_enable,
output [0:0] finish,
output [0:0] readstrobe,
output [0:0] flush,
output [7:0] writech
);
   
   reg 	     irst;

 reg [7:0] reset_delay;

 always @(posedge clk)
   begin
      if (reset || ~(&reset_delay))
	begin
	   reset_delay = reset ? 0 : reset_delay + 1;
	   irst = 1;
	end
      else
	irst = 0;
   end
   
   wire irq = 0;
   wire rst = irst | reset;

wire dbg_mem = 0;

standalone top(
.a23_clk(clk), 
.a23_rst(rst), 
.dbg_mem(dbg_mem), 
.read_data(read_data),
.execute_write_data(execute_write_data),
.execute_write_data_nxt(execute_write_data_nxt),
.execute_address(execute_address),
.execute_address_nxt(execute_address_nxt),
.execute_byte_enable_nxt(execute_byte_enable_nxt),
.execute_write_enable(execute_write_enable),
.execute_write_enable_nxt(execute_write_enable_nxt),
.read_enable(read_enable),
.finish(finish),
.flush(flush),
.readstrobe(readstrobe),
.readch(readch),
.writech(writech)
);

endmodule

// eof
