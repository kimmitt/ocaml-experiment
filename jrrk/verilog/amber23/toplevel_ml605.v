//
// Top-level for simple demo on ML605 card.
//

module TOPLEVEL(
`ifdef DBG_TOPLEVEL
		output   [0:0]            finish,
		output   [31:0]           execute_address,  // registered version of execute_address to the ram
		output   [31:0]           execute_address_nxt,  // un-registered version of execute_address to the ram
		output   [3:0]            execute_byte_enable_nxt,
		output                    execute_write_enable,
		output                    execute_write_enable_nxt,
		output   [31:0]           execute_write_data,
		output   [31:0]           execute_write_data_nxt,
		output   [31:0]           read_data,
		output   [0:0] 	          read_enable,
		output   [0:0]            readstrobe,
		output   [31:0]           r0_out,
		output   [31:0]           r1_out,
		output   [31:0]           r2_out,
		output   [31:0]           r3_out,
		output   [31:0]           r4_out,
		output   [31:0]           r5_out,
		output   [31:0]           r6_out,
		output   [31:0]           r7_out,
		output   [31:0]           r8_out,
		output   [31:0]           r9_out,
		output   [31:0]           r10_out,
		output   [31:0]           r11_out,
		output   [31:0]           r12_out,
		output   [31:0]           r13_out,
		output   [31:0]           r14_out,
		output   [31:0]           r15_out,
		output   [31:0]           pc_nxt_out,
		output                    irst,
`endif
 input SYSCLK_N, input SYSCLK_P, 
 output GPIO_LED_C,  output GPIO_LED_N,  output GPIO_LED_S,  output GPIO_LED_W,  output GPIO_LED_E,
 input GPIO_SW_C,   input GPIO_SW_N, input   GPIO_SW_S,  input  GPIO_SW_E,  input  GPIO_SW_W,
 input CPU_RESET, input CLK_33MHZ_FPGA,

 // Chrontel.
 inout IIC_SCL_DVI,    inout IIC_SDA_DVI,
 output [11:0] DVI_D, output DVI_DE, output DVI_H, output DVI_V, output DVI_RESET_B, output DVI_XCLK_N, output DVI_XCLK_P,

 output [7:0] GPIO_LED,
 input [8:1] GPIO_DIP_SW);

`ifndef DBG_TOPLEVEL
wire irst;		
`endif

   wire dvi_reset = 0;
   wire fstore_clear = GPIO_SW_S & GPIO_SW_N; 
   wire i2c_reset = CPU_RESET;
   wire color = GPIO_SW_S & GPIO_SW_C;
   wire i2c_mode  = GPIO_SW_E & GPIO_SW_N;
   wire fstore_reset = GPIO_SW_W & GPIO_SW_N;
   wire my_reset = irst || CPU_RESET;
   wire [7:0] writech;
   
   //----------------------------------------------------------------  
   // Clock control.
   wire clk200;
   IBUFGDS my_clk_inst (.O(clk200), .I  (SYSCLK_P), .IB (SYSCLK_N));

   reg [7:0] clk2;
   always @(posedge clk200) begin
      clk2 <= CPU_RESET ? 0 : clk2 + 1'd1;
       end

   wire clk_2MHz = clk2[7];
   wire clk100 = clk2[0];
   
   //----------------------------------------------------------------  
   // Chrontel I2C connections
   wire iic_scl_dvi_out, iic_scl_dvi_openable;
   wire iic_sda_dvi_out, iic_sda_dvi_openable;   
   bufif1(IIC_SCL_DVI, iic_scl_dvi_out, iic_scl_dvi_openable);
   bufif1(IIC_SDA_DVI, iic_sda_dvi_out, iic_sda_dvi_openable);   
   wire iic_scl_dvi_in = IIC_SCL_DVI;
   wire iic_sda_dvi_in = IIC_SDA_DVI;
   
   wire i2c_done;   
   i2c_control chrontel_i2c_control(
    // .i2c_scl_in(iic_scl_dvi_in), 
    .done(i2c_done),
    .i2c_sda_in(iic_sda_dvi_in),   
    .i2c_scl_out(iic_scl_dvi_out), 
    .i2c_sda_out(iic_sda_dvi_out),   
    .i2c_scl_openable(iic_scl_dvi_openable), 
    .i2c_sda_openable(iic_sda_dvi_openable),   
    
    .clk(clk_2MHz),
    .reset(my_reset),
    .checker(i2c_mode)    
    );

   //----------------------------------------------------------------  
   // Chrontel I2C connections

    
   wire blank;

   wire [7:0] red,  green, blue;

   assign DVI_RESET_B = !dvi_reset;   
  
   fstore2 the_fstore(
	 .clk_200MHz(clk200),
	 .clk100(clk100),
	 .reset(fstore_reset),
	 .color(color),
	 .clear(fstore_clear),
	 .blank(blank),
	 .DVI_D(DVI_D), .DVI_XCLK_P(DVI_XCLK_P), .DVI_XCLK_N(DVI_XCLK_N), // .DVI_RESET_B(DVI_RESET_B),
	 .DVI_H(DVI_H), .DVI_V(DVI_V), .DVI_DE(DVI_DE),
	 .vsyn(vsyn),
	 .hsyn(hsyn),
	 .red(red),
	 .green(green),
	 .blue(blue),
	 .writech(writech),
	 .irst(irst),
	 .clk(CLK_33MHZ_FPGA),
	 .GPIO_SW_C(GPIO_SW_C),
	 .GPIO_SW_N(GPIO_SW_N),
	 .GPIO_SW_S(GPIO_SW_S),
	 .GPIO_SW_E(GPIO_SW_E),
	 .GPIO_SW_W(GPIO_SW_W)		      
    );
 
   assign GPIO_LED = writech; // was {blank,red[6],green[6],blue[6],hsyn,vsyn,fstore_reset,clk_2MHz};  

// CPU section

`ifdef DBG_TOPLEVEL
`else
   wire [0:0] finish;
   wire     [31:0]           execute_address;  // registered version of execute_address to the ram
   wire     [31:0]           execute_address_nxt;  // un-registered version of execute_address to the ram
   wire     [3:0]            execute_byte_enable_nxt;
   wire                      execute_write_enable;
   wire                      execute_write_enable_nxt;
   wire     [31:0]           execute_write_data;
   wire     [31:0]           execute_write_data_nxt;
   wire     [31:0]           read_data;
   wire     [0:0] 	     read_enable;
   wire     [0:0]            readstrobe;
   wire     [31:0]           r0_out;
   wire     [31:0]           r1_out;
   wire     [31:0]           r2_out;
   wire     [31:0]           r3_out;
   wire     [31:0]           r4_out;
   wire     [31:0]           r5_out;
   wire     [31:0]           r6_out;
   wire     [31:0]           r7_out;
   wire     [31:0]           r8_out;
   wire     [31:0]           r9_out;
   wire     [31:0]           r10_out;
   wire     [31:0]           r11_out;
   wire     [31:0]           r12_out;
   wire     [31:0]           r13_out;
   wire     [31:0]           r14_out;
   wire     [31:0]           r15_out;
   wire     [31:0]           pc_nxt_out;
`endif // !`ifdef DBG_TOPLEVEL

   assign GPIO_LED_N = GPIO_SW_N;
   assign GPIO_LED_S = GPIO_SW_S;
   assign GPIO_LED_W = GPIO_SW_W;
   assign GPIO_LED_E = GPIO_SW_E;
   assign GPIO_LED_C = finish;

mem_block block1 (
  .clk(CLK_33MHZ_FPGA),
  .read_enable(read_enable),
  .write_enable(execute_write_enable_nxt),
  .byte_strobes(execute_byte_enable_nxt),
  .readwrite_addr(execute_address_nxt[19:0]),
  .write_data(execute_write_data_nxt),
  .read_data(read_data),
  .dbg_mem(1'b1)
 );

wire [75:0] xstate;   
wire       a23_read_enable = my_reset | !finish;

progmem rom1(.clk(CLK_33MHZ_FPGA), .din(xstate), .addr(pc_nxt_out[17:2]), .we(1'b0), .dout(xstate), .en({10{a23_read_enable}}));

standalone top(
	      .a23_clk(CLK_33MHZ_FPGA), 
	      .a23_rst(my_reset),
	      .dbg_mem(1'b0), 
	      .read_data(read_data),
	      .execute_write_data(execute_write_data),
	      .execute_write_data_nxt(execute_write_data_nxt),
	      .execute_address(execute_address),
	      .execute_address_nxt(execute_address_nxt),
	      .execute_byte_enable_nxt(execute_byte_enable_nxt),
	      .execute_write_enable(execute_write_enable),
	      .write_data_wen_out(execute_write_enable_nxt),
	      .read_enable_out(read_enable),
	      .finish(finish),
	      .readstrobe(readstrobe),
	      .readch(readch),
	      .writech(writech),
	      .r0_out(r0_out),
	      .r1_out(r1_out),
	      .r2_out(r2_out),
	      .r3_out(r3_out),
	      .r4_out(r4_out),
	      .r5_out(r5_out),
	      .r6_out(r6_out),
	      .r7_out(r7_out),
	      .r8_out(r8_out),
	      .r9_out(r9_out),
	      .r10_out(r10_out),
	      .r11_out(r11_out),
	      .r12_out(r12_out),
	      .r13_out(r13_out),
	      .r14_out(r14_out),
	      .r15_out(r15_out),
	      .pc_nxt_out(pc_nxt_out),
	      .xstate(xstate),
	      .hw_exn(|GPIO_DIP_SW),
	      .hw_exn_en(hw_exn_en)
);
  
endmodule
