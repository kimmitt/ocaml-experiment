
//
// Aim for 1280x1024x75 -> 66MZ/75 = 880K = 1024*859
//

 
module TOPLEVEL(
 input 	       SYSCLK_N,
 input 	       SYSCLK_P, 
 output        GPIO_LED_C,
 output        GPIO_LED_N,
 output        GPIO_LED_S, 
 output        GPIO_LED_W,
 output        GPIO_LED_E,
 input 	       GPIO_SW_C, 
 input 	       GPIO_SW_N,
 input 	       GPIO_SW_S,
 input 	       GPIO_SW_E,
 input 	       GPIO_SW_W,
 input 	       CPU_RESET,

 // Chrontel.
 inout 	       IIC_SCL_DVI,
 inout 	       IIC_SDA_DVI,

 output [11:0] DVI_D,
 output        DVI_DE,
 output        DVI_H,
 output        DVI_V,
 output        DVI_RESET_B,
 output        DVI_XCLK_N,
 output        DVI_XCLK_P,

 output [7:0]  GPIO_LED,

 input 	       FPGA_SERIAL1_RX,
 input 	       CLK_33MHZ_FPGA,
 input 	       GPIO_DIP_SW1,
 input 	       GPIO_DIP_SW2,
 input 	       GPIO_DIP_SW3,
 input 	       GPIO_DIP_SW4,
 input 	       GPIO_DIP_SW5,
 input 	       GPIO_DIP_SW6,
 input 	       GPIO_DIP_SW7,
 input 	       GPIO_DIP_SW8,
 output        FPGA_SERIAL1_TX,
 inout 	       PS2_K_CLK_IO,
 inout 	       PS2_K_DATA_IO,
 inout 	       PS2_M_CLK_IO,
 inout 	       PS2_M_DATA_IO,

output HDR1_2,
output HDR1_4,
output HDR1_6,
output HDR1_8,
output HDR1_10,
output HDR1_12,
output HDR1_14,
output HDR1_16,
output HDR1_18	
);

   assign FPGA_SERIAL1_TX = FPGA_SERIAL1_RX;
   
   // DCM_BASE: Base Digital Clock Manager Circuit
   //           Virtex-5
   // Xilinx HDL Language Template, version 14.5

   DCM_BASE #(
      .CLKDV_DIVIDE(3.0), // Divide by: 1.5,2.0,2.5,3.0,3.5,4.0,4.5,5.0,5.5,6.0,6.5
                          //   7.0,7.5,8.0,9.0,10.0,11.0,12.0,13.0,14.0,15.0 or 16.0
      .CLKFX_DIVIDE(1), // Can be any integer from 1 to 32
      .CLKFX_MULTIPLY(4), // Can be any integer from 2 to 32
      .CLKIN_DIVIDE_BY_2("FALSE"), // TRUE/FALSE to enable CLKIN divide by two feature
      .CLKIN_PERIOD(30.303), // Specify period of input clock in ns from 1.25 to 1000.00
      .CLKOUT_PHASE_SHIFT("NONE"), // Specify phase shift mode of NONE or FIXED
      .CLK_FEEDBACK("1X"), // Specify clock feedback of NONE or 1X
      .DCM_PERFORMANCE_MODE("MAX_SPEED"), // Can be MAX_SPEED or MAX_RANGE
      .DESKEW_ADJUST("SYSTEM_SYNCHRONOUS"), // SOURCE_SYNCHRONOUS, SYSTEM_SYNCHRONOUS or
                                            //   an integer from 0 to 15
      .DFS_FREQUENCY_MODE("LOW"), // LOW or HIGH frequency mode for frequency synthesis
      .DLL_FREQUENCY_MODE("LOW"), // LOW, HIGH, or HIGH_SER frequency mode for DLL
      .DUTY_CYCLE_CORRECTION("TRUE"), // Duty cycle correction, TRUE or FALSE
      .FACTORY_JF(16'hf0f0), // FACTORY JF value suggested to be set to 16'hf0f0
      .PHASE_SHIFT(0), // Amount of fixed phase shift from -255 to 1023
      .STARTUP_WAIT("FALSE") // Delay configuration DONE until DCM LOCK, TRUE/FALSE
   ) DCM_BASE_inst (
      .CLK0(CLK0),         // 0 degree DCM CLK output
      .CLK180(CLK180),     // 180 degree DCM CLK output
      .CLK270(CLK270),     // 270 degree DCM CLK output
      .CLK2X(CLK2X),       // 2X DCM CLK output
      .CLK2X180(CLK2X180), // 2X, 180 degree DCM CLK out
      .CLK90(CLK90),       // 90 degree DCM CLK output
      .CLKDV(CLKDV),       // Divided DCM CLK out (CLKDV_DIVIDE)
      .CLKFX(CLKFX),       // DCM CLK synthesis out (M/D)
      .CLKFX180(CLKFX180), // 180 degree CLK synthesis out
      .LOCKED(LOCKED),     // DCM LOCK status output
      .CLKFB(CLK0),        // DCM clock feedback
      .CLKIN(CLK_33MHZ_FPGA),       // Clock input (from IBUFG, BUFG or DCM)
      .RST(!CPU_RESET)            // DCM asynchronous reset input
   );

   wire [7:0] writech;
   wire [7:0] readch;

   wire i2c_reset = !CPU_RESET;
   wire color = GPIO_SW_S;
   wire i2c_mode  = GPIO_SW_E;
   wire fstore_reset = !CPU_RESET;
   wire fstore_clear = GPIO_SW_N; 
  
   ps2 keyb_mouse(
        .clk(CLK2X),
        .rst(irst),
        .PS2_K_CLK_IO(PS2_K_CLK_IO),
        .PS2_K_DATA_IO(PS2_K_DATA_IO),
        .PS2_M_CLK_IO(PS2_M_CLK_IO),
        .PS2_M_DATA_IO(PS2_M_DATA_IO),
        .ascii_code(readch[6:0]),
        .ascii_data_ready(readch[7]),
        .rx_translated_scan_code(),
        .rx_ascii_read(readch[7]));

  assign GPIO_LED_N = GPIO_SW_N;
  assign GPIO_LED_S = GPIO_SW_S;
  assign GPIO_LED_W = GPIO_SW_W;
  assign GPIO_LED_E = GPIO_SW_E;
  assign GPIO_LED_C = GPIO_SW_C;
  assign GPIO_LED = readch;
  
   //----------------------------------------------------------------  
   // Clock control.
   wire clk200;
   IBUFGDS my_clk_inst (.O(clk200), .I  (SYSCLK_P), .IB (SYSCLK_N));


//   wire clk_135MHz ;
//   clk_wiz_v1_5 instance_name
//    (// Clock in ports
//    .CLK_IN1_P          (SYSCLK_P),    // IN
//    .CLK_IN1_N          (SYSCLK_N),    // IN
//    // Clock out ports
//    .CLK_OUT1           (clk_135MHz));    // OUT

   reg [7:0] clk2;
   always @(posedge clk200) begin
      clk2 <= clk2 + 1'd1;
      end

   wire clk_2MHz = clk2[7];

   //----------------------------------------------------------------  
   // Chrontel I2C connections
   wire iic_scl_dvi_out, iic_scl_dvi_openable;
   wire iic_sda_dvi_out, iic_sda_dvi_openable;   
   bufif1(IIC_SCL_DVI, iic_scl_dvi_out, iic_scl_dvi_openable);
   bufif1(IIC_SDA_DVI, iic_sda_dvi_out, iic_sda_dvi_openable);   
   wire iic_scl_dvi_in = IIC_SCL_DVI;
   wire iic_sda_dvi_in = IIC_SDA_DVI;
   
   wire i2c_done;   
   i2c_control chrontel_i2c_control(
    // .i2c_scl_in(iic_scl_dvi_in), 
    .done(i2c_done),
    .i2c_sda_in(iic_sda_dvi_in),   
    .i2c_scl_out(iic_scl_dvi_out), 
    .i2c_sda_out(iic_sda_dvi_out),   
    .i2c_scl_openable(iic_scl_dvi_openable), 
    .i2c_sda_openable(iic_sda_dvi_openable),   
    
    .clk(clk_2MHz),
    .reset(i2c_reset),
    .checker(i2c_mode)    
    );

   //----------------------------------------------------------------  
   // Chrontel I2C connections

   wire blank;

   wire [7:0] red,  green, blue;

   assign DVI_RESET_B = CPU_RESET;   
   
   fstore2 the_fstore(.clk_200MHz(clk200),
    .reset(!CPU_RESET),
    .irst(irst),
    .color(color),
    .clear(fstore_clear),
    .blank(blank),
    .DVI_D(DVI_D), .DVI_XCLK_P(DVI_XCLK_P), .DVI_XCLK_N(DVI_XCLK_N),
    .DVI_H(DVI_H), .DVI_V(DVI_V), .DVI_DE(DVI_DE),
    .writech(writech[7] ? writech : readch),
    .vsyn(vsyn), .hsyn(hsyn), .red(red), .green(green), .blue(blue),
    .GPIO_SW_C(a23_rst),
    .GPIO_SW_N(a23_rst),
    .GPIO_SW_S(a23_rst),
    .GPIO_SW_E(a23_rst),
    .GPIO_SW_W(a23_rst),
    .clk(CLK2X)
    ); 

// CPU section

   wire [0:0] finish;
   wire [0:0] flush;

   assign {
	  HDR1_2,
	  HDR1_4,
	  HDR1_6,
	  HDR1_8,
	  HDR1_10,
	  HDR1_12,
	  HDR1_14,
	  HDR1_16
	   } = writech;
   
   assign  HDR1_18 = finish;
   
   wire     [31:0]           execute_address;  // registered version of execute_address to the ram
   wire     [31:0]           execute_address_nxt;  // un-registered version of execute_address to the ram
   wire     [3:0]            execute_byte_enable_nxt;
   wire                      execute_write_enable;
   wire                      execute_write_enable_nxt;
   wire     [31:0]           execute_write_data;
   wire     [31:0]           execute_write_data_nxt;
   wire     [31:0]           read_data;
   wire     [0:0] 	     read_enable;
   wire     [0:0]            readstrobe;
   
globals rom1(.clk(CLKDV), .din(execute_write_data_nxt), .addr(execute_address_nxt[15:2]), .we(in_range_nxt && execute_write_enable_nxt), .dout(read_data), .en(execute_byte_enable_nxt));

wire my_reset = irst || GPIO_SW_C || !LOCKED;
   
CORE0_TOP coretop(
	      .clk(CLKDV), 
	      .reset(my_reset),
	      .logging(0),
	      .read_data(read_data),
	      .execute_write_data(execute_write_data),
	      .execute_write_data_nxt(execute_write_data_nxt),
	      .execute_address(execute_address),
	      .execute_address_nxt(execute_address_nxt),
	      .execute_byte_enable_nxt(execute_byte_enable_nxt),
	      .execute_write_enable(execute_write_enable),
	      .execute_write_enable_nxt(execute_write_enable_nxt),
	      .read_enable(read_enable),
	      .finish(finish),
	      .readstrobe(readstrobe),
	      .flush(flush),
	      .readch(readch),
	      .writech(writech)
);
   
endmodule // TOPLEVEL
