//////////////////////////////////////////////////////////////////
//                                                              //
//  Main memory for simulations.                                //
//                                                              //
//  This file is part of the Amber project                      //
//  http://www.opencores.org/project,amber                      //
//                                                              //
//  Description                                                 //
//  Non-synthesizable main memory. Holds 128MBytes              //
//  The memory path in this module is purely combinational.     //
//  Addresses and write_cmd_req data are registered as          //
//  the leave the execute module and read data is registered    //
//  as it enters the instruction_decode module.                 //
//                                                              //
//  Author(s):                                                  //
//      - Conor Santifort, csantifort.amber@gmail.com           //
//                                                              //
//////////////////////////////////////////////////////////////////
//                                                              //
// Copyright (C) 2010 Authors and OPENCORES.ORG                 //
//                                                              //
// This source file may be used and distributed without         //
// restriction provided that this copyright statement is not    //
// removed from the file and that any derivative work contains  //
// the original copyright notice and the associated disclaimer. //
//                                                              //
// This source file is free software; you can redistribute it   //
// and/or modify it under the terms of the GNU Lesser General   //
// Public License as published by the Free Software Foundation; //
// either version 2.1 of the License, or (at your option) any   //
// later version.                                               //
//                                                              //
// This source is distributed in the hope that it will be       //
// useful, but WITHOUT ANY WARRANTY; without even the implied   //
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      //
// PURPOSE.  See the GNU Lesser General Public License for more //
// details.                                                     //
//                                                              //
// You should have received a copy of the GNU Lesser General    //
// Public License along with this source; if not, download it   //
// from http://www.opencores.org/lgpl.shtml                     //
//                                                              //
//////////////////////////////////////////////////////////////////


module main_mem(
   i_clk,
   i_system_rdy,
   i_mem_ctrl,  // 0, 1=32MB
   i_wb_adr,
   i_wb_sel,
   i_wb_we,
   o_wb_dat,
   i_wb_dat,
   i_wb_cyc,
   i_wb_stb,
   o_wb_ack,
   o_wb_err
);

parameter WB_DWIDTH  = 32;
parameter WB_SWIDTH  = 4;

input                          i_clk;
input						   		 i_system_rdy;
input                          i_mem_ctrl;  // 0=128MB; 1=32MB
// Wishbone Bus
input       [31:0]             i_wb_adr;
input       [WB_SWIDTH-1:0]    i_wb_sel;
input                          i_wb_we;
output      [WB_DWIDTH-1:0]    o_wb_dat;
input       [WB_DWIDTH-1:0]    i_wb_dat;
input                          i_wb_cyc;
input                          i_wb_stb;
output                         o_wb_ack;
output                         o_wb_err;

`include "memory_configuration.v"

reg     [31:0]     ram   [1<<(MAIN_MSB-2)-1:0];
wire                start_write;
wire                start_read;
reg                 start_read_d1;
reg                 start_read_d2;
reg     [31:0]      wb_rdata32;
wire    [31:0]      rd_data;

reg                 wr_en           ;
reg     [15:0]      wr_mask         ;
reg     [27:0]      addr_d1         ;
wire                busy;

assign start_write = i_wb_stb &&  i_wb_we && !busy;
assign start_read  = i_wb_stb && !i_wb_we && !busy;
assign busy        = start_read_d1 || start_read_d2;

assign o_wb_err    = 'd0;
assign rd_data = ram[addr_d1];

    always @( posedge i_clk )
        begin
		  wr_en <= start_write;
		  wr_mask <= ~i_wb_sel;
		  addr_d1 <= i_wb_adr;
        start_read_d1   <= start_read;
        start_read_d2   <= start_read_d1;
        if ( start_read_d1 )
            begin
            wb_rdata32 <= rd_data[ 31: 0] ;
            end
        end

    assign o_wb_dat = rd_data; // wb_rdata32 ;                  
    assign o_wb_ack = i_wb_stb && ( start_write || start_read_d2 ) && i_system_rdy;

initial
	$readmemh("/home/jrrk/regression/amber/hw/vlog/amber23/main_mem.mem", ram);
	
endmodule



