// 
//  Hardware ML processor core and testbench.
//  (C) 2012 DJ Greaves, M Myrene, University of Cambridge Computer Laboratory.
// 

module keytest(
 input clk,
 input reset,
 input [7:0] dil8_sw_data,
 output [7:0] rx_ascii_code,
 
 // Code memory connections
 input rx_serial_uart,
 output reg tx_serial_uart, 
 output failed,
 output reg [7:0] dinb,
 output [10:0] addrb,
 output reg [0:0] web,
 output [0:0] enb,
 output reg irst,
 input [7:0] doutb,
 input [7:0] writech,
 inout PS2_K_CLK_IO,
 inout PS2_K_DATA_IO,
 inout PS2_M_CLK_IO,
 inout PS2_M_DATA_IO
);

  reg [10:0] addrb_int;
  reg [15:0] reset_delay;
  reg rx_ascii_read, writech_read;
  wire [7:0] rx_translated_scan_code;

  assign addrb = irst ? reset_delay[10:0] : addrb_int;
  assign failed = 0;

 always @(posedge clk)
   begin
      tx_serial_uart = rx_serial_uart;
      if (reset || ~(&reset_delay))
	begin
	   reset_delay = reset ? 0 : reset_delay + 1;
	   irst <= 1;
	   web <= 1;
	   addrb_int <= 0;
	   dinb = 8'h20;
	end
      else
	 begin
	   irst <= 0;
	    rx_ascii_read <= rx_ascii_code[7] & !writech[7];
	    writech_read <= writech[7];
	    if ((writech[7] && !writech_read) || (rx_ascii_code[7] && !rx_ascii_read) && !(&addrb_int))
	      begin
		 dinb = {1'b0,writech[7] ? writech[6:0] : rx_ascii_code[6:0]};
		 addrb_int <= (dinb==10 && !(&addrb_int[10:5]) ? addrb_int|31 : addrb_int) + 1;
		 web <= dinb!=10;
	      end
	    else
	      web <= 0;
	 end
   end
   
/*
  wire rd_en;
  wire rst;
  wire wr_ack;
  wire empty;
  wire wr_en;
  wire underflow;
  wire rd_clk;
  wire overflow;
  wire valid;
  wire full;
  wire wr_clk;
  wire [7 : 0] dout;
  wire [7 : 0] din = rx_ascii_code;

fifo_generator_v9_3 kfifo (
. rd_en(  rd_en),
. rst( rst),
. wr_ack( wr_ack),
. empty( empty),
. wr_en( wr_en),
. underflow( underflow),
. rd_clk( rd_clk),
. overflow( overflow),
. valid( valid),
. full( full),
. wr_clk( wr_clk),
. dout( dout),
. din( din));
 */
  
   ps2 keyb_mouse(
        .clk(clk),
        .rst(irst),
        .PS2_K_CLK_IO(PS2_K_CLK_IO),
        .PS2_K_DATA_IO(PS2_K_DATA_IO),
        .PS2_M_CLK_IO(PS2_M_CLK_IO),
        .PS2_M_DATA_IO(PS2_M_DATA_IO),
        .ascii_code(rx_ascii_code[6:0]),
        .ascii_data_ready(rx_ascii_code[7]),
        .rx_translated_scan_code(rx_translated_scan_code),
        .rx_ascii_read(rx_ascii_read));
 
   assign enb = 1;
   
endmodule

// eof
