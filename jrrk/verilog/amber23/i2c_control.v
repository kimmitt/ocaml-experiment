

// CBG Orangepath HPR/LS System

// Verilog output file generated at 16/05/2012 13:21:39
// KiwiC (.net/CIL/C# to Verilog/SystemC compiler): Version alpha 54j: 20th-December-2011 Unix 2.6.32.34
//  /home/djg11/d320/hprls/kiwic/distro/lib/kiwic.exe -onehot-pc=false i2c_control.exe -vnl i2c_control.v -sim 1000

module i2c_control(input checker, input i2c_sda_in, output reg i2c_scl_openable, output reg i2c_sda_openable, output reg i2c_sda_out, output reg i2c_scl_out, output reg done, input clk, input reset);
  integer TCISe0_22_V_0;
  integer TCISe0_22_V_1;
  integer TCISe0_29_V_0;
  integer TCISe0_29_V_1;
  integer TCISe0_31_V_0;
  integer TCISe0_31_V_1;
  reg [10:0] xpc10;
 always   @(posedge clk )  begin 
      //Start HPR i2c_control.exe
      if (reset)  begin 
               TCISe0_22_V_0 <= 32'd0;
               TCISe0_22_V_1 <= 32'd0;
               TCISe0_29_V_0 <= 32'd0;
               TCISe0_29_V_1 <= 32'd0;
               TCISe0_31_V_0 <= 32'd0;
               TCISe0_31_V_1 <= 32'd0;
               i2c_scl_openable <= 1'd0;
               i2c_sda_openable <= 1'd0;
               i2c_sda_out <= 1'd0;
               i2c_scl_out <= 1'd0;
               done <= 1'd0;
               xpc10 <= 11'd0;

               end 
               else  begin 
              
              case (xpc10)

              0/*0:bashint*/: $display("Start condition");

              3/*3:bashint*/: $display("Sending device ID");
              endcase
              if ((1>=TCISe0_22_V_1) && (xpc10==6/*6:bashint*/)) $display("Indicate write operation.");
                  
              case (xpc10)

              9/*9:bashint*/: $display("Processing ACK");

              13/*13:bashint*/: $display("Sending device register address");
              endcase
              if ((1>=TCISe0_29_V_1) && (xpc10==16/*16:bashint*/)) $display("Processing ACK");
                  if ((1>=TCISe0_31_V_1) && (xpc10==23/*23:bashint*/)) $display("Processing ACK");
                  
              case (xpc10)

              27/*27:bashint*/: $display("Generate stop condition.");

              30/*30:bashint*/: $display("Start condition");

              33/*33:bashint*/: $display("Sending device ID");
              endcase
              if ((1>=TCISe0_22_V_1) && (xpc10==36/*36:bashint*/)) $display("Indicate write operation.");
                  
              case (xpc10)

              39/*39:bashint*/: $display("Processing ACK");

              43/*43:bashint*/: $display("Sending device register address");
              endcase
              if ((1>=TCISe0_29_V_1) && (xpc10==46/*46:bashint*/)) $display("Processing ACK");
                  if ((1>=TCISe0_31_V_1) && (xpc10==53/*53:bashint*/)) $display("Processing ACK");
                  
              case (xpc10)

              57/*57:bashint*/: $display("Generate stop condition.");

              60/*60:bashint*/: $display("Start condition");

              63/*63:bashint*/: $display("Sending device ID");
              endcase
              if ((1>=TCISe0_22_V_1) && (xpc10==66/*66:bashint*/)) $display("Indicate write operation.");
                  
              case (xpc10)

              69/*69:bashint*/: $display("Processing ACK");

              73/*73:bashint*/: $display("Sending device register address");
              endcase
              if ((1>=TCISe0_29_V_1) && (xpc10==76/*76:bashint*/)) $display("Processing ACK");
                  if ((1>=TCISe0_31_V_1) && (xpc10==83/*83:bashint*/)) $display("Processing ACK");
                  if ((xpc10==87/*87:bashint*/)) $display("Generate stop condition.");
                  if (checker && (xpc10==90/*90:bashint*/)) $display("Start condition");
                  if ((xpc10==93/*93:bashint*/)) $display("Sending device ID");
                  if ((1>=TCISe0_22_V_1) && (xpc10==96/*96:bashint*/)) $display("Indicate write operation.");
                  
              case (xpc10)

              99/*99:bashint*/: $display("Processing ACK");

              103/*103:bashint*/: $display("Sending device register address");
              endcase
              if ((1>=TCISe0_29_V_1) && (xpc10==106/*106:bashint*/)) $display("Processing ACK");
                  if ((1>=TCISe0_31_V_1) && (xpc10==113/*113:bashint*/)) $display("Processing ACK");
                  if ((xpc10==117/*117:bashint*/)) $display("Generate stop condition.");
                  if (checker && (xpc10==121/*121:bashint*/)) $display("Start condition");
                  if ((xpc10==124/*124:bashint*/)) $display("Sending device ID");
                  if ((1>=TCISe0_22_V_1) && (xpc10==127/*127:bashint*/)) $display("Indicate write operation.");
                  
              case (xpc10)

              130/*130:bashint*/: $display("Processing ACK");

              134/*134:bashint*/: $display("Sending device register address");
              endcase
              if ((1>=TCISe0_29_V_1) && (xpc10==137/*137:bashint*/)) $display("Processing ACK");
                  if ((1>=TCISe0_31_V_1) && (xpc10==144/*144:bashint*/)) $display("Processing ACK");
                  if ((xpc10==148/*148:bashint*/)) $display("Generate stop condition.");
                  if (checker && (xpc10==152/*152:bashint*/)) $display("Start condition");
                  if ((xpc10==155/*155:bashint*/)) $display("Sending device ID");
                  if ((1>=TCISe0_22_V_1) && (xpc10==158/*158:bashint*/)) $display("Indicate write operation.");
                  
              case (xpc10)

              161/*161:bashint*/: $display("Processing ACK");

              165/*165:bashint*/: $display("Sending device register address");
              endcase
              if ((1>=TCISe0_29_V_1) && (xpc10==168/*168:bashint*/)) $display("Processing ACK");
                  if ((1>=TCISe0_31_V_1) && (xpc10==175/*175:bashint*/)) $display("Processing ACK");
                  
              case (xpc10)

              0/*0:bashint*/:  begin 
                   i2c_scl_openable <= 1;
                   i2c_sda_openable <= 1;
                   i2c_sda_out <= 1'd1;
                   i2c_scl_out <= 1;
                   done <= 0;
                   xpc10 <= 1/*1:xpc10:1*/;

                   end 
                  
              1/*1:bashint*/:  begin 
                   i2c_sda_out <= 0;
                   xpc10 <= 2/*2:xpc10:2*/;

                   end 
                  
              2/*2:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 3/*3:xpc10:3*/;

                   end 
                  
              3/*3:bashint*/:  begin 
                   TCISe0_22_V_0 <= 118;
                   TCISe0_22_V_1 <= 7;
                   i2c_sda_out <= 1'd1;
                   i2c_scl_out <= 0;
                   xpc10 <= 4/*4:xpc10:4*/;

                   end 
                  
              4/*4:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 5/*5:xpc10:5*/;

                   end 
                  
              5/*5:bashint*/:  begin 
                   TCISe0_22_V_0 <= (TCISe0_22_V_0<<1);
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 6/*6:xpc10:6*/;

                   end 
                  
              6/*6:bashint*/:  begin 
                   TCISe0_22_V_1 <= -1+TCISe0_22_V_1;
                   i2c_sda_out <= (0!=(64&TCISe0_22_V_0)) && (1<TCISe0_22_V_1) || !checker && (0!=(64&TCISe0_22_V_0)) && (1<TCISe0_22_V_1
                  );

                   i2c_scl_out <= ((1<TCISe0_22_V_1)? 0: i2c_scl_out);
                   xpc10 <= ((1<TCISe0_22_V_1)? ((1<TCISe0_22_V_1)? 4/*4:xpc10:4*/: xpc10): 7/*7:xpc10:7*/);

                   end 
                  
              7/*7:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 8/*8:xpc10:8*/;

                   end 
                  
              8/*8:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 9/*9:xpc10:9*/;

                   end 
                  
              9/*9:bashint*/:  begin 
                   i2c_sda_openable <= 0;
                   i2c_sda_out <= 1'd1;
                   xpc10 <= 10/*10:xpc10:10*/;

                   end 
                  
              10/*10:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 11/*11:xpc10:11*/;

                   end 
                  
              11/*11:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 12/*12:xpc10:12*/;

                   end 
                  
              12/*12:bashint*/:  begin 
                   i2c_sda_openable <= 1;
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= 13/*13:xpc10:13*/;

                   end 
                  
              13/*13:bashint*/:  begin 
                   TCISe0_29_V_0 <= 201;
                   TCISe0_29_V_1 <= 8;
                   i2c_sda_out <= 1'd1;
                   i2c_scl_out <= 0;
                   xpc10 <= 14/*14:xpc10:14*/;

                   end 
                  
              14/*14:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 15/*15:xpc10:15*/;

                   end 
                  
              15/*15:bashint*/:  begin 
                   TCISe0_29_V_0 <= (TCISe0_29_V_0<<1);
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 16/*16:xpc10:16*/;

                   end 
                  
              16/*16:bashint*/:  begin 
                   TCISe0_29_V_1 <= -1+TCISe0_29_V_1;
                   i2c_sda_openable <= ((1<TCISe0_29_V_1)? i2c_sda_openable: 0);
                   i2c_sda_out <= (0!=(128&TCISe0_29_V_0)) && (1<TCISe0_29_V_1) || (1>=TCISe0_29_V_1) || !checker && (0!=(128&TCISe0_29_V_0
                  )) && (1<TCISe0_29_V_1) || !checker && (1>=TCISe0_29_V_1);

                   i2c_scl_out <= ((1<TCISe0_29_V_1)? 0: i2c_scl_out);
                   xpc10 <= ((1<TCISe0_29_V_1)? ((1<TCISe0_29_V_1)? 14/*14:xpc10:14*/: xpc10): 17/*17:xpc10:17*/);

                   end 
                  
              17/*17:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 18/*18:xpc10:18*/;

                   end 
                  
              18/*18:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 19/*19:xpc10:19*/;

                   end 
                  
              19/*19:bashint*/:  begin 
                   i2c_sda_openable <= 1;
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= 20/*20:xpc10:20*/;

                   end 
                  
              20/*20:bashint*/:  begin 
                   TCISe0_31_V_0 <= 192;
                   TCISe0_31_V_1 <= 8;
                   i2c_sda_out <= 1'd1;
                   i2c_scl_out <= 0;
                   xpc10 <= 21/*21:xpc10:21*/;

                   end 
                  
              21/*21:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 22/*22:xpc10:22*/;

                   end 
                  
              22/*22:bashint*/:  begin 
                   TCISe0_31_V_0 <= (TCISe0_31_V_0<<1);
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 23/*23:xpc10:23*/;

                   end 
                  
              23/*23:bashint*/:  begin 
                   TCISe0_31_V_1 <= -1+TCISe0_31_V_1;
                   i2c_sda_openable <= ((1<TCISe0_31_V_1)? i2c_sda_openable: 0);
                   i2c_sda_out <= (1>=TCISe0_31_V_1) || (0!=(128&TCISe0_31_V_0)) && (1<TCISe0_31_V_1) || !checker && (1>=TCISe0_31_V_1
                  ) || !checker && (0!=(128&TCISe0_31_V_0)) && (1<TCISe0_31_V_1);

                   i2c_scl_out <= ((1<TCISe0_31_V_1)? 0: i2c_scl_out);
                   xpc10 <= ((1<TCISe0_31_V_1)? ((1<TCISe0_31_V_1)? 21/*21:xpc10:21*/: xpc10): 24/*24:xpc10:24*/);

                   end 
                  
              24/*24:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 25/*25:xpc10:25*/;

                   end 
                  
              25/*25:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 26/*26:xpc10:26*/;

                   end 
                  
              26/*26:bashint*/:  begin 
                   i2c_sda_openable <= 1;
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= 27/*27:xpc10:27*/;

                   end 
                  
              27/*27:bashint*/:  begin 
                   i2c_sda_out <= 0;
                   i2c_scl_out <= 1;
                   xpc10 <= 28/*28:xpc10:28*/;

                   end 
                  
              28/*28:bashint*/:  begin 
                   i2c_sda_out <= 1'd1;
                   xpc10 <= 29/*29:xpc10:29*/;

                   end 
                  
              29/*29:bashint*/:  begin 
                   i2c_scl_openable <= 0;
                   i2c_sda_openable <= 0;
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= 30/*30:xpc10:30*/;

                   end 
                  
              30/*30:bashint*/:  begin 
                   i2c_scl_openable <= 1;
                   i2c_sda_openable <= 1;
                   i2c_sda_out <= 1'd1;
                   i2c_scl_out <= 1;
                   done <= 0;
                   xpc10 <= 31/*31:xpc10:31*/;

                   end 
                  
              31/*31:bashint*/:  begin 
                   i2c_sda_out <= 0;
                   xpc10 <= 32/*32:xpc10:32*/;

                   end 
                  
              32/*32:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 33/*33:xpc10:33*/;

                   end 
                  
              33/*33:bashint*/:  begin 
                   TCISe0_22_V_0 <= 118;
                   TCISe0_22_V_1 <= 7;
                   i2c_sda_out <= 1'd1;
                   i2c_scl_out <= 0;
                   xpc10 <= 34/*34:xpc10:34*/;

                   end 
                  
              34/*34:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 35/*35:xpc10:35*/;

                   end 
                  
              35/*35:bashint*/:  begin 
                   TCISe0_22_V_0 <= (TCISe0_22_V_0<<1);
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 36/*36:xpc10:36*/;

                   end 
                  
              36/*36:bashint*/:  begin 
                   TCISe0_22_V_1 <= -1+TCISe0_22_V_1;
                   i2c_sda_out <= (0!=(64&TCISe0_22_V_0)) && (1<TCISe0_22_V_1) || !checker && (0!=(64&TCISe0_22_V_0)) && (1<TCISe0_22_V_1
                  );

                   i2c_scl_out <= ((1<TCISe0_22_V_1)? 0: i2c_scl_out);
                   xpc10 <= ((1<TCISe0_22_V_1)? ((1<TCISe0_22_V_1)? 34/*34:xpc10:34*/: xpc10): 37/*37:xpc10:37*/);

                   end 
                  
              37/*37:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 38/*38:xpc10:38*/;

                   end 
                  
              38/*38:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 39/*39:xpc10:39*/;

                   end 
                  
              39/*39:bashint*/:  begin 
                   i2c_sda_openable <= 0;
                   i2c_sda_out <= 1'd1;
                   xpc10 <= 40/*40:xpc10:40*/;

                   end 
                  
              40/*40:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 41/*41:xpc10:41*/;

                   end 
                  
              41/*41:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 42/*42:xpc10:42*/;

                   end 
                  
              42/*42:bashint*/:  begin 
                   i2c_sda_openable <= 1;
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= 43/*43:xpc10:43*/;

                   end 
                  
              43/*43:bashint*/:  begin 
                   TCISe0_29_V_0 <= 161;
                   TCISe0_29_V_1 <= 8;
                   i2c_sda_out <= 1'd1;
                   i2c_scl_out <= 0;
                   xpc10 <= 44/*44:xpc10:44*/;

                   end 
                  
              44/*44:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 45/*45:xpc10:45*/;

                   end 
                  
              45/*45:bashint*/:  begin 
                   TCISe0_29_V_0 <= (TCISe0_29_V_0<<1);
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 46/*46:xpc10:46*/;

                   end 
                  
              46/*46:bashint*/:  begin 
                   TCISe0_29_V_1 <= -1+TCISe0_29_V_1;
                   i2c_sda_openable <= ((1<TCISe0_29_V_1)? i2c_sda_openable: 0);
                   i2c_sda_out <= (0!=(128&TCISe0_29_V_0)) && (1<TCISe0_29_V_1) || (1>=TCISe0_29_V_1) || !checker && (0!=(128&TCISe0_29_V_0
                  )) && (1<TCISe0_29_V_1) || !checker && (1>=TCISe0_29_V_1);

                   i2c_scl_out <= ((1<TCISe0_29_V_1)? 0: i2c_scl_out);
                   xpc10 <= ((1<TCISe0_29_V_1)? ((1<TCISe0_29_V_1)? 44/*44:xpc10:44*/: xpc10): 47/*47:xpc10:47*/);

                   end 
                  
              47/*47:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 48/*48:xpc10:48*/;

                   end 
                  
              48/*48:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 49/*49:xpc10:49*/;

                   end 
                  
              49/*49:bashint*/:  begin 
                   i2c_sda_openable <= 1;
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= 50/*50:xpc10:50*/;

                   end 
                  
              50/*50:bashint*/:  begin 
                   TCISe0_31_V_0 <= 9;
                   TCISe0_31_V_1 <= 8;
                   i2c_sda_out <= 0;
                   i2c_scl_out <= 0;
                   xpc10 <= 51/*51:xpc10:51*/;

                   end 
                  
              51/*51:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 52/*52:xpc10:52*/;

                   end 
                  
              52/*52:bashint*/:  begin 
                   TCISe0_31_V_0 <= (TCISe0_31_V_0<<1);
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 53/*53:xpc10:53*/;

                   end 
                  
              53/*53:bashint*/:  begin 
                   TCISe0_31_V_1 <= -1+TCISe0_31_V_1;
                   i2c_sda_openable <= ((1<TCISe0_31_V_1)? i2c_sda_openable: 0);
                   i2c_sda_out <= (1>=TCISe0_31_V_1) || (0!=(128&TCISe0_31_V_0)) && (1<TCISe0_31_V_1) || !checker && (1>=TCISe0_31_V_1
                  ) || !checker && (0!=(128&TCISe0_31_V_0)) && (1<TCISe0_31_V_1);

                   i2c_scl_out <= ((1<TCISe0_31_V_1)? 0: i2c_scl_out);
                   xpc10 <= ((1<TCISe0_31_V_1)? ((1<TCISe0_31_V_1)? 51/*51:xpc10:51*/: xpc10): 54/*54:xpc10:54*/);

                   end 
                  
              54/*54:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 55/*55:xpc10:55*/;

                   end 
                  
              55/*55:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 56/*56:xpc10:56*/;

                   end 
                  
              56/*56:bashint*/:  begin 
                   i2c_sda_openable <= 1;
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= 57/*57:xpc10:57*/;

                   end 
                  
              57/*57:bashint*/:  begin 
                   i2c_sda_out <= 0;
                   i2c_scl_out <= 1;
                   xpc10 <= 58/*58:xpc10:58*/;

                   end 
                  
              58/*58:bashint*/:  begin 
                   i2c_sda_out <= 1'd1;
                   xpc10 <= 59/*59:xpc10:59*/;

                   end 
                  
              59/*59:bashint*/:  begin 
                   i2c_scl_openable <= 0;
                   i2c_sda_openable <= 0;
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= 60/*60:xpc10:60*/;

                   end 
                  
              60/*60:bashint*/:  begin 
                   i2c_scl_openable <= 1;
                   i2c_sda_openable <= 1;
                   i2c_sda_out <= 1'd1;
                   i2c_scl_out <= 1;
                   done <= 0;
                   xpc10 <= 61/*61:xpc10:61*/;

                   end 
                  
              61/*61:bashint*/:  begin 
                   i2c_sda_out <= 0;
                   xpc10 <= 62/*62:xpc10:62*/;

                   end 
                  
              62/*62:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 63/*63:xpc10:63*/;

                   end 
                  
              63/*63:bashint*/:  begin 
                   TCISe0_22_V_0 <= 118;
                   TCISe0_22_V_1 <= 7;
                   i2c_sda_out <= 1'd1;
                   i2c_scl_out <= 0;
                   xpc10 <= 64/*64:xpc10:64*/;

                   end 
                  
              64/*64:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 65/*65:xpc10:65*/;

                   end 
                  
              65/*65:bashint*/:  begin 
                   TCISe0_22_V_0 <= (TCISe0_22_V_0<<1);
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 66/*66:xpc10:66*/;

                   end 
                  
              66/*66:bashint*/:  begin 
                   TCISe0_22_V_1 <= -1+TCISe0_22_V_1;
                   i2c_sda_out <= (0!=(64&TCISe0_22_V_0)) && (1<TCISe0_22_V_1) || !checker && (0!=(64&TCISe0_22_V_0)) && (1<TCISe0_22_V_1
                  );

                   i2c_scl_out <= ((1<TCISe0_22_V_1)? 0: i2c_scl_out);
                   xpc10 <= ((1<TCISe0_22_V_1)? ((1<TCISe0_22_V_1)? 64/*64:xpc10:64*/: xpc10): 67/*67:xpc10:67*/);

                   end 
                  
              67/*67:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 68/*68:xpc10:68*/;

                   end 
                  
              68/*68:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 69/*69:xpc10:69*/;

                   end 
                  
              69/*69:bashint*/:  begin 
                   i2c_sda_openable <= 0;
                   i2c_sda_out <= 1'd1;
                   xpc10 <= 70/*70:xpc10:70*/;

                   end 
                  
              70/*70:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 71/*71:xpc10:71*/;

                   end 
                  
              71/*71:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 72/*72:xpc10:72*/;

                   end 
                  
              72/*72:bashint*/:  begin 
                   i2c_sda_openable <= 1;
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= 73/*73:xpc10:73*/;

                   end 
                  
              73/*73:bashint*/:  begin 
                   TCISe0_29_V_0 <= 162;
                   TCISe0_29_V_1 <= 8;
                   i2c_sda_out <= 1'd1;
                   i2c_scl_out <= 0;
                   xpc10 <= 74/*74:xpc10:74*/;

                   end 
                  
              74/*74:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 75/*75:xpc10:75*/;

                   end 
                  
              75/*75:bashint*/:  begin 
                   TCISe0_29_V_0 <= (TCISe0_29_V_0<<1);
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 76/*76:xpc10:76*/;

                   end 
                  
              76/*76:bashint*/:  begin 
                   TCISe0_29_V_1 <= -1+TCISe0_29_V_1;
                   i2c_sda_openable <= ((1<TCISe0_29_V_1)? i2c_sda_openable: 0);
                   i2c_sda_out <= (0!=(128&TCISe0_29_V_0)) && (1<TCISe0_29_V_1) || (1>=TCISe0_29_V_1) || !checker && (0!=(128&TCISe0_29_V_0
                  )) && (1<TCISe0_29_V_1) || !checker && (1>=TCISe0_29_V_1);

                   i2c_scl_out <= ((1<TCISe0_29_V_1)? 0: i2c_scl_out);
                   xpc10 <= ((1<TCISe0_29_V_1)? ((1<TCISe0_29_V_1)? 74/*74:xpc10:74*/: xpc10): 77/*77:xpc10:77*/);

                   end 
                  
              77/*77:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 78/*78:xpc10:78*/;

                   end 
                  
              78/*78:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 79/*79:xpc10:79*/;

                   end 
                  
              79/*79:bashint*/:  begin 
                   i2c_sda_openable <= 1;
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= 80/*80:xpc10:80*/;

                   end 
                  
              80/*80:bashint*/:  begin 
                   TCISe0_31_V_0 <= 22;
                   TCISe0_31_V_1 <= 8;
                   i2c_sda_out <= 0;
                   i2c_scl_out <= 0;
                   xpc10 <= 81/*81:xpc10:81*/;

                   end 
                  
              81/*81:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 82/*82:xpc10:82*/;

                   end 
                  
              82/*82:bashint*/:  begin 
                   TCISe0_31_V_0 <= (TCISe0_31_V_0<<1);
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 83/*83:xpc10:83*/;

                   end 
                  
              83/*83:bashint*/:  begin 
                   TCISe0_31_V_1 <= -1+TCISe0_31_V_1;
                   i2c_sda_openable <= ((1<TCISe0_31_V_1)? i2c_sda_openable: 0);
                   i2c_sda_out <= (1>=TCISe0_31_V_1) || (0!=(128&TCISe0_31_V_0)) && (1<TCISe0_31_V_1) || !checker && (1>=TCISe0_31_V_1
                  ) || !checker && (0!=(128&TCISe0_31_V_0)) && (1<TCISe0_31_V_1);

                   i2c_scl_out <= ((1<TCISe0_31_V_1)? 0: i2c_scl_out);
                   xpc10 <= ((1<TCISe0_31_V_1)? ((1<TCISe0_31_V_1)? 81/*81:xpc10:81*/: xpc10): 84/*84:xpc10:84*/);

                   end 
                  
              84/*84:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 85/*85:xpc10:85*/;

                   end 
                  
              85/*85:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 86/*86:xpc10:86*/;

                   end 
                  
              86/*86:bashint*/:  begin 
                   i2c_sda_openable <= 1;
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= 87/*87:xpc10:87*/;

                   end 
                  
              87/*87:bashint*/:  begin 
                   i2c_sda_out <= 0;
                   i2c_scl_out <= 1;
                   xpc10 <= 88/*88:xpc10:88*/;

                   end 
                  
              88/*88:bashint*/:  begin 
                   i2c_sda_out <= 1'd1;
                   xpc10 <= 89/*89:xpc10:89*/;

                   end 
                  
              89/*89:bashint*/:  begin 
                   i2c_scl_openable <= 0;
                   i2c_sda_openable <= 0;
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= 90/*90:xpc10:90*/;

                   end 
                  
              90/*90:bashint*/:  begin 
                   i2c_scl_openable <= (checker? 1: i2c_scl_openable);
                   i2c_sda_openable <= (checker? 1: i2c_sda_openable);
                   i2c_sda_out <= checker || !checker && i2c_sda_out;
                   i2c_scl_out <= (checker? 1: i2c_scl_out);
                   done <= (checker? (checker? 0: done): 1);
                   xpc10 <= (checker? 91/*91:xpc10:91*/: (checker? xpc10: 183/*183:xpc10:183*/));

                   end 
                  
              91/*91:bashint*/:  begin 
                   i2c_sda_out <= 0;
                   xpc10 <= 92/*92:xpc10:92*/;

                   end 
                  
              92/*92:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 93/*93:xpc10:93*/;

                   end 
                  
              93/*93:bashint*/:  begin 
                   TCISe0_22_V_0 <= 118;
                   TCISe0_22_V_1 <= 7;
                   i2c_sda_out <= 1'd1;
                   i2c_scl_out <= 0;
                   xpc10 <= 94/*94:xpc10:94*/;

                   end 
                  
              94/*94:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 95/*95:xpc10:95*/;

                   end 
                  
              95/*95:bashint*/:  begin 
                   TCISe0_22_V_0 <= (TCISe0_22_V_0<<1);
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 96/*96:xpc10:96*/;

                   end 
                  
              96/*96:bashint*/:  begin 
                   TCISe0_22_V_1 <= -1+TCISe0_22_V_1;
                   i2c_sda_out <= (0!=(64&TCISe0_22_V_0)) && (1<TCISe0_22_V_1) || !checker && (0!=(64&TCISe0_22_V_0)) && (1<TCISe0_22_V_1
                  );

                   i2c_scl_out <= ((1<TCISe0_22_V_1)? 0: i2c_scl_out);
                   xpc10 <= ((1<TCISe0_22_V_1)? ((1<TCISe0_22_V_1)? 94/*94:xpc10:94*/: xpc10): 97/*97:xpc10:97*/);

                   end 
                  
              97/*97:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 98/*98:xpc10:98*/;

                   end 
                  
              98/*98:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 99/*99:xpc10:99*/;

                   end 
                  
              99/*99:bashint*/:  begin 
                   i2c_sda_openable <= 0;
                   i2c_sda_out <= 1'd1;
                   xpc10 <= 100/*100:xpc10:100*/;

                   end 
                  
              100/*100:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 101/*101:xpc10:101*/;

                   end 
                  
              101/*101:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 102/*102:xpc10:102*/;

                   end 
                  
              102/*102:bashint*/:  begin 
                   i2c_sda_openable <= 1;
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= 103/*103:xpc10:103*/;

                   end 
                  
              103/*103:bashint*/:  begin 
                   TCISe0_29_V_0 <= 200;
                   TCISe0_29_V_1 <= 8;
                   i2c_sda_out <= 1'd1;
                   i2c_scl_out <= 0;
                   xpc10 <= 104/*104:xpc10:104*/;

                   end 
                  
              104/*104:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 105/*105:xpc10:105*/;

                   end 
                  
              105/*105:bashint*/:  begin 
                   TCISe0_29_V_0 <= (TCISe0_29_V_0<<1);
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 106/*106:xpc10:106*/;

                   end 
                  
              106/*106:bashint*/:  begin 
                   TCISe0_29_V_1 <= -1+TCISe0_29_V_1;
                   i2c_sda_openable <= ((1<TCISe0_29_V_1)? i2c_sda_openable: 0);
                   i2c_sda_out <= (0!=(128&TCISe0_29_V_0)) && (1<TCISe0_29_V_1) || (1>=TCISe0_29_V_1) || !checker && (0!=(128&TCISe0_29_V_0
                  )) && (1<TCISe0_29_V_1) || !checker && (1>=TCISe0_29_V_1);

                   i2c_scl_out <= ((1<TCISe0_29_V_1)? 0: i2c_scl_out);
                   xpc10 <= ((1<TCISe0_29_V_1)? ((1<TCISe0_29_V_1)? 104/*104:xpc10:104*/: xpc10): 107/*107:xpc10:107*/);

                   end 
                  
              107/*107:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 108/*108:xpc10:108*/;

                   end 
                  
              108/*108:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 109/*109:xpc10:109*/;

                   end 
                  
              109/*109:bashint*/:  begin 
                   i2c_sda_openable <= 1;
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= 110/*110:xpc10:110*/;

                   end 
                  
              110/*110:bashint*/:  begin 
                   TCISe0_31_V_0 <= 25;
                   TCISe0_31_V_1 <= 8;
                   i2c_sda_out <= 0;
                   i2c_scl_out <= 0;
                   xpc10 <= 111/*111:xpc10:111*/;

                   end 
                  
              111/*111:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 112/*112:xpc10:112*/;

                   end 
                  
              112/*112:bashint*/:  begin 
                   TCISe0_31_V_0 <= (TCISe0_31_V_0<<1);
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 113/*113:xpc10:113*/;

                   end 
                  
              113/*113:bashint*/:  begin 
                   TCISe0_31_V_1 <= -1+TCISe0_31_V_1;
                   i2c_sda_openable <= ((1<TCISe0_31_V_1)? i2c_sda_openable: 0);
                   i2c_sda_out <= (1>=TCISe0_31_V_1) || (0!=(128&TCISe0_31_V_0)) && (1<TCISe0_31_V_1) || !checker && (1>=TCISe0_31_V_1
                  ) || !checker && (0!=(128&TCISe0_31_V_0)) && (1<TCISe0_31_V_1);

                   i2c_scl_out <= ((1<TCISe0_31_V_1)? 0: i2c_scl_out);
                   xpc10 <= ((1<TCISe0_31_V_1)? ((1<TCISe0_31_V_1)? 111/*111:xpc10:111*/: xpc10): 114/*114:xpc10:114*/);

                   end 
                  
              114/*114:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 115/*115:xpc10:115*/;

                   end 
                  
              115/*115:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 116/*116:xpc10:116*/;

                   end 
                  
              116/*116:bashint*/:  begin 
                   i2c_sda_openable <= 1;
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= 117/*117:xpc10:117*/;

                   end 
                  
              117/*117:bashint*/:  begin 
                   i2c_sda_out <= 0;
                   i2c_scl_out <= 1;
                   xpc10 <= 118/*118:xpc10:118*/;

                   end 
                  
              118/*118:bashint*/:  begin 
                   i2c_sda_out <= 1'd1;
                   xpc10 <= 119/*119:xpc10:119*/;

                   end 
                  
              119/*119:bashint*/:  begin 
                   i2c_scl_openable <= 0;
                   i2c_sda_openable <= 0;
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= 120/*120:xpc10:120*/;

                   end 
                  
              120/*120:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   done <= 1;
                   xpc10 <= (checker? (checker? 186/*186:xpc10:186*/: xpc10): 121/*121:xpc10:121*/);

                   end 
                  
              121/*121:bashint*/:  begin 
                   i2c_scl_openable <= (checker? 1: i2c_scl_openable);
                   i2c_sda_openable <= (checker? 1: i2c_sda_openable);
                   i2c_sda_out <= checker || !checker && i2c_sda_out;
                   i2c_scl_out <= (checker? 1: i2c_scl_out);
                   done <= (checker? 0: done);
                   xpc10 <= (checker? (checker? 122/*122:xpc10:122*/: xpc10): 121/*121:xpc10:121*/);

                   end 
                  
              122/*122:bashint*/:  begin 
                   i2c_sda_out <= 0;
                   xpc10 <= 123/*123:xpc10:123*/;

                   end 
                  
              123/*123:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 124/*124:xpc10:124*/;

                   end 
                  
              124/*124:bashint*/:  begin 
                   TCISe0_22_V_0 <= 118;
                   TCISe0_22_V_1 <= 7;
                   i2c_sda_out <= 1'd1;
                   i2c_scl_out <= 0;
                   xpc10 <= 125/*125:xpc10:125*/;

                   end 
                  
              125/*125:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 126/*126:xpc10:126*/;

                   end 
                  
              126/*126:bashint*/:  begin 
                   TCISe0_22_V_0 <= (TCISe0_22_V_0<<1);
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 127/*127:xpc10:127*/;

                   end 
                  
              127/*127:bashint*/:  begin 
                   TCISe0_22_V_1 <= -1+TCISe0_22_V_1;
                   i2c_sda_out <= (0!=(64&TCISe0_22_V_0)) && (1<TCISe0_22_V_1) || !checker && (0!=(64&TCISe0_22_V_0)) && (1<TCISe0_22_V_1
                  );

                   i2c_scl_out <= ((1<TCISe0_22_V_1)? 0: i2c_scl_out);
                   xpc10 <= ((1<TCISe0_22_V_1)? ((1<TCISe0_22_V_1)? 125/*125:xpc10:125*/: xpc10): 128/*128:xpc10:128*/);

                   end 
                  
              128/*128:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 129/*129:xpc10:129*/;

                   end 
                  
              129/*129:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 130/*130:xpc10:130*/;

                   end 
                  
              130/*130:bashint*/:  begin 
                   i2c_sda_openable <= 0;
                   i2c_sda_out <= 1'd1;
                   xpc10 <= 131/*131:xpc10:131*/;

                   end 
                  
              131/*131:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 132/*132:xpc10:132*/;

                   end 
                  
              132/*132:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 133/*133:xpc10:133*/;

                   end 
                  
              133/*133:bashint*/:  begin 
                   i2c_sda_openable <= 1;
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= 134/*134:xpc10:134*/;

                   end 
                  
              134/*134:bashint*/:  begin 
                   TCISe0_29_V_0 <= 200;
                   TCISe0_29_V_1 <= 8;
                   i2c_sda_out <= 1'd1;
                   i2c_scl_out <= 0;
                   xpc10 <= 135/*135:xpc10:135*/;

                   end 
                  
              135/*135:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 136/*136:xpc10:136*/;

                   end 
                  
              136/*136:bashint*/:  begin 
                   TCISe0_29_V_0 <= (TCISe0_29_V_0<<1);
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 137/*137:xpc10:137*/;

                   end 
                  
              137/*137:bashint*/:  begin 
                   TCISe0_29_V_1 <= -1+TCISe0_29_V_1;
                   i2c_sda_openable <= ((1<TCISe0_29_V_1)? i2c_sda_openable: 0);
                   i2c_sda_out <= (0!=(128&TCISe0_29_V_0)) && (1<TCISe0_29_V_1) || (1>=TCISe0_29_V_1) || !checker && (0!=(128&TCISe0_29_V_0
                  )) && (1<TCISe0_29_V_1) || !checker && (1>=TCISe0_29_V_1);

                   i2c_scl_out <= ((1<TCISe0_29_V_1)? 0: i2c_scl_out);
                   xpc10 <= ((1<TCISe0_29_V_1)? ((1<TCISe0_29_V_1)? 135/*135:xpc10:135*/: xpc10): 138/*138:xpc10:138*/);

                   end 
                  
              138/*138:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 139/*139:xpc10:139*/;

                   end 
                  
              139/*139:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 140/*140:xpc10:140*/;

                   end 
                  
              140/*140:bashint*/:  begin 
                   i2c_sda_openable <= 1;
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= 141/*141:xpc10:141*/;

                   end 
                  
              141/*141:bashint*/:  begin 
                   TCISe0_31_V_0 <= 26;
                   TCISe0_31_V_1 <= 8;
                   i2c_sda_out <= 0;
                   i2c_scl_out <= 0;
                   xpc10 <= 142/*142:xpc10:142*/;

                   end 
                  
              142/*142:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 143/*143:xpc10:143*/;

                   end 
                  
              143/*143:bashint*/:  begin 
                   TCISe0_31_V_0 <= (TCISe0_31_V_0<<1);
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 144/*144:xpc10:144*/;

                   end 
                  
              144/*144:bashint*/:  begin 
                   TCISe0_31_V_1 <= -1+TCISe0_31_V_1;
                   i2c_sda_openable <= ((1<TCISe0_31_V_1)? i2c_sda_openable: 0);
                   i2c_sda_out <= (1>=TCISe0_31_V_1) || (0!=(128&TCISe0_31_V_0)) && (1<TCISe0_31_V_1) || !checker && (1>=TCISe0_31_V_1
                  ) || !checker && (0!=(128&TCISe0_31_V_0)) && (1<TCISe0_31_V_1);

                   i2c_scl_out <= ((1<TCISe0_31_V_1)? 0: i2c_scl_out);
                   xpc10 <= ((1<TCISe0_31_V_1)? ((1<TCISe0_31_V_1)? 142/*142:xpc10:142*/: xpc10): 145/*145:xpc10:145*/);

                   end 
                  
              145/*145:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 146/*146:xpc10:146*/;

                   end 
                  
              146/*146:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 147/*147:xpc10:147*/;

                   end 
                  
              147/*147:bashint*/:  begin 
                   i2c_sda_openable <= 1;
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= 148/*148:xpc10:148*/;

                   end 
                  
              148/*148:bashint*/:  begin 
                   i2c_sda_out <= 0;
                   i2c_scl_out <= 1;
                   xpc10 <= 149/*149:xpc10:149*/;

                   end 
                  
              149/*149:bashint*/:  begin 
                   i2c_sda_out <= 1'd1;
                   xpc10 <= 150/*150:xpc10:150*/;

                   end 
                  
              150/*150:bashint*/:  begin 
                   i2c_scl_openable <= 0;
                   i2c_sda_openable <= 0;
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= 151/*151:xpc10:151*/;

                   end 
                  
              151/*151:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   done <= 1;
                   xpc10 <= (checker? (checker? 185/*185:xpc10:185*/: xpc10): 152/*152:xpc10:152*/);

                   end 
                  
              152/*152:bashint*/:  begin 
                   i2c_scl_openable <= (checker? 1: i2c_scl_openable);
                   i2c_sda_openable <= (checker? 1: i2c_sda_openable);
                   i2c_sda_out <= checker || !checker && i2c_sda_out;
                   i2c_scl_out <= (checker? 1: i2c_scl_out);
                   done <= (checker? 0: done);
                   xpc10 <= (checker? (checker? 153/*153:xpc10:153*/: xpc10): 152/*152:xpc10:152*/);

                   end 
                  
              153/*153:bashint*/:  begin 
                   i2c_sda_out <= 0;
                   xpc10 <= 154/*154:xpc10:154*/;

                   end 
                  
              154/*154:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 155/*155:xpc10:155*/;

                   end 
                  
              155/*155:bashint*/:  begin 
                   TCISe0_22_V_0 <= 118;
                   TCISe0_22_V_1 <= 7;
                   i2c_sda_out <= 1'd1;
                   i2c_scl_out <= 0;
                   xpc10 <= 156/*156:xpc10:156*/;

                   end 
                  
              156/*156:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 157/*157:xpc10:157*/;

                   end 
                  
              157/*157:bashint*/:  begin 
                   TCISe0_22_V_0 <= (TCISe0_22_V_0<<1);
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 158/*158:xpc10:158*/;

                   end 
                  
              158/*158:bashint*/:  begin 
                   TCISe0_22_V_1 <= -1+TCISe0_22_V_1;
                   i2c_sda_out <= (0!=(64&TCISe0_22_V_0)) && (1<TCISe0_22_V_1) || !checker && (0!=(64&TCISe0_22_V_0)) && (1<TCISe0_22_V_1
                  );

                   i2c_scl_out <= ((1<TCISe0_22_V_1)? 0: i2c_scl_out);
                   xpc10 <= ((1<TCISe0_22_V_1)? ((1<TCISe0_22_V_1)? 156/*156:xpc10:156*/: xpc10): 159/*159:xpc10:159*/);

                   end 
                  
              159/*159:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 160/*160:xpc10:160*/;

                   end 
                  
              160/*160:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 161/*161:xpc10:161*/;

                   end 
                  
              161/*161:bashint*/:  begin 
                   i2c_sda_openable <= 0;
                   i2c_sda_out <= 1'd1;
                   xpc10 <= 162/*162:xpc10:162*/;

                   end 
                  
              162/*162:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 163/*163:xpc10:163*/;

                   end 
                  
              163/*163:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 164/*164:xpc10:164*/;

                   end 
                  
              164/*164:bashint*/:  begin 
                   i2c_sda_openable <= 1;
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= 165/*165:xpc10:165*/;

                   end 
                  
              165/*165:bashint*/:  begin 
                   TCISe0_29_V_0 <= 200;
                   TCISe0_29_V_1 <= 8;
                   i2c_sda_out <= 1'd1;
                   i2c_scl_out <= 0;
                   xpc10 <= 166/*166:xpc10:166*/;

                   end 
                  
              166/*166:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 167/*167:xpc10:167*/;

                   end 
                  
              167/*167:bashint*/:  begin 
                   TCISe0_29_V_0 <= (TCISe0_29_V_0<<1);
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 168/*168:xpc10:168*/;

                   end 
                  
              168/*168:bashint*/:  begin 
                   TCISe0_29_V_1 <= -1+TCISe0_29_V_1;
                   i2c_sda_openable <= ((1<TCISe0_29_V_1)? i2c_sda_openable: 0);
                   i2c_sda_out <= (1>=TCISe0_29_V_1) || (0!=(128&TCISe0_29_V_0)) && (1<TCISe0_29_V_1) || !checker && (1>=TCISe0_29_V_1
                  ) || !checker && (0!=(128&TCISe0_29_V_0)) && (1<TCISe0_29_V_1);

                   i2c_scl_out <= ((1<TCISe0_29_V_1)? 0: i2c_scl_out);
                   xpc10 <= ((1<TCISe0_29_V_1)? ((1<TCISe0_29_V_1)? 166/*166:xpc10:166*/: xpc10): 169/*169:xpc10:169*/);

                   end 
                  
              169/*169:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 170/*170:xpc10:170*/;

                   end 
                  
              170/*170:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 171/*171:xpc10:171*/;

                   end 
                  
              171/*171:bashint*/:  begin 
                   i2c_sda_openable <= 1;
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= 172/*172:xpc10:172*/;

                   end 
                  
              172/*172:bashint*/:  begin 
                   TCISe0_31_V_0 <= 24;
                   TCISe0_31_V_1 <= 8;
                   i2c_sda_out <= 0;
                   i2c_scl_out <= 0;
                   xpc10 <= 173/*173:xpc10:173*/;

                   end 
                  
              173/*173:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 174/*174:xpc10:174*/;

                   end 
                  
              174/*174:bashint*/:  begin 
                   TCISe0_31_V_0 <= (TCISe0_31_V_0<<1);
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 175/*175:xpc10:175*/;

                   end 
                  
              175/*175:bashint*/:  begin 
                   TCISe0_31_V_1 <= -1+TCISe0_31_V_1;
                   i2c_sda_openable <= ((1<TCISe0_31_V_1)? i2c_sda_openable: 0);
                   i2c_sda_out <= (0!=(128&TCISe0_31_V_0)) && (1<TCISe0_31_V_1) || (1>=TCISe0_31_V_1) || !checker && (0!=(128&TCISe0_31_V_0
                  )) && (1<TCISe0_31_V_1) || !checker && (1>=TCISe0_31_V_1);

                   i2c_scl_out <= ((1<TCISe0_31_V_1)? 0: i2c_scl_out);
                   xpc10 <= ((1<TCISe0_31_V_1)? ((1<TCISe0_31_V_1)? 173/*173:xpc10:173*/: xpc10): 176/*176:xpc10:176*/);

                   end 
                  
              176/*176:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 1;
                   xpc10 <= 177/*177:xpc10:177*/;

                   end 
                  
              177/*177:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   i2c_scl_out <= 0;
                   xpc10 <= 178/*178:xpc10:178*/;

                   end 
                  
              178/*178:bashint*/:  begin 
                   i2c_sda_openable <= 1;
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= 179/*179:xpc10:179*/;

                   end 
                  
              179/*179:bashint*/:  begin 
                  $display("Generate stop condition.");
                   i2c_sda_out <= 0;
                   i2c_scl_out <= 1;
                   xpc10 <= 180/*180:xpc10:180*/;

                   end 
                  
              180/*180:bashint*/:  begin 
                   i2c_sda_out <= 1'd1;
                   xpc10 <= 181/*181:xpc10:181*/;

                   end 
                  
              181/*181:bashint*/:  begin 
                   i2c_scl_openable <= 0;
                   i2c_sda_openable <= 0;
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= 182/*182:xpc10:182*/;

                   end 
                  
              182/*182:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   done <= 1;
                   xpc10 <= (checker? (checker? 184/*184:xpc10:184*/: xpc10): 183/*183:xpc10:183*/);

                   end 
                  
              183/*183:bashint*/:  begin 
                  if (checker) $display("Start condition");
                       i2c_scl_openable <= (checker? 1: i2c_scl_openable);
                   i2c_sda_openable <= (checker? 1: i2c_sda_openable);
                   i2c_sda_out <= !checker && i2c_sda_out || checker;
                   i2c_scl_out <= (checker? 1: i2c_scl_out);
                   done <= (checker? 0: done);
                   xpc10 <= (checker? (checker? 91/*91:xpc10:91*/: xpc10): 183/*183:xpc10:183*/);

                   end 
                  
              184/*184:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= (checker? 184/*184:xpc10:184*/: (checker? xpc10: 183/*183:xpc10:183*/));

                   end 
                  
              185/*185:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= (checker? 185/*185:xpc10:185*/: (checker? xpc10: 152/*152:xpc10:152*/));

                   end 
                  
              186/*186:bashint*/:  begin 
                   i2c_sda_out <= i2c_sda_out || !checker && i2c_sda_out;
                   xpc10 <= (checker? 186/*186:xpc10:186*/: (checker? xpc10: 121/*121:xpc10:121*/));

                   end 
                  endcase

               end 
              //End HPR i2c_control.exe


       end 
      

// Total area 0
// 1 vectors of width 11
// 192 bits in scalar variables
// Total state bits in module = 203 bits.
// Total number of leaf cells = 0
endmodule

// LCP delay estimations included: turn off with -vnl-lcp-delay-estimate=disable
// eof (HPR/LS Verilog)
