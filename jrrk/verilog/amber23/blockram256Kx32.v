
module blockram256Kx32 (clka, ena, wea, addra, dina, douta);

  input clka;
  input ena;
  input [3 : 0] wea;
  input [17 : 0] addra;
  input [31 : 0] dina;
  output [31 : 0] douta;
  
   RAM256K   RAM256K_inst_0 (
      .CLK(clka),      // Port A Clock
      .DO(douta[0]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[0]),   // Port A 1-bit Data Input
      .WE(wea[0]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_1 (
      .CLK(clka),      // Port A Clock
      .DO(douta[1]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[1]),   // Port A 1-bit Data Input
      .WE(wea[0]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_2 (
      .CLK(clka),      // Port A Clock
      .DO(douta[2]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[2]),   // Port A 1-bit Data Input
      .WE(wea[0]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_3 (
      .CLK(clka),      // Port A Clock
      .DO(douta[3]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[3]),   // Port A 1-bit Data Input
      .WE(wea[0]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_4 (
      .CLK(clka),      // Port A Clock
      .DO(douta[4]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[4]),   // Port A 1-bit Data Input
      .WE(wea[0]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_5 (
      .CLK(clka),      // Port A Clock
      .DO(douta[5]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[5]),   // Port A 1-bit Data Input
      .WE(wea[0]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_6 (
      .CLK(clka),      // Port A Clock
      .DO(douta[6]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[6]),   // Port A 1-bit Data Input
      .WE(wea[0]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_7 (
      .CLK(clka),      // Port A Clock
      .DO(douta[7]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[7]),   // Port A 1-bit Data Input
      .WE(wea[0]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_8 (
      .CLK(clka),      // Port A Clock
      .DO(douta[8]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[8]),   // Port A 1-bit Data Input
      .WE(wea[1]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_9 (
      .CLK(clka),      // Port A Clock
      .DO(douta[9]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[9]),   // Port A 1-bit Data Input
      .WE(wea[1]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_10 (
      .CLK(clka),      // Port A Clock
      .DO(douta[10]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[10]),   // Port A 1-bit Data Input
      .WE(wea[1]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_11 (
      .CLK(clka),      // Port A Clock
      .DO(douta[11]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[11]),   // Port A 1-bit Data Input
      .WE(wea[1]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_12 (
      .CLK(clka),      // Port A Clock
      .DO(douta[12]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[12]),   // Port A 1-bit Data Input
      .WE(wea[1]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_13 (
      .CLK(clka),      // Port A Clock
      .DO(douta[13]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[13]),   // Port A 1-bit Data Input
      .WE(wea[1]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_14 (
      .CLK(clka),      // Port A Clock
      .DO(douta[14]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[14]),   // Port A 1-bit Data Input
      .WE(wea[1]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_15 (
      .CLK(clka),      // Port A Clock
      .DO(douta[15]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[15]),   // Port A 1-bit Data Input
      .WE(wea[1]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_16 (
      .CLK(clka),      // Port A Clock
      .DO(douta[16]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[16]),   // Port A 1-bit Data Input
      .WE(wea[2]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_17 (
      .CLK(clka),      // Port A Clock
      .DO(douta[17]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[17]),   // Port A 1-bit Data Input
      .WE(wea[2]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_18 (
      .CLK(clka),      // Port A Clock
      .DO(douta[18]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[18]),   // Port A 1-bit Data Input
      .WE(wea[2]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_19 (
      .CLK(clka),      // Port A Clock
      .DO(douta[19]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[19]),   // Port A 1-bit Data Input
      .WE(wea[2]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_20 (
      .CLK(clka),      // Port A Clock
      .DO(douta[20]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[20]),   // Port A 1-bit Data Input
      .WE(wea[2]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_21 (
      .CLK(clka),      // Port A Clock
      .DO(douta[21]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[21]),   // Port A 1-bit Data Input
      .WE(wea[2]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_22 (
      .CLK(clka),      // Port A Clock
      .DO(douta[22]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[22]),   // Port A 1-bit Data Input
      .WE(wea[2]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_23 (
      .CLK(clka),      // Port A Clock
      .DO(douta[23]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[23]),   // Port A 1-bit Data Input
      .WE(wea[2]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_24 (
      .CLK(clka),      // Port A Clock
      .DO(douta[24]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[24]),   // Port A 1-bit Data Input
      .WE(wea[3]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_25 (
      .CLK(clka),      // Port A Clock
      .DO(douta[25]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[25]),   // Port A 1-bit Data Input
      .WE(wea[3]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_26 (
      .CLK(clka),      // Port A Clock
      .DO(douta[26]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[26]),   // Port A 1-bit Data Input
      .WE(wea[3]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_27 (
      .CLK(clka),      // Port A Clock
      .DO(douta[27]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[27]),   // Port A 1-bit Data Input
      .WE(wea[3]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_28 (
      .CLK(clka),      // Port A Clock
      .DO(douta[28]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[28]),   // Port A 1-bit Data Input
      .WE(wea[3]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_29 (
      .CLK(clka),      // Port A Clock
      .DO(douta[29]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[29]),   // Port A 1-bit Data Input
      .WE(wea[3]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_30 (
      .CLK(clka),      // Port A Clock
      .DO(douta[30]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[30]),   // Port A 1-bit Data Input
      .WE(wea[3]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

   RAM256K   RAM256K_inst_31 (
      .CLK(clka),      // Port A Clock
      .DO(douta[31]),  // Port A 1-bit Data Output
      .ADDR(addra),    // Port A 14-bit Address Input
      .DI(dina[31]),   // Port A 1-bit Data Input
      .WE(wea[3]),    // Port A RAM Enable Input
      .EN(ena)         // Port A Write Enable Input
   );

endmodule

// synthesis translate_off

`ifndef GLBL
`define GLBL

`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;

    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule

`endif

// synthesis translate_on
