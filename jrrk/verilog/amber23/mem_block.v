module mem_block(clk,
	readwrite_addr,
	byte_strobes,
	write_enable,
	read_enable,
	write_data,
	read_data,
	dbg_mem);

parameter siz = 18;

input[31:0] write_data;
output[31:0] read_data;
input [siz+1:0] readwrite_addr;
input [3:0] byte_strobes;
input clk, write_enable, read_enable, dbg_mem;

   wire [31:0] dout, block_dout;
   wire        in_range_nxt = ~(|readwrite_addr[siz+1:16]);

   reg 	  in_range;

   always @(posedge clk)
     begin
	in_range <= in_range_nxt;
     end
   
blockram256Kx32 block1 (
  .clka(clk),
  .ena((read_enable|write_enable)&~in_range_nxt),
  .wea(write_enable ? byte_strobes : 4'b0),
  .addra(readwrite_addr[19:2]),
  .dina(write_data),
  .douta(block_dout)
 );

globals rom1(
	     .clk(clk),
	     .din(write_data),
	     .addr(readwrite_addr[15:2]),
	     .we(in_range_nxt && write_enable),
	     .dout(dout),
	     .en(byte_strobes));

assign read_data = in_range ? dout : block_dout;

endmodule
