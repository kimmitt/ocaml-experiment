
module RAM256K (CLK, EN, WE, ADDR, DI, DO);

  input CLK;
  input EN;
  input WE;
  input [17 : 0] ADDR;
  input [0 : 0] DI;
  output [0 : 0] DO;
  
   wire [15:0] 	 douta;
   
   wire [15:0] 	 ena  = EN ? (1 << ADDR[17:14]) : 16'b0;
   reg [17 : 14] addr0;

   assign DO = douta[addr0];
   assign douta[0] = 1'b0;
		 
   always @(posedge CLK)
	addr0 <= ADDR[17:14];

/*   
   RAMB16_S1   RAMB16_inst_0 (
      .CLK(CLK),      // Port A Clock
      .DO(douta[0]),  // Port A 1-bit Data Output
      .ADDR(ADDR[13:0]),    // Port A 14-bit Address Input
      .EN(ena[0]),   // Port A 1-bit Data Input
      .DI(DI),    // Port A RAM Enable Input
      .SSR(1'b0),     // Port A Synchronous Set/Reset Input
      .WE(WE)         // Port A Write Enable Input
   );
*/
   
   RAMB16_S1   RAMB16_inst_1 (
      .CLK(CLK),      // Port A Clock
      .DO(douta[1]),  // Port A 1-bit Data Output
      .ADDR(ADDR[13:0]),    // Port A 14-bit Address Input
      .EN(ena[1]),   // Port A 1-bit Data Input
      .DI(DI),    // Port A RAM Enable Input
      .SSR(1'b0),     // Port A Synchronous Set/Reset Input
      .WE(WE)         // Port A Write Enable Input
   );

   RAMB16_S1   RAMB16_inst_2 (
      .CLK(CLK),      // Port A Clock
      .DO(douta[2]),  // Port A 1-bit Data Output
      .ADDR(ADDR[13:0]),    // Port A 14-bit Address Input
      .EN(ena[2]),   // Port A 1-bit Data Input
      .DI(DI),    // Port A RAM Enable Input
      .SSR(1'b0),     // Port A Synchronous Set/Reset Input
      .WE(WE)         // Port A Write Enable Input
   );

   RAMB16_S1   RAMB16_inst_3 (
      .CLK(CLK),      // Port A Clock
      .DO(douta[3]),  // Port A 1-bit Data Output
      .ADDR(ADDR[13:0]),    // Port A 14-bit Address Input
      .EN(ena[3]),   // Port A 1-bit Data Input
      .DI(DI),    // Port A RAM Enable Input
      .SSR(1'b0),     // Port A Synchronous Set/Reset Input
      .WE(WE)         // Port A Write Enable Input
   );

   RAMB16_S1   RAMB16_inst_4 (
      .CLK(CLK),      // Port A Clock
      .DO(douta[4]),  // Port A 1-bit Data Output
      .ADDR(ADDR[13:0]),    // Port A 14-bit Address Input
      .EN(ena[4]),   // Port A 1-bit Data Input
      .DI(DI),    // Port A RAM Enable Input
      .SSR(1'b0),     // Port A Synchronous Set/Reset Input
      .WE(WE)         // Port A Write Enable Input
   );

   RAMB16_S1   RAMB16_inst_5 (
      .CLK(CLK),      // Port A Clock
      .DO(douta[5]),  // Port A 1-bit Data Output
      .ADDR(ADDR[13:0]),    // Port A 14-bit Address Input
      .EN(ena[5]),   // Port A 1-bit Data Input
      .DI(DI),    // Port A RAM Enable Input
      .SSR(1'b0),     // Port A Synchronous Set/Reset Input
      .WE(WE)         // Port A Write Enable Input
   );

   RAMB16_S1   RAMB16_inst_6 (
      .CLK(CLK),      // Port A Clock
      .DO(douta[6]),  // Port A 1-bit Data Output
      .ADDR(ADDR[13:0]),    // Port A 14-bit Address Input
      .EN(ena[6]),   // Port A 1-bit Data Input
      .DI(DI),    // Port A RAM Enable Input
      .SSR(1'b0),     // Port A Synchronous Set/Reset Input
      .WE(WE)         // Port A Write Enable Input
   );

   RAMB16_S1   RAMB16_inst_7 (
      .CLK(CLK),      // Port A Clock
      .DO(douta[7]),  // Port A 1-bit Data Output
      .ADDR(ADDR[13:0]),    // Port A 14-bit Address Input
      .EN(ena[7]),   // Port A 1-bit Data Input
      .DI(DI),    // Port A RAM Enable Input
      .SSR(1'b0),     // Port A Synchronous Set/Reset Input
      .WE(WE)         // Port A Write Enable Input
   );

   RAMB16_S1   RAMB16_inst_8 (
      .CLK(CLK),      // Port A Clock
      .DO(douta[8]),  // Port A 1-bit Data Output
      .ADDR(ADDR[13:0]),    // Port A 14-bit Address Input
      .EN(ena[8]),   // Port A 1-bit Data Input
      .DI(DI),    // Port A RAM Enable Input
      .SSR(1'b0),     // Port A Synchronous Set/Reset Input
      .WE(WE)         // Port A Write Enable Input
   );

   RAMB16_S1   RAMB16_inst_9 (
      .CLK(CLK),      // Port A Clock
      .DO(douta[9]),  // Port A 1-bit Data Output
      .ADDR(ADDR[13:0]),    // Port A 14-bit Address Input
      .EN(ena[9]),   // Port A 1-bit Data Input
      .DI(DI),    // Port A RAM Enable Input
      .SSR(1'b0),     // Port A Synchronous Set/Reset Input
      .WE(WE)         // Port A Write Enable Input
   );

   RAMB16_S1   RAMB16_inst_10 (
      .CLK(CLK),      // Port A Clock
      .DO(douta[10]),  // Port A 1-bit Data Output
      .ADDR(ADDR[13:0]),    // Port A 14-bit Address Input
      .EN(ena[10]),   // Port A 1-bit Data Input
      .DI(DI),    // Port A RAM Enable Input
      .SSR(1'b0),     // Port A Synchronous Set/Reset Input
      .WE(WE)         // Port A Write Enable Input
   );

   RAMB16_S1   RAMB16_inst_11 (
      .CLK(CLK),      // Port A Clock
      .DO(douta[11]),  // Port A 1-bit Data Output
      .ADDR(ADDR[13:0]),    // Port A 14-bit Address Input
      .EN(ena[11]),   // Port A 1-bit Data Input
      .DI(DI),    // Port A RAM Enable Input
      .SSR(1'b0),     // Port A Synchronous Set/Reset Input
      .WE(WE)         // Port A Write Enable Input
   );

   RAMB16_S1   RAMB16_inst_12 (
      .CLK(CLK),      // Port A Clock
      .DO(douta[12]),  // Port A 1-bit Data Output
      .ADDR(ADDR[13:0]),    // Port A 14-bit Address Input
      .EN(ena[12]),   // Port A 1-bit Data Input
      .DI(DI),    // Port A RAM Enable Input
      .SSR(1'b0),     // Port A Synchronous Set/Reset Input
      .WE(WE)         // Port A Write Enable Input
   );

   RAMB16_S1   RAMB16_inst_13 (
      .CLK(CLK),      // Port A Clock
      .DO(douta[13]),  // Port A 1-bit Data Output
      .ADDR(ADDR[13:0]),    // Port A 14-bit Address Input
      .EN(ena[13]),   // Port A 1-bit Data Input
      .DI(DI),    // Port A RAM Enable Input
      .SSR(1'b0),     // Port A Synchronous Set/Reset Input
      .WE(WE)         // Port A Write Enable Input
   );

   RAMB16_S1   RAMB16_inst_14 (
      .CLK(CLK),      // Port A Clock
      .DO(douta[14]),  // Port A 1-bit Data Output
      .ADDR(ADDR[13:0]),    // Port A 14-bit Address Input
      .EN(ena[14]),   // Port A 1-bit Data Input
      .DI(DI),    // Port A RAM Enable Input
      .SSR(1'b0),     // Port A Synchronous Set/Reset Input
      .WE(WE)         // Port A Write Enable Input
   );

   RAMB16_S1   RAMB16_inst_15 (
      .CLK(CLK),      // Port A Clock
      .DO(douta[15]),  // Port A 1-bit Data Output
      .ADDR(ADDR[13:0]),    // Port A 14-bit Address Input
      .EN(ena[15]),   // Port A 1-bit Data Input
      .DI(DI),    // Port A RAM Enable Input
      .SSR(1'b0),     // Port A Synchronous Set/Reset Input
      .WE(WE)         // Port A Write Enable Input
   );

endmodule
