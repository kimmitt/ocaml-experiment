module MUL18 (P, A, B); // black-box

    output [35:0] P;

    input  [17:0] A;
    input  [17:0] B;

MULT18X18 mult0 (
  .P(P),
  .A(A),
  .B(B)
    );

endmodule
