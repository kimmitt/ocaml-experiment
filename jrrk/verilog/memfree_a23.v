//////////////////////////////////////////////////////////////////
//                                                              //
//  Standalone Execute module based on Amber 2 Core             //
//                                                              //
//  This file is based on the Amber project                     //
//  http://www.opencores.org/project,amber                      //
//                                                              //
//  Description                                                 //
//  Executes control store contents in a state machine          //
//                                                              //
//  Author(s):                                                  //
//      - Conor Santifort, csantifort.amber@gmail.com           //
//      - Jonathan Kimmitt, jonathan@kimmitt.co.uk              //
//                                                              //
//////////////////////////////////////////////////////////////////
//                                                              //
// Copyright (C) 2010 Authors and OPENCORES.ORG                 //
//                                                              //
// This source file may be used and distributed without         //
// restriction provided that this copyright statement is not    //
// removed from the file and that any derivative work contains  //
// the original copyright notice and the associated disclaimer. //
//                                                              //
// This source file is free software; you can redistribute it   //
// and/or modify it under the terms of the GNU Lesser General   //
// Public License as published by the Free Software Foundation; //
// either version 2.1 of the License, or (at your option) any   //
// later version.                                               //
//                                                              //
// This source is distributed in the hope that it will be       //
// useful, but WITHOUT ANY WARRANTY; without even the implied   //
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      //
// PURPOSE.  See the GNU Lesser General Public License for more //
// details.                                                     //
//                                                              //
// You should have received a copy of the GNU Lesser General    //
// Public License along with this source; if not, download it   //
// from http://www.opencores.org/lgpl.shtml                     //
//                                                              //
//////////////////////////////////////////////////////////////////


module standalone(
input a23_clk, a23_rst, dbg_mem,
input [31:0] read_data,
output reg [31:0]           execute_write_data,
output     [31:0]           execute_write_data_nxt,
output reg [31:0]           execute_address,      // registered version of execute_address to the ram
output     [31:0]           execute_address_nxt,  // un-registered version of execute_address to the ram
output     [3:0]            execute_byte_enable_nxt,
output reg                  execute_write_enable,
output                      write_data_wen_out,
output     [0:0] 	    read_enable_out,
output       finish,
output [0:0] flush,
output [0:0] readstrobe,
input [7:0] readch,
output [7:0] writech,
output reg	 [31:0] 	  r0_out,
output reg	 [31:0] 	  r1_out,
output reg	 [31:0] 	  r2_out,
output reg	 [31:0] 	  r3_out,
output reg	 [31:0] 	  r4_out,
output reg	 [31:0] 	  r5_out,
output reg	 [31:0] 	  r6_out,
output reg	 [31:0] 	  r7_out,
output reg	 [31:0] 	  r8_out,
output reg	 [31:0] 	  r9_out,
output reg	 [31:0] 	  r10_out,
output reg	 [31:0] 	  r11_out,
output reg	 [31:0] 	  r12_out,
output reg	 [31:0] 	  r13_out,
output reg	 [31:0] 	  r14_out,
output	 [31:0] 	  r15_out,
output	 [31:0] 	  pc_nxt_out,
input	 [75:0] 	  xstate);

wire [4:0] oreg_sel;
wire [0:0] copro_write_data_wen;
wire [0:0] write_data_wen;
wire [1:0] reg_write_sel;
wire [1:0] byte_enable_sel;
wire [17:2] pc_nxt;
wire [2:0] pc_sel;
wire [2:0] address_sel;
wire [6:0] alu_function;
wire [1:0] barrel_shift_function;
wire [1:0] barrel_shift_data_sel;
wire [1:0] barrel_shift_amount_sel;
wire [3:0] rn_sel;
wire [3:0] rds_sel;
wire [3:0] rm_sel;
wire [4:0] imm_shift_amount;
wire [7:0] imm8;
wire [0:0] read_enable;
wire [3:0] a23_ccode;

reg     [31:0]           execute_copro_write_data;
assign writech = {copro_write_data_wen,execute_copro_write_data[7:1]};
wire     [2:0]            interrupt_vector_sel = 0;

wire                      system_rdy = !a23_rst;

wire     [31:0]           copro_read_data = 0;

reg                       execute_address_valid;

reg      [3:0]            execute_byte_enable;
wire                      fetch_stall = finish;

   

assign                    finish = &pc_nxt_out[17:2];

parameter _caml_int32_ops = -1,
	  ASR = 2,
	  LSL = 0,
	  LSR = 1,
	  alu_inb = 0,
	  alu_not_sel = 32,
	  alu_swap_sel = 64,
	  alu_Eadd = 1,
	  alu_zex16 = 2,
	  alu_zex8 = 3,
	  alu_sex16 = 4,
	  alu_sex8 = 5,
	  alu_Exor = 6,
	  alu_Eor = 7,
	  alu_Eand = 8,
	  alu_Esub = 49,
	  alu_Enot = 32,
	  alu_rsub = 113,
	  alu_Easr = 0,
	  alu_Elsl = 0,
	  alu_Elsr = 0,
	  alu_Emul = 12,
	  alu_Ediv = 13,
	  alu_Emod = 14,
	  barrel_Eadd = 0,
	  barrel_Eand = 0,
	  barrel_Easr = 2,
	  barrel_Elsl = 0,
	  barrel_Elsr = 1,
	  barrel_Emul = 0,
	  barrel_Ediv = 0,
	  barrel_Emod = 0,
	  barrel_Eor = 0,
	  barrel_Exor = 0,
	  barrel_Esub = 0,
	  Eunsigned_Eeq = 1,
	  Esigned_Eeq = 2,
	  Eunsigned_Ene = 3,
	  Esigned_Ene = 4,
	  Esigned_Ele = 5,
	  Esigned_Ege = 6,
	  Esigned_Elt = 7,
	  Esigned_Egt = 8,
	  Eunsigned_Ele = 9,
	  Eunsigned_Ege = 10,
	  Eunsigned_Elt = 11,
	  Eunsigned_Egt = 12;

// ========================================================
// Instantiate Register Bank
// ========================================================

reg  [23:0] r15 ;
integer  i;

// ========================================================
// Internal signals
// ========================================================
wire [31:0]         pc_plus4;
wire [31:0]         pc_minus4;
wire [31:0]         address_plus4;
wire [31:0]         alu_plus4;
wire [31:0]         rn_plus4;
wire [31:0]         rn_minus4;
wire [31:0]         alu_out;
wire [31:0]         pc = {6'd0, r15, 2'd0};
reg [31:0] 	    rd, rm, rn, rs;
reg                 compare_a23;
wire [31:0]         interrupt_vector;
wire [7:0]          shift_amount;
wire [31:0]         barrel_shift_in;
wire [32:0]         barrel_shift_out;
wire                alu_flag_neg, alu_flag_zero, alu_flag_cout, alu_flag_ov;

wire [31:0]         reg_write_nxt;
wire [31:0]         multiply_out;
wire [1:0]          multiply_flags;

wire                address_update;
wire                write_data_update;
wire                copro_write_data_update;
wire                byte_enable_update;
wire                write_enable_update;

wire [31:0]         alu_out_pc_filtered;

// ========================================================
// Rds Selector
// ========================================================
always @*
    case (rds_sel)
       4'd0  :  rd = r0_out  ;
       4'd1  :  rd = r1_out  ; 
       4'd2  :  rd = r2_out  ; 
       4'd3  :  rd = r3_out  ; 
       4'd4  :  rd = r4_out  ; 
       4'd5  :  rd = r5_out  ; 
       4'd6  :  rd = r6_out  ; 
       4'd7  :  rd = r7_out  ; 
       4'd8  :  rd = r8_out  ; 
       4'd9  :  rd = r9_out  ; 
       4'd10 :  rd = r10_out ; 
       4'd11 :  rd = r11_out ; 
       4'd12 :  rd = r12_out ; 
       4'd13 :  rd = r13_out ; 
       4'd14 :  rd = r14_out ; 
       default: rd = {6'd0, pc_nxt_out[25:2], 2'b0} ; 
    endcase
// ========================================================
// Rn Selector
// ========================================================
always @*
    case (rn_sel)
       4'd0  :  rn = r0_out  ;
       4'd1  :  rn = r1_out  ; 
       4'd2  :  rn = r2_out  ; 
       4'd3  :  rn = r3_out  ; 
       4'd4  :  rn = r4_out  ; 
       4'd5  :  rn = r5_out  ; 
       4'd6  :  rn = r6_out  ; 
       4'd7  :  rn = r7_out  ; 
       4'd8  :  rn = r8_out  ; 
       4'd9  :  rn = r9_out  ; 
       4'd10 :  rn = r10_out ; 
       4'd11 :  rn = r11_out ; 
       4'd12 :  rn = r12_out ; 
       4'd13 :  rn = r13_out ; 
       4'd14 :  rn = r14_out ; 
       default: rn = pc ; 
    endcase
// ========================================================
// Rm Selector
// ========================================================
always @*
    case (rm_sel)
       4'd0  :  rm = r0_out  ;
       4'd1  :  rm = r1_out  ; 
       4'd2  :  rm = r2_out  ; 
       4'd3  :  rm = r3_out  ; 
       4'd4  :  rm = r4_out  ; 
       4'd5  :  rm = r5_out  ; 
       4'd6  :  rm = r6_out  ; 
       4'd7  :  rm = r7_out  ; 
       4'd8  :  rm = r8_out  ; 
       4'd9  :  rm = r9_out  ; 
       4'd10 :  rm = r10_out ; 
       4'd11 :  rm = r11_out ; 
       4'd12 :  rm = r12_out ; 
       4'd13 :  rm = r13_out ; 
       4'd14 :  rm = r14_out ; 
       default: rm = pc ; 
    endcase
// ========================================================
// Rds Selector
// ========================================================
always @*
    case (rds_sel)
       4'd0  :  rs = r0_out  ;
       4'd1  :  rs = r1_out  ; 
       4'd2  :  rs = r2_out  ; 
       4'd3  :  rs = r3_out  ; 
       4'd4  :  rs = r4_out  ; 
       4'd5  :  rs = r5_out  ; 
       4'd6  :  rs = r6_out  ; 
       4'd7  :  rs = r7_out  ; 
       4'd8  :  rs = r8_out  ; 
       4'd9  :  rs = r9_out  ; 
       4'd10 :  rs = r10_out ; 
       4'd11 :  rs = r11_out ; 
       4'd12 :  rs = r12_out ; 
       4'd13 :  rs = r13_out ; 
       4'd14 :  rs = r14_out ; 
       default: rs = pc ; 
    endcase

// ========================================================
// Adders
// ========================================================
assign pc_plus4      = pc        + 32'd4;
assign pc_minus4     = pc        - 32'd4;
assign address_plus4 = execute_address + 32'd4;
assign alu_plus4     = alu_out   + 32'd4;
assign rn_plus4      = rn        + 32'd4;
assign rn_minus4     = rn        - 32'd4;

// ========================================================
// Barrel Shift Amount Select
// ========================================================
// An immediate shift value of 0 is translated into 32
assign shift_amount = barrel_shift_amount_sel == 2'd0 ? 8'd0                           :
                      barrel_shift_amount_sel == 2'd1 ? rs[7:0]                        :
                      barrel_shift_amount_sel == 2'd2 ? {3'd0, imm_shift_amount    } :
                                                          {3'd0, execute_address[1:0], 3'b0 };

// ========================================================
// Barrel Shift Data Select
// ========================================================
assign barrel_shift_in = barrel_shift_data_sel == 2'd0 ? {24'b0,imm8}:
                         barrel_shift_data_sel == 2'd1 ? read_data   :
                         barrel_shift_data_sel == 2'd2 ? rm            :
                                                         {14'b0,pc_nxt,2'b0};
                            
// ========================================================
// Interrupt vector Select
// ========================================================

assign interrupt_vector = // Reset vector
                          (interrupt_vector_sel == 3'd0) ? 32'h00000000 :  
                          // Data abort interrupt vector                 
                          (interrupt_vector_sel == 3'd1) ? 32'h00000010 :
                          // Fast interrupt vector  
                          (interrupt_vector_sel == 3'd2) ? 32'h0000001c :
                          // Regular interrupt vector
                          (interrupt_vector_sel == 3'd3) ? 32'h00000018 :
                          // Prefetch abort interrupt vector
                          (interrupt_vector_sel == 3'd5) ? 32'h0000000c :
                          // Undefined instruction interrupt vector
                          (interrupt_vector_sel == 3'd6) ? 32'h00000004 :
                          // Software (SWI) interrupt vector
                          (interrupt_vector_sel == 3'd7) ? 32'h00000008 :
                          // Default is the address exception interrupt
                                                             32'h00000014;


// ========================================================
// Address Select
// ========================================================

// If rd is the pc, then separate the address bits from the status bits for
// generating the next address to fetch
assign alu_out_pc_filtered = pc_sel == 3'd1 ? {6'd0, alu_out[25:2], 2'd0} : alu_out;

assign execute_address_nxt = (address_sel == 3'd0) ? alu_out_pc_filtered   :
                       (address_sel == 3'd1) ? rn                    :
                       (address_sel == 3'd2) ? rn_plus4              :
                       (address_sel == 3'd3) ? rn_minus4             :
                                                 32'hDEAD_BEEF         ;

// ========================================================
// Program Counter Select
// ========================================================

assign r15_out = pc;
   
always @(alu_flag_zero or alu_flag_neg or alu_flag_cout or a23_ccode or pc_plus4) 
  case (a23_ccode)
      Eunsigned_Eeq : compare_a23 = alu_flag_zero;
      Esigned_Eeq : compare_a23 = alu_flag_zero;
      Eunsigned_Ene : compare_a23 = !alu_flag_zero;
      Esigned_Ene : compare_a23 = !alu_flag_zero;
      Esigned_Ele : compare_a23 = alu_flag_neg | alu_flag_zero;
      Esigned_Ege : compare_a23 = alu_flag_zero | !alu_flag_neg;
      Esigned_Elt : compare_a23 = alu_flag_neg;
      Esigned_Egt : compare_a23 = !(alu_flag_neg | alu_flag_zero);
      Eunsigned_Ele : compare_a23 = alu_flag_zero | !alu_flag_cout;
      Eunsigned_Ege : compare_a23 = alu_flag_cout;
      Eunsigned_Elt : compare_a23 = !alu_flag_cout;
      Eunsigned_Egt : compare_a23 = alu_flag_cout & !alu_flag_zero;
      default: compare_a23 = pc_plus4;
  endcase
   
assign pc_nxt_out = !system_rdy    ? 32'b0                 :
                pc_sel == 3'd0 ? pc_plus4              :
                pc_sel == 3'd1 ? alu_out               :
                pc_sel == 3'd2 ? interrupt_vector      :
                pc_sel == 3'd3 ? {14'b0,pc_nxt,2'b0}  :
                pc_sel == 3'd4 ? r14_out               :
                pc_sel == 3'd5 ? (compare_a23 ? {14'b0,pc_nxt,2'b0} : pc_plus4) :
                pc_sel == 3'd6 ? rm                    :
		                   32'hDEAD_BEEF         ;

// ========================================================
// Register Write Select
// ========================================================

assign reg_write_nxt = reg_write_sel == 2'd0 ? alu_out               :
                       reg_write_sel == 2'd1 ? multiply_out          :
                       reg_write_sel == 2'd2 ? copro_read_data     :  // mrc
                       reg_write_sel == 2'd3 ? {31'b0,compare_a23}   : 0; // Boolean comparison

// ========================================================
// Byte Enable Select
// ========================================================
assign execute_byte_enable_nxt = byte_enable_sel == 2'd0  ? 4'b1111 :  // word write
                           byte_enable_sel == 2'd2  ?            // halfword write
                         ( execute_address_nxt[1] == 1'd0 ? 4'b0011 : 
                                                      4'b1100  ) :
                           
                         execute_address_nxt[1:0] == 2'd0 ? 4'b0001 :  // byte write
                         execute_address_nxt[1:0] == 2'd1 ? 4'b0010 :
                         execute_address_nxt[1:0] == 2'd2 ? 4'b0100 :
                                                      4'b1000;


// ========================================================
// Write Data Select
// ========================================================
   
assign execute_write_data_nxt = byte_enable_sel == 2'd0 ? rd : {rd[ 7:0],rd[ 7:0],rd[ 7:0],rd[ 7:0]};

// ========================================================
// Register Update
// ========================================================

assign write_enable_update             = !fetch_stall;
assign write_data_update               = !fetch_stall && write_data_wen;
assign address_update                  = !fetch_stall;
assign byte_enable_update              = !fetch_stall && write_data_wen;
assign copro_write_data_update         = !fetch_stall && copro_write_data_wen;

always @( posedge a23_clk )
if (!system_rdy)
	begin
	execute_copro_write_data <= 'd0;
	execute_write_data <= 'd0;
	execute_address <= 32'hdead_dead;
	execute_address_valid <= 'd0;  // Prevents the reset address value being a
				// wishbone access
				// cache rams address ports
	execute_write_enable <= 'd0;
	execute_byte_enable <= 'd0;
	end
else
    begin                                                                                                             
    execute_write_enable          <= write_enable_update            ? write_data_wen             : execute_write_enable;
    execute_write_data            <= write_data_update              ? execute_write_data_nxt             : execute_write_data; 
    execute_address               <= address_update                 ? execute_address_nxt                : execute_address;    
    execute_address_valid         <= address_update                 ? 1'd1                         : execute_address_valid;
    execute_byte_enable           <= byte_enable_update             ? execute_byte_enable_nxt            : execute_byte_enable;
    execute_copro_write_data      <= copro_write_data_update        ? execute_write_data_nxt             : execute_copro_write_data; 
    end


// ========================================================
// Instantiate Barrel Shift
// ========================================================

wire [32:0] lsl_out = barrel_shift_in << shift_amount;
wire [32:0] lsr_out = barrel_shift_in >> shift_amount;
wire [32:0] asr_out = {{32{barrel_shift_in[31]}},barrel_shift_in} >>
	    (shift_amount[7:5] ? 31 : shift_amount[4:0]);

assign barrel_shift_out =
			 barrel_shift_function == LSL ? lsl_out :
			 barrel_shift_function == LSR ? lsr_out :
			 barrel_shift_function == ASR ? asr_out :
                         32'hDEAD_BEEF ;

// ========================================================
// Instantiate ALU
// ========================================================
wire     [31:0]         a     = (alu_function[6] ) ? barrel_shift_out[31:0] : rn ;
wire     [31:0]         b     = (alu_function[6] ) ? rn : barrel_shift_out[31:0] ;
wire     [31:0]         b_not = (alu_function[5] ) ? 32'hFFFFFFFF ^ b : b ;
wire     [32:0] 	fadder_out       = { 1'd0,a} + {1'd0,b_not} + {32'd0,alu_function[4]};

wire     [31:0]          and_out          = a & b_not;
wire     [31:0]          or_out           = a | b_not;
wire     [31:0]          xor_out          = a ^ b_not;
wire     [31:0]          zero_ex8_out     = {24'd0,  b_not[7:0]};
wire     [31:0]          zero_ex_16_out   = {16'd0,  b_not[15:0]};
wire     [31:0]          sign_ex8_out     = {{24{b_not[7]}}, b_not[7:0]};
wire     [31:0]          sign_ex_16_out   = {{16{b_not[15]}}, b_not[15:0]};
                          
assign alu_out = alu_function[3:0] == 4'd0 ? b_not            : 
               alu_function[3:0] == 4'd1 ? fadder_out[31:0] : 
               alu_function[3:0] == 4'd2 ? zero_ex_16_out   :
               alu_function[3:0] == 4'd3 ? zero_ex8_out     :
               alu_function[3:0] == 4'd4 ? sign_ex_16_out   :
               alu_function[3:0] == 4'd5 ? sign_ex8_out     :
               alu_function[3:0] == 4'd6 ? xor_out          :
               alu_function[3:0] == 4'd7 ? or_out           :
                                         and_out          ;
   
   assign alu_flag_neg = alu_out[31];
   assign alu_flag_zero = |alu_out == 1'd0;
   assign alu_flag_cout = fadder_out[32];
   assign alu_flag_ov = alu_function[3:0] == 4'd1 &&
                          ((!a[31] && !b_not[31] && fadder_out[31]) ||
                           (a[31] && b_not[31] && !fadder_out[31]));

// Parallel multiply based on DSP48E

   wire [35:0] absa = a[31] ? 36'b0-rn : rn;
   wire [35:0] absb = b[31] ? 36'b0-rm : rm;

`ifdef state_mem
   wire [31:0] 	 absmult = absa * absb;
`else
   wire [35:0] 	 p0, p1, p2;

   wire [17:0] pada0 = {1'b0,absa[16:0]};
   wire [17:0] padb0 = {1'b0,absb[16:0]};
   wire [17:0] pada1 = {1'b0,absa[33:17]};
   wire [17:0] padb1 = {1'b0,absb[33:17]};

MUL18 mult0 (
  .P(p0),
  .A(pada0),
  .B(padb0)
    );
   
MUL18 mult1 (
  .P(p1),
  .A(pada0),
  .B(padb1)
    );
   
MUL18 mult2 (
  .P(p2),
  .A(pada1),
  .B(padb0)
    );
  
   wire [31:0] 	 absmult = {p0[35:17]+p1+p2,p0[16:0]};
`endif

   assign multiply_out = a[31]^b[31] ? 32'b0-absmult : absmult;

// ========================================================
// Register Update
// ========================================================
always @ ( posedge a23_clk )
if (!system_rdy)
  begin
     r0_out = 32'hdead_beef;     
     r1_out = 32'hdead_beef;
     r2_out = 32'hdead_beef;     
     r3_out = 32'hdead_beef;     
     r4_out = 32'hdead_beef;     
     r5_out = 32'hdead_beef;     
     r6_out = 32'hdead_beef;     
     r7_out = 32'hdead_beef;     
     r8_out = 32'hdead_beef;     
     r9_out = 32'hdead_beef;     
     r10_out = 32'hdead_beef;     
     r11_out = 32'hdead_beef;     
     r12_out = 32'hdead_beef;     
     r13_out = 32'hdead_beef;
     r14_out = 32'hdead_beef;
     r15 <= 24'h0;
  end
else
    if (!fetch_stall)
      begin
	 if (oreg_sel[4] == 1'b1) case(oreg_sel[3:0])	 
       4'd0  :  r0_out  = reg_write_nxt;
       4'd1  :  r1_out  = reg_write_nxt; 
       4'd2  :  r2_out  = reg_write_nxt; 
       4'd3  :  r3_out  = reg_write_nxt; 
       4'd4  :  r4_out  = reg_write_nxt; 
       4'd5  :  r5_out  = reg_write_nxt; 
       4'd6  :  r6_out  = reg_write_nxt; 
       4'd7  :  r7_out  = reg_write_nxt; 
       4'd8  :  r8_out  = reg_write_nxt; 
       4'd9  :  r9_out  = reg_write_nxt; 
       4'd10 :  r10_out = reg_write_nxt; 
       4'd11 :  r11_out = reg_write_nxt; 
       4'd12 :  r12_out = reg_write_nxt; 
       4'd13 :  r13_out = reg_write_nxt; 
       4'd14 :  r14_out = reg_write_nxt; 
      endcase

        r15      <= pc_nxt_out[25:2];
        end
    
assign readstrobe = 0;
assign write_data_wen_out = write_data_wen;
assign read_enable_out = read_enable;


assign
	{
	oreg_sel,
	copro_write_data_wen,
	write_data_wen,
	reg_write_sel,
	byte_enable_sel,
	pc_nxt,
	pc_sel,
	address_sel,
	alu_function,
	barrel_shift_function,
	barrel_shift_data_sel,
	barrel_shift_amount_sel,
	rn_sel,
	rds_sel,
	rm_sel,
	imm_shift_amount,
	imm8,
	read_enable,
	a23_ccode} = xstate;

endmodule
