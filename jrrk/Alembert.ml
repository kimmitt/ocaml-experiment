open Datatypes
open PartSum
open Raxioms
open Rbasic_fun
open Rdefinitions
open Rpow_def
open Specif

let __ = let rec f _ = Obj.repr f in Obj.repr f

(** val coq_Alembert_C1 : (nat -> coq_R) -> coq_R **)

let coq_Alembert_C1 an =
  completeness __

(** val coq_Alembert_C2 : (nat -> coq_R) -> coq_R **)

let coq_Alembert_C2 an =
  let vn = fun i ->
    coq_Rdiv
      (coq_Rplus (coq_Rmult (coq_Rplus coq_R1 coq_R1) (coq_Rabs (an i)))
        (an i)) (coq_Rplus coq_R1 coq_R1)
  in
  let wn = fun i ->
    coq_Rdiv
      (coq_Rminus (coq_Rmult (coq_Rplus coq_R1 coq_R1) (coq_Rabs (an i)))
        (an i)) (coq_Rplus coq_R1 coq_R1)
  in
  let h5 = coq_Alembert_C1 vn in
  let h6 = coq_Alembert_C1 wn in coq_Rminus h5 h6

(** val coq_AlembertC3_step1 : (nat -> coq_R) -> coq_R -> coq_R **)

let coq_AlembertC3_step1 an x =
  let bn = fun i -> coq_Rmult (an i) (pow x i) in coq_Alembert_C2 bn

(** val coq_AlembertC3_step2 : (nat -> coq_R) -> coq_R -> coq_R **)

let coq_AlembertC3_step2 an x =
  an O

(** val coq_Alembert_C3 : (nat -> coq_R) -> coq_R -> coq_R **)

let coq_Alembert_C3 an x =
  match total_order_T x coq_R0 with
  | Coq_inleft s ->
    if s then coq_AlembertC3_step1 an x else coq_AlembertC3_step2 an x
  | Coq_inright -> coq_AlembertC3_step1 an x

(** val coq_Alembert_C4 : (nat -> coq_R) -> coq_R -> coq_R **)

let coq_Alembert_C4 an k =
  completeness __

(** val coq_Alembert_C5 : (nat -> coq_R) -> coq_R -> coq_R **)

let coq_Alembert_C5 an k =
  cv_cauchy_2 an

(** val coq_Alembert_C6 : (nat -> coq_R) -> coq_R -> coq_R -> coq_R **)

let coq_Alembert_C6 an x k =
  match total_order_T x coq_R0 with
  | Coq_inleft s ->
    if s
    then coq_Alembert_C5 (fun i -> coq_Rmult (an i) (pow x i))
           (coq_Rmult k (coq_Rabs x))
    else an O
  | Coq_inright ->
    coq_Alembert_C5 (fun i -> coq_Rmult (an i) (pow x i))
      (coq_Rmult k (coq_Rabs x))

