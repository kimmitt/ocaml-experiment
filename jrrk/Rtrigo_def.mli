open Alembert
open Datatypes
open Factorial
open Peano
open RIneq
open Raxioms
open Rdefinitions
open Rpow_def

val exist_exp : coq_R -> coq_R

val exp : coq_R -> coq_R

val exist_exp0 : coq_R

val cosh : coq_R -> coq_R

val sinh : coq_R -> coq_R

val tanh : coq_R -> coq_R

val cos_n : nat -> coq_R

val exist_cos : coq_R -> coq_R

val cos : coq_R -> coq_R

val sin_n : nat -> coq_R

val exist_sin : coq_R -> coq_R

val sin : coq_R -> coq_R

val exist_cos0 : coq_R

