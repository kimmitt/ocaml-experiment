open Datatypes
open Peano
open Raxioms
open Rdefinitions
open Specif

let __ = let rec f _ = Obj.repr f in Obj.repr f

(** val opp_seq : (nat -> coq_R) -> nat -> coq_R **)

let opp_seq un n =
  coq_Ropp (un n)

(** val growing_cv : (nat -> coq_R) -> coq_R **)

let growing_cv un =
  projT1 (sigT_of_sig (completeness __))

(** val decreasing_cv : (nat -> coq_R) -> coq_R **)

let decreasing_cv un =
  let x = growing_cv (opp_seq un) in coq_Ropp x

(** val ub_to_lub : (nat -> coq_R) -> coq_R **)

let ub_to_lub un =
  completeness __

(** val lb_to_glb : (nat -> coq_R) -> coq_R **)

let lb_to_glb un =
  completeness __

(** val lub : (nat -> coq_R) -> coq_R **)

let lub un =
  ub_to_lub un

(** val glb : (nat -> coq_R) -> coq_R **)

let glb un =
  coq_Ropp (lb_to_glb un)

(** val sequence_ub : (nat -> coq_R) -> nat -> coq_R **)

let sequence_ub un i =
  lub (fun k -> un (plus i k))

(** val sequence_lb : (nat -> coq_R) -> nat -> coq_R **)

let sequence_lb un i =
  glb (fun k -> un (plus i k))

(** val maj_cv : (nat -> coq_R) -> coq_R **)

let maj_cv un =
  decreasing_cv (sequence_ub un)

(** val min_cv : (nat -> coq_R) -> coq_R **)

let min_cv un =
  growing_cv (sequence_lb un)

