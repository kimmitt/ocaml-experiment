open Mylib
open Ml_decls
open Parseml

module ParseSample = LRTTdebug.Parser(BackusParse)

let _ = let argv = Sys.argv in match Array.length argv with
  | 1 -> ()
  | _ -> let concat = String.concat " " (List.tl (Array.to_list argv)) in
         print_endline concat;
         let codes = init_codes() in
         let p = demo codes.impl ParseSample.streamparse concat in
	 let d = Dump.dump4exp codes.impl p in
	 print_endline d
