open Datatypes
open EqNat

type __ = Obj.t

(** val ble_nat : nat -> nat -> bool **)

let rec ble_nat n m =
  match n with
  | O -> true
  | S n' ->
    (match m with
     | O -> false
     | S m' -> ble_nat n' m')

type 'x relation = __

(** val next_nat_rect : nat -> 'a1 -> nat -> 'a1 **)

let next_nat_rect n f n0 =
  f

(** val next_nat_rec : nat -> 'a1 -> nat -> 'a1 **)

let next_nat_rec n f n0 =
  f

(** val empty_relation_rect : nat -> nat -> 'a1 **)

let empty_relation_rect n n0 =
  assert false (* absurd case *)

(** val empty_relation_rec : nat -> nat -> 'a1 **)

let empty_relation_rec n n0 =
  assert false (* absurd case *)

type id =
  nat
  (* singleton inductive, whose constructor was Id *)

(** val id_rect : (nat -> 'a1) -> id -> 'a1 **)

let id_rect f i =
  f i

(** val id_rec : (nat -> 'a1) -> id -> 'a1 **)

let id_rec f i =
  f i

(** val beq_id : id -> id -> bool **)

let beq_id id1 id2 =
  beq_nat id1 id2

type 'a partial_map = id -> 'a option

(** val empty : 'a1 partial_map **)

let empty x =
  None

(** val extend : 'a1 partial_map -> id -> 'a1 -> id -> 'a1 option **)

let extend gamma x t x' =
  if beq_id x x' then Some t else gamma x'

