open Mylib

let oot = 113
let div x y = x / y
let div113 x = x / oot
let pi = div 355000000 oot
let pi' = div113 355000000
let rec print_hex n = if n > 15 then print_hex (n lsr 4); print_char ("0123456789ABCDEF".[n land 15])

let _ = print_hex pi; print_char '\n'; print_int pi; print_char '\n'
let _ = print_hex pi'; print_char '\n'; print_int pi'; print_char '\n'
let rec format_int_d ostr ptr n len =
    if n > 9 && len > 0 then
      format_int_d ostr ptr (n/10) (len-1);
    let ch = Char.unsafe_chr (n mod 10 + Char.code '0') in
    ostr.[!ptr] <- ch;
    incr ptr
let ostr = String.create 20
let len = String.length ostr
let _ = (print_int len; print_char '\n')
let ptr = ref 0
let _ = format_int_d ostr ptr pi len
let _ = (print_int !ptr; print_char '\n')
let str = String.sub ostr 0 !ptr
let len = String.length str
let _ = (print_int len; print_char '\n')
let _ = print_endline str
