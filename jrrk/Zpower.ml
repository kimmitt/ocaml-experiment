open BinInt
open BinNums
open BinPos
open Datatypes
open Peano

type __ = Obj.t
let __ = let rec f _ = Obj.repr f in Obj.repr f

(** val coq_Zpower_nat : coq_Z -> nat -> coq_Z **)

let coq_Zpower_nat z n =
  nat_iter n (Z.mul z) (Zpos Coq_xH)

(** val shift_nat : nat -> positive -> positive **)

let shift_nat n z =
  nat_iter n (fun x -> Coq_xO x) z

(** val shift_pos : positive -> positive -> positive **)

let shift_pos n z =
  Pos.iter n (fun x -> Coq_xO x) z

(** val shift : coq_Z -> positive -> positive **)

let shift n z =
  match n with
  | Zpos p -> Pos.iter p (fun x -> Coq_xO x) z
  | _ -> z

(** val two_power_nat : nat -> coq_Z **)

let two_power_nat n =
  Zpos (shift_nat n Coq_xH)

(** val two_power_pos : positive -> coq_Z **)

let two_power_pos x =
  Zpos (shift_pos x Coq_xH)

(** val two_p : coq_Z -> coq_Z **)

let two_p = function
| Z0 -> Zpos Coq_xH
| Zpos y -> two_power_pos y
| Zneg y -> Z0

(** val coq_Zdiv_rest_aux :
    ((coq_Z, coq_Z) prod, coq_Z) prod -> ((coq_Z, coq_Z) prod, coq_Z) prod **)

let coq_Zdiv_rest_aux = function
| Coq_pair (p, d) ->
  let Coq_pair (q, r) = p in
  Coq_pair
  ((match q with
    | Z0 -> Coq_pair (Z0, r)
    | Zpos p0 ->
      (match p0 with
       | Coq_xI n -> Coq_pair ((Zpos n), (Z.add d r))
       | Coq_xO n -> Coq_pair ((Zpos n), r)
       | Coq_xH -> Coq_pair (Z0, (Z.add d r)))
    | Zneg p0 ->
      (match p0 with
       | Coq_xI n -> Coq_pair ((Z.sub (Zneg n) (Zpos Coq_xH)), (Z.add d r))
       | Coq_xO n -> Coq_pair ((Zneg n), r)
       | Coq_xH -> Coq_pair ((Zneg Coq_xH), (Z.add d r)))),
  (Z.mul (Zpos (Coq_xO Coq_xH)) d))

(** val coq_Zdiv_rest : coq_Z -> positive -> (coq_Z, coq_Z) prod **)

let coq_Zdiv_rest x p =
  let Coq_pair (qr, d) =
    Pos.iter p coq_Zdiv_rest_aux (Coq_pair ((Coq_pair (x, Z0)), (Zpos
      Coq_xH)))
  in
  qr

type coq_Zdiv_rest_proofs =
| Zdiv_rest_proof of coq_Z * coq_Z

(** val coq_Zdiv_rest_proofs_rect :
    coq_Z -> positive -> (coq_Z -> coq_Z -> __ -> __ -> __ -> 'a1) ->
    coq_Zdiv_rest_proofs -> 'a1 **)

let coq_Zdiv_rest_proofs_rect x p f = function
| Zdiv_rest_proof (x0, x1) -> f x0 x1 __ __ __

(** val coq_Zdiv_rest_proofs_rec :
    coq_Z -> positive -> (coq_Z -> coq_Z -> __ -> __ -> __ -> 'a1) ->
    coq_Zdiv_rest_proofs -> 'a1 **)

let coq_Zdiv_rest_proofs_rec x p f = function
| Zdiv_rest_proof (x0, x1) -> f x0 x1 __ __ __

(** val coq_Zdiv_rest_correct : coq_Z -> positive -> coq_Zdiv_rest_proofs **)

let coq_Zdiv_rest_correct x p =
  let p0 =
    Pos.iter p coq_Zdiv_rest_aux (Coq_pair ((Coq_pair (x, Z0)), (Zpos
      Coq_xH)))
  in
  let Coq_pair (p1, x0) = p0 in
  let Coq_pair (q, r) = p1 in Zdiv_rest_proof (q, r)

