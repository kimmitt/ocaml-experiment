open BinInt
open BinNums

type __ = Obj.t
let __ = let rec f _ = Obj.repr f in Obj.repr f

(** val eq_dep_elim : 'a1 -> 'a2 -> 'a1 -> 'a2 **)

let eq_dep_elim x f y =
  f

(** val coq_Zeven : coq_Z -> bool **)

let coq_Zeven = function
| Z0 -> true
| Zpos p ->
  (match p with
   | Coq_xO p0 -> true
   | _ -> false)
| Zneg p ->
  (match p with
   | Coq_xO p0 -> true
   | _ -> false)

type radix =
  coq_Z
  (* singleton inductive, whose constructor was Build_radix *)

(** val radix_rect : (coq_Z -> __ -> 'a1) -> radix -> 'a1 **)

let radix_rect f r =
  f r __

(** val radix_rec : (coq_Z -> __ -> 'a1) -> radix -> 'a1 **)

let radix_rec f r =
  f r __

(** val radix_val : radix -> coq_Z **)

let radix_val r =
  r

(** val cond_Zopp : bool -> coq_Z -> coq_Z **)

let cond_Zopp b m =
  if b then Z.opp m else m

