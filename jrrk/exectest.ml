open Mylib
open Dump
open Ml_decls
open To_from_Z
open String_of_float
open BinNums
open Fappli_IEEE
open MoreZStlc
open STLCExtended

let args str =
  let enc = encstr str in
  print_endline (fromZ enc);
  let dec = decstr enc in
  print_endline dec

(*
let fn = LET ([Declare (Var "f", Var "x",
    If (Eq (Var "x", Int 0), Int 1,
     Mul (Var "x", App (Var "f", [Sub (Var "x", Int 1)]))))],
 App (Var "f", [Int 6]))
*)

let rec dumpP = function
  | Coq_xH -> "xH"
  | Coq_xO num -> dumpP num^"_xO"
  | Coq_xI num -> dumpP num^"_xI"

let dumpZ = function
  | Z0 -> "zero"
  | Zpos num -> dumpP num
  | Zneg num -> "neg("^dumpP num^")"

let extb754 = function
  | B754_finite (a, b, c) -> string_of_bool a^" : "^dumpP b^" : "^dumpZ c
  | B754_zero arg1 -> "B754_zero "^string_of_bool arg1
  | B754_infinity arg1 -> "B754_infinity "^string_of_bool arg1
  | B754_nan (arg1, arg2) -> "B754_nan("^string_of_bool arg1^", "^dumpP arg2^")"

let debug_float f = string_of_float_b754 false f^" = "^extb754 f

let num = float_of_string_b754 false "1.0"
let _ = print_endline (debug_float num)
let expnum = Fappli_IEEE.B754_finite (false, asPi 8388608, asZi (-23))
let _ = print_endline (debug_float expnum)
let _ = print_endline ("num: "^string_of_bool (String.eqb (extb754 num) (extb754 expnum)))
let denom = float_of_string_b754 false "0.81"
let _ = print_endline (debug_float denom)
let expdenom = Fappli_IEEE.B754_finite (false, asPi 13589544, asZi (-24))
let _ = print_endline (debug_float expdenom)
let _ = print_endline ("denom: "^string_of_bool (String.eqb (extb754 denom) (extb754 expdenom)))

let expdiv = Fappli_IEEE.B754_finite (false, asPi 10356306, asZi (-23))
let _ = print_endline (debug_float expdiv)
let dividend = Fappli_IEEE_bits.b32_div Fappli_IEEE.Coq_mode_ZR num denom
let _ = print_endline (debug_float dividend)
let _ = print_endline ("dividend: "^string_of_bool (String.eqb (extb754 dividend) (extb754 expdiv)))
let dividend2 = Fappli_IEEE_bits.b32_div Fappli_IEEE.Coq_mode_ZR expnum expdenom
let _ = print_endline (debug_float dividend2)
let _ = print_endline ("dividend2: "^string_of_bool (String.eqb (extb754 dividend2) (extb754 expdiv)))

let fn = FDiv (Float (num), Float (denom))
let fn = LET
 ([Declare (true, Var "print_int", List [Var "x"],
    LET
     ([Declare (false, Var "_", List [],
        If (GT (Var "x", Int 9),
         App (Var "print_int", [Div (Var "x", Int 10)]), Unit))],
     App (Var "print_char",
      [App (Var "char_of_int", [Add (Mod (Var "x", Int 10), Int 48)])])));
   Declare (false, Var "_", List [], App (Var "print_int", [Int 12345]))],
 App (Var "print_char", [App (Var "char_of_int", [Int 10])]))

let exectest () =
  let (executable_name, argv) = Sys.get_argv() in
  List.iter args (List.tl (Array.to_list argv));
  print_endline ("p4exp: ");
  try
  let codes = init_codes() in
  codes.impl.string_of_float<-string_of_float_b754 false;
  codes.impl.float_of_string<-float_of_string_b754 false;
  print_endline (Dump.dump4exp codes.impl fn);
  let rw = Ml_compiling.rewrite codes fn in
  print_endline "entry";
  print_endline (dumpentry' codes rw);
  print_endline "side effects";
  let x = Exec.exec_till (codes, rw) in
  print_endline "execution result";
  print_endline (dumpentry' codes x);
  (match x with Coq_tm_flt f -> print_endline (debug_float f) | _ -> ());
  print_endline "codes";
  List.iter (fun (k,x) -> print_endline(k^" -> "^string_of_int(fromNat x))) !(codes.code);
  with _ -> print_endline "raised exception"

let _ = exectest()
