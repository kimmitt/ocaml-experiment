open BinNums
open Datatypes
open Fappli_IEEE
open Fappli_IEEE_bits
open SfLib
open Zops

module STLCExtended : 
 sig 
  type ty =
  | Coq_ty_arrow of ty * ty
  | Coq_ty_prod of ty * ty
  | Coq_ty_sum of ty * ty
  | Coq_ty_List of ty
  | Coq_ty_Nat
  | Coq_ty_Flt
  
  val ty_rect :
    (ty -> 'a1 -> ty -> 'a1 -> 'a1) -> (ty -> 'a1 -> ty -> 'a1 -> 'a1) -> (ty
    -> 'a1 -> ty -> 'a1 -> 'a1) -> (ty -> 'a1 -> 'a1) -> 'a1 -> 'a1 -> ty ->
    'a1
  
  val ty_rec :
    (ty -> 'a1 -> ty -> 'a1 -> 'a1) -> (ty -> 'a1 -> ty -> 'a1 -> 'a1) -> (ty
    -> 'a1 -> ty -> 'a1 -> 'a1) -> (ty -> 'a1 -> 'a1) -> 'a1 -> 'a1 -> ty ->
    'a1
  
  type tm =
  | Coq_tm_var of id
  | Coq_tm_app of tm * tm
  | Coq_tm_abs of id * ty * tm
  | Coq_tm_pair of tm * tm
  | Coq_tm_fst of tm
  | Coq_tm_snd of tm
  | Coq_tm_inl of ty * tm
  | Coq_tm_inr of ty * tm
  | Coq_tm_case of tm * id * tm * id * tm
  | Coq_tm_nil of ty
  | Coq_tm_cons of tm * tm
  | Coq_tm_lcase of tm * tm * id * id * tm
  | Coq_tm_nat of coq_Z
  | Coq_tm_flt of binary64
  | Coq_tm_succ of tm
  | Coq_tm_pred of tm
  | Coq_tm_add of tm * tm
  | Coq_tm_sub of tm * tm
  | Coq_tm_mult of tm * tm
  | Coq_tm_div of tm * tm
  | Coq_tm_mod of tm * tm
  | Coq_tm_addf of tm * tm
  | Coq_tm_subf of tm * tm
  | Coq_tm_mulf of tm * tm
  | Coq_tm_divf of tm * tm
  | Coq_tm_if0 of tm * tm * tm
  | Coq_tm_iflt of tm * tm * tm
  | Coq_tm_let of id * tm * tm
  | Coq_tm_fix of tm
  | Coq_tm_io of nat * tm
  
  val tm_rect :
    (id -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (id -> ty -> tm -> 'a1
    -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> 'a1) -> (tm
    -> 'a1 -> 'a1) -> (ty -> tm -> 'a1 -> 'a1) -> (ty -> tm -> 'a1 -> 'a1) ->
    (tm -> 'a1 -> id -> tm -> 'a1 -> id -> tm -> 'a1 -> 'a1) -> (ty -> 'a1)
    -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> id -> id
    -> tm -> 'a1 -> 'a1) -> (coq_Z -> 'a1) -> (binary64 -> 'a1) -> (tm -> 'a1
    -> 'a1) -> (tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm
    -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm ->
    'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1
    -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 ->
    tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm
    -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> tm -> 'a1 ->
    'a1) -> (id -> tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> 'a1) ->
    (nat -> tm -> 'a1 -> 'a1) -> tm -> 'a1
  
  val tm_rec :
    (id -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (id -> ty -> tm -> 'a1
    -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> 'a1) -> (tm
    -> 'a1 -> 'a1) -> (ty -> tm -> 'a1 -> 'a1) -> (ty -> tm -> 'a1 -> 'a1) ->
    (tm -> 'a1 -> id -> tm -> 'a1 -> id -> tm -> 'a1 -> 'a1) -> (ty -> 'a1)
    -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> id -> id
    -> tm -> 'a1 -> 'a1) -> (coq_Z -> 'a1) -> (binary64 -> 'a1) -> (tm -> 'a1
    -> 'a1) -> (tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm
    -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm ->
    'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1
    -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 ->
    tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm
    -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> tm -> 'a1 ->
    'a1) -> (id -> tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> 'a1) ->
    (nat -> tm -> 'a1 -> 'a1) -> tm -> 'a1
  
  val subst : id -> tm -> tm -> tm
  
  type context = ty partial_map
  
  val redvalue : tm -> bool
  
  val axiom_io : nat -> coq_Z -> coq_Z
  
  val coq_Fplus : binary_float -> binary_float -> binary_float
  
  val coq_Fminus : binary_float -> binary_float -> binary_float
  
  val coq_Fmult : binary_float -> binary_float -> binary_float
  
  val coq_Fdiv : binary_float -> binary_float -> binary_float
  
  val coq_FplusZ : coq_Z -> coq_Z -> coq_Z
  
  val coq_FminusZ : coq_Z -> coq_Z -> coq_Z
  
  val coq_FmultZ : coq_Z -> coq_Z -> coq_Z
  
  val coq_FdivZ : coq_Z -> coq_Z -> coq_Z
  
  val reduce : tm -> tm
  
  module Examples : 
   sig 
    module NumTest : 
     sig 
      val test : tm
     end
    
    module ProdTest : 
     sig 
      val test : tm
     end
    
    module LetTest : 
     sig 
      val test : tm
     end
    
    module SumTest1 : 
     sig 
      val test : tm
     end
    
    module SumTest2 : 
     sig 
      val test : tm
     end
    
    module ListTest : 
     sig 
      val test : tm
     end
    
    module FixTest1 : 
     sig 
      val fact : tm
      
      val fact_calc : coq_Z -> tm
     end
    
    module FixTest2 : 
     sig 
      val map : tm
      
      val map_func : tm
      
      val map_arg : tm
      
      val map_rslt : tm
     end
    
    module FixTest3 : 
     sig 
      val equal : tm
      
      val equal1 : tm
      
      val equal2 : tm
     end
    
    module FixTest4 : 
     sig 
      val eotest : tm
      
      val eotest_rslt : tm
     end
   end
 end

