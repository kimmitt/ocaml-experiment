open Mylib

let run () =
(*
  try
*)
    print_endline "Testing get_config()";
    let (os_type, word_size, big_endian) = Sys.get_config() in
    print_endline (os_type^" "^string_of_int word_size^" "^string_of_bool big_endian)
(*
  with _ ->
    print_endline "Sorry, an exception occured"
*)

let _ = run()
