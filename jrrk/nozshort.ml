open Mylib
  
module Z = 
 struct 
  type t = int
  let pred = fun n -> (n-1)
  let mul = ( * )  
 end
 
type id =
  int

let beq_id id1 id2 =
  (fun x y -> x - y = 0) id1 id2

type ty =
| Coq_ty_arrow of ty * ty
| Coq_ty_Nat

type tm =
| Coq_tm_var of id
| Coq_tm_app of tm * tm
| Coq_tm_abs of id * ty * tm
| Coq_tm_nat of int
| Coq_tm_pred of tm
| Coq_tm_mult of tm * tm
| Coq_tm_if0 of tm * tm * tm
| Coq_tm_fix of tm

let rec subst x s t = match t with
| Coq_tm_var y -> if beq_id x y then s else t
| Coq_tm_app (t1, t2) -> Coq_tm_app ((subst x s t1), (subst x s t2))
| Coq_tm_abs (y, t0, t1) ->
  Coq_tm_abs (y, t0, (if beq_id x y then t1 else subst x s t1))
| Coq_tm_nat n -> Coq_tm_nat n
| Coq_tm_pred t0 -> Coq_tm_pred (subst x s t0)
| Coq_tm_mult (t1, t2) -> Coq_tm_mult ((subst x s t1), (subst x s t2))
| Coq_tm_if0 (t1, t2, t3) ->
  Coq_tm_if0 ((subst x s t1), (subst x s t2), (subst x s t3))
| Coq_tm_fix t0 -> Coq_tm_fix (subst x s t0)

let rec redvalue = function
| Coq_tm_abs (x, t11, t12) -> true
| Coq_tm_nat n -> true
| _ -> false

let rec reduce = function
| Coq_tm_app (a, b) ->
  (match a with
   | Coq_tm_abs (x, ty0, t12) ->
     if redvalue b
     then subst x b t12
     else Coq_tm_app ((reduce a), (reduce b))
   | _ -> Coq_tm_app ((reduce a), (reduce b)))
| Coq_tm_abs (a, b, c) -> Coq_tm_abs (a, b, (reduce c))
| Coq_tm_pred v ->
  (match v with
   | Coq_tm_nat n -> Coq_tm_nat (Z.pred n)
   | _ -> Coq_tm_pred (reduce v))
| Coq_tm_mult (a, b) ->
  (match a with
   | Coq_tm_nat n1 ->
     (match b with
      | Coq_tm_nat n2 -> Coq_tm_nat (Z.mul n1 n2)
      | _ -> Coq_tm_mult ((reduce a), (reduce b)))
   | _ -> Coq_tm_mult ((reduce a), (reduce b)))
| Coq_tm_if0 (a, b, c) ->
  (match a with
   | Coq_tm_nat z ->
     ((fun fO fpos fneg n -> if n=0 then fO () else if n > 0 then fpos n else fneg (-n))
        (fun _ ->
        b)
        (fun p ->
        c)
        (fun p ->
        c)
        z)
   | _ -> Coq_tm_if0 ((reduce a), b, c))
| Coq_tm_fix v ->
  (match v with
   | Coq_tm_abs (x, t1, t2) ->
     subst x (Coq_tm_fix (Coq_tm_abs (x, t1, t2))) t2
   | _ -> Coq_tm_fix (reduce v))
| x -> x

let fact =
  Coq_tm_fix (Coq_tm_abs ((succ 0), (Coq_ty_arrow (Coq_ty_Nat, Coq_ty_Nat)),
    (Coq_tm_abs (0, Coq_ty_Nat, (Coq_tm_if0 ((Coq_tm_var 0), (Coq_tm_nat (+
    1)), (Coq_tm_mult ((Coq_tm_var 0), (Coq_tm_app ((Coq_tm_var (succ 0)),
    (Coq_tm_pred (Coq_tm_var 0))))))))))))

let fact_calc n =
  Coq_tm_app (fact, (Coq_tm_nat n))

let rec reduce_n n t =
  (fun fO fS n -> if n=0 then fO () else fS (n-1))
    (fun _ ->
    t)
    (fun n0 ->
    reduce_n n0 (reduce t))
    n

