open BinNums

type coq_R = Axioms.coq_R

(** val coq_R0 : coq_R **)

let coq_R0 = Axioms.coq_R0

(** val coq_R1 : coq_R **)

let coq_R1 = Axioms.coq_R1

(** val coq_Rplus : coq_R -> coq_R -> coq_R **)

let coq_Rplus = Axioms.coq_Rplus

(** val coq_Rmult : coq_R -> coq_R -> coq_R **)

let coq_Rmult = Axioms.coq_Rmult

(** val coq_Ropp : coq_R -> coq_R **)

let coq_Ropp = Axioms.coq_Ropp

(** val coq_Rinv : coq_R -> coq_R **)

let coq_Rinv = Axioms.coq_Rinv

(** val up : coq_R -> coq_Z **)

let up = Axioms.up

(** val coq_Rminus : coq_R -> coq_R -> coq_R **)

let coq_Rminus r1 r2 =
  coq_Rplus r1 (coq_Ropp r2)

(** val coq_Rdiv : coq_R -> coq_R -> coq_R **)

let coq_Rdiv r1 r2 =
  coq_Rmult r1 (coq_Rinv r2)

