type nat =
| O
| S of nat

type positive =
| Coq_xI of positive
| Coq_xO of positive
| Coq_xH

type ('a, 'b) prod =
| Coq_pair of 'a * 'b

let rec nat_iter n f x =
  match n with
  | O -> x
  | S n' -> f (nat_iter n' f x)

let default_nan_pl64 =
  Coq_pair (false,
    (nat_iter (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S
      (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S
      (S (S (S (S (S (S O)))))))))))))))))))))))))))))))))))))))))))))))))))
      (fun x -> Coq_xO x) Coq_xH))
