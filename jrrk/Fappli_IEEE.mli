open BinInt
open BinNums
open BinPos
open Bool
open Datatypes
open Fcalc_bracket
open Fcalc_round
open Fcore_FLT
open Fcore_Raux
open Fcore_Zaux
open Fcore_defs
open Fcore_digits
open Fcore_generic_fmt
open PreOmega
open Rdefinitions
open Zbool
open Zpower

type __ = Obj.t

type full_float =
| F754_zero of bool
| F754_infinity of bool
| F754_nan of bool * positive
| F754_finite of bool * positive * coq_Z

val full_float_rect :
  (bool -> 'a1) -> (bool -> 'a1) -> (bool -> positive -> 'a1) -> (bool ->
  positive -> coq_Z -> 'a1) -> full_float -> 'a1

val full_float_rec :
  (bool -> 'a1) -> (bool -> 'a1) -> (bool -> positive -> 'a1) -> (bool ->
  positive -> coq_Z -> 'a1) -> full_float -> 'a1

val coq_FF2R : radix -> full_float -> coq_R

val canonic_mantissa : coq_Z -> coq_Z -> positive -> coq_Z -> bool

val bounded : coq_Z -> coq_Z -> positive -> coq_Z -> bool

val valid_binary : coq_Z -> coq_Z -> full_float -> bool

type nan_pl = positive

type binary_float =
| B754_zero of bool
| B754_infinity of bool
| B754_nan of bool * nan_pl
| B754_finite of bool * positive * coq_Z

val binary_float_rect :
  coq_Z -> coq_Z -> (bool -> 'a1) -> (bool -> 'a1) -> (bool -> nan_pl -> 'a1)
  -> (bool -> positive -> coq_Z -> __ -> 'a1) -> binary_float -> 'a1

val binary_float_rec :
  coq_Z -> coq_Z -> (bool -> 'a1) -> (bool -> 'a1) -> (bool -> nan_pl -> 'a1)
  -> (bool -> positive -> coq_Z -> __ -> 'a1) -> binary_float -> 'a1

val coq_FF2B : coq_Z -> coq_Z -> full_float -> binary_float

val coq_B2FF : coq_Z -> coq_Z -> binary_float -> full_float

val radix2 : radix

val coq_B2R : coq_Z -> coq_Z -> binary_float -> coq_R

val is_finite_strict : coq_Z -> coq_Z -> binary_float -> bool

val coq_Bsign : coq_Z -> coq_Z -> binary_float -> bool

val sign_FF : full_float -> bool

val is_finite : coq_Z -> coq_Z -> binary_float -> bool

val is_finite_FF : full_float -> bool

val is_nan : coq_Z -> coq_Z -> binary_float -> bool

val is_nan_FF : full_float -> bool

val coq_Bopp :
  coq_Z -> coq_Z -> (bool -> nan_pl -> (bool, nan_pl) prod) -> binary_float
  -> binary_float

type shr_record = { shr_m : coq_Z; shr_r : bool; shr_s : bool }

val shr_record_rect : (coq_Z -> bool -> bool -> 'a1) -> shr_record -> 'a1

val shr_record_rec : (coq_Z -> bool -> bool -> 'a1) -> shr_record -> 'a1

val shr_m : shr_record -> coq_Z

val shr_r : shr_record -> bool

val shr_s : shr_record -> bool

val shr_1 : shr_record -> shr_record

val loc_of_shr_record : shr_record -> location

val shr_record_of_loc : coq_Z -> location -> shr_record

val shr : shr_record -> coq_Z -> coq_Z -> (shr_record, coq_Z) prod

val coq_Zdigits2 : coq_Z -> coq_Z

val shr_fexp :
  coq_Z -> coq_Z -> coq_Z -> coq_Z -> location -> (shr_record, coq_Z) prod

type mode =
| Coq_mode_NE
| Coq_mode_ZR
| Coq_mode_DN
| Coq_mode_UP
| Coq_mode_NA

val mode_rect : 'a1 -> 'a1 -> 'a1 -> 'a1 -> 'a1 -> mode -> 'a1

val mode_rec : 'a1 -> 'a1 -> 'a1 -> 'a1 -> 'a1 -> mode -> 'a1

val round_mode : mode -> coq_R -> coq_Z

val choice_mode : mode -> bool -> coq_Z -> location -> coq_Z

val overflow_to_inf : mode -> bool -> bool

val binary_overflow : coq_Z -> coq_Z -> mode -> bool -> full_float

val binary_round_aux :
  coq_Z -> coq_Z -> mode -> bool -> positive -> coq_Z -> location ->
  full_float

val coq_Bmult :
  coq_Z -> coq_Z -> (binary_float -> binary_float -> (bool, nan_pl) prod) ->
  mode -> binary_float -> binary_float -> binary_float

val coq_Bmult_FF :
  coq_Z -> coq_Z -> (full_float -> full_float -> (bool, positive) prod) ->
  mode -> full_float -> full_float -> full_float

val shl_align : positive -> coq_Z -> coq_Z -> (positive, coq_Z) prod

val shl_align_fexp :
  coq_Z -> coq_Z -> positive -> coq_Z -> (positive, coq_Z) prod

val binary_round :
  coq_Z -> coq_Z -> mode -> bool -> positive -> coq_Z -> full_float

val binary_normalize :
  coq_Z -> coq_Z -> mode -> coq_Z -> coq_Z -> bool -> binary_float

val coq_Bplus :
  coq_Z -> coq_Z -> (binary_float -> binary_float -> (bool, nan_pl) prod) ->
  mode -> binary_float -> binary_float -> binary_float

val coq_Bminus :
  coq_Z -> coq_Z -> (binary_float -> binary_float -> (bool, nan_pl) prod) ->
  mode -> binary_float -> binary_float -> binary_float

val coq_Fdiv_core_binary :
  coq_Z -> coq_Z -> coq_Z -> coq_Z -> coq_Z -> ((coq_Z, coq_Z) prod,
  location) prod

val coq_Bdiv :
  coq_Z -> coq_Z -> (binary_float -> binary_float -> (bool, nan_pl) prod) ->
  mode -> binary_float -> binary_float -> binary_float

val coq_Fsqrt_core_binary :
  coq_Z -> coq_Z -> coq_Z -> ((coq_Z, coq_Z) prod, location) prod

val coq_Bsqrt :
  coq_Z -> coq_Z -> (binary_float -> (bool, nan_pl) prod) -> mode ->
  binary_float -> binary_float

