open BinInt
open BinNums
open Datatypes
open Fappli_IEEE
open Peano
open Zbool

(** val join_bits : coq_Z -> coq_Z -> bool -> coq_Z -> coq_Z -> coq_Z **)

let join_bits mw ew s m e =
  Z.add
    (Z.mul (Z.add (if s then Z.pow (Zpos (Coq_xO Coq_xH)) ew else Z0) e)
      (Z.pow (Zpos (Coq_xO Coq_xH)) mw)) m

(** val split_bits :
    coq_Z -> coq_Z -> coq_Z -> ((bool, coq_Z) prod, coq_Z) prod **)

let split_bits mw ew x =
  let mm = Z.pow (Zpos (Coq_xO Coq_xH)) mw in
  let em = Z.pow (Zpos (Coq_xO Coq_xH)) ew in
  Coq_pair ((Coq_pair ((Z.leb (Z.mul mm em) x), (Z.modulo x mm))),
  (Z.modulo (Z.div x mm) em))

(** val bits_of_binary_float : coq_Z -> coq_Z -> binary_float -> coq_Z **)

let bits_of_binary_float mw ew =
  let emax = Z.pow (Zpos (Coq_xO Coq_xH)) (Z.sub ew (Zpos Coq_xH)) in
  let prec = Z.add mw (Zpos Coq_xH) in
  let emin = Z.sub (Z.sub (Zpos (Coq_xI Coq_xH)) emax) prec in
  (fun x ->
  match x with
  | B754_zero sx -> join_bits mw ew sx Z0 Z0
  | B754_infinity sx ->
    join_bits mw ew sx Z0
      (Z.sub (Z.pow (Zpos (Coq_xO Coq_xH)) ew) (Zpos Coq_xH))
  | B754_nan (sx, n) ->
    join_bits mw ew sx (Zpos n)
      (Z.sub (Z.pow (Zpos (Coq_xO Coq_xH)) ew) (Zpos Coq_xH))
  | B754_finite (sx, mx, ex) ->
    if Z.leb (Z.pow (Zpos (Coq_xO Coq_xH)) mw) (Zpos mx)
    then join_bits mw ew sx
           (Z.sub (Zpos mx) (Z.pow (Zpos (Coq_xO Coq_xH)) mw))
           (Z.add (Z.sub ex emin) (Zpos Coq_xH))
    else join_bits mw ew sx (Zpos mx) Z0)

(** val split_bits_of_binary_float :
    coq_Z -> coq_Z -> binary_float -> ((bool, coq_Z) prod, coq_Z) prod **)

let split_bits_of_binary_float mw ew =
  let emax = Z.pow (Zpos (Coq_xO Coq_xH)) (Z.sub ew (Zpos Coq_xH)) in
  let prec = Z.add mw (Zpos Coq_xH) in
  let emin = Z.sub (Z.sub (Zpos (Coq_xI Coq_xH)) emax) prec in
  (fun x ->
  match x with
  | B754_zero sx -> Coq_pair ((Coq_pair (sx, Z0)), Z0)
  | B754_infinity sx ->
    Coq_pair ((Coq_pair (sx, Z0)),
      (Z.sub (Z.pow (Zpos (Coq_xO Coq_xH)) ew) (Zpos Coq_xH)))
  | B754_nan (sx, n) ->
    Coq_pair ((Coq_pair (sx, (Zpos n))),
      (Z.sub (Z.pow (Zpos (Coq_xO Coq_xH)) ew) (Zpos Coq_xH)))
  | B754_finite (sx, mx, ex) ->
    if Z.leb (Z.pow (Zpos (Coq_xO Coq_xH)) mw) (Zpos mx)
    then Coq_pair ((Coq_pair (sx,
           (Z.sub (Zpos mx) (Z.pow (Zpos (Coq_xO Coq_xH)) mw)))),
           (Z.add (Z.sub ex emin) (Zpos Coq_xH)))
    else Coq_pair ((Coq_pair (sx, (Zpos mx))), Z0))

(** val binary_float_of_bits_aux : coq_Z -> coq_Z -> coq_Z -> full_float **)

let binary_float_of_bits_aux mw ew =
  let emax = Z.pow (Zpos (Coq_xO Coq_xH)) (Z.sub ew (Zpos Coq_xH)) in
  let prec = Z.add mw (Zpos Coq_xH) in
  let emin = Z.sub (Z.sub (Zpos (Coq_xI Coq_xH)) emax) prec in
  (fun x ->
  let Coq_pair (p, ex) = split_bits mw ew x in
  let Coq_pair (sx, mx) = p in
  if coq_Zeq_bool ex Z0
  then (match mx with
        | Z0 -> F754_zero sx
        | Zpos px -> F754_finite (sx, px, emin)
        | Zneg p0 -> F754_nan (false, Coq_xH))
  else if coq_Zeq_bool ex
            (Z.sub (Z.pow (Zpos (Coq_xO Coq_xH)) ew) (Zpos Coq_xH))
       then (match mx with
             | Z0 -> F754_infinity sx
             | Zpos plx -> F754_nan (sx, plx)
             | Zneg p0 -> F754_nan (false, Coq_xH))
       else (match Z.add mx (Z.pow (Zpos (Coq_xO Coq_xH)) mw) with
             | Zpos px ->
               F754_finite (sx, px, (Z.sub (Z.add ex emin) (Zpos Coq_xH)))
             | _ -> F754_nan (false, Coq_xH)))

(** val binary_float_of_bits : coq_Z -> coq_Z -> coq_Z -> binary_float **)

let binary_float_of_bits mw ew x =
  let emax = Z.pow (Zpos (Coq_xO Coq_xH)) (Z.sub ew (Zpos Coq_xH)) in
  let prec = Z.add mw (Zpos Coq_xH) in
  coq_FF2B prec emax (binary_float_of_bits_aux mw ew x)

type binary32 = binary_float

(** val default_nan_pl32 : (bool, nan_pl) prod **)

let default_nan_pl32 =
  Coq_pair (false,
    (nat_iter (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S
      (S O)))))))))))))))))))))) (fun x -> Coq_xO x) Coq_xH))

(** val unop_nan_pl32 : binary32 -> (bool, nan_pl) prod **)

let unop_nan_pl32 = function
| B754_nan (s, pl) -> Coq_pair (s, pl)
| _ -> default_nan_pl32

(** val binop_nan_pl32 : binary32 -> binary32 -> (bool, nan_pl) prod **)

let binop_nan_pl32 f1 f2 =
  match f1 with
  | B754_nan (s1, pl1) -> Coq_pair (s1, pl1)
  | _ ->
    (match f2 with
     | B754_nan (s2, pl2) -> Coq_pair (s2, pl2)
     | _ -> default_nan_pl32)

(** val b32_opp : binary_float -> binary_float **)

let b32_opp =
  coq_Bopp (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xI Coq_xH))))) (Zpos (Coq_xO
    (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO Coq_xH))))))))
    (fun x x0 -> Coq_pair (x, x0))

(** val b32_plus : mode -> binary_float -> binary_float -> binary_float **)

let b32_plus =
  coq_Bplus (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xI Coq_xH))))) (Zpos (Coq_xO
    (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO Coq_xH))))))))
    binop_nan_pl32

(** val b32_minus : mode -> binary_float -> binary_float -> binary_float **)

let b32_minus =
  coq_Bminus (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xI Coq_xH))))) (Zpos (Coq_xO
    (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO Coq_xH))))))))
    binop_nan_pl32

(** val b32_mult : mode -> binary_float -> binary_float -> binary_float **)

let b32_mult =
  coq_Bmult (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xI Coq_xH))))) (Zpos (Coq_xO
    (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO Coq_xH))))))))
    binop_nan_pl32

(** val b32_div : mode -> binary_float -> binary_float -> binary_float **)

let b32_div =
  coq_Bdiv (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xI Coq_xH))))) (Zpos (Coq_xO
    (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO Coq_xH))))))))
    binop_nan_pl32

(** val b32_sqrt : mode -> binary_float -> binary_float **)

let b32_sqrt =
  coq_Bsqrt (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xI Coq_xH))))) (Zpos (Coq_xO
    (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO Coq_xH))))))))
    unop_nan_pl32

(** val b32_of_bits : coq_Z -> binary32 **)

let b32_of_bits =
  binary_float_of_bits (Zpos (Coq_xI (Coq_xI (Coq_xI (Coq_xO Coq_xH)))))
    (Zpos (Coq_xO (Coq_xO (Coq_xO Coq_xH))))

(** val bits_of_b32 : binary32 -> coq_Z **)

let bits_of_b32 =
  bits_of_binary_float (Zpos (Coq_xI (Coq_xI (Coq_xI (Coq_xO Coq_xH)))))
    (Zpos (Coq_xO (Coq_xO (Coq_xO Coq_xH))))

type binary64 = binary_float

(** val default_nan_pl64 : (bool, nan_pl) prod **)

let default_nan_pl64 =
  Coq_pair (false,
    (nat_iter (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S
      (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S
      (S (S (S (S (S (S O)))))))))))))))))))))))))))))))))))))))))))))))))))
      (fun x -> Coq_xO x) Coq_xH))

(** val unop_nan_pl64 : binary64 -> (bool, nan_pl) prod **)

let unop_nan_pl64 = function
| B754_nan (s, pl) -> Coq_pair (s, pl)
| _ -> default_nan_pl64

(** val binop_nan_pl64 : binary64 -> binary64 -> (bool, nan_pl) prod **)

let binop_nan_pl64 pl1 pl2 =
  match pl1 with
  | B754_nan (s1, pl3) -> Coq_pair (s1, pl3)
  | _ ->
    (match pl2 with
     | B754_nan (s2, pl3) -> Coq_pair (s2, pl3)
     | _ -> default_nan_pl64)

(** val b64_opp : binary_float -> binary_float **)

let b64_opp =
  coq_Bopp (Zpos (Coq_xI (Coq_xO (Coq_xI (Coq_xO (Coq_xI Coq_xH)))))) (Zpos
    (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO
    (Coq_xO Coq_xH))))))))))) (fun x x0 -> Coq_pair (x, x0))

(** val b64_plus : mode -> binary_float -> binary_float -> binary_float **)

let b64_plus =
  coq_Bplus (Zpos (Coq_xI (Coq_xO (Coq_xI (Coq_xO (Coq_xI Coq_xH)))))) (Zpos
    (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO
    (Coq_xO Coq_xH))))))))))) binop_nan_pl64

(** val b64_minus : mode -> binary_float -> binary_float -> binary_float **)

let b64_minus =
  coq_Bminus (Zpos (Coq_xI (Coq_xO (Coq_xI (Coq_xO (Coq_xI Coq_xH)))))) (Zpos
    (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO
    (Coq_xO Coq_xH))))))))))) binop_nan_pl64

(** val b64_mult : mode -> binary_float -> binary_float -> binary_float **)

let b64_mult =
  coq_Bmult (Zpos (Coq_xI (Coq_xO (Coq_xI (Coq_xO (Coq_xI Coq_xH)))))) (Zpos
    (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO
    (Coq_xO Coq_xH))))))))))) binop_nan_pl64

(** val b64_div : mode -> binary_float -> binary_float -> binary_float **)

let b64_div =
  coq_Bdiv (Zpos (Coq_xI (Coq_xO (Coq_xI (Coq_xO (Coq_xI Coq_xH)))))) (Zpos
    (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO
    (Coq_xO Coq_xH))))))))))) binop_nan_pl64

(** val b64_sqrt : mode -> binary_float -> binary_float **)

let b64_sqrt =
  coq_Bsqrt (Zpos (Coq_xI (Coq_xO (Coq_xI (Coq_xO (Coq_xI Coq_xH)))))) (Zpos
    (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO
    (Coq_xO Coq_xH))))))))))) unop_nan_pl64

(** val b64_of_bits : coq_Z -> binary64 **)

let b64_of_bits =
  binary_float_of_bits (Zpos (Coq_xO (Coq_xO (Coq_xI (Coq_xO (Coq_xI
    Coq_xH)))))) (Zpos (Coq_xI (Coq_xI (Coq_xO Coq_xH))))

(** val bits_of_b64 : binary64 -> coq_Z **)

let bits_of_b64 =
  bits_of_binary_float (Zpos (Coq_xO (Coq_xO (Coq_xI (Coq_xO (Coq_xI
    Coq_xH)))))) (Zpos (Coq_xI (Coq_xI (Coq_xO Coq_xH))))

