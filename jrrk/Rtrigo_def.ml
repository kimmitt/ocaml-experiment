open Alembert
open Datatypes
open Factorial
open Peano
open RIneq
open Raxioms
open Rdefinitions
open Rpow_def

(** val exist_exp : coq_R -> coq_R **)

let exist_exp x =
  coq_Alembert_C3 (fun n -> coq_Rinv (coq_INR (fact n))) x

(** val exp : coq_R -> coq_R **)

let exp x =
  exist_exp x

(** val exist_exp0 : coq_R **)

let exist_exp0 =
  coq_R1

(** val cosh : coq_R -> coq_R **)

let cosh x =
  coq_Rdiv (coq_Rplus (exp x) (exp (coq_Ropp x))) (coq_Rplus coq_R1 coq_R1)

(** val sinh : coq_R -> coq_R **)

let sinh x =
  coq_Rdiv (coq_Rminus (exp x) (exp (coq_Ropp x))) (coq_Rplus coq_R1 coq_R1)

(** val tanh : coq_R -> coq_R **)

let tanh x =
  coq_Rdiv (sinh x) (cosh x)

(** val cos_n : nat -> coq_R **)

let cos_n n =
  coq_Rdiv (pow (coq_Ropp coq_R1) n) (coq_INR (fact (mult (S (S O)) n)))

(** val exist_cos : coq_R -> coq_R **)

let exist_cos x =
  coq_Alembert_C3 cos_n x

(** val cos : coq_R -> coq_R **)

let cos x =
  exist_cos (coq_Rsqr x)

(** val sin_n : nat -> coq_R **)

let sin_n n =
  coq_Rdiv (pow (coq_Ropp coq_R1) n)
    (coq_INR (fact (plus (mult (S (S O)) n) (S O))))

(** val exist_sin : coq_R -> coq_R **)

let exist_sin x =
  coq_Alembert_C3 sin_n x

(** val sin : coq_R -> coq_R **)

let sin x =
  coq_Rmult x (exist_sin (coq_Rsqr x))

(** val exist_cos0 : coq_R **)

let exist_cos0 =
  coq_R1

