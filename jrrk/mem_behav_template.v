module mem_behav(clk,
	readwrite_addr,
	byte_strobes,
	write_enable,
	read_enable,
	write_data,
	read_data,
	dbg_mem);

parameter siz = 18;

input[31:0] write_data;
output[31:0] read_data;
input [siz+1:0] readwrite_addr;
input [3:0] byte_strobes;
input clk, write_enable, read_enable, dbg_mem;
reg[31:0] readwrite_data;
reg[31:0] read_data;

reg [31:0] mem[0:(1<<siz)-1];
integer i;
   
initial
  begin
     for (i = 0; i < 1<<siz; i=i+1)
       mem[i] = 0;
     $readmemh("verilog/~.mem", mem);
  end
   
always @(negedge clk)
	begin
	if (read_enable) begin read_data = mem[readwrite_addr[siz+1:2]]; if (dbg_mem) $display("read %X => %X", readwrite_addr, read_data); end
	if (write_enable) readwrite_data = mem[readwrite_addr[siz+1:2]];
	if (write_enable & byte_strobes[0]) readwrite_data[7:0] = write_data[7:0];
	if (write_enable & byte_strobes[1]) readwrite_data[15:8] = write_data[15:8];
	if (write_enable & byte_strobes[2]) readwrite_data[23:16] = write_data[23:16];
	if (write_enable & byte_strobes[3]) readwrite_data[31:24] = write_data[31:24];
	if (write_enable) begin mem[readwrite_addr[siz+1:2]] = readwrite_data; if (dbg_mem) $display("write %X => %X", readwrite_data, readwrite_addr); end
	end

endmodule
