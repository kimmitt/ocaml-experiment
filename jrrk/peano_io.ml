open Mylib
open Factorial
open Axiom_io

let (_,argv) = get_argv()
let _ = print_string (array_get argv (0)); print_char ' '; print_string (array_get argv (1)); print_char '='; let n = int_of_string (array_get argv (1)) in print_int_nl (fromNat (fact (asNat n))); sys_exit 0
