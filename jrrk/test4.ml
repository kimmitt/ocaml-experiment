open Mylib

let print_char c = output_char stdout c
let rec print_int n = if n > 9 then print_int (n/10); print_char (Char.unsafe_chr (n mod 10 + Char.code '0'))
let print_int n = if n < 0 then (print_char '-'; print_int (0-n)) else print_int n

let fact n =
  let rec factorial m n =
  if (n) < 1 then m
  else factorial (m*n) (n-1) in
  factorial 1 n

let _ = print_char 'J'; print_char 'K'; print_int 12345; print_char '\n'; flush stdout
let _ = try
  for i = 0 to 13 do
  print_int i; print_char ' '; print_int (fact i); print_char '\n'; flush stdout
  done;
  with _ -> print_int 42
