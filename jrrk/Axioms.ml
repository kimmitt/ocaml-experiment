open BinNums
open Mylib

type coq_R = R0 | R1

(** val coq_R0 : coq_R **)

let coq_R0 = R0

(** val coq_R1 : coq_R **)

let coq_R1 = R1

(** val coq_Rplus : coq_R -> coq_R -> coq_R **)

let coq_Rplus x y =
  failwith "AXIOM TO BE REALIZED"

(** val coq_Rmult : coq_R -> coq_R -> coq_R **)

let coq_Rmult x y =
  failwith "AXIOM TO BE REALIZED"

(** val coq_Ropp : coq_R -> coq_R **)

let coq_Ropp x =
  failwith "AXIOM TO BE REALIZED"

(** val coq_Rinv : coq_R -> coq_R **)

let coq_Rinv x =
  failwith "AXIOM TO BE REALIZED"

(** val up : coq_R -> coq_Z **)

let up x =
  failwith "AXIOM TO BE REALIZED"

(** val coq_Rminus : coq_R -> coq_R -> coq_R **)

let coq_Rminus r1 r2 =
  coq_Rplus r1 (coq_Ropp r2)

(** val coq_Rdiv : coq_R -> coq_R -> coq_R **)

let coq_Rdiv r1 r2 =
  coq_Rmult r1 (coq_Rinv r2)

(** val total_order_T : coq_R -> coq_R -> bool sumor **)

let total_order_T x y =
  failwith "AXIOM TO BE REALIZED"

(** val completeness : __ -> coq_R **)

let completeness _ =
  failwith "AXIOM TO BE REALIZED"

