open Datatypes
open Mylib
open BinNums
open BinPos
open BinNat
open BinInt
open Zops

let rec asPi = function
  | 0 -> failwith "not positive"
  | 1 -> Coq_xH
  | n -> let asp = asPi(n lsr 1) in if n land 1 = 1 then Coq_xI asp else Coq_xO asp

let asZi = function
  | 0 -> Z0
  | 1 -> Zpos Coq_xH
  | n -> if n < 0 then Zneg (asPi(-n)) else Zpos (asPi n)

let asZ str = let tot = ref Z0 in
  String.iter (fun arg -> tot := coq_Zplus (coq_Zmult (!tot) (asZi 10)) (coq_Zminus (asZi (int_of_char (Char.uppercase arg))) (asZi 48))) str;
  !tot

let rec fromPi = function
  | Coq_xH -> 1
  | Coq_xO num -> (fromPi num) lsl 1
  | Coq_xI num -> ((fromPi num) lsl 1) + 1

let fromZi = function
  | Z0 -> 0
  | Zpos num -> fromPi num
  | Zneg num -> - (fromPi num)

let rec fromZ num =
  (if coq_Zgt_bool num (asZi 9) then fromZ (coq_Zdiv num (asZi 10)) else "")^String.make 1 (char_of_int (48 + (fromZi (coq_Zmod num (asZi 10)))))

let asZ str = let tot = ref Z0 in
  String.iter (fun arg -> tot := coq_Zplus (coq_Zmult (!tot) (asZi 10)) (coq_Zminus (asZi (int_of_char (Char.uppercase arg))) (asZi 48))) str;
  !tot

let rec fromZ num =
  (if coq_Zgt_bool num (asZi 9) then fromZ (coq_Zdiv num (asZi 10)) else "")^String.make 1 (char_of_int (48 + (fromZi (coq_Zmod num (asZi 10)))))

let encstr str = let tot = ref Z0 in
  String.iter (fun arg -> tot := coq_Zplus (coq_Zmult (!tot) (asZi 64)) (coq_Zmod (asZi (int_of_char (Char.uppercase arg))) (asZi 64))) str;
  !tot

let rec decstr num =
  (if coq_Zgt_bool num (asZi 63) then decstr (coq_Zdiv num (asZi 64)) else "")^String.make 1 (Char.lowercase (char_of_int (32 + (32 lxor (fromZi (coq_Zmod num (asZi 64)))))))
