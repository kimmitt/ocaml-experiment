open Mylib
open Ml_decls
open Parseml
open String_of_float
open Dump

module ParseSample = LRTTparser.Parser(BackusParse)

let process str =
  print_endline "Expression entered:";
  print_endline str;
  let codes = init_codes() in
  codes.impl.string_of_float<-string_of_float_b754 false;
  codes.impl.float_of_string<-float_of_string_b754 false;
  codes.impl.dump<-Dump.dump4exp codes.impl;
(*
  codes.impl.debug.debugall <- true;
*)
  try 
    let p = demo codes.impl ParseSample.streamparse str in
    let d = Dump.dump4exp codes.impl p in
    print_endline "Parser returned:";
    print_endline d;
    let rw = Ml_compiling.rewrite codes p in
    print_endline "Compiled to:";
    Dump.dumpentry (codes,rw);
    let x = Exec.exec_till (codes,rw) in
    print_endline "Reduced to:";
    print_endline (dumpentry' codes x);
    (p, d, rw, x)
  with
    Failure s -> raise (Failure ("Failed with string: "^s))

let main () =
  let (executable_name, argv) = Sys.get_argv() in
  if Array.length (argv) > 1 then
    begin
    let concat = String.concat " " (List.tl (Array.to_list argv)) in
    let (p, d, rw, x) = process concat in ignore (p, d, rw, x)
    end
  else
    try 
      while true do
	print_endline "Enter an ML expression";
	let line = String.create 256 and ix = ref 0 in
	let ch = ref (input_char (Std.in_)) in
	while !ch <> '\n' && !ix < String.length line do
	  line.[!ix] <- !ch;
	  ch := input_char (Std.in_);
	  incr ix;
	done;
	let (p, d, rw, x) = process (String.sub line 0 !ix) in ignore (p, d, rw, x)
      done
    with End_of_file -> print_endline "Exiting due to end-of-file"
