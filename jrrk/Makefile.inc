P=../../regression/coq83/sf-solutions
F=-nopervasives
G=-g

IO=std.cmi obj.cmi mylib.cmi
XO=std.cmx
I=Axioms.cmi Datatypes.cmi Specif.cmi Peano_dec.cmi EqNat.cmi SfLib.cmi Peano.cmi Bool.cmi BinNums.cmi BinPosDef.cmi BinPos.cmi BinNat.cmi BinInt.cmi Zeven.cmi Sumbool.cmi ZArith_dec.cmi Zbool.cmi Zdiv.cmi Zops.cmi Rdefinitions.cmi Raxioms.cmi RIneq.cmi Rpow_def.cmi Ranalysis1.cmi Rbasic_fun.cmi SeqProp.cmi Rcomplete.cmi Rfunctions.cmi PartSum.cmi Alembert.cmi Factorial.cmi Rtrigo_def.cmi Rsqrt_def.cmi Rpower.cmi Fcore_Zaux.cmi Fcore_Raux.cmi Fcalc_bracket.cmi Fcore_digits.cmi Fcalc_round.cmi Fcore_FLT.cmi Fcore_defs.cmi Fcore_generic_fmt.cmi PreOmega.cmi Zpower.cmi Fappli_IEEE.cmi Fappli_IEEE_bits.cmi MoreZStlc.cmi ctypes0.cmi ml_decls.cmi
Q=Axioms.cmx Datatypes.cmx Specif.cmx Peano_dec.cmx EqNat.cmx SfLib.cmx Peano.cmx Bool.cmx BinNums.cmx BinPosDef.cmx BinPos.cmx BinNat.cmx BinInt.cmx Zeven.cmx Sumbool.cmx ZArith_dec.cmx Zbool.cmx Zdiv.cmx Zops.cmx Rdefinitions.cmx Raxioms.cmx RIneq.cmx Rpow_def.cmx Ranalysis1.cmx Rbasic_fun.cmx SeqProp.cmx Rcomplete.cmx Rfunctions.cmx PartSum.cmx Alembert.cmx Factorial.cmx Rtrigo_def.cmx Rsqrt_def.cmx Rpower.cmx Fcore_Zaux.cmx Fcore_Raux.cmx Fcalc_bracket.cmx Fcore_digits.cmx Fcalc_round.cmx Fcore_FLT.cmx Fcore_defs.cmx Fcore_generic_fmt.cmx PreOmega.cmx Zpower.cmx Fappli_IEEE.cmx Fappli_IEEE_bits.cmx axiom_io.cmx MoreZStlc.cmx ml_decls.cmx
E0=Datatypes.cmx Specif.cmx Peano_dec.cmx EqNat.cmx SfLib.cmx Peano.cmx Bool.cmx BinPos.cmx BinNat.cmx BinInt.cmx Zeven.cmx Sumbool.cmx ZArith_dec.cmx Zbool.cmx Zdiv.cmx Zops.cmx mylib.cmx Axioms.cmx Rdefinitions.cmx Raxioms.cmx RIneq.cmx Rpow_def.cmx Ranalysis1.cmx Rbasic_fun.cmx SeqProp.cmx Rcomplete.cmx Rfunctions.cmx PartSum.cmx Alembert.cmx Factorial.cmx Rtrigo_def.cmx Rsqrt_def.cmx Rpower.cmx Fcore_Zaux.cmx Fcore_Raux.cmx Fcalc_bracket.cmx Fcore_digits.cmx Fcalc_round.cmx Fcore_FLT.cmx Fcore_defs.cmx Fcore_generic_fmt.cmx PreOmega.cmx Zpower.cmx Fappli_IEEE.cmx Fappli_IEEE_bits.cmx ml_decls.cmx to_from_Z.cmx axiom_io.cmx MoreZStlc.cmx string_of_float.cmx dump.cmx ml_compiling.cmx exec.cmx LRTTtypes.cmx LRTTparser.cmx parseml.cmx parsemain.cmx
E=$(XO) $(E0) parsentry.cmx
E1=std.cmo $(subst .cmx,.cmo,$(E0)) unsafe.cmo
T=mylib.cmx Datatypes.cmx Peano.cmx Bool.cmx EqNat.cmx BinNums.cmx BinPosDef.cmx BinPos.cmx BinNat.cmx BinInt.cmx Zeven.cmx Sumbool.cmx ZArith_dec.cmx Zbool.cmx Zdiv.cmx Zops.cmx ml_decls.cmx to_from_Z.cmx dump.cmx ../LRTTdebug.ml parseml.cmx parsetest.cmx
T1=$(subst .cmx,.cmo,$T)
N=mylib.cmx Datatypes.cmx Peano.cmx Bool.cmx EqNat.cmx BinNums.cmx BinPosDef.cmx BinPos.cmx BinNat.cmx BinInt.cmx Zeven.cmx Sumbool.cmx ZArith_dec.cmx Zbool.cmx Zdiv.cmx Zops.cmx ml_decls.cmx to_from_Z.cmx dump.cmx ml_compiling.cmx enctest.cmx
N1=$(subst .cmx,.cmo,$N)
X0=Datatypes.cmx Specif.cmx Peano_dec.cmx EqNat.cmx SfLib.cmx Peano.cmx Bool.cmx BinPos.cmx BinNat.cmx BinInt.cmx Zeven.cmx Sumbool.cmx ZArith_dec.cmx Zbool.cmx Zdiv.cmx Zops.cmx mylib.cmx Axioms.cmx Rdefinitions.cmx Raxioms.cmx RIneq.cmx Rpow_def.cmx Ranalysis1.cmx Rbasic_fun.cmx SeqProp.cmx Rcomplete.cmx Rfunctions.cmx PartSum.cmx Alembert.cmx Factorial.cmx Rtrigo_def.cmx Rsqrt_def.cmx Rpower.cmx Fcore_Zaux.cmx Fcore_Raux.cmx Fcalc_bracket.cmx Fcore_digits.cmx Fcalc_round.cmx Fcore_FLT.cmx Fcore_defs.cmx Fcore_generic_fmt.cmx PreOmega.cmx Zpower.cmx Fappli_IEEE.cmx Fappli_IEEE_bits.cmx ml_decls.cmx to_from_Z.cmx axiom_io.cmx MoreZStlc.cmx string_of_float.cmx dump.cmx ml_compiling.cmx exec.cmx
X=$(XO) $(X0) exectest.cmx
S=std.cmx Datatypes.cmx Specif.cmx Peano_dec.cmx EqNat.cmx SfLib.cmx Peano.cmx Bool.cmx BinPos.cmx BinNat.cmx BinInt.cmx Zeven.cmx Sumbool.cmx ZArith_dec.cmx Zbool.cmx Zdiv.cmx Zops.cmx mylib.cmx Axioms.cmx Rdefinitions.cmx Raxioms.cmx RIneq.cmx Rpow_def.cmx Ranalysis1.cmx Rbasic_fun.cmx SeqProp.cmx Rcomplete.cmx Rfunctions.cmx PartSum.cmx Alembert.cmx Factorial.cmx Rtrigo_def.cmx Rsqrt_def.cmx Rpower.cmx Fcore_Zaux.cmx Fcore_Raux.cmx Fcalc_bracket.cmx Fcore_digits.cmx Fcalc_round.cmx Fcore_FLT.cmx Fcore_defs.cmx Fcore_generic_fmt.cmx PreOmega.cmx Zpower.cmx Fappli_IEEE.cmx Fappli_IEEE_bits.cmx ml_decls.cmx to_from_Z.cmx string_of_float.cmx
X1=$(subst .cmx,.cmo,$X)
A=$(XO) Datatypes.cmx Specif.cmx Peano_dec.cmx EqNat.cmx SfLib.cmx Peano.cmx Bool.cmx BinPosDef.cmx BinPos.cmx mylib.cmx argtest.cmx
B=$(XO) mylib.cmx argtest2.cmx
C=$(XO) mylib.cmx argtest3.cmx
Z=$(XO) mylib.cmx argtest4.cmx
Y=$(XO) mylib.cmx logictest.cmx
W=$(XO) mylib.cmx argtest1.cmx
V=$(XO) mylib.cmx argtest0.cmx
U0=Datatypes.cmx Specif.cmx Peano_dec.cmx EqNat.cmx SfLib.cmx Peano.cmx Bool.cmx BinPos.cmx BinNat.cmx BinInt.cmx Zeven.cmx Sumbool.cmx ZArith_dec.cmx Zbool.cmx Zdiv.cmx Zops.cmx mylib.cmx Axioms.cmx Rdefinitions.cmx Raxioms.cmx RIneq.cmx Rpow_def.cmx Ranalysis1.cmx Rbasic_fun.cmx SeqProp.cmx Rcomplete.cmx Rfunctions.cmx PartSum.cmx Alembert.cmx Factorial.cmx Rtrigo_def.cmx Rsqrt_def.cmx Rpower.cmx Fcore_Zaux.cmx Fcore_Raux.cmx Fcalc_bracket.cmx Fcore_digits.cmx Fcalc_round.cmx Fcore_FLT.cmx Fcore_defs.cmx Fcore_generic_fmt.cmx PreOmega.cmx Zpower.cmx Fappli_IEEE.cmx Fappli_IEEE_bits.cmx ml_decls.cmx to_from_Z.cmx string_of_float.cmx
U=$(XO) $(U0) unsafe.cmx
R=$(XO) mylib.cmx div.cmx
J=$(XO) mylib.cmx test4.cmx
K=$(XO) mylib.cmx test5.cmx
M=$(XO) mylib.cmx ack.cmx
H=$(XO) mylib.cmx nozshort.cmx nozshortmain.cmx
D=$(XO) mylib.cmx Datatypes.cmx Bool.cmx Peano.cmx BinPos.cmx BinNat.cmx BinInt.cmx short.cmx shortmain.cmx

debug: $(IO) $I $S debug.cmx
	$O $F $L -o $@ $G $S debug.cmx

pidigits: $(IO) $I $S pidigits.cmx
	$O $F $L -o $@ $G $S pidigits.cmx

exectest: $(IO) $I $X
	$O $F $L -o $@ $G $X

exectop: $I $(X1)
	ocamlmktop -o $@ $G $(X1)

enctest: $(IO) $I $N
	$O $F $L -o $@ $G $N

enctop: $I $(N1)
	ocamlmktop -o $@ $G $(N1)

lrttest: $T
	$O -I .. -o $@ $G $T

lrttop: $I $(T1)
	ocamlmktop -o $@ $G $(T1)

parseopt: $(IO) $I $E
	$O $F $L -o $@ $G $E

parsetop: $I $(E1)
	ocamlmktop -o $@ $G $(E1)

standalone: $(IO) $I $E
	$O $F $L -fstandalone -o $@ $G $E

argtest: $(IO) $I $A
	$O $F $L -o $@ $G $A

argtest0: $(IO) $V $O
	$O $F $L -o $@ $G $V

argtest1: $(IO) $W
	$O $F $L -o $@ $G $W

argtest2: $(IO) $B
	$O $F $L -o $@ $G $B

argtest3: $(IO) $C
	$O $F $L -o $@ $G $C

argtest4: $(IO) $Z
	$O $F $L -o $@ $G $Z

logictest: $(IO) $Y
	$O $F $L -o $@ $G $Y

div: $(IO) $R $O
	$O $F $L -o $@ $G $R

unsafe: $(IO) $U $O
	$O $F $L -o $@ $G $U

test4: obj.cmi $J ../../ocamlopt
	$O $F $L -o $@ $G $J

test5: obj.cmi $K
	$O $F $L -o $@ $G $K

ack: obj.cmi $M
	$O $F $L -o $@ $G $M

short: obj.cmi $D
	$O $F $L -o $@ $G $D

nozshort: obj.cmi $H
	$O $F $L -o $@ $G $H
       
doc: $(IO) $I $E
	(cd ..; nostdlib -latex $(subst .cmx,.ml,$E))

%.cmo: ../%.ml
	ocamlc $G -c $<
	mv ../$@ .
	for i in $(subst .cmo,.cmi,../$@); do if test -f $$i ; then mv $$i .; fi; done

%.cmx: ../%.ml
	$O -I .. $F -nostdlib $L $G -c $<
	mv ../$@ $(subst .cmx,.o,../$@) .
	for i in $(subst .cmx,.cmi,../$@); do if test -f $$i ; then mv $$i .; fi; done
	for i in $(subst .cmx,.s,../$@); do if test -f $$i ; then mv $$i .; fi; done

%.cmi: ../%.mli
	$O $<
	mv ../$@ .

depend:
	(cd ..; ocamldep *.ml *.mli | sed s=obj.cm.==g ) > .depend

include .depend
