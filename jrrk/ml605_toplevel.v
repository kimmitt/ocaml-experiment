`timescale 1ns/1ps

module tb;

wire [0:0] finish;
wire [0:0] flush;
wire [0:0] readstrobe;
wire [7:0] writech;   
reg [7:0] readch;
reg [31:0] stdin, stdout;
reg a23_clk, a23_rst, dbg_mem;

   reg 	       SYSCLK_N;
   reg SYSCLK_P;

wire  GPIO_LED_N ;
wire  GPIO_LED_S ;
wire  GPIO_LED_W ;
wire  GPIO_LED_E ;
reg   GPIO_SW_C ;
reg   GPIO_SW_N ;
reg   GPIO_SW_S ;
reg   GPIO_SW_E ;
reg   GPIO_SW_W ;
wire  CPU_RESET  = a23_rst;
wire   IIC_SCL_DVI /* synthesis syn_tristate = 1 */ ;
wire   IIC_SDA_DVI /* synthesis syn_tristate = 1 */ ;
wire   [11:0] DVI_D ;
wire   DVI_DE ;
wire   DVI_H ;
wire   DVI_V ;
wire   DVI_RESET_B ;
wire   DVI_XCLK_N ;
wire   DVI_XCLK_P ;
wire  CLK_33MHZ_FPGA  = a23_clk;

TOPLEVEL top1 (
  .SYSCLK_N(SYSCLK_N),
  .SYSCLK_P(SYSCLK_P),
  .GPIO_LED_C(finish),
  .GPIO_LED_N(GPIO_LED_N),
  .GPIO_LED_S(GPIO_LED_S),
  .GPIO_LED_W(GPIO_LED_W),
  .GPIO_LED_E(GPIO_LED_E),
  .GPIO_SW_C(GPIO_SW_C),
  .GPIO_SW_N(GPIO_SW_N),
  .GPIO_SW_S(GPIO_SW_S),
  .GPIO_SW_E(GPIO_SW_E),
  .GPIO_SW_W(GPIO_SW_W),
  .CPU_RESET(CPU_RESET),
  .IIC_SCL_DVI(IIC_SCL_DVI),
  .IIC_SDA_DVI(IIC_SDA_DVI),
  .DVI_D(DVI_D),
  .DVI_DE(DVI_DE),
  .DVI_H(DVI_H),
  .DVI_V(DVI_V),
  .DVI_RESET_B(DVI_RESET_B),
  .DVI_XCLK_N(DVI_XCLK_N),
  .DVI_XCLK_P(DVI_XCLK_P),
  .GPIO_LED(writech),
  .CLK_33MHZ_FPGA(CLK_33MHZ_FPGA)
);

reg old_writech;

always @(posedge a23_clk)
	begin
	if (flush) $fflush(stdout);
	if (writech[7] && !old_writech)
	  begin
	     if (writech[6:0] == 10) $fdisplay(stdout);
	     $fwrite(stdout, "%c", writech[6:0]);
	  end
	old_writech = writech[7];
	if (readstrobe) readch = $fgetc(stdin);
	end

initial
	begin
	   if ($test$plusargs("DBG_DUMPVARS") != 0) $dumpvars;
	   dbg_mem = $test$plusargs("DBG_MEM") != 0;
	   stdin = $fopen("/dev/stdin", "r");
	   stdout = $fopen("/dev/stdout", "w");
	   GPIO_SW_N = 0;
	   GPIO_SW_S = 0;
	   GPIO_SW_E = 0;
	   GPIO_SW_W = 0;
	   GPIO_SW_C = 0;
	   a23_rst = 1'b1;
	   a23_clk = 1'b0;	   
	   #1000 a23_rst = 1'b0;
	   @(posedge finish);
	   $fdisplay(stdout);
	   $fflush(stdout);
	   $finish(0);
	end

   always
     begin
     #2.5
     SYSCLK_N = 0;
     SYSCLK_P = 1;
     #2.5
     SYSCLK_N = 1;
     SYSCLK_P = 0;
     end

   always
     begin
	#15.15 a23_clk = 1'b1;
	#15.15 a23_clk = 1'b0;
     end
   
endmodule
