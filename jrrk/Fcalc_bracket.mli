open BinInt
open BinNums
open Datatypes
open Fcore_Raux
open Fcore_Zaux
open Rdefinitions
open Zbool

type location =
| Coq_loc_Exact
| Coq_loc_Inexact of comparison

val location_rect : 'a1 -> (comparison -> 'a1) -> location -> 'a1

val location_rec : 'a1 -> (comparison -> 'a1) -> location -> 'a1

val inbetween_loc : coq_R -> coq_R -> coq_R -> location

val new_location_even : coq_Z -> coq_Z -> location -> location

val new_location_odd : coq_Z -> coq_Z -> location -> location

val new_location : coq_Z -> coq_Z -> location -> location

