open BinInt
open BinNums
open Datatypes
open Fcore_Zaux

val digits2_Pnat : positive -> nat

val coq_Zdigit : radix -> coq_Z -> coq_Z -> coq_Z

val coq_Zsum_digit : radix -> (coq_Z -> coq_Z) -> nat -> coq_Z

val coq_Zscale : radix -> coq_Z -> coq_Z -> coq_Z

val coq_Zslice : radix -> coq_Z -> coq_Z -> coq_Z -> coq_Z

val coq_Zdigits_aux : radix -> coq_Z -> coq_Z -> coq_Z -> nat -> coq_Z

val coq_Zdigits : radix -> coq_Z -> coq_Z

