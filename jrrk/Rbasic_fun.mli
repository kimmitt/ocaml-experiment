open RIneq
open Rdefinitions

type __ = Obj.t

val coq_Rmin : coq_R -> coq_R -> coq_R

val coq_Rmin_case : coq_R -> coq_R -> 'a1 -> 'a1 -> 'a1

val coq_Rmin_case_strong :
  coq_R -> coq_R -> (__ -> 'a1) -> (__ -> 'a1) -> 'a1

val coq_Rmax : coq_R -> coq_R -> coq_R

val coq_Rmax_case : coq_R -> coq_R -> 'a1 -> 'a1 -> 'a1

val coq_Rmax_case_strong :
  coq_R -> coq_R -> (__ -> 'a1) -> (__ -> 'a1) -> 'a1

val coq_Rcase_abs : coq_R -> bool

val coq_Rabs : coq_R -> coq_R

