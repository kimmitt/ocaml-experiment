(*
Pi = SUM(k=0 to infinity) 16^(-k) [ 4/(8k+1) - 2/(8k+4) - 1/(8k+5) - 1/(8k+6) ].
*)

let pidig n =
  let dig = Array.create (n+1) 0. in
  for k = 0 to n do
    let k8 = float_of_int (k lsl 3) in
    dig.(n-k) <- 4./.(k8+.1.) -. 2./.(k8+.4.) -. 1./.(k8+.5.) -. 1./.(k8+.6.);
  done;
  dig
;;

let pisum dig =
  let dig' = Array.create (Array.length dig) 0. in
  for k = 1 to Array.length dig do
    let scale = ref 1. in
    for j = 0 to Array.length dig - k do
	dig'.(Array.length dig - k) <- dig'.(Array.length dig - k) +. dig.(Array.length dig - k - j) /. !scale;
      scale := !scale *. 16.;
      done;
  done;
  dig'
;;

let dig = pidig 100;;
let dig' = pisum dig;;
