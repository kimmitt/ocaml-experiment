open BinInt
open BinNums
open Datatypes
open Fcore_Zaux

(** val digits2_Pnat : positive -> nat **)

let rec digits2_Pnat = function
| Coq_xI p -> S (digits2_Pnat p)
| Coq_xO p -> S (digits2_Pnat p)
| Coq_xH -> O

(** val coq_Zdigit : radix -> coq_Z -> coq_Z -> coq_Z **)

let coq_Zdigit beta n k =
  Z.rem (Z.quot n (Z.pow (radix_val beta) k)) (radix_val beta)

(** val coq_Zsum_digit : radix -> (coq_Z -> coq_Z) -> nat -> coq_Z **)

let rec coq_Zsum_digit beta f = function
| O -> Z0
| S k0 ->
  Z.add (coq_Zsum_digit beta f k0)
    (Z.mul (f (Z.of_nat k0)) (Z.pow (radix_val beta) (Z.of_nat k0)))

(** val coq_Zscale : radix -> coq_Z -> coq_Z -> coq_Z **)

let coq_Zscale beta n k =
  if Z.leb Z0 k
  then Z.mul n (Z.pow (radix_val beta) k)
  else Z.quot n (Z.pow (radix_val beta) (Z.opp k))

(** val coq_Zslice : radix -> coq_Z -> coq_Z -> coq_Z -> coq_Z **)

let coq_Zslice beta n k1 k2 =
  if Z.leb Z0 k2
  then Z.rem (coq_Zscale beta n (Z.opp k1)) (Z.pow (radix_val beta) k2)
  else Z0

(** val coq_Zdigits_aux :
    radix -> coq_Z -> coq_Z -> coq_Z -> nat -> coq_Z **)

let rec coq_Zdigits_aux beta p nb pow0 = function
| O -> nb
| S n0 ->
  if Z.ltb p pow0
  then nb
  else coq_Zdigits_aux beta p (Z.add nb (Zpos Coq_xH))
         (Z.mul (radix_val beta) pow0) n0

(** val coq_Zdigits : radix -> coq_Z -> coq_Z **)

let coq_Zdigits beta n = match n with
| Z0 -> Z0
| Zpos p ->
  coq_Zdigits_aux beta n (Zpos Coq_xH) (radix_val beta) (digits2_Pnat p)
| Zneg p ->
  coq_Zdigits_aux beta (Zpos p) (Zpos Coq_xH) (radix_val beta)
    (digits2_Pnat p)

