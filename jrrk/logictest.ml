open Mylib

let run () =
  try
    print_endline "Testing logic";
    for i = 0 to 10 do
      let d = - i in
      print_int d;
      print_char ' ';
      print_int (d land 255);
      print_char ' ';
      print_int (d lor 8);
      print_char ' ';
      print_int (d lxor 7);
      print_char '\n';
    done;
  with
  | _ -> print_endline "Sorry, an exception occured"

let _ = run()
