open Datatypes
open Rdefinitions

(** val pow : coq_R -> nat -> coq_R **)

let rec pow r = function
| O -> coq_R1
| S n0 -> coq_Rmult r (pow r n0)

