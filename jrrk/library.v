/****************************************************************************
  FileName     [ lib2.v ]
  Synopsis     [ MACRO Library for QuteRTL Synthesis. ]
  Copyright    [ Copyleft(c) 2011 LaDs(III), GIEE, NTU, Taiwan ]
****************************************************************************/

module DVL_X_CELL(Y);
parameter dataWidth= 8;
output [dataWidth-1:0] Y;

assign Y = {dataWidth{1'b1}};

endmodule

module DVL_Z_CELL(Y);
parameter dataWidth= 8;
output [dataWidth-1:0] Y;

assign Y = {dataWidth{1'bz}};

endmodule

module DVL_BUFIF1(Y, A, S);
parameter dataWidth= 8;
output [dataWidth-1:0] Y;
input [dataWidth-1:0] A;
input S;

assign Y = S? A: {dataWidth{1'bz}};

endmodule


module DVL_DFF_ASYNC (Q, QN, D, DEF, CK, RN);
parameter dataWidth= 8;
output [dataWidth-1:0] Q;
output [dataWidth-1:0] QN;
input [dataWidth-1:0]  D;
input [dataWidth-1:0]  DEF;
input CK, RN;
reg [dataWidth-1:0] Q;
reg [dataWidth-1:0] QN;

   always@(posedge CK or posedge RN)
   begin
      if(RN)
         Q= DEF;
      else
         Q= D;
   end

endmodule



module DVL_DFF_SYNC (Q, D, CK);
parameter dataWidth= 8;
output [dataWidth-1:0] Q;
input [dataWidth-1:0]  D;
input CK;
reg [dataWidth-1:0] Q;

   always@ (posedge CK)
   begin
      Q= D;
   end

endmodule



module DVL_LATCH(Q, QN, D, L);
parameter dataWidth= 8;
output [dataWidth-1:0] Q;
output [dataWidth-1:0] QN;
input [dataWidth-1:0] D;
input L;
reg [dataWidth-1:0] Q;
reg [dataWidth-1:0] QN;

   always@(L or D)
   begin
      if(L)
         Q= D;
   end

endmodule

module DVL_BUF(Y, A); //buf
parameter dataWidth= 8;
input [dataWidth-1:0] A;
output [dataWidth-1:0] Y;
reg [dataWidth-1:0] Y;

   always@(A)
   begin
      Y = A;
   end
endmodule

module DVL_MUX (Y, A, B, S);
/*
        |---\
   A----|F   \
        |    |---Y
   B----|T   /
        |---/
          |
          |
          S
*/
parameter dataWidth= 8;
output [dataWidth-1:0] Y;
input [dataWidth-1:0] A;
input [dataWidth-1:0] B;
input S;
reg [dataWidth-1:0] Y;

   always@(S or A or B)
   begin
      if(S==1'b1)
         Y = B;
      else
         Y = A;
   end

endmodule

/*
   ----------------------------------|
   |                                 |
   |             operator            |
   |                                 |
   ----------------------------------|

*/
/*================  reduction  ================*/
module DVL_RED_AND(Y, A); //reduction and
parameter dataWidth= 8;
input [dataWidth-1:0] A;
output Y;
reg Y;

   always@(A)
   begin
      Y = &A;
   end
endmodule

module DVL_RED_OR(Y, A); //reduction or
parameter dataWidth= 8;
input [dataWidth-1:0] A;
output Y;
reg Y;

   always@(A)
   begin
      Y = |A;
   end
endmodule

module DVL_RED_NAND(Y, A); //reduction nand
parameter dataWidth= 8;
input [dataWidth-1:0] A;
output Y;
reg Y;

   always@(A)
   begin
      Y = ~&A;
   end
endmodule

module DVL_RED_NOR(Y, A); //reduction nor
parameter dataWidth= 8;
input [dataWidth-1:0] A;
output Y;
reg Y;

   always@(A)
   begin
      Y = ~|A;
   end
endmodule

module DVL_RED_XOR(Y, A); //reduction xor
parameter dataWidth= 8;
input [dataWidth-1:0] A;
output Y;
reg Y;

   always@(A)
   begin
      Y = ^A;
   end
endmodule

module DVL_RED_XNOR(Y, A); //reduction xnor
parameter dataWidth= 8;
input [dataWidth-1:0] A;
output Y;
reg Y;

   always@(A)
   begin
      Y = ~^A;
   end
endmodule

/*================  arithmetic  ================*/
module DVL_ARI_ADD(Y, A, B); //arithmetic add
parameter dataWidthY = 8;
parameter dataWidthA= 8;
parameter dataWidthB= 8;
input [dataWidthA-1:0] A;
input [dataWidthB-1:0] B;
output [dataWidthY-1:0] Y;
reg [dataWidthY-1:0] Y;

   always@(A or B)
   begin
      Y = A+ B;
   end
endmodule

module DVL_ARI_SUB(Y, A, B); //arithmetic subtract
parameter dataWidthY = 8;
parameter dataWidthA= 8;
parameter dataWidthB= 8;
input [dataWidthA-1:0] A;
input [dataWidthB-1:0] B;
output [dataWidthY-1:0] Y;
reg [dataWidthY-1:0] Y;

   always@(A or B)
   begin
      Y = A- B;
   end
endmodule

module DVL_ARI_MUL(Y, A, B); //multiply
parameter dataWidthY = 8;
parameter dataWidthA= 8;
parameter dataWidthB= 8;
input [dataWidthA-1:0] A;
input [dataWidthB-1:0] B;
output [dataWidthY-1:0] Y;
reg [dataWidthY-1:0] Y;

   always@(A or B)
   begin
      Y = A* B;
   end
endmodule

module DVL_ARI_DIV(Y, A, B); //divide
parameter dataWidthY = 8;
parameter dataWidthA= 8;
parameter dataWidthB= 8;
input [dataWidthA-1:0] A;
input [dataWidthB-1:0] B;
output [dataWidthY-1:0] Y;
reg [dataWidthY-1:0] Y;

   always@(A or B)
   begin
      Y = A/ B;
   end
endmodule

module DVL_ARI_MOD(Y, A, B); //modulus
parameter dataWidthY = 8;
parameter dataWidthA= 8;
parameter dataWidthB= 8;
input [dataWidthA-1:0] A;
input [dataWidthB-1:0] B;
output [dataWidthY-1:0] Y;
reg [dataWidthY-1:0] Y;

   always@(A or B)
   begin
      Y = A% B;
   end
endmodule

/*==================shift==================*/
module DVL_SH_L(Y, A, B); //shift left
parameter dataWidthY = 8;
parameter dataWidthA= 8;
parameter shiftBit= 8;
input [dataWidthA-1:0] A;
input [shiftBit-1:0] B;
output [dataWidthY-1:0] Y;
reg [dataWidthY-1:0] Y;

   always@(A or B)
   begin
      Y = A<< B;
   end
endmodule

module DVL_SH_R(Y, A, B); //shift
parameter dataWidthY = 8;
parameter dataWidthA= 8;
parameter shiftBit= 8;
input [dataWidthA-1:0] A;
input [shiftBit-1:0] B;
output [dataWidthY-1:0] Y;
reg [dataWidthY-1:0] Y;

   always@(A or B)
   begin
      Y = A>> B;
   end
endmodule

/*================  relational  ================*/
module DVL_GEQ(Y, A, B); //greater and equal, >=
parameter dataWidthA= 8;
parameter dataWidthB= 8;
input [dataWidthA-1:0] A;
input [dataWidthB-1:0] B;
output Y;
reg Y;

   always@(A or B)
   begin
      Y = A>= B;
   end
endmodule

module DVL_GREATER(Y, A, B); //greater, >
parameter dataWidthA= 8;
parameter dataWidthB= 8;
input [dataWidthA-1:0] A;
input [dataWidthB-1:0] B;
output Y;
reg Y;

   always@(A or B)
   begin
      Y = A> B;
   end
endmodule

module DVL_LESS(Y, A, B); //less, <
parameter dataWidthA= 8;
parameter dataWidthB= 8;
input [dataWidthA-1:0] A;
input [dataWidthB-1:0] B;
output Y;
reg Y;

   always@(A or B)
   begin
      Y = A< B;
   end
endmodule

module DVL_LEQ(Y, A, B); //less and equal, <=
parameter dataWidthA= 8;
parameter dataWidthB= 8;
input [dataWidthA-1:0] A;
input [dataWidthB-1:0] B;
output Y;
reg Y;

   always@(A or B)
   begin
      Y = A<= B;
   end
endmodule

/*================  equality  ================*/

module DVL_EQ(Y, A, B); //logical equality, ==
parameter dataWidthA= 8;
parameter dataWidthB= 8;
input [dataWidthA-1:0] A;
input [dataWidthB-1:0] B;
output Y;
reg Y;

   always@(A or B)
   begin
      if(A== B)
         Y = 1'b1;
      else
         Y = 1'b0;
   end
endmodule

module DVL_NEQ(Y, A, B); //logical inequality, !=
parameter dataWidthA= 8;
parameter dataWidthB= 8;
input [dataWidthA-1:0] A;
input [dataWidthB-1:0] B;
output Y;
reg Y;

   always@(A or B)
   begin
      if(A!= B)
         Y = 1'b1;
      else
         Y = 1'b0;
   end
endmodule

module DVL_CASE_EQ(Y, A, B); //case equality, ===
parameter dataWidthA= 8;
parameter dataWidthB= 8;
input [dataWidthA-1:0] A;
input [dataWidthB-1:0] B;
output Y;
reg Y;

   always@(A or B)
   begin
      Y = A=== B;
   end
endmodule

module DVL_CASE_NEQ(Y, A, B); //case inequality, !==
parameter dataWidthA= 8;
parameter dataWidthB= 8;
input [dataWidthA-1:0] A;
input [dataWidthB-1:0] B;
output Y;
reg Y;

   always@(A or B)
   begin
      Y = A!== B;
   end
endmodule

/*================  bitwise  ================*/
module DVL_BW_NOT(Y, A); //bitwise not
parameter dataWidth= 8;
input [dataWidth-1:0] A;
output [dataWidth-1:0] Y;
reg [dataWidth-1:0] Y;

   always@(A)
   begin
      Y = ~A;
   end
endmodule

module DVL_BW_AND(Y, A, B); //bitwise and
parameter dataWidth= 8;
input [dataWidth-1:0] A;
input [dataWidth-1:0] B;
output [dataWidth-1:0] Y;
reg [dataWidth-1:0] Y;

   always@(A or B)
   begin
      Y = A& B;
   end
endmodule

module DVL_BW_XOR(Y, A, B); //bitwise xor
parameter dataWidth= 8;
input [dataWidth-1:0] A;
input [dataWidth-1:0] B;
output [dataWidth-1:0] Y;
reg [dataWidth-1:0] Y;

   always@(A or B)
   begin
      Y = A^ B;
   end
endmodule

module DVL_BW_XNOR(Y, A, B); //bitwise xnor
parameter dataWidth= 8;
input [dataWidth-1:0] A;
input [dataWidth-1:0] B;
output [dataWidth-1:0] Y;
reg [dataWidth-1:0] Y;

   always@(A or B)
   begin
      Y = A~^ B;
   end
endmodule

module DVL_BW_OR(Y, A, B); //bitwise or
parameter dataWidth= 8;
input [dataWidth-1:0] A;
input [dataWidth-1:0] B;
output [dataWidth-1:0] Y;
reg [dataWidth-1:0] Y;

   always@(A or B)
   begin
      Y = A| B;
   end
endmodule

/*================  logical  ================*/
module DVL_LOG_NOT(Y, A); //logical not
parameter dataWidth= 8;
input [dataWidth-1:0] A;
output Y;
reg Y;

   always@(A)
   begin
      Y = !A;
   end
endmodule

module DVL_LOG_AND(Y, A, B); //logical and
parameter dataWidth= 8;
input [dataWidth-1:0] A;
input [dataWidth-1:0] B;
output Y;
reg Y;

   always@(A or B)
   begin
      Y = A&& B;
   end
endmodule

module DVL_LOG_OR(Y, A, B); //logical or
parameter dataWidth= 8;
input [dataWidth-1:0] A;
input [dataWidth-1:0] B;
output Y;
reg Y;

   always@(A or B)
   begin
      Y = A|| B;
   end
endmodule


