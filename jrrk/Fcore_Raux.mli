open BinInt
open BinNums
open Datatypes
open Fcore_Zaux
open Raxioms
open Rbasic_fun
open Rdefinitions
open Rpower
open Specif

type __ = Obj.t

val coq_P2R : positive -> coq_R

val coq_Z2R : coq_Z -> coq_R

val coq_Rcompare : coq_R -> coq_R -> comparison

val coq_Rle_bool : coq_R -> coq_R -> bool

val coq_Rlt_bool : coq_R -> coq_R -> bool

val coq_Req_bool : coq_R -> coq_R -> bool

val coq_Zfloor : coq_R -> coq_Z

val coq_Zceil : coq_R -> coq_Z

val coq_Ztrunc : coq_R -> coq_Z

val coq_Zaway : coq_R -> coq_Z

val bpow : radix -> coq_Z -> coq_R

type ln_beta_prop =
  coq_Z
  (* singleton inductive, whose constructor was Build_ln_beta_prop *)

val ln_beta_prop_rect :
  radix -> coq_R -> (coq_Z -> __ -> 'a1) -> ln_beta_prop -> 'a1

val ln_beta_prop_rec :
  radix -> coq_R -> (coq_Z -> __ -> 'a1) -> ln_beta_prop -> 'a1

val ln_beta_val : radix -> coq_R -> ln_beta_prop -> coq_Z

val ln_beta : radix -> coq_R -> ln_beta_prop

val cond_Ropp : bool -> coq_R -> coq_R

