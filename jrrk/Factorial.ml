open Datatypes
open Peano

(** val fact : nat -> nat **)

let rec fact = function
| O -> S O
| S n0 -> mult (S n0) (fact n0)

