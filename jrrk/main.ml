open Datatypes;;
open MoreStlc;;
open STLCExtended;;
open Examples;;

type reduced =
  | Int of int*int
  | Reduced of int*tm

let rec asNat = function
  | 0 -> Datatypes.O
  | n -> Datatypes.S (asNat(n-1))

let rec fromNat = function
  | Datatypes.O -> 0
  | Datatypes.S num -> 1 + fromNat num

let from_tm = function Coq_tm_nat n -> Some (fromNat n) | _ -> None

let rec reduce' cnt prev = function
    | Coq_tm_nat n -> Int (cnt,fromNat n)
    | oth -> if prev <> oth && (cnt < 100) then reduce' (cnt+1) oth (reduce oth) else Reduced (cnt,oth)

let reduce' = reduce' 0 (Coq_tm_nil Coq_ty_Nat)

let n = reduce' Numtest.test;;

let p = reduce' Prodtest.test;;
let lt = reduce' LetTest.test;;
let s1 = reduce' Sumtest1.test;;
let s2 = reduce' Sumtest2.test;;
let lst = reduce' ListTest.test;;
let f n = reduce' (FixTest1.fact_calc (asNat n));;
let f1 = f 5;;
let f2 = reduce' (FixTest2.map);;
let f3 = reduce' (FixTest3.equal);;
let f4 = reduce' (FixTest4.eotest);;
