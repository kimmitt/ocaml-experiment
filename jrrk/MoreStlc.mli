open Datatypes
open Peano
open SfLib

module STLCExtended : 
 sig 
  type ty =
  | Coq_ty_arrow of ty * ty
  | Coq_ty_prod of ty * ty
  | Coq_ty_sum of ty * ty
  | Coq_ty_List of ty
  | Coq_ty_Nat
  
  val ty_rect :
    (ty -> 'a1 -> ty -> 'a1 -> 'a1) -> (ty -> 'a1 -> ty -> 'a1 -> 'a1) -> (ty
    -> 'a1 -> ty -> 'a1 -> 'a1) -> (ty -> 'a1 -> 'a1) -> 'a1 -> ty -> 'a1
  
  val ty_rec :
    (ty -> 'a1 -> ty -> 'a1 -> 'a1) -> (ty -> 'a1 -> ty -> 'a1 -> 'a1) -> (ty
    -> 'a1 -> ty -> 'a1 -> 'a1) -> (ty -> 'a1 -> 'a1) -> 'a1 -> ty -> 'a1
  
  type tm =
  | Coq_tm_var of id
  | Coq_tm_app of tm * tm
  | Coq_tm_abs of id * ty * tm
  | Coq_tm_pair of tm * tm
  | Coq_tm_fst of tm
  | Coq_tm_snd of tm
  | Coq_tm_inl of ty * tm
  | Coq_tm_inr of ty * tm
  | Coq_tm_case of tm * id * tm * id * tm
  | Coq_tm_nil of ty
  | Coq_tm_cons of tm * tm
  | Coq_tm_lcase of tm * tm * id * id * tm
  | Coq_tm_nat of nat
  | Coq_tm_succ of tm
  | Coq_tm_pred of tm
  | Coq_tm_mult of tm * tm
  | Coq_tm_if0 of tm * tm * tm
  | Coq_tm_let of id * tm * tm
  | Coq_tm_fix of tm
  
  val tm_rect :
    (id -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (id -> ty -> tm -> 'a1
    -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> 'a1) -> (tm
    -> 'a1 -> 'a1) -> (ty -> tm -> 'a1 -> 'a1) -> (ty -> tm -> 'a1 -> 'a1) ->
    (tm -> 'a1 -> id -> tm -> 'a1 -> id -> tm -> 'a1 -> 'a1) -> (ty -> 'a1)
    -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> id -> id
    -> tm -> 'a1 -> 'a1) -> (nat -> 'a1) -> (tm -> 'a1 -> 'a1) -> (tm -> 'a1
    -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 ->
    tm -> 'a1 -> 'a1) -> (id -> tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1
    -> 'a1) -> tm -> 'a1
  
  val tm_rec :
    (id -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (id -> ty -> tm -> 'a1
    -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> 'a1) -> (tm
    -> 'a1 -> 'a1) -> (ty -> tm -> 'a1 -> 'a1) -> (ty -> tm -> 'a1 -> 'a1) ->
    (tm -> 'a1 -> id -> tm -> 'a1 -> id -> tm -> 'a1 -> 'a1) -> (ty -> 'a1)
    -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> id -> id
    -> tm -> 'a1 -> 'a1) -> (nat -> 'a1) -> (tm -> 'a1 -> 'a1) -> (tm -> 'a1
    -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 ->
    tm -> 'a1 -> 'a1) -> (id -> tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1
    -> 'a1) -> tm -> 'a1
  
  val subst : id -> tm -> tm -> tm
  
  type context = ty partial_map
  
  val redvalue : tm -> bool
  
  val reduce : tm -> tm
  
  module Examples : 
   sig 
    module Numtest : 
     sig 
      val test : tm
     end
    
    module Prodtest : 
     sig 
      val test : tm
     end
    
    module LetTest : 
     sig 
      val test : tm
     end
    
    module Sumtest1 : 
     sig 
      val test : tm
     end
    
    module Sumtest2 : 
     sig 
      val test : tm
     end
    
    module ListTest : 
     sig 
      val test : tm
     end
    
    module FixTest1 : 
     sig 
      val fact : tm
      
      val fact_calc : nat -> tm
     end
    
    module FixTest2 : 
     sig 
      val map : tm
     end
    
    module FixTest3 : 
     sig 
      val equal : tm
     end
    
    module FixTest4 : 
     sig 
      val eotest : tm
     end
   end
 end

