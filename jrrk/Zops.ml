open BinInt
open BinNums
open Zbool

(** val coq_Zplus : coq_Z -> coq_Z -> coq_Z **)

let coq_Zplus x y =
  Z.add x y

(** val coq_Zminus : coq_Z -> coq_Z -> coq_Z **)

let coq_Zminus m n =
  Z.sub m n

(** val coq_Zsucc : coq_Z -> coq_Z **)

let coq_Zsucc x =
  Z.succ x

(** val coq_Zpred : coq_Z -> coq_Z **)

let coq_Zpred x =
  Z.pred x

(** val coq_Zmult : coq_Z -> coq_Z -> coq_Z **)

let coq_Zmult x y =
  Z.mul x y

(** val coq_Zdiv : coq_Z -> coq_Z -> coq_Z **)

let coq_Zdiv a b =
  Z.div a b

(** val coq_Zmod : coq_Z -> coq_Z -> coq_Z **)

let coq_Zmod a b =
  Z.modulo a b

(** val coq_Zle_bool : coq_Z -> coq_Z -> bool **)

let coq_Zle_bool x y =
  Z.leb x y

(** val coq_Zge_bool : coq_Z -> coq_Z -> bool **)

let coq_Zge_bool x y =
  Z.geb x y

(** val coq_Zlt_bool : coq_Z -> coq_Z -> bool **)

let coq_Zlt_bool x y =
  Z.ltb x y

(** val coq_Zgt_bool : coq_Z -> coq_Z -> bool **)

let coq_Zgt_bool x y =
  Z.gtb x y

(** val coq_Zeq_bool : coq_Z -> coq_Z -> bool **)

let coq_Zeq_bool x y =
  coq_Zeq_bool x y

(** val coq_Zneq_bool : coq_Z -> coq_Z -> bool **)

let coq_Zneq_bool x y =
  coq_Zneq_bool x y

