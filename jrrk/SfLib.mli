open Datatypes
open EqNat

type __ = Obj.t

val ble_nat : nat -> nat -> bool

type 'x relation = __

val next_nat_rect : nat -> 'a1 -> nat -> 'a1

val next_nat_rec : nat -> 'a1 -> nat -> 'a1

val empty_relation_rect : nat -> nat -> 'a1

val empty_relation_rec : nat -> nat -> 'a1

type id =
  nat
  (* singleton inductive, whose constructor was Id *)

val id_rect : (nat -> 'a1) -> id -> 'a1

val id_rec : (nat -> 'a1) -> id -> 'a1

val beq_id : id -> id -> bool

type 'a partial_map = id -> 'a option

val empty : 'a1 partial_map

val extend : 'a1 partial_map -> id -> 'a1 -> id -> 'a1 option

