open Mylib

let mydiv denom = try
  print_int (355000000 / denom); print_newline()
   with
   | Division_by_zero -> print_endline "Divide by zero trap"
   | Invalid_argument err -> print_endline (err^": exception caught")

let mymod num denom = try
  print_int (num mod denom); print_newline()
   with
   | Division_by_zero -> print_endline "Divide by zero trap"
   | Invalid_argument err -> print_endline (err^": exception caught")

let _ =
(* *)
  mydiv 113;
  mydiv 1;
(* *)
  mydiv 0;
(* *)
  mymod 12345 10;
  mymod 12345 0

    
