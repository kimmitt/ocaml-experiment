open BinInt
open BinNums

type __ = Obj.t

val eq_dep_elim : 'a1 -> 'a2 -> 'a1 -> 'a2

val coq_Zeven : coq_Z -> bool

type radix =
  coq_Z
  (* singleton inductive, whose constructor was Build_radix *)

val radix_rect : (coq_Z -> __ -> 'a1) -> radix -> 'a1

val radix_rec : (coq_Z -> __ -> 'a1) -> radix -> 'a1

val radix_val : radix -> coq_Z

val cond_Zopp : bool -> coq_Z -> coq_Z

