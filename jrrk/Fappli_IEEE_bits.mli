open BinInt
open BinNums
open Datatypes
open Fappli_IEEE
open Peano
open Zbool

val join_bits : coq_Z -> coq_Z -> bool -> coq_Z -> coq_Z -> coq_Z

val split_bits : coq_Z -> coq_Z -> coq_Z -> ((bool, coq_Z) prod, coq_Z) prod

val bits_of_binary_float : coq_Z -> coq_Z -> binary_float -> coq_Z

val split_bits_of_binary_float :
  coq_Z -> coq_Z -> binary_float -> ((bool, coq_Z) prod, coq_Z) prod

val binary_float_of_bits_aux : coq_Z -> coq_Z -> coq_Z -> full_float

val binary_float_of_bits : coq_Z -> coq_Z -> coq_Z -> binary_float

type binary32 = binary_float

val default_nan_pl32 : (bool, nan_pl) prod

val unop_nan_pl32 : binary32 -> (bool, nan_pl) prod

val binop_nan_pl32 : binary32 -> binary32 -> (bool, nan_pl) prod

val b32_opp : binary_float -> binary_float

val b32_plus : mode -> binary_float -> binary_float -> binary_float

val b32_minus : mode -> binary_float -> binary_float -> binary_float

val b32_mult : mode -> binary_float -> binary_float -> binary_float

val b32_div : mode -> binary_float -> binary_float -> binary_float

val b32_sqrt : mode -> binary_float -> binary_float

val b32_of_bits : coq_Z -> binary32

val bits_of_b32 : binary32 -> coq_Z

type binary64 = binary_float

val default_nan_pl64 : (bool, nan_pl) prod

val unop_nan_pl64 : binary64 -> (bool, nan_pl) prod

val binop_nan_pl64 : binary64 -> binary64 -> (bool, nan_pl) prod

val b64_opp : binary_float -> binary_float

val b64_plus : mode -> binary_float -> binary_float -> binary_float

val b64_minus : mode -> binary_float -> binary_float -> binary_float

val b64_mult : mode -> binary_float -> binary_float -> binary_float

val b64_div : mode -> binary_float -> binary_float -> binary_float

val b64_sqrt : mode -> binary_float -> binary_float

val b64_of_bits : coq_Z -> binary64

val bits_of_b64 : binary64 -> coq_Z

