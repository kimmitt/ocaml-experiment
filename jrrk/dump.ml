open Datatypes
open Mylib
open BinNums
open BinPos
open BinNat
open BinInt
open Ml_decls
open MoreZStlc
open STLCExtended
open Examples
open Fappli_IEEE
open Fappli_IEEE_bits
open Zops
open To_from_Z
(*
open String_of_float
*)

let rec dumptyp = function
  | TUnit -> "unit"
  | TBool -> "bool"
  | TChar -> "char"
  | TInt -> "int"
  | TFloat -> "float"
  | TString -> "string"
  | TUser u -> u
  | TVar ty' -> (match !ty' with Some ty -> "TVar " ^ dumptyp ty | None -> "TNone")
  | TArray typ -> (dumptyp typ)^" option"
  | TFun(ty1,ty2) -> String.concat "" (List.map (fun arg -> dumptyp arg ^ " -> ") ty1) ^ dumptyp ty2
  | TTuple ty -> String.concat " * " (List.map dumptyp ty)
  | Unknown -> "unknown"

let rec dump4exp impl = function
  | Int n -> " Int "^string_of_int n
  | Char n -> " Char "^String.make 1 n
  | String str -> " String \""^str^"\""
  | Bool bool -> " Bool "^string_of_bool bool
  | Var t -> " Var "^t
  | App (p4exp1, p4exp2) -> " App (" ^ dump4exp impl p4exp1 ^ ", [" ^ dump4lst impl p4exp2 ^ "]) "
  | If (p4exp1, p4exp2, p4exp3) ->
     " If (" ^
     dump4exp impl p4exp1 ^
     ") Then (" ^
     dump4exp impl p4exp2 ^
     ") Else (" ^
     dump4exp impl p4exp3 ^
     ") "
  | List (p4exp) -> " List [" ^ dump4lst impl p4exp ^ "] "
  | Match (str, p4exp) -> " Match " ^ str ^ " with [" ^ dump4lst impl p4exp ^ "] "
  | TypCons (str, ty) -> str ^ " of " ^ dumptyp ty
  | ArgCons (exp, ty) -> dumptyp ty ^ " ( " ^ dump4exp impl exp ^ " ) "
  | Tuple (p4exp1 :: p4exp2 :: []) -> " Pair (" ^ dump4exp impl p4exp1 ^ ", " ^ dump4exp impl p4exp2 ^ ") "
  | LetRec ({name=nam,tnam; args=arg; body=body}, nxt) -> " Let Rec " ^ nam ^ " " ^ (String.concat " " (List.map fst arg)) ^ " = " ^ dump4exp impl body ^ ") in "^dump4exp impl nxt
  | Unit -> "()"
  | Float f -> "Float "^impl.string_of_float f
  | Not p -> "Not "^dump4exp impl p
  | Neg p -> "Neg "^dump4exp impl p
  | Add (p, q) -> "Add ("^dump4exp impl p^", "^dump4exp impl q^")"
  | Sub (p, q) -> "Sub ("^dump4exp impl p^", "^dump4exp impl q^")"
  | Mul (p, q) -> "Mul ("^dump4exp impl p^", "^dump4exp impl q^")"
  | Div (p, q) -> "Div ("^dump4exp impl p^", "^dump4exp impl q^")"
  | Mod (p, q) -> "Mod ("^dump4exp impl p^", "^dump4exp impl q^")"
  | FNeg p -> "FNeg "^dump4exp impl p
  | FAdd (p, q) -> "FAdd ("^dump4exp impl p^", "^dump4exp impl q^")"
  | FSub (p, q) -> "FSub ("^dump4exp impl p^", "^dump4exp impl q^")"
  | FMul (p, q) -> "FMul ("^dump4exp impl p^", "^dump4exp impl q^")"
  | FDiv (p, q) -> "FDiv ("^dump4exp impl p^", "^dump4exp impl q^")"
  | Eq (p, q) -> "Eq ("^dump4exp impl p^", "^dump4exp impl q^")"
  | LE (p, q) -> "Le ("^dump4exp impl p^", "^dump4exp impl q^")"
  | LT (p, q) -> "Lt ("^dump4exp impl p^", "^dump4exp impl q^")"
  | GE (p, q) -> "Ge ("^dump4exp impl p^", "^dump4exp impl q^")"
  | GT (p, q) -> "Gt ("^dump4exp impl p^", "^dump4exp impl q^")"
  | Let ((id,typ), exp, nxt) -> "Let "^id^" = "^dump4exp impl exp^" in "^dump4exp impl nxt
  | LetTuple (_, _, _) -> "LetTuple (_, _, _)"
  | Compose (arg1, arg2) -> "Compose ("^dump4exp impl arg1^", "^dump4exp impl arg2^")"
  | Abstraction (arg1, arg2) -> "Abstraction ("^dump4exp impl arg1^", "^dump4exp impl arg2^")"
  | Declare (kind, arg1, arg2, arg3) -> "Declare ("^string_of_bool kind^", "^dump4exp impl arg1^", "^dump4exp impl arg2^", "^dump4exp impl arg3^")"
  | Tuple _ -> "Tuple _"
  | INTEGER _ -> "INTEGER _"
  | STRING _ -> "STRING _"
  | LISTCONS _ -> "LISTCONS _"
  | TYPCONS (_, _) -> "TYPCONS (_, _)"
  | TOP arg -> ("TOP ([ "^dump4lst impl arg^" ])")
  | STR arg -> ("STR ("^dump4exp impl arg^")")
  | LET (dst, src) -> "LET (["^dump4lst impl dst^" ], "^dump4exp impl src^")"
  | ANY _ -> "ANY _"
  | LBL _ -> "LBL _"
  | TYPE _ -> "TYPE _"
  | TYPDECL _ -> "TYPDECL _"
  | REC _ -> "REC _"
  | NONREC _ -> "NONREC _"
  | PATTERN (fn, _) -> "PATTERN ("^fn^", _)"
  | MATCH (_, _) -> "MATCH (_, _)"
  | PATEXP (_, _) -> "PATEXP (_, _)"
  | FORMAL (_, _) -> "FORMAL (_, _)"
  | TUPLE (_) -> "TUPLE (_)"
  | TYPLOC (_, _) -> "TYPLOC (_, _)"
  | FUNARG _ -> "FUNARG _"
  | PATCASE (_, _, _) -> "PATCASE (_, _, _)"
  | CONSTRAINED (_, _) -> "CONSTRAINED (_, _)"
  | APPLY1 (_, _) -> "APPLY1 (_, _)"
  | APPLY2 (_, _, _) -> "APPLY2 (_, _, _)"
  | APPLYLST (_, _) -> "APPLYLST (_, _)"

and dump4lst impl p4exp = String.concat ";" (List.map (dump4exp impl) p4exp)

let rec dump4top impl = function
  | Type (str, p4exp) -> " Type " ^ str ^ " = " ^ (String.concat "|" (List.map (dump4exp impl) p4exp))
  | LetTop ((id,typ), exp) -> "Let "^id^" = "^dump4exp impl exp
  | LetRecTop {name=nam,tnam; args=arg; body=body} -> " Let Rec " ^ nam ^ " " ^ (String.concat " " (List.map fst arg)) ^ " = " ^ dump4exp impl body ^ ")"
  | TopIn exp -> " In " ^ dump4exp impl exp
  | Ctype typ -> " Ctype "

let rec dumptyp = function
  | Coq_ty_arrow (typ1, typ2) -> dumptyp typ1 ^ " -> " ^ dumptyp typ2
  | Coq_ty_Nat -> "nat"
  | Coq_ty_Flt -> "float"
  | Coq_ty_prod (typ1, typ2) -> "prod (" ^ dumptyp typ1 ^ ", " ^ dumptyp typ2 ^ " ) "
  | Coq_ty_sum (typ1, typ2) -> "sum (" ^ dumptyp typ1 ^ ", " ^ dumptyp typ2 ^ " ) "
  | Coq_ty_List typ -> "list (" ^ dumptyp typ ^ " ) "

let dumpvar codes id = " Var " ^ decode codes id

let rec dumpentry' codes = function
  | Coq_tm_var id -> dumpvar codes.code id
  | Coq_tm_app (tm1, tm2) -> " App (" ^ dumpentry' codes tm1 ^ ", " ^ dumpentry' codes tm2 ^ ") "
  | Coq_tm_abs (id,ty,tm) -> "\n\tAbs (" ^ dumpvar codes.code id ^ ", " ^ dumptyp ty ^ ", " ^ dumpentry' codes tm ^ ") "
  | Coq_tm_pair (tm1, tm2) -> " Pair (" ^ dumpentry' codes tm1 ^ ", " ^ dumpentry' codes tm2 ^ ") "
  | Coq_tm_fst tm -> " Fst (" ^ dumpentry' codes tm ^ ") "
  | Coq_tm_snd tm -> " Snd (" ^ dumpentry' codes tm ^ ") "
  | Coq_tm_inl(ty, tm) -> " Inl (" ^ dumptyp ty ^ ", " ^ dumpentry' codes tm ^ ") "
  | Coq_tm_inr(ty, tm) -> " Inr (" ^ dumptyp ty ^ ", " ^ dumpentry' codes tm ^ ") "
  | Coq_tm_case(tm1, id1, tm2, id2, tm3) -> " Case (" ^ dumpentry' codes tm1 ^ ", " ^ dumpvar codes.code id1 ^ ", " ^ dumpentry' codes tm2 ^ ", " ^ dumpvar codes.code id2 ^ ", " ^ dumpentry' codes tm3 ^ ") "
  | Coq_tm_nil(ty) -> " Nil (" ^ dumptyp ty ^ ") "
  | Coq_tm_cons(tm1, tm2) -> " Cons (" ^ dumpentry' codes tm1 ^ ", " ^ dumpentry' codes tm2 ^ ") "
  | Coq_tm_lcase(tm1, tm2, id1, id2, tm3) -> "Lcase (" ^ dumpentry' codes tm1 ^ ", " ^ dumpentry' codes tm2 ^ ", " ^ dumpvar codes.code id1 ^ ", " ^ dumpvar codes.code id2 ^ ", " ^ dumpentry' codes tm3 ^ ") "
  | Coq_tm_nat coq_Z -> (" Int "^fromZ coq_Z)
  | Coq_tm_flt coq_flt -> " Float "^codes.impl.string_of_float coq_flt
  | Coq_tm_succ(tm) -> " Succ (" ^ dumpentry' codes tm ^ ") "
  | Coq_tm_pred(tm) -> " Pred (" ^ dumpentry' codes tm ^ ") "
  | Coq_tm_add (tm1, tm2) -> " Add (" ^ dumpentry' codes tm1 ^ ", " ^ dumpentry' codes tm2 ^ ") "
  | Coq_tm_sub (tm1, tm2) -> " Sub (" ^ dumpentry' codes tm1 ^ ", " ^ dumpentry' codes tm2 ^ ") "
  | Coq_tm_mult (tm1, tm2) -> " Mul (" ^ dumpentry' codes tm1 ^ ", " ^ dumpentry' codes tm2 ^ ") "
  | Coq_tm_div (tm1, tm2) -> " Div (" ^ dumpentry' codes tm1 ^ ", " ^ dumpentry' codes tm2 ^ ") "
  | Coq_tm_mod (tm1, tm2) -> " Mod (" ^ dumpentry' codes tm1 ^ ", " ^ dumpentry' codes tm2 ^ ") "
  | Coq_tm_addf (tm1, tm2) -> " FAdd (" ^ dumpentry' codes tm1 ^ ", " ^ dumpentry' codes tm2 ^ ") "
  | Coq_tm_subf (tm1, tm2) -> " FSub (" ^ dumpentry' codes tm1 ^ ", " ^ dumpentry' codes tm2 ^ ") "
  | Coq_tm_mulf (tm1, tm2) -> " FMul (" ^ dumpentry' codes tm1 ^ ", " ^ dumpentry' codes tm2 ^ ") "
  | Coq_tm_divf (tm1, tm2) -> " FDiv (" ^ dumpentry' codes tm1 ^ ", " ^ dumpentry' codes tm2 ^ ") "
  | Coq_tm_if0 (tm1, tm2, tm3) ->
     " If (" ^
     dumpentry' codes tm1 ^
     ") = 0 Then (" ^
     dumpentry' codes tm2 ^
     ") Else (" ^
     dumpentry' codes tm3 ^
     ") "
  | Coq_tm_iflt (tm1, tm2, tm3) ->
     " If (" ^
     dumpentry' codes tm1 ^
     ") < 0 Then (" ^
     dumpentry' codes tm2 ^
     ") Else (" ^
     dumpentry' codes tm3 ^
     ") "
  | Coq_tm_let(id1, tm1, tm2) -> "\n\tLet " ^ decode codes.code id1 ^ " = " ^ dumpentry' codes tm1 ^ " in\n\t" ^ dumpentry' codes tm2
  | Coq_tm_fix tm -> " Fix (" ^ dumpentry' codes tm ^ ") "
  | Coq_tm_io(sel, tm1) -> " Io (" ^ string_of_int (fromNat sel) ^ ", " ^ dumpentry' codes tm1 ^ ") "

let dumpentry (codes,cod) = print_endline (dumpentry' codes cod)
