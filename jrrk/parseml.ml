open Mylib
open Ml_decls
open LRTTtypes

    (* instantiating the parser functor *)

module BackusParse = struct
  type t = syntax_t
  type parsevalue = t
  and grammar = production list
  and production = string * choice list       (* rule name -> e1 | e2 | ... *)
  and choice = string * string * parsing list (* choice name, comment, sequence *)
  and semantic = int -> int -> parsevalue -> parsevalue
  and messaging = int -> int -> string -> parsevalue -> unit
  and acceptor = int -> string -> int 
  and parsing =
    | A of acceptor * string              (* parsing test callback *)
    | S of semantic                       (* semantic action *)
    | D of semantic                       (* error recovery by deletion *)
    | M of messaging * string             (* debugging action executed "en passant" *)
    | Z of parsing                        (* e* *)
    | P of parsing                        (* e+ *)
    | Opt of parsing                      (* e? *)
    | L of parsing                        (* lookahead, parsing resumes at the START *)
    | NT of string                        (* nonterminal 'name' *)
    | Empty                               (* epsilon *)
  and parsepoint = {  
      mutable from : parsepoint;              (* source node back pointer *)
      mutable source : string;                (* source node id *)
      mutable pos : int;                      (* text position at start of production *)
      mutable stamp : int;                    (* node stamp for debugging *)
      mutable prod : parsing list;            (* start of production *)
      mutable curs : parsing list;            (* end of production segment for this node  *)
      mutable recursive : parsing list;       (* location of recursive call in this production *)
      mutable hook : parsepoint list;         (* current recursive call of this node *)
      mutable successors : parsepoint list }  (* recursion stack + production list + node successor *)
  let full_debug = 4096
  let run_debug = 8192
  let flush_memo = 16384
  let single_parse = 32768
  let node_stamping = 65536
end

open BackusParse

exception Failed

let demo impl streamparse f = 

   (* incremental parser input *)

let parseloc = ref 0 in
let lines = ref [0] in
let chanstack = ref [] in
let filestack = ref [] in
let startstack = ref [[]] in

let input = ref "" in

let len = ref 0 in

let startrule = ref "" in

let rec stream_line (ix,strm,fd) =
  if !ix >= String.length strm then raise (Failure strm);
  let ch = strm.[!ix] in incr ix;
  if ch <> '\n' then String.make 1 ch^stream_line (ix,strm,fd) else "" in
let rec stream_close (ix,strm,fd) = () in
let charfeed ask lng =
  let pos = ask in
  if ask < 0 || lng < 0 then
    ""
  else begin
    while pos + lng > !len do
      try
	let line = stream_line (List.hd !chanstack) in
	input := !input ^ "\n" ^ line;
	len := String.length !input;
      with
	_ ->
	  if (List.tl !chanstack) = [] then raise Not_found;
	  stream_close (List.hd !chanstack);
	  chanstack := List.tl !chanstack;
	  filestack := List.tl !filestack;
	  lines := List.hd !startstack;
	  startstack := List.tl !startstack;
    done;
   
    for i = (if !lines = [] then 0 else (List.hd !lines) + 1) to pos+lng-1 do
      if !input.[i] = '\n' then lines := i :: !lines;
    done;

    if pos > !parseloc then parseloc := pos;   (* for the 'read' stream interleave *)

    (String.sub !input pos lng)
  end in
    (* "on-the-fly" lexing functions *)
let testeof pos _ =
  (try
    let _ = charfeed pos 0 in (); 
    (try
      let _ = charfeed pos 1 in ();
      0                     (* before eof *)
    with Not_found -> -1)   (* exactly at eof *)
  with Not_found -> 0) in   (* BEYOND 'eof' isn't eof *)
let tsymbol pos name =
  let k = String.length name in
  if String.eqb (try charfeed pos k with Not_found -> "") name then k else 0 in
let nquotchar pos =
  if (charfeed pos 1).[0] = '\\' then
      (* peek one extra char beyond to account for the closing quote  *)
      let _ = charfeed (pos+2) 1 in 
      2
  else
    1 in
let lineskip ptr = while (not (String.eqb (charfeed !ptr 1) "\n")) do  ptr := !ptr +1; done in
let tspace pos _ =
  let ptr = ref pos  in
  (try 
    while (let ch = String.get (charfeed !ptr 1) 0 in ch = ' ' || ch = '\n' || ch = '\t') do
      ptr := !ptr +1;
    done;
    if String.eqb (charfeed !ptr 2) "--" then begin
      ptr := !ptr +2;
      lineskip ptr;
      ptr := !ptr +1;
  end;
  with Not_found -> ());
  (!ptr - pos) in
let stk = ref [] in
let pushvalue pval = stk := pval :: !stk in
let popvalue () =
    let pop = List.hd !stk in
    stk := List.tl !stk;
    pop in
let slet recurse beg pos pval =
  (try let tok = charfeed pos (pos - beg) in
  if impl.debug.debugall then print_endline ("let_rec "^tok);
  with Not_found -> ());
  pushvalue pval; Bool recurse in

let keywords = [ "let"; "rec"; "in"; "if"; "then"; "else"; "mod" ] in

let grammar : grammar = [
  "start", [
    "", "", [NT "osp"; NT "expr"];
    "", "", [NT "osp"; NT "top"];
  ];

  "top", [
    "", "", [NT "osp"; NT "topexp"; NT "expr"; S (fun beg pos pval ->
      print_endline ("top: "^impl.dump pval);
      match popvalue() with List lst ->
	LET (List.rev lst, pval) | oth -> failwith (String.concat ", " (List.map impl.dump [oth;pval])))];
  ];
  
  "lets", [
    "", "", [A(tsymbol, "let"); NT "sp"; A(tsymbol, "rec"); NT "sp"; NT "composition"; S (slet true)];
    "", "", [A(tsymbol, "let"); NT "sp"; NT "composition"; S (slet false)]
  ];

  "let", [
  "let", "declaration", [NT "lets"; NT "assign"; NT "start"; NT "sp"; A(tsymbol, "in"); NT "sp";
			  S (fun beg pos pval ->
      let flg = popvalue() in
      let lft,mid = match popvalue() with List lst ->
	let rlst = List.rev lst in (List.hd rlst,List(List.tl rlst)) | oth -> failwith (impl.dump oth) in
      if impl.debug.debugall then print_endline (String.concat ", " (List.map impl.dump [flg;lft;mid;pval]));
      Declare ((match flg with Bool t -> t | oth -> failwith (impl.dump oth)), lft, mid, pval))]
  ];

  "assign", [
    "", "", [NT "osp"; A(tsymbol, "="); NT "osp"]
  ];

  "topexp", [
  "inlst", "" , [NT "topexp"; NT "let"; S (fun beg pos pval ->
    print_endline ("inlst: "^impl.dump pval);
    
    match popvalue() with
      List lst -> List (pval :: lst)
    | oth -> failwith (String.concat ", " (List.map impl.dump [oth;pval])))];
    "topone", "", [NT "let"; S (fun beg pos pval ->
      print_endline ("topone: "^impl.dump pval);
      List [pval])];
  ];

  "expr", [
  "fundecl", "function declaration",
  [NT "osp"; A(tsymbol, "fun"); NT "sp"; NT "patt"; NT "osp"; A(tsymbol, "->"); NT "osp"; NT "expr";
    S (fun beg pos pval -> Abstraction (popvalue (), pval))];
  "fadd", "", [NT "subexp"; NT "osp"; A(tsymbol, "+."); NT "osp"; NT "subexp";
		S (fun beg pos pval -> FAdd (popvalue (), pval))];
  "fsub", "", [NT "subexp"; NT "osp"; A(tsymbol, "-."); NT "osp"; NT "subexp";
		S (fun beg pos pval -> FSub (popvalue (), pval))];
  "addition", "", [NT "subexp"; NT "osp"; A(tsymbol, "+"); NT "osp"; NT "subexp";
		    S (fun beg pos pval -> Add (popvalue (), pval))];
  "subtraction", "", [NT "subexp"; NT "osp"; A(tsymbol, "-"); NT "osp"; NT "subexp";
		       S (fun beg pos pval -> Sub (popvalue (), pval))];
  "if", "", [NT "osp"; A (tsymbol, "if") ; NT "osp"; NT "boolean"; NT "osp"; A (tsymbol, "then") ;
	      NT "expr"; NT "osp"; A (tsymbol, "else"); NT "osp"; NT "expr";
		S (fun beg pos pval -> let mid = popvalue () in If (popvalue (), mid, pval))];
    "subexp", "", [NT "subexp"];
    "eof", "", [A (testeof, "EOF")]; 
  ];

  "subexp", [
  
  "funappl", "", [NT "primary"; NT "sp"; NT "funargs"; S (fun beg pos pval -> App (popvalue (),
      match pval with List lst -> List.rev lst | oth -> failwith (impl.dump oth)))];
  "fmult", "", [NT "primary"; NT "osp"; A(tsymbol, "*."); NT "osp"; NT "primary";
		 S (fun beg pos pval -> FMul (popvalue (), pval))];
  "fdiv", "", [NT "primary"; NT "osp"; A(tsymbol, "/."); NT "osp"; NT "primary";
		S (fun beg pos pval -> FDiv (popvalue (), pval))];
  "multiplication", "", [NT "subexp"; NT "osp"; A(tsymbol, "*"); NT "osp"; NT "primary";
			  S (fun beg pos pval -> Mul (popvalue (), pval))];
  "division", "", [NT "primary"; NT "osp"; A(tsymbol, "/"); NT "osp"; NT "primary";
		    S (fun beg pos pval -> Div (popvalue (), pval))];
  "modulo", "", [NT "primary"; NT "osp"; A(tsymbol, "mod"); NT "osp"; NT "primary";
		  S (fun beg pos pval -> Mod (popvalue (), pval))];
  "primary", "", [NT "primary"];
  "eof", "", [A (testeof, "EOF")];
  ];
  
  "funargs", [
    "arglist", "" , [NT "funargs"; NT "sp"; NT "primary"; S (fun beg pos pval ->
      match popvalue() with List lst -> List (pval :: lst) | oth ->
	failwith (String.concat ", " (List.map impl.dump [oth;pval])))];
    "onearg", "", [NT "primary"; S (fun beg pos pval -> List [pval])];
    "eof", "", [A (testeof, "EOF"); S (fun beg pos pval -> List [])];
  ];

  "primary", [
    "unit", "", [NT "osp"; A (tsymbol, "("); A(tsymbol, ")"); S (fun _ _ pval -> pushvalue pval; Unit)];
    "paren", "", [NT "osp"; A (tsymbol, "("); NT "osp"; NT "start"; NT "osp"; A(tsymbol, ")")];
    "ident", "", [NT "osp"; NT "patt"];
    "float", "", [NT "osp"; NT "float"];
    "integer", "", [NT "osp"; NT "integer"];
    "eof", "", [A (testeof, "EOF")]; 
  ];

   "float", [
    "float", "", [A ((fun pos _ ->
  let ptr = ref pos in
  (try lineskip ptr; with Not_found -> ());  
  let line = charfeed pos (!ptr - pos) and need_dot = ref true and k = ref 0 in
  while (!k < String.length line) && (((line.[!k] >= '0') && (line.[!k] <= '9')) || ((line.[!k] = '.') && !k > 0)) do
    if line.[!k] = '.' then need_dot := false;
    incr k
  done;
  if !need_dot then 0 else
    begin
      if impl.debug.debugall && !k > 0 then
	print_endline ("Float "^string_of_int !ptr^" "^string_of_int(!k)^" '"^(String.sub line 0 !k)^"'");
      !k
    end),"float"); S (fun beg pos pval -> pushvalue pval;
  let res = ref Unit in
  let str = charfeed (beg) (pos - beg) in
  if impl.debug.debugall then print_endline ("Float "^str);
  (try
    res := Float (impl.float_of_string str)
  with _ -> ());
  !res)];
  ];

   "integer", [
  "", "", [A ((fun pos _ -> let ptr = ref pos in (try lineskip ptr; with Not_found -> ());  
  let line = charfeed pos (!ptr - pos) in
  let k = ref 0 in
  while (!k < String.length line) && (line.[!k] >= '0') && (line.[!k] <= '9') do incr k done;
  if !k > 0 && impl.debug.debugall then print_endline ("Integer "^string_of_int !k^" '"^(String.sub line 0 !k)^"'\n");
  !k), "integer"); S (fun beg pos pval -> pushvalue pval;
  let res = ref Unit in
  let str = charfeed (beg) (pos - beg) in
  if impl.debug.debugall then print_endline ("Integer "^str);
  (try
    res := Int (int_of_string str)
  with _ -> ());
  !res) ];
  ];

  "boolean", [
  "paren", "", [NT "osp"; A(tsymbol, "("); NT "osp"; NT "boolean"; NT "osp"; A(tsymbol, ")")];
  "equality", "", [NT "expr"; NT "assign"; NT "expr";
		    S (fun beg pos pval -> Eq (popvalue (), pval))];
  "less", "", [NT "expr"; NT "osp"; A(tsymbol, "<"); NT "osp"; NT "expr";
		S (fun beg pos pval -> LT (popvalue (), pval))];
  "greater", "", [NT "expr"; NT "osp"; A(tsymbol, ">"); NT "osp"; NT "expr";
		   S (fun beg pos pval -> GT (popvalue (), pval))];
  "lessequal", "", [NT "expr"; NT "osp"; A(tsymbol, "<="); NT "osp"; NT "expr";
		     S (fun beg pos pval -> LE (popvalue (), pval))];
  "greaterequal", "", [NT "expr"; NT "osp"; A(tsymbol, ">="); NT "osp"; NT "expr";
			S (fun beg pos pval -> GE (popvalue (), pval))];
    "", "", [NT "primary"];
    "false", "", [A (tsymbol, "false") ; S (fun beg pos pval -> pushvalue pval; Bool false)];
    "true", "", [A (tsymbol, "true"); S (fun beg pos pval -> pushvalue pval; Bool true)];
    "eof", "", [A (testeof, "EOF")]; 
  ];
  
  "composition", [
    "leftrecurs", "" , [NT "composition"; NT "sp"; NT "patt"; S  (fun beg pos pval ->
      match popvalue() with
	List lst -> List (pval :: lst)
      | oth -> failwith (String.concat ", " (List.map impl.dump [oth;pval])))];
    "initial", "", [NT "patt"; S (fun beg pos pval -> List [pval])];
  ];

  "patt", [ "", "", [A ((fun pos _ -> let ptr = ref pos  in
  (try 
    while (let ch = charfeed !ptr 1 in ((ch.[0]) >= 'a' && (ch.[0]) <= 'z') || (ch.[0] = '_')) do
      ptr := !ptr +1;
    done;
  with Not_found -> ());
  let oth = charfeed pos (!ptr - pos) in
  if impl.debug.debugall then print_endline oth;
  if List.mem' String.eqb oth keywords then 0 else (!ptr - pos)), "-id-"); S (fun beg pos pval ->
  pushvalue pval;
  Var (charfeed beg (pos - beg)))];
  ];

  "tstring", [ "", "", [A ((fun pos _ ->
  (try 
    if String.eqb (charfeed pos 1) "\"" then
      let ptr = ref (pos + 1) in
      while not (String.eqb (charfeed !ptr 1) "\"") do
	ptr := !ptr + (nquotchar !ptr);
      done;
      ((!ptr - pos) + 1)
    else
      0
  with Not_found -> 0)), "string"); S (fun beg pos pval ->
  pushvalue pval;
  String (charfeed (beg + 1) (pos - (beg + 2))))];
  ];

  "tquotchar", [ "", "", [A ((fun pos _ ->
  (try 
    let k = nquotchar (pos+1) in
    if (charfeed pos 1).[0] = '\'' && (charfeed (pos+k+1) 1).[0] = '\'' then
      k + 2 
    else
      0
  with Not_found -> 0)), "quotchar"); S (fun beg pos pval ->
  pushvalue pval;
  let ch = (charfeed (beg + 1) 1).[0] in
  if ch <> '\\' then Char ch
  else match (charfeed (beg + 2) 1).[0] with
    | 'n' -> Char '\n'
    | 't' -> Char '\t'
    | '\'' -> Char '\''
    | oth -> Char oth)];
  ];

  "sp", [
    "", "", [P (A(tspace, "_"))];
  ];
  "osp", [
    "", "", [Z (A(tspace, "_"))];
  ];
]
in
  begin
    chanstack := (ref 0,(f^"\n"),Std.in_) :: !chanstack;
    filestack := "-" :: !filestack;
    startstack := !lines :: !startstack;

  (try 
    input := stream_line (List.hd !chanstack);
    len := String.length !input;
  with
    _ ->
      print_endline "NO input";
      );

  let (parsed,i) = streamparse !parseloc Unit grammar 
      (if String.eqb !startrule "" then "start" else !startrule)
      ((if impl.debug.debugrun then run_debug else 0)
	 + (if impl.debug.debugall then full_debug else 0)
	 + (if impl.debug.stamping then node_stamping else 0)
	 + (if impl.debug.memflush then flush_memo else 0)
	 + (impl.debug.memosize land 4095))
  in

  if i <= 0 then
    failwith ("parse error: parsing failed "^string_of_int i)
  else if i < !len then
    failwith ("parse error: "^string_of_int (!len - i)^
	      " extra characters after end of acceptable input: "^String.sub !input 0 i)
  else
    begin
    if impl.debug.debugall then print_endline ("parser returned: "^ !input);
    parsed
    end
  end
