open Mylib
open Ml_decls
open To_from_Z
open BinNums
open BinInt

type rat = { num:coq_Z; den:coq_Z; }

let rec dumpP = function
  | Coq_xH -> "xH"
  | Coq_xO num -> dumpP num^"_xO"
  | Coq_xI num -> dumpP num^"_xI"

let dumpZ = function
  | Z0 -> "zero"
  | Zpos num -> dumpP num
  | Zneg num -> "neg("^dumpP num^")"

let floor_num r = Z.div r.num r.den
let asrat n = { num=asZi n; den=asZi 1}

let ( +/ ) x y = {num=(Z.add (Z.mul x.num y.den) (Z.mul x.den y.num)); den=(Z.mul x.den y.den)}
let ( -/ ) x y = {num=(Z.sub (Z.mul x.num y.den) (Z.mul x.den y.num)); den=(Z.mul x.den y.den)}
let ( */ ) x y = {num=Z.mul x.num y.num; den=Z.mul x.den y.den}
let ( // ) x y = {num=Z.mul x.num y.den; den=Z.mul x.den y.num}
let ( </ ) x y = Z.ltb (Z.mul x.num y.den) (Z.mul x.den y.num)
let ( >/ ) x y = Z.gtb (Z.mul x.num y.den) (Z.mul x.den y.num)

let abs_num {num=num; den=den} = match num with
  | Zneg n -> {num=Zpos n;den=den}
  | oth -> {num=num;den=den}

let int_of_num n = fromZi n

(* series for: c*atan(1/k) *)
type atan_sum = {
    c: rat;
    k: rat;
    kk: rat;
    mutable n: int;
    mutable kpow: rat;
    mutable pterm: rat;
    mutable psum: rat;
    mutable sum: rat;
  }

let atan_sum c k = {
   c = c;
   k = k;
   kk = k*/k;
   n = 0;
   kpow = k;
   pterm = c*/k;
   psum = asrat 0;
   sum = c*/k;
}

let method_next sum =
      sum.n <- sum.n+1; sum.kpow <- sum.kpow*/sum.kk;
      let t = sum.c*/sum.kpow//(asrat (2*sum.n+1)) in
      sum.pterm <- if sum.n mod 2 = 0 then t else asrat 0 -/ t;
      sum.psum <- sum.sum;
      sum.sum <- sum.sum +/ sum.pterm

let method_error sum = abs_num sum.pterm
let method_bounds sum = if sum.pterm </ asrat 0 then (sum.sum, sum.psum) else (sum.psum, sum.sum)
 
let _ =
let inv i = (asrat 1)//(asrat i) in
let t1 = atan_sum (asrat 16) (inv 5) in
let t2 = atan_sum (asrat (-4)) (inv 239) in
let base = asrat 10 in
let npr = ref 0 in
let shift = ref (asrat 1) in
let d_acc = inv 10000 in
let acc = ref d_acc in
let shown = ref (asrat 0) in
while true do
   while method_error t1 >/ !acc do method_next t1 done;
   while method_error t2 >/ !acc do method_next t2 done;
   let (lo1, hi1), (lo2, hi2) = method_bounds t1, method_bounds t2 in
   let digit x = int_of_num (floor_num ((x -/ !shown) */ !shift)) in
   let d, d' = digit (lo1+/lo2), digit (hi1+/hi2) in
   if d = d' then (
      print_int d;
      if !npr = 0 then print_char '.';
      flush stdout;
      shown := !shown +/ ((asrat d) // !shift);
      incr npr; shift := !shift */ base;
   ) else (acc := !acc */ d_acc);
done
