open BinNums
open BinPos
open Datatypes
open Rdefinitions
open Specif

type __ = Obj.t

(** val total_order_T : coq_R -> coq_R -> bool sumor **)

let total_order_T = Axioms.total_order_T

(** val coq_INR : nat -> coq_R **)

let rec coq_INR = function
| O -> coq_R0
| S n0 ->
  (match n0 with
   | O -> coq_R1
   | S n1 -> coq_Rplus (coq_INR n0) coq_R1)

(** val coq_IZR : coq_Z -> coq_R **)

let coq_IZR = function
| Z0 -> coq_R0
| Zpos n -> coq_INR (Pos.to_nat n)
| Zneg n -> coq_Ropp (coq_INR (Pos.to_nat n))

(** val completeness : __ -> coq_R **)

let completeness = Axioms.completeness

