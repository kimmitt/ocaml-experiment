open BinNums
open Axiom_io
open Datatypes
open BinNat
open BinPos
open BinInt
open MoreZStlc
open STLCExtended
open Examples
open Dump
open Ml_compiling
open Exec
open Zmain
open Ml_decls

(** val prod_curry : (('a1, 'a2) prod -> 'a3) -> 'a1 -> 'a2 -> 'a3 **)

let prod_curry f x y = f (Coq_pair (x, y))

(** val prod_uncurry : ('a1 -> 'a2 -> 'a3) -> ('a1, 'a2) prod -> 'a3 **)

let prod_uncurry f = function Coq_pair (x, y) -> f x y

let codes' = init_codes()
let r = reduce
let a' = exec_till (codes',NumTest.test) = dumpentry' codes' (Coq_tm_nat (asZi 6))
let b' = exec_till (codes',ProdTest.test) = dumpentry' codes' (Coq_tm_nat (asZi 6))
let c' = exec_till (codes',LetTest.test) = dumpentry' codes' (Coq_tm_nat (asZi 6))
let d' = exec_till (codes',SumTest1.test) = dumpentry' codes' (Coq_tm_nat (asZi 5))
let e' = exec_till (codes',SumTest2.test) = dumpentry' codes' (Coq_tm_pair (Coq_tm_nat (asZi 5), Coq_tm_nat(asZi 0)))
let f' = exec_till (codes',ListTest.test) = dumpentry' codes' (Coq_tm_nat (asZi 25))
let g' = exec_till (codes',FixTest1.fact_calc (asZi 6)) = dumpentry' codes' (Coq_tm_nat (asZi 720))
let h' = exec_till (codes',Coq_tm_app (FixTest2.map_func, FixTest2.map_arg)) = dumpentry' codes' (FixTest2.map_rslt)
let i' = exec_till (codes',FixTest3.equal1) = dumpentry' codes' (Coq_tm_nat (asZi 1))
let j' = exec_till (codes',FixTest3.equal2) = dumpentry' codes' (Coq_tm_nat (asZi 0))
let k' = exec_till (codes',FixTest4.eotest) = dumpentry' codes' (FixTest4.eotest_rslt)

let rslt = (a',b',c',d',e',f',g',h',i',j',k')
(*
let rec fact n = if sign_big_int n = 0 then unit_big_int else mult_big_int n (fact (pred_big_int n));;
let fact n = string_of_big_int (fact (big_int_of_int n));;
*)
let x = strcompile "let rec f n = if n = 0 then 1 else n*f(n-1) in f 6";;
let y = strcompile "let rec f n = if n > 2 then n * f(n-1) else n in f 4";;
let z = strcompile "let rec p' arg = let (p,q,r) = arg in p+q+r in p' (2,3,4)";;

let decl1 = exec_till (strcompile("let n = 1 in if n > 2 then 4 else 8"));;
let decl2 = exec_till (strcompile("let n = 2 in if n > 2 then 4 else 8"));;
let decl3 = exec_till (strcompile("let n = 3 in if n > 2 then 4 else 8"));;
let decl4 = exec_till (strcompile("let n = 4 in if n > 2 then 4 else 8"));;
let decl5 = exec_till (strcompile("let n = 1 in if n < 2 then 4 else 8"));;
let decl6 = exec_till (strcompile("let n = 2 in if n < 2 then 4 else 8"));;
let decl7 = exec_till (strcompile("let n = 3 in if n < 2 then 4 else 8"));;
let decl8 = exec_till (strcompile("let n = 4 in if n < 2 then 4 else 8"));;

let _,d1 = strcompile("let n = 1 in n");;
let d2 = reduce d1;;
let _,d3 = strcompile("let n = 1 in if n <= 2 then 4 else 8");;
let d4 = reduce d3;;
let d5 = reduce d4;;
let d6 = reduce d5;;

let n = asNat 12
let eq = asNat 13
let m = asNat 14

let eq' = Coq_tm_fix
 (Coq_tm_abs (eq,
   Coq_ty_arrow (Coq_ty_Nat, Coq_ty_arrow (Coq_ty_Nat, Coq_ty_Nat)),
   Coq_tm_abs (m,
    Coq_ty_Nat,
    Coq_tm_abs (n, Coq_ty_Nat,
     Coq_tm_if0
      (Coq_tm_var (m),
      Coq_tm_if0
       (Coq_tm_var (n),
       Coq_tm_nat (Zpos Coq_xH), Coq_tm_nat Z0),
      Coq_tm_if0
       (Coq_tm_var (n),
       Coq_tm_nat Z0,
       Coq_tm_app
        (Coq_tm_app
          (Coq_tm_var (eq),
          Coq_tm_pred
           (Coq_tm_var
             (m))),
        Coq_tm_pred
         (Coq_tm_var (n)))))))))

let n = asNat 0
let ml = asNat 1
let m = asNat 2

let mul''' = Coq_tm_fix
 (Coq_tm_abs (ml,
   Coq_ty_arrow (Coq_ty_Nat, Coq_ty_arrow (Coq_ty_Nat, Coq_ty_Nat)),
   Coq_tm_abs (m,
    Coq_ty_Nat,
    Coq_tm_abs (n, Coq_ty_Nat,
     Coq_tm_mult
      (Coq_tm_var (m),
       Coq_tm_var (n))))))

let mul'' arg = Coq_tm_app (mul''', Coq_tm_nat (asZi arg))
let mul' arg1 arg2 = Coq_tm_app (mul'' arg1, Coq_tm_nat (asZi arg2))
let mul x y = exec_till (codes',(mul' x y))
let m0 = mul 4 4

let cm = compile'' "let rec mul x y = x * y in mul 3 4";;

let cm' = rw cm;;
let cm'' = exec_till cm';;

let p = compile'' "let rec print_int x = let _ = if x > 9 then print_int (x / 10) else () in print_char (char_of_int (x mod 10 + 48)) in let _ = print_int (12345) in print_char(char_of_int 10)"

let p' = rw p;;
let p'' = exec_till p';;

let putc = compile'' "let _ = print_char 'J'";;
let putc' = rw putc;;
let putc'' = exec_till putc';;

let src = Coq_tm_let(asNat 3, Coq_tm_cons ( Coq_tm_nat (asZi 5), Coq_tm_cons ( Coq_tm_nat (asZi 6),  Coq_tm_nil(Coq_ty_Nat) ) ),
  Coq_tm_lcase ( Coq_tm_var (asNat 3), Coq_tm_nat Z0, asNat 9, asNat 10, Coq_tm_mult ( Coq_tm_var (asNat 9), Coq_tm_var (asNat 9)) ));;

src = ListTest.test;;

let src1 = "let l = [5;6] in
   match l with
     [] -> 0
   | x::y -> x*x"

let src2 = compile'' src1;;
let src3 = rw src2;;

let len1 = "
let rec length_aux len = function
    [] -> len
  | a::l -> length_aux (len + 1) l
in
let rec length l = length_aux 0 l
in
length [7;13;17;23]";;

let len2 = compile'' len1;;
let len3 = exec_till (rw len2);;

let hd1 = "let rec hd = function
    [] -> failwith \"hd\"
  | a::l -> a";;

let hd2 = hd1^" in hd [7;13;17;23]";;
let hd3 = compile'' hd2;;
let hd4 = rw hd3;;

let tl1 = "let tl = function
    [] -> failwith \"tl\"
  | a::l -> l";;

let nth1 = "let nth l n =
  if n < 0 then invalid_arg \"List.nth\" else
  let rec nth_aux l n =
    match l with
    | [] -> failwith \"nth\"
    | a::l -> if n = 0 then a else nth_aux l (n-1)
  in nth_aux l n";;

let len3 = rw len2;;

let exn = strcompile "let _ = failwith \"hello\"";;
let coi = strcompile "let _ = char_of_int 48";;
let pch = strcompile "let _ = print_char '@'";;

exec_till exn;;

let sum1 = "
type sumtype =
    Inl of int
  | Inr of int

let rec processSum x = (match x with
    Inl n -> n
  | Inr n -> if n = 0 then 1 else 0)

let pair = (processSum (Inl 5), processSum (Inr 5))
;;
pair";;

let fd = open_out "sumtype.ml" in output_string fd sum1; close_out fd;;

let sum2 = compile'' sum1;;

(* let processSum =
     \x:Nat+Nat.
        case x of
          inl n => n
          inr n => if0 n then 1 else 0 in
   (processSum (inl Nat 5), processSum (inr Nat 5))    *)

let sum3 = rw sum2;;
(*
let sum4 = dumpentry sum3
*)
let rslt = exec_till sum3
