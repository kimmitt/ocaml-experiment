open BinInt
open BinNums
open BinPos
open Bool
open Datatypes
open Fcalc_bracket
open Fcalc_round
open Fcore_FLT
open Fcore_Raux
open Fcore_Zaux
open Fcore_defs
open Fcore_digits
open Fcore_generic_fmt
open PreOmega
open Rdefinitions
open Zbool
open Zpower

type __ = Obj.t
let __ = let rec f _ = Obj.repr f in Obj.repr f

type full_float =
| F754_zero of bool
| F754_infinity of bool
| F754_nan of bool * positive
| F754_finite of bool * positive * coq_Z

(** val full_float_rect :
    (bool -> 'a1) -> (bool -> 'a1) -> (bool -> positive -> 'a1) -> (bool ->
    positive -> coq_Z -> 'a1) -> full_float -> 'a1 **)

let full_float_rect f f0 f1 f2 = function
| F754_zero x -> f x
| F754_infinity x -> f0 x
| F754_nan (x, x0) -> f1 x x0
| F754_finite (x, x0, x1) -> f2 x x0 x1

(** val full_float_rec :
    (bool -> 'a1) -> (bool -> 'a1) -> (bool -> positive -> 'a1) -> (bool ->
    positive -> coq_Z -> 'a1) -> full_float -> 'a1 **)

let full_float_rec f f0 f1 f2 = function
| F754_zero x -> f x
| F754_infinity x -> f0 x
| F754_nan (x, x0) -> f1 x x0
| F754_finite (x, x0, x1) -> f2 x x0 x1

(** val coq_FF2R : radix -> full_float -> coq_R **)

let coq_FF2R beta = function
| F754_finite (s, m, e) ->
  coq_F2R beta { coq_Fnum = (cond_Zopp s (Zpos m)); coq_Fexp = e }
| _ -> coq_R0

(** val canonic_mantissa : coq_Z -> coq_Z -> positive -> coq_Z -> bool **)

let canonic_mantissa prec emax =
  let emin = Z.sub (Z.sub (Zpos (Coq_xI Coq_xH)) emax) prec in
  let fexp = coq_FLT_exp emin prec in
  (fun m e ->
  coq_Zeq_bool (fexp (Z.add (Z.of_nat (S (digits2_Pnat m))) e)) e)

(** val bounded : coq_Z -> coq_Z -> positive -> coq_Z -> bool **)

let bounded prec emax m e =
  if canonic_mantissa prec emax m e then Z.leb e (Z.sub emax prec) else false

(** val valid_binary : coq_Z -> coq_Z -> full_float -> bool **)

let valid_binary prec emax = function
| F754_nan (b, pl) -> Z.ltb (coq_Z_of_nat' (S (digits2_Pnat pl))) prec
| F754_finite (b, m, e) -> bounded prec emax m e
| _ -> true

type nan_pl = positive

type binary_float =
| B754_zero of bool
| B754_infinity of bool
| B754_nan of bool * nan_pl
| B754_finite of bool * positive * coq_Z

(** val binary_float_rect :
    coq_Z -> coq_Z -> (bool -> 'a1) -> (bool -> 'a1) -> (bool -> nan_pl ->
    'a1) -> (bool -> positive -> coq_Z -> __ -> 'a1) -> binary_float -> 'a1 **)

let binary_float_rect prec emax f f0 f1 f2 = function
| B754_zero x -> f x
| B754_infinity x -> f0 x
| B754_nan (x, x0) -> f1 x x0
| B754_finite (x, x0, x1) -> f2 x x0 x1 __

(** val binary_float_rec :
    coq_Z -> coq_Z -> (bool -> 'a1) -> (bool -> 'a1) -> (bool -> nan_pl ->
    'a1) -> (bool -> positive -> coq_Z -> __ -> 'a1) -> binary_float -> 'a1 **)

let binary_float_rec prec emax f f0 f1 f2 = function
| B754_zero x -> f x
| B754_infinity x -> f0 x
| B754_nan (x, x0) -> f1 x x0
| B754_finite (x, x0, x1) -> f2 x x0 x1 __

(** val coq_FF2B : coq_Z -> coq_Z -> full_float -> binary_float **)

let coq_FF2B prec emax = function
| F754_zero s -> B754_zero s
| F754_infinity s -> B754_infinity s
| F754_nan (b, pl) -> B754_nan (b, pl)
| F754_finite (s, m, e) -> B754_finite (s, m, e)

(** val coq_B2FF : coq_Z -> coq_Z -> binary_float -> full_float **)

let coq_B2FF prec emax = function
| B754_zero s -> F754_zero s
| B754_infinity s -> F754_infinity s
| B754_nan (b, n) -> F754_nan (b, n)
| B754_finite (s, m, e) -> F754_finite (s, m, e)

(** val radix2 : radix **)

let radix2 =
  Zpos (Coq_xO Coq_xH)

(** val coq_B2R : coq_Z -> coq_Z -> binary_float -> coq_R **)

let coq_B2R prec emax = function
| B754_finite (s, m, e) ->
  coq_F2R radix2 { coq_Fnum = (cond_Zopp s (Zpos m)); coq_Fexp = e }
| _ -> coq_R0

(** val is_finite_strict : coq_Z -> coq_Z -> binary_float -> bool **)

let is_finite_strict prec emax = function
| B754_finite (b, m, e) -> true
| _ -> false

(** val coq_Bsign : coq_Z -> coq_Z -> binary_float -> bool **)

let coq_Bsign prec emax = function
| B754_zero s -> s
| B754_infinity s -> s
| B754_nan (s, n) -> s
| B754_finite (s, m, e) -> s

(** val sign_FF : full_float -> bool **)

let sign_FF = function
| F754_zero s -> s
| F754_infinity s -> s
| F754_nan (s, p) -> s
| F754_finite (s, p, z) -> s

(** val is_finite : coq_Z -> coq_Z -> binary_float -> bool **)

let is_finite prec emax = function
| B754_zero b -> true
| B754_finite (b, m, e) -> true
| _ -> false

(** val is_finite_FF : full_float -> bool **)

let is_finite_FF = function
| F754_zero b -> true
| F754_finite (b, p, z) -> true
| _ -> false

(** val is_nan : coq_Z -> coq_Z -> binary_float -> bool **)

let is_nan prec emax = function
| B754_nan (b, n) -> true
| _ -> false

(** val is_nan_FF : full_float -> bool **)

let is_nan_FF = function
| F754_nan (b, p) -> true
| _ -> false

(** val coq_Bopp :
    coq_Z -> coq_Z -> (bool -> nan_pl -> (bool, nan_pl) prod) -> binary_float
    -> binary_float **)

let coq_Bopp prec emax opp_nan = function
| B754_zero sx -> B754_zero (negb sx)
| B754_infinity sx -> B754_infinity (negb sx)
| B754_nan (sx, plx) ->
  let Coq_pair (sres, plres) = opp_nan sx plx in B754_nan (sres, plres)
| B754_finite (sx, mx, ex) -> B754_finite ((negb sx), mx, ex)

type shr_record = { shr_m : coq_Z; shr_r : bool; shr_s : bool }

(** val shr_record_rect :
    (coq_Z -> bool -> bool -> 'a1) -> shr_record -> 'a1 **)

let shr_record_rect f s =
  let { shr_m = x; shr_r = x0; shr_s = x1 } = s in f x x0 x1

(** val shr_record_rec :
    (coq_Z -> bool -> bool -> 'a1) -> shr_record -> 'a1 **)

let shr_record_rec f s =
  let { shr_m = x; shr_r = x0; shr_s = x1 } = s in f x x0 x1

(** val shr_m : shr_record -> coq_Z **)

let shr_m x = x.shr_m

(** val shr_r : shr_record -> bool **)

let shr_r x = x.shr_r

(** val shr_s : shr_record -> bool **)

let shr_s x = x.shr_s

(** val shr_1 : shr_record -> shr_record **)

let shr_1 mrs =
  let { shr_m = m; shr_r = r; shr_s = s } = mrs in
  let s0 = if r then true else s in
  (match m with
   | Z0 -> { shr_m = Z0; shr_r = false; shr_s = s0 }
   | Zpos p0 ->
     (match p0 with
      | Coq_xI p -> { shr_m = (Zpos p); shr_r = true; shr_s = s0 }
      | Coq_xO p -> { shr_m = (Zpos p); shr_r = false; shr_s = s0 }
      | Coq_xH -> { shr_m = Z0; shr_r = true; shr_s = s0 })
   | Zneg p0 ->
     (match p0 with
      | Coq_xI p -> { shr_m = (Zneg p); shr_r = true; shr_s = s0 }
      | Coq_xO p -> { shr_m = (Zneg p); shr_r = false; shr_s = s0 }
      | Coq_xH -> { shr_m = Z0; shr_r = true; shr_s = s0 }))

(** val loc_of_shr_record : shr_record -> location **)

let loc_of_shr_record mrs =
  let { shr_m = shr_m0; shr_r = shr_r0; shr_s = shr_s0 } = mrs in
  if shr_r0
  then if shr_s0 then Coq_loc_Inexact Gt else Coq_loc_Inexact Eq
  else if shr_s0 then Coq_loc_Inexact Lt else Coq_loc_Exact

(** val shr_record_of_loc : coq_Z -> location -> shr_record **)

let shr_record_of_loc m = function
| Coq_loc_Exact -> { shr_m = m; shr_r = false; shr_s = false }
| Coq_loc_Inexact c ->
  (match c with
   | Eq -> { shr_m = m; shr_r = true; shr_s = false }
   | Lt -> { shr_m = m; shr_r = false; shr_s = true }
   | Gt -> { shr_m = m; shr_r = true; shr_s = true })

(** val shr : shr_record -> coq_Z -> coq_Z -> (shr_record, coq_Z) prod **)

let shr mrs e n = match n with
| Zpos p -> Coq_pair ((Pos.iter p shr_1 mrs), (Z.add e n))
| _ -> Coq_pair (mrs, e)

(** val coq_Zdigits2 : coq_Z -> coq_Z **)

let coq_Zdigits2 m = match m with
| Z0 -> m
| Zpos p -> Z.of_nat (S (digits2_Pnat p))
| Zneg p -> Z.of_nat (S (digits2_Pnat p))

(** val shr_fexp :
    coq_Z -> coq_Z -> coq_Z -> coq_Z -> location -> (shr_record, coq_Z) prod **)

let shr_fexp prec emax =
  let emin = Z.sub (Z.sub (Zpos (Coq_xI Coq_xH)) emax) prec in
  let fexp = coq_FLT_exp emin prec in
  (fun m e l ->
  shr (shr_record_of_loc m l) e (Z.sub (fexp (Z.add (coq_Zdigits2 m) e)) e))

type mode =
| Coq_mode_NE
| Coq_mode_ZR
| Coq_mode_DN
| Coq_mode_UP
| Coq_mode_NA

(** val mode_rect : 'a1 -> 'a1 -> 'a1 -> 'a1 -> 'a1 -> mode -> 'a1 **)

let mode_rect f f0 f1 f2 f3 = function
| Coq_mode_NE -> f
| Coq_mode_ZR -> f0
| Coq_mode_DN -> f1
| Coq_mode_UP -> f2
| Coq_mode_NA -> f3

(** val mode_rec : 'a1 -> 'a1 -> 'a1 -> 'a1 -> 'a1 -> mode -> 'a1 **)

let mode_rec f f0 f1 f2 f3 = function
| Coq_mode_NE -> f
| Coq_mode_ZR -> f0
| Coq_mode_DN -> f1
| Coq_mode_UP -> f2
| Coq_mode_NA -> f3

(** val round_mode : mode -> coq_R -> coq_Z **)

let round_mode = function
| Coq_mode_NE -> coq_Znearest (fun x -> negb (coq_Zeven x))
| Coq_mode_ZR -> coq_Ztrunc
| Coq_mode_DN -> coq_Zfloor
| Coq_mode_UP -> coq_Zceil
| Coq_mode_NA -> coq_Znearest (Z.leb Z0)

(** val choice_mode : mode -> bool -> coq_Z -> location -> coq_Z **)

let choice_mode m sx mx lx =
  match m with
  | Coq_mode_NE -> cond_incr (round_N (negb (coq_Zeven mx)) lx) mx
  | Coq_mode_ZR -> mx
  | Coq_mode_DN -> cond_incr (round_sign_DN sx lx) mx
  | Coq_mode_UP -> cond_incr (round_sign_UP sx lx) mx
  | Coq_mode_NA -> cond_incr (round_N true lx) mx

(** val overflow_to_inf : mode -> bool -> bool **)

let overflow_to_inf m s =
  match m with
  | Coq_mode_ZR -> false
  | Coq_mode_DN -> s
  | Coq_mode_UP -> negb s
  | _ -> true

(** val binary_overflow : coq_Z -> coq_Z -> mode -> bool -> full_float **)

let binary_overflow prec emax m s =
  if overflow_to_inf m s
  then F754_infinity s
  else F754_finite (s,
         (match Z.sub (Z.pow (Zpos (Coq_xO Coq_xH)) prec) (Zpos Coq_xH) with
          | Zpos p -> p
          | _ -> Coq_xH), (Z.sub emax prec))

(** val binary_round_aux :
    coq_Z -> coq_Z -> mode -> bool -> positive -> coq_Z -> location ->
    full_float **)

let binary_round_aux prec emax mode0 sx mx ex lx =
  let Coq_pair (mrs', e') = shr_fexp prec emax (Zpos mx) ex lx in
  let Coq_pair (mrs'', e'') =
    shr_fexp prec emax
      (choice_mode mode0 sx mrs'.shr_m (loc_of_shr_record mrs')) e'
      Coq_loc_Exact
  in
  (match mrs''.shr_m with
   | Z0 -> F754_zero sx
   | Zpos m ->
     if Z.leb e'' (Z.sub emax prec)
     then F754_finite (sx, m, e'')
     else binary_overflow prec emax mode0 sx
   | Zneg p -> F754_nan (false, Coq_xH))

(** val coq_Bmult :
    coq_Z -> coq_Z -> (binary_float -> binary_float -> (bool, nan_pl) prod)
    -> mode -> binary_float -> binary_float -> binary_float **)

let coq_Bmult prec emax mult_nan m x y =
  match x with
  | B754_zero sx ->
    let f = fun pl -> B754_nan ((fst pl), (snd pl)) in
    (match y with
     | B754_zero sy -> B754_zero (xorb sx sy)
     | B754_finite (sy, m0, e) -> B754_zero (xorb sx sy)
     | _ -> f (mult_nan x y))
  | B754_infinity sx ->
    let f = fun pl -> B754_nan ((fst pl), (snd pl)) in
    (match y with
     | B754_infinity sy -> B754_infinity (xorb sx sy)
     | B754_finite (sy, m0, e) -> B754_infinity (xorb sx sy)
     | _ -> f (mult_nan x y))
  | B754_nan (b, n) -> let pl = mult_nan x y in B754_nan ((fst pl), (snd pl))
  | B754_finite (sx, mx, ex) ->
    let f = fun pl -> B754_nan ((fst pl), (snd pl)) in
    (match y with
     | B754_zero sy -> B754_zero (xorb sx sy)
     | B754_infinity sy -> B754_infinity (xorb sx sy)
     | B754_nan (b, n) -> f (mult_nan x y)
     | B754_finite (sy, my, ey) ->
       coq_FF2B prec emax
         (binary_round_aux prec emax m (xorb sx sy) (Pos.mul mx my)
           (Z.add ex ey) Coq_loc_Exact))

(** val coq_Bmult_FF :
    coq_Z -> coq_Z -> (full_float -> full_float -> (bool, positive) prod) ->
    mode -> full_float -> full_float -> full_float **)

let coq_Bmult_FF prec emax mult_nan m x y =
  match x with
  | F754_zero sx ->
    let f = fun pl -> F754_nan ((fst pl), (snd pl)) in
    (match y with
     | F754_zero sy -> F754_zero (xorb sx sy)
     | F754_finite (sy, p, z) -> F754_zero (xorb sx sy)
     | _ -> f (mult_nan x y))
  | F754_infinity sx ->
    let f = fun pl -> F754_nan ((fst pl), (snd pl)) in
    (match y with
     | F754_infinity sy -> F754_infinity (xorb sx sy)
     | F754_finite (sy, p, z) -> F754_infinity (xorb sx sy)
     | _ -> f (mult_nan x y))
  | F754_nan (b, p) -> let pl = mult_nan x y in F754_nan ((fst pl), (snd pl))
  | F754_finite (sx, mx, ex) ->
    let f = fun pl -> F754_nan ((fst pl), (snd pl)) in
    (match y with
     | F754_zero sy -> F754_zero (xorb sx sy)
     | F754_infinity sy -> F754_infinity (xorb sx sy)
     | F754_nan (b, p) -> f (mult_nan x y)
     | F754_finite (sy, my, ey) ->
       binary_round_aux prec emax m (xorb sx sy) (Pos.mul mx my)
         (Z.add ex ey) Coq_loc_Exact)

(** val shl_align : positive -> coq_Z -> coq_Z -> (positive, coq_Z) prod **)

let shl_align mx ex ex' =
  match Z.sub ex' ex with
  | Zneg d -> Coq_pair ((shift_pos d mx), ex')
  | _ -> Coq_pair (mx, ex)

(** val shl_align_fexp :
    coq_Z -> coq_Z -> positive -> coq_Z -> (positive, coq_Z) prod **)

let shl_align_fexp prec emax =
  let emin = Z.sub (Z.sub (Zpos (Coq_xI Coq_xH)) emax) prec in
  let fexp = coq_FLT_exp emin prec in
  (fun mx ex ->
  shl_align mx ex (fexp (Z.add (Z.of_nat (S (digits2_Pnat mx))) ex)))

(** val binary_round :
    coq_Z -> coq_Z -> mode -> bool -> positive -> coq_Z -> full_float **)

let binary_round prec emax m sx mx ex =
  let Coq_pair (mz, ez) = shl_align_fexp prec emax mx ex in
  binary_round_aux prec emax m sx mz ez Coq_loc_Exact

(** val binary_normalize :
    coq_Z -> coq_Z -> mode -> coq_Z -> coq_Z -> bool -> binary_float **)

let binary_normalize prec emax mode0 m e szero =
  match m with
  | Z0 -> B754_zero szero
  | Zpos m0 -> coq_FF2B prec emax (binary_round prec emax mode0 false m0 e)
  | Zneg m0 -> coq_FF2B prec emax (binary_round prec emax mode0 true m0 e)

(** val coq_Bplus :
    coq_Z -> coq_Z -> (binary_float -> binary_float -> (bool, nan_pl) prod)
    -> mode -> binary_float -> binary_float -> binary_float **)

let coq_Bplus prec emax plus_nan m x y =
  match x with
  | B754_zero sx ->
    let f = fun pl -> B754_nan ((fst pl), (snd pl)) in
    (match y with
     | B754_zero sy ->
       if eqb sx sy
       then x
       else (match m with
             | Coq_mode_DN -> B754_zero true
             | _ -> B754_zero false)
     | B754_nan (b, n) -> f (plus_nan x y)
     | _ -> y)
  | B754_infinity sx ->
    let f = fun pl -> B754_nan ((fst pl), (snd pl)) in
    (match y with
     | B754_infinity sy -> if eqb sx sy then x else f (plus_nan x y)
     | B754_nan (b, n) -> f (plus_nan x y)
     | _ -> x)
  | B754_nan (b, n) -> let pl = plus_nan x y in B754_nan ((fst pl), (snd pl))
  | B754_finite (sx, mx, ex) ->
    let f = fun pl -> B754_nan ((fst pl), (snd pl)) in
    (match y with
     | B754_zero b -> x
     | B754_infinity b -> y
     | B754_nan (b, n) -> f (plus_nan x y)
     | B754_finite (sy, my, ey) ->
       let ez = Z.min ex ey in
       binary_normalize prec emax m
         (Z.add (cond_Zopp sx (Zpos (fst (shl_align mx ex ez))))
           (cond_Zopp sy (Zpos (fst (shl_align my ey ez))))) ez
         (match m with
          | Coq_mode_DN -> true
          | _ -> false))

(** val coq_Bminus :
    coq_Z -> coq_Z -> (binary_float -> binary_float -> (bool, nan_pl) prod)
    -> mode -> binary_float -> binary_float -> binary_float **)

let coq_Bminus prec emax minus_nan m x y =
  coq_Bplus prec emax minus_nan m x
    (coq_Bopp prec emax (fun x0 x1 -> Coq_pair (x0, x1)) y)

(** val coq_Fdiv_core_binary :
    coq_Z -> coq_Z -> coq_Z -> coq_Z -> coq_Z -> ((coq_Z, coq_Z) prod,
    location) prod **)

let coq_Fdiv_core_binary prec m1 e1 m2 e2 =
  let d1 = coq_Zdigits2 m1 in
  let d2 = coq_Zdigits2 m2 in
  let e = Z.sub e1 e2 in
  (match Z.sub (Z.add d2 prec) d1 with
   | Zpos p ->
     let m = Z.mul m1 (Z.pow_pos (Zpos (Coq_xO Coq_xH)) p) in
     let e' = Z.add e (Zneg p) in
     let Coq_pair (q, r) = Z.div_eucl m m2 in
     Coq_pair ((Coq_pair (q, e')), (new_location m2 r Coq_loc_Exact))
   | _ ->
     let Coq_pair (q, r) = Z.div_eucl m1 m2 in
     Coq_pair ((Coq_pair (q, e)), (new_location m2 r Coq_loc_Exact)))

(** val coq_Bdiv :
    coq_Z -> coq_Z -> (binary_float -> binary_float -> (bool, nan_pl) prod)
    -> mode -> binary_float -> binary_float -> binary_float **)

let coq_Bdiv prec emax div_nan m x y =
  match x with
  | B754_zero sx ->
    let f = fun pl -> B754_nan ((fst pl), (snd pl)) in
    (match y with
     | B754_infinity sy -> B754_zero (xorb sx sy)
     | B754_finite (sy, m0, e) -> B754_zero (xorb sx sy)
     | _ -> f (div_nan x y))
  | B754_infinity sx ->
    let f = fun pl -> B754_nan ((fst pl), (snd pl)) in
    (match y with
     | B754_zero sy -> B754_infinity (xorb sx sy)
     | B754_finite (sy, m0, e) -> B754_infinity (xorb sx sy)
     | _ -> f (div_nan x y))
  | B754_nan (b, n) -> let pl = div_nan x y in B754_nan ((fst pl), (snd pl))
  | B754_finite (sx, mx, ex) ->
    let f = fun pl -> B754_nan ((fst pl), (snd pl)) in
    (match y with
     | B754_zero sy -> B754_infinity (xorb sx sy)
     | B754_infinity sy -> B754_zero (xorb sx sy)
     | B754_nan (b, n) -> f (div_nan x y)
     | B754_finite (sy, my, ey) ->
       coq_FF2B prec emax
         (let Coq_pair (p, lz) =
            coq_Fdiv_core_binary prec (Zpos mx) ex (Zpos my) ey
          in
          let Coq_pair (mz, ez) = p in
          (match mz with
           | Zpos mz0 -> binary_round_aux prec emax m (xorb sx sy) mz0 ez lz
           | _ -> F754_nan (false, Coq_xH))))

(** val coq_Fsqrt_core_binary :
    coq_Z -> coq_Z -> coq_Z -> ((coq_Z, coq_Z) prod, location) prod **)

let coq_Fsqrt_core_binary prec m e =
  let d = coq_Zdigits2 m in
  let s = Z.max (Z.sub (Z.mul (Zpos (Coq_xO Coq_xH)) prec) d) Z0 in
  let e' = Z.sub e s in
  if coq_Zeven e'
  then let m' =
         match s with
         | Zpos p -> Z.mul m (Z.pow_pos (Zpos (Coq_xO Coq_xH)) p)
         | _ -> m
       in
       let Coq_pair (q, r) = Z.sqrtrem m' in
       let l =
         if coq_Zeq_bool r Z0
         then Coq_loc_Exact
         else Coq_loc_Inexact (if Z.leb r q then Lt else Gt)
       in
       Coq_pair ((Coq_pair (q, (Z.div2 e'))), l)
  else let s' = Z.add s (Zpos Coq_xH) in
       let e'' = Z.sub e' (Zpos Coq_xH) in
       let m' =
         match s' with
         | Zpos p -> Z.mul m (Z.pow_pos (Zpos (Coq_xO Coq_xH)) p)
         | _ -> m
       in
       let Coq_pair (q, r) = Z.sqrtrem m' in
       let l =
         if coq_Zeq_bool r Z0
         then Coq_loc_Exact
         else Coq_loc_Inexact (if Z.leb r q then Lt else Gt)
       in
       Coq_pair ((Coq_pair (q, (Z.div2 e''))), l)

(** val coq_Bsqrt :
    coq_Z -> coq_Z -> (binary_float -> (bool, nan_pl) prod) -> mode ->
    binary_float -> binary_float **)

let coq_Bsqrt prec emax sqrt_nan m x =
  let f = fun pl -> B754_nan ((fst pl), (snd pl)) in
  (match x with
   | B754_zero b -> x
   | B754_infinity b -> if b then f (sqrt_nan x) else x
   | B754_nan (sx, plx) -> f (sqrt_nan x)
   | B754_finite (sx, mx, ex) ->
     if sx
     then f (sqrt_nan x)
     else coq_FF2B prec emax
            (let Coq_pair (p, lz) = coq_Fsqrt_core_binary prec (Zpos mx) ex
             in
             let Coq_pair (mz, ez) = p in
             (match mz with
              | Zpos mz0 -> binary_round_aux prec emax m false mz0 ez lz
              | _ -> F754_nan (false, Coq_xH))))

