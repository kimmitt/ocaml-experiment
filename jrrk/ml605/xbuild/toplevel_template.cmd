setmode -acecf
addConfigDevice -mode cf -size 0 -name % -path .
addcollection -name %
adddesign -version 0 -name %
adddevicechain -index 0
adddevice -p 1 -file TOPLEVEL_%.bit
assignfile -p 1 -file TOPLEVEL_%.bit
generate -active %
quit
