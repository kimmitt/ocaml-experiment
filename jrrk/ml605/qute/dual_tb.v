`timescale  10 ns / 1 ps

module mem_behav #(parameter siz = 18) (clk,
        readwrite_addr,
        byte_strobes,
        write_enable,
        read_enable,
        write_data,
        read_data,
        dbg_mem);

input[31:0] write_data;
output[31:0] read_data;
input [siz+1:0] readwrite_addr;
input [3:0] byte_strobes;
input clk, write_enable, read_enable, dbg_mem;
reg[31:0] readwrite_data;
reg[31:0] read_data;

reg [31:0] mem[0:1<<siz];

initial
        $readmemb("~/d320/ocaml-experiment/jrrk/verilog/test4.mem", mem);

always @(negedge clk)
        begin
        if (read_enable)
                begin
                if (readwrite_addr[siz+1:2] >= (1<<siz)) begin $display("readwrite_addr %X", readwrite_addr[siz+1:2]); @(posedge clk) $stop; end
                read_data = mem[readwrite_addr[siz+1:2]];
                if (dbg_mem) $display("read %X => %X", readwrite_addr, read_data);
                end
        if (write_enable) readwrite_data = mem[readwrite_addr[siz+1:2]];
        if (write_enable & byte_strobes[0]) readwrite_data[7:0] = write_data[7:0];
        if (write_enable & byte_strobes[1]) readwrite_data[15:8] = write_data[15:8];
        if (write_enable & byte_strobes[2]) readwrite_data[23:16] = write_data[23:16];
        if (write_enable & byte_strobes[3]) readwrite_data[31:24] = write_data[31:24];
        if (write_enable)
                begin
                if (readwrite_addr[siz+1:2] >= (1<<siz)) begin $display("readwrite_addr %X", readwrite_addr[siz+1:2]); @(posedge clk) $stop; end
                mem[readwrite_addr[siz+1:2]] = readwrite_data;
                if (dbg_mem) $display("write %X => %X", readwrite_data, readwrite_addr);
                end
        end

endmodule

module tb;

parameter siz = 18;
wire [31:0] nxtpc;

wire readstrobe;
reg [7:0] readch, gpio_dip_sw;
reg [31:0] stdin, stdout;
reg a23_clk, a23_rst, rst, dbg_x, dbg_extcall, dbg_regs, dbg_mem, dbg_str, dbg_arg, dbg_enc, dbg_exn, dbg_rom, dbg_goto, dbg_heap, dbg_return;

wire [31:0] sm_readwrite_addr;
wire  [3:0] sm_byte_strobes;
wire  [0:0] sm_write_enable;
wire  [0:0] sm_read_enable;
wire [31:0] sm_read_data;
wire [31:0] sm_write_data;
wire  [7:0] sm_writech;
wire [31:0] sm_readchan;
wire [31:0] sm_writechan;
wire [31:0] sm_flush;
   
wire [31:0] read_data;

wire [0:0] copro_write_data_wen;
wire [0:0] write_data_wen;
wire [1:0] reg_write_sel;
wire [1:0] byte_enable_sel;
wire [17:2] pc_nxt;
wire [2:0] pc_sel;
wire [2:0] address_sel;
wire [6:0] alu_function;
wire [1:0] barrel_shift_function;
wire [1:0] barrel_shift_data_sel;
wire [1:0] barrel_shift_amount_sel;
wire [3:0] rn_sel;
wire [3:0] rds_sel;
wire [3:0] rm_sel;
wire [4:0] imm_shift_amount;
wire [7:0] imm8;
wire [0:0] divrst;
wire [31:0] dividend;
wire [31:0] divider;
wire [0:0] flush;
wire [31:0] a23_crnt;
wire [0:0] read_enable;
wire [0:0] write_enable;
wire [3:0] a23_ccode;
wire [3:0] byte_strobes;
wire [31:0] readwrite_addr;
wire [31:0] write_data;

reg [31:0] crnt;
reg [31:0] prev_readwrite_addr;

wire [7:0] writech;

wire                      system_rdy = !a23_rst;
wire     [31:0]           copro_read_data = 0;

wire     [31:0]           execute_write_data;
wire     [31:0]           execute_write_data_nxt;
wire     [31:0]           execute_copro_write_data;
wire     [31:0]           execute_address;
wire                      execute_address_valid;
wire     [31:0]           execute_address_nxt;  // un-registered version of execute_address to the cache rams

wire     [3:0]            execute_byte_enable;
wire     [3:0]            execute_byte_enable_nxt;
wire                      execute_write_enable;

wire  	 [31:0] 	  r0_out;
wire  	 [31:0] 	  r1_out;
wire  	 [31:0] 	  r2_out;
wire  	 [31:0] 	  r3_out;
wire  	 [31:0] 	  r4_out;
wire  	 [31:0] 	  r5_out;
wire  	 [31:0] 	  r6_out;
wire  	 [31:0] 	  r7_out;
wire  	 [31:0] 	  r8_out;
wire  	 [31:0] 	  r9_out;
wire  	 [31:0] 	  r10_out;
wire  	 [31:0] 	  r11_out;
wire  	 [31:0] 	  r12_out;
wire  	 [31:0] 	  r13_out;
wire  	 [31:0] 	  r14_out;
wire  	 [31:0] 	  r15_out;
wire  	 [31:0] 	  pc_nxt_out;

wire [31:0]   r0_prev;
wire [31:0]   r1_prev;
wire [31:0]   r2_prev;
wire [31:0]   r3_prev;
wire [31:0]   r4_prev;
wire [31:0]   r5_prev;
wire [31:0]   r6_prev;
wire [31:0]   r7_prev;
wire [31:0]   r8_prev;
wire [31:0]   r9_prev;
wire [31:0]   r10_prev;
wire [31:0]   r11_prev;
wire [31:0]   r12_prev;
wire [31:0]   r13_prev;
wire [31:0]   r14_prev;
wire [31:0]   r15_prev;

wire     wrterr = sm_write_enable ^ write_data_wen;
wire     byterr = sm_write_enable && (sm_byte_strobes^execute_byte_enable_nxt);
wire     adwerr = sm_write_enable && (sm_readwrite_addr^execute_address_nxt);
wire     adrerr = sm_read_enable && (sm_readwrite_addr^execute_address_nxt);
wire     adperr = 0; // delay_slot_enable && (prev_readwrite_addr^execute_address);
wire     pcerr = nxtpc != pc_nxt_out;
wire     cherr = sm_writech != writech;

reg	 [15:0] err, errcnt;
reg	 anyerr0, anyerr;

always @(posedge a23_clk)
  begin
     if (a23_rst)
       errcnt = 0;
     anyerr <= a23_rst ? 0 : anyerr0;
     anyerr0 <= a23_rst ? 0 : |{err,wrterr,byterr,adrerr,adwerr,adperr,pcerr,cherr};
     if (anyerr0===1 & anyerr===1)
       begin
	  $display("%t: testbench comparison failure %b", $time, {err,wrterr,byterr,adrerr,adwerr,adperr,pcerr,cherr});
	  errcnt = errcnt + 1;
	  if (errcnt === 100) $stop;
	  $stop;
       end
  end

always @(negedge a23_clk)
 err =
 {(r15_out != r15_prev ? 1'b1 : 1'b0), (r14_out != r14_prev ? 1'b1 : 1'b0), (r13_out != r13_prev ? 1'b1 : 1'b0), (r12_out != r12_prev ? 1'b1 : 1'b0),
  (r11_out != r11_prev ? 1'b1 : 1'b0), (r10_out != r10_prev ? 1'b1 : 1'b0), (r9_out != r9_prev ? 1'b1 : 1'b0), (r8_out != r8_prev ? 1'b1 : 1'b0),
  (r7_out != r7_prev ? 1'b1 : 1'b0), (r6_out != r6_prev ? 1'b1 : 1'b0), (r5_out != r5_prev ? 1'b1 : 1'b0), (r4_out != r4_prev ? 1'b1 : 1'b0),
  (r3_out != r3_prev ? 1'b1 : 1'b0), (r2_out != r2_prev ? 1'b1 : 1'b0), (r1_out != r1_prev ? 1'b1 : 1'b0), (r0_out != r0_prev ? 1'b1 : 1'b0)};
   
assign readwrite_addr = execute_address_nxt;
assign write_data = execute_write_data_nxt;
assign write_enable = write_data_wen;
assign byte_strobes = execute_byte_enable_nxt;
   
TOPLEVEL top(
	      .SYSCLK_N(~a23_clk), 
	      .SYSCLK_P(a23_clk), 
	      .CPU_RESET(a23_rst),
	      .CLK_33MHZ_FPGA(a23_clk), 
	      .GPIO_SW_C(1'b0),
	      .GPIO_SW_N(1'b0),
	      .GPIO_SW_S(1'b0),
	      .GPIO_SW_E(1'b0),
	      .GPIO_SW_W(1'b0),
	      .GPIO_LED(writech),
	      .GPIO_DIP_SW(gpio_dip_sw),
	      .r0_out(r0_out),
	      .r1_out(r1_out),
	      .r2_out(r2_out),
	      .r3_out(r3_out),
	      .r4_out(r4_out),
	      .r5_out(r5_out),
	      .r6_out(r6_out),
	      .r7_out(r7_out),
	      .r8_out(r8_out),
	      .r9_out(r9_out),
	      .r10_out(r10_out),
	      .r11_out(r11_out),
	      .r12_out(r12_out),
	      .r13_out(r13_out),
	      .r14_out(r14_out),
	      .r15_out(r15_out),
	      .pc_nxt_out(pc_nxt_out)
);

mem_behav mem(.clk(a23_clk),
	.readwrite_addr(sm_readwrite_addr[siz+1:0]),
	.byte_strobes(sm_byte_strobes),
	.write_enable(sm_write_enable),
	.read_enable(sm_read_enable),
	.write_data(sm_write_data),
	.read_data(sm_read_data),
	.dbg_mem(dbg_mem));

state_mach sm(
	.a23_clk(a23_clk),
	.rst(rst),
	.dbg_x(dbg_x), 
	.dbg_extcall(dbg_extcall), 
	.dbg_regs(dbg_regs), 
	.dbg_mem(dbg_mem), 
	.dbg_str(dbg_str), 
	.dbg_arg(dbg_arg), 
	.dbg_enc(dbg_enc),
	.dbg_exn(dbg_exn),
	.dbg_rom(dbg_rom),
	.dbg_goto(dbg_goto), 
	.dbg_heap(dbg_heap), 
	.dbg_return(dbg_return),
	.nxtpc(nxtpc),
	.r0_prev( r0_prev),
	.r1_prev( r1_prev),
	.r2_prev( r2_prev),
	.r3_prev( r3_prev),
	.r4_prev( r4_prev),
	.r5_prev( r5_prev),
	.r6_prev( r6_prev),
	.r7_prev( r7_prev),
	.r8_prev( r8_prev),
	.r9_prev( r9_prev),
	.r10_prev( r10_prev),
	.r11_prev( r11_prev),
	.r12_prev( r12_prev),
	.r13_prev( r13_prev),
	.r14_prev( r14_prev),
	.r15_prev( r15_prev),
	.readwrite_addr(sm_readwrite_addr),
	.byte_strobes(sm_byte_strobes),
	.write_enable(sm_write_enable),
	.read_enable(sm_read_enable),
	.write_data(sm_write_data),
	.read_data(sm_read_data),
	.stdin(stdin),
	.stdout(stdout),
	.flush(sm_flush),
	.finish(sm_finish),
	.writechan(sm_writechan),
	.writech(sm_writech),
	.readchan(sm_readchan),
	.readch(readch),
	.readstrobe()
	);

always @(negedge a23_clk)
	begin
	if (flush) $fflush(stdout);
	if (sm_writech[7]) $fwrite(stdout, "%c", sm_writech[6:0]);
	if (readstrobe) readch = $fgetc(stdin);
	if (sm_finish) $finish;
	prev_readwrite_addr = sm_readwrite_addr;
	end

initial
	begin
	if ($test$plusargs("DBG_DUMPVARS") != 0) $dumpvars;
	dbg_x = $test$plusargs("DBG_X") != 0;
	dbg_arg = $test$plusargs("DBG_ARGS") != 0;
	dbg_mem = $test$plusargs("DBG_MEM") != 0;
	dbg_str = $test$plusargs("DBG_STRING") != 0;
	dbg_goto = $test$plusargs("DBG_GOTO") != 0;
	dbg_heap = $test$plusargs("DBG_HEAP") != 0;
	dbg_regs = $test$plusargs("DBG_REGS") != 0;
	dbg_extcall = $test$plusargs("DBG_EXTCALL") != 0;
	dbg_enc = $test$plusargs("DBG_ENCODE") != 0;
	dbg_exn = $test$plusargs("DBG_EXCEPTION") != 0;
	dbg_rom = $test$plusargs("DBG_ROM") != 0;
	dbg_return = $test$plusargs("DBG_RETURN") != 0;
	stdin = $fopen("/dev/stdin", "r");
	stdout = $fopen("/dev/stdout", "w");
        readch = 0;
        gpio_dip_sw = 0;
	rst = 1'b1;
	a23_rst = 1'b1;
	a23_clk = 1'b0;
	#16 a23_clk = 1'b1;
	#16 a23_clk = 1'b0;
	#16 a23_clk = 1'b1;
	#16 a23_clk = 1'b0;
	#16 a23_clk = 1'b1;
	#16 a23_clk = 1'b0;
	#16 a23_clk = 1'b1;
	#16 a23_clk = 1'b0; rst = 1'b0;
	#16 a23_clk = 1'b1;
	forever
		begin
		#16 a23_clk = 1'b0; a23_rst = 1'b0;
		#16 a23_clk = 1'b1;
		end
	end

initial
  begin
     #200000 gpio_dip_sw = 1;
  end
   
endmodule
