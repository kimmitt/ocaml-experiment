setmode -acecf
addConfigDevice -mode cf -size 0 -name % -path .
addcollection -name %
adddesign -version 0 -name %
adddevicechain -index 0
adddevice -p 1 -file %.bit
assignfile -p 1 -file %.bit
generate -active %
quit
