module F_MULT18X18 (P, A, B);

    output [71:0] P;

    input  [35:0] A;
    input  [35:0] B;

   wire [35:0] F_P, true_P;

MULT18X18 mult1 (
  .P(true_P),
  .A(A[17:0]),
  .B(B[17:0])
    );

MULT18X18 F_mult1 (
  .P(F_P),
  .A(~A[35:18]),
  .B(~B[35:18])
    );

  assign P = {~F_P, true_P};
   
endmodule

module F_DVL_BUF #(parameter width = 1) (Y, A);

   input [width*2-1:0] A;
   output [width*2-1:0] Y;

   assign Y = A;

endmodule // DVL_BUF

module F_DVL_ARI_MUL(Y, A, B);
   parameter width1 = 2;
   parameter width2 = 1;
   parameter width3 = 1;
   output [width1*2-1:0] Y;
   input [width2*2-1:0] A;
   input [width3*2-1:0] B;

   assign Y[width1-1:0] = A[width2-1:0] * B[width3-1:0];
   assign Y[width1*2-1:width1] = ~((~A[width2*2-1:width2]) * (~B[width3*2-1:width3]));

endmodule // DVL_ARI_MUL

module F_DVL_EQ(Y, A, B);
   parameter width1 = 1;
   parameter width2 = 1;
   output [1:0] Y;
   input [width1*2-1:0] A;
   input [width2*2-1:0] B;

   assign Y[0] = !(|(A[width1-1:0]^B[width2-1:0]));
   assign Y[1] = &(A[width1*2-1:width1]^B[width2*2-1:width2]);

endmodule // DVL_EQ

module F_DVL_BW_NOT(Y, A);
   parameter width = 1;
   input [width*2-1:0] A;
   output [width*2-1:0] Y;
   
   assign Y[width-1:0] = A[width*2-1:width];
   assign Y[width*2-1:width] = A[width-1:0];

endmodule // DVL_BW_NOT

module F_DVL_RED_AND(Y, A);
   parameter width = 1;
   output [1:0] Y;
   input [width*2-1:0] A;
   
   assign Y[0] = & A[width-1:0];
   assign Y[1] = | A[width-1:0];
   
endmodule // DVL_RED_AND

module F_DVL_RED_OR(Y, A);
   parameter width = 1;
   output [1:0] Y;
   input [width*2-1:0] A;
   
   assign Y[0] = | A[width-1:0];
   assign Y[1] = & A[width*2-1:width];

endmodule // DVL_RED_OR

module F_DVL_ARI_ADD(Y, A, B);
   parameter width1 = 1;
   parameter width2 = 1;
   parameter width3 = 1;
   output [width1*2-1:0] Y;
   input [width2*2-1:0] A;
   input [width3*2-1:0] B;
   
   assign Y[width1-1:0] = A[width2-1:0] + B[width3-1:0];
   assign Y[width1*2-1:width1] = ~((~A[width2*2-1:width2]) + (~B[width3*2-1:width3]));

endmodule // DVL_ARI_ADD

module F_DVL_ARI_SUB(Y, A, B);
   parameter width1 = 1;
   parameter width2 = 1;
   parameter width3 = 1;
   output [width1*2-1:0] Y;
   input [width2*2-1:0] A;
   input [width3*2-1:0] B;
   
   assign Y[width1-1:0] = A[width2-1:0] - B[width3-1:0];
   assign Y[width1*2-1:width1] = ~((~A[width2*2-1:width2]) - (~B[width3*2-1:width3]));

endmodule // DVL_ARI_SUB

module F_DVL_MUX(Y, A, B, S);
   parameter width = 1;
   output [width*2-1:0] Y;
   input [width*2-1:0] A, B;
   input [1:0]	   S;   
   
   assign Y = S[0] ? B : A;
   
endmodule // DVL_MUX

module F_DVL_LOG_OR(Y, A, B);
   parameter width = 2;
   input [width*2-1:0] A, B;
   output [1:0] Y;
   
   assign Y[0] = A[width-1:0] || B[width-1:0];
   assign Y[1] = A[width*2-1:width] && B[width*2-1:width];
   
endmodule // DVL_LOG_OR

module F_DVL_LOG_AND(Y, A, B);
   parameter width = 2;
   input [width*2-1:0] A, B;
   output [1:0] Y;
   
   assign Y[0] = A[width-1:0] && B[width-1:0];
   assign Y[1] = A[width*2-1:width] || B[width*2-1:width];
   
endmodule // DVL_LOG_AND

module F_DVL_X_CELL(Y);
   parameter width = 1;
   output [width*2-1:0] Y;
   wire [width-1:0] zero = 'b0;
   
   assign Y[width-1:0] = ~zero;
   assign Y[width*2-1:width] = zero;
   
endmodule // DVL_X_CELL

module F_DVL_SH_L(Y, A, B);
   parameter width1 = 1;
   parameter width2 = 1;
   parameter width3 = 1;
   output [width1*2-1:0] Y;
   input [width2*2-1:0] A;
   input [width3*2-1:0] B;
   
   assign Y[width1-1:0] = A[width2-1:0] << B[width3-1:0];
   assign Y[width1*2-1:width1] = ~((~A[width2*2-1:width2]) << (~B[width3*2-1:width3]));

endmodule // DVL_SH_L

module F_DVL_SH_R(Y, A, B);
   parameter width1 = 1;
   parameter width2 = 1;
   parameter width3 = 1;
   output [width1*2-1:0] Y;
   input [width2*2-1:0] A;
   input [width3*2-1:0] B;
   
   assign Y[width1-1:0] = A[width2-1:0] >> B[width3-1:0];
   assign Y[width1*2-1:width1] = ~((~A[width2*2-1:width2]) >> (~B[width3*2-1:width3]));

endmodule // DVL_SH_R

module F_DVL_BW_XOR(Y, A, B);
   parameter width = 2;
   output [width*2-1:0] Y;
   input [width*2-1:0] A, B;
   
   assign Y[width-1:0] = A[width-1:0] ^ B[width-1:0];
   assign Y[width*2-1:width] = ~(A[width*2-1:width] ^ B[width*2-1:width]);
   
endmodule // DVL_BW_XOR

module F_DVL_BW_AND(Y, A, B);
   parameter width = 2;
   output [width*2-1:0] Y;
   input [width*2-1:0] A, B;

   assign Y[width-1:0] = A[width-1:0] & B[width-1:0];
   assign Y[width*2-1:width] = (A[width*2-1:width] | B[width*2-1:width]) ^ (A[width*2-1:width]^A[width-1:0]^B[width*2-1:width]^B[width-1:0]);   

endmodule // DVL_BW_AND

module F_DVL_BW_OR(Y, A, B);
   parameter width = 2;
   input [width*2-1:0] A, B;
   output [width*2-1:0] Y;

   assign Y[width-1:0] = A[width-1:0] | B[width-1:0];
   assign Y[width*2-1:width] = (A[width*2-1:width] & B[width*2-1:width]) ^ (A[width*2-1:width]^A[width-1:0]^B[width*2-1:width]^B[width-1:0]);   

endmodule // DVL_BW_OR

module F_DVL_DFF_SYNC(faultsim, Q, D, CK);
   parameter width = 1;
   input 	   faultsim;
   output reg [width*2-1:0] Q;
   input [width*2-1:0] D;
   input [1:0]	   CK;
   
   always @(posedge CK[0]) Q[width-1:0] = D[width-1:0];
   
   always @(negedge CK[1]) Q[width*2-1:width] = faultsim ? D[width-1:0] : D[width*2-1:width];
   
endmodule // DVL_DFF_SYNC

module F_BUF(I, O);

input [1:0] I;
output [1:0] O;

   assign O = I;
  
endmodule // F_BUF

module F_DVL_BUFIF1(Y, A, S);
parameter dataWidth=1;
output [dataWidth*2-1:0] Y;
input [dataWidth*2-1:0] A;
input [1:0] S;

wire Z = 1'bz;
   
assign Y = S[0] ? A : {{dataWidth{Z}},{dataWidth{Z}}};

endmodule // F_DVL_BUFIF1

module F_DVL_LOG_NOT(Y, A); //logical not
parameter width=1;
input [width*2-1:0] A;
output [1:0] Y;

assign Y[0] = !A[width-1:0];
assign Y[1] = ~(!(~A[width*2-1:width]));

endmodule

module F_DVL_NEQ(Y, A, B);
   parameter width1 = 1;
   parameter width2 = 1;
   output [1:0] Y;
   input [width1*2-1:0] A;
   input [width2*2-1:0] B;

   assign Y[0] = A[width1-1:0] != B[width2-1:0];
   assign Y[1] = A[width1*2-1:width1] == B[width2*2-1:width2];

endmodule // DVL_EQ
