`timescale 1ns/1ps

module mult_tb;

   reg [17:0] A, B;
   wire [35:0] P;

   initial
     begin
	$monitor(A,B,P);
	A = 0;
	B = 1;
	#1000 A = 10;
	#1000 B = 10;
	#1000 B = 42;
	#1000 A = 99;
	#1000 A = 65536;
	#1000 B = 1000;
	
     end

   MULT18X18 dut (A,B,P);


endmodule // adder_tb
