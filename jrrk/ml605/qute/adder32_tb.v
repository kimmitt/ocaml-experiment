`timescale 1ns/1ps

module adder_tb;

   reg [31:0] A, B;
   wire [31:0] Y;

   initial
     begin
	$monitor(A,B,Y);
	A = 0;
	B = 1;
	#1000 A = 10;
	#1000 B = 10;
	#1000 B = 42;
	#1000 A = 99;
	#1000 A = 65536;
	#1000 B = 1000;
	
     end

   DVL_ARI_ADD_dataWidthY_32_dataWidthA_32_dataWidthB_32 dut (A,B,Y);

endmodule // adder_tb
