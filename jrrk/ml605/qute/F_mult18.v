`timescale  1 ps / 1 ps

module F_MULT18X18 (P, A, B);

    output [71:0] P;

    input  [35:0] A;
    input  [35:0] B;

   wire [35:0] F_P, true_P;

MULT18X18 mult1 (
  .P(true_P),
  .A(A[17:0]),
  .B(B[17:0])
    );

MULT18X18 F_mult1 (
  .P(F_P),
  .A(~A[35:18]),
  .B(~B[35:18])
    );

   assign P = {~F_P, true_P};
   
endmodule
