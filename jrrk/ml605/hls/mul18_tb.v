module MUL18_TB;

    wire   [35:0] P;

    reg    [17:0] A;
    reg    [17:0] B;

MULT18X18 mult0 (
  .P(P),
  .A(A),
  .B(B)
    );

initial
  begin
     for (A = 0; A < 10; A=A+1)
       for (B = 0; B < 10; B=B+1)
	 #1000 $display("A=%X, B=%X, P=%X, expected = %X, err = %X", A, B, P, A*B, A*B-P);
  end
   
endmodule
