#!/bin/bash
export PATH=~/regression/ver_toolbox:$PATH
sed -e "s=%=$1=" -e "s=@=$HOME/regression=" > $$.scr << EOF
library_env @/xilinx_verilog/src/unisims/
scan_lib xilinx.skip
vparse ../../library.v
vparse @/xilinx_verilog/src/glbl.v
mods
read_lib
dump_lib %
bye
EOF
trap "echo generate lib failed" EXIT
~/regression/ver_toolbox/vscr $$.scr </dev/null || exit 1
trap "echo output in $1" EXIT
mv -f $$.scr debug_libgen.scr
exit 0
