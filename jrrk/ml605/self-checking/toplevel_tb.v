`timescale  1 us / 1 ps

module tb;

parameter siz = 18;
wire [31:0] nxtpc;

wire readstrobe;
reg [7:0] readch;
reg [31:0] stdin, stdout;
reg a23_clk, a23_rst, rst, dbg_x, dbg_extcall, dbg_regs, dbg_mem, dbg_str, dbg_arg, dbg_enc, dbg_exn, dbg_rom, dbg_goto, dbg_heap, dbg_return;

wire [31:0] sm_readwrite_addr;
wire  [3:0] sm_byte_strobes;
wire  [0:0] sm_write_enable;
wire  [0:0] sm_read_enable;
wire [31:0] sm_read_data;
wire [31:0] sm_write_data;
wire  [7:0] sm_writech;
wire [31:0] sm_readchan;
wire [31:0] sm_writechan;
wire [31:0] sm_flush;
   
wire [31:0] read_data;

wire [0:0] copro_write_data_wen;
wire [0:0] write_data_wen;
wire [1:0] reg_write_sel;
wire [1:0] byte_enable_sel;
wire [17:2] pc_nxt;
wire [2:0] pc_sel;
wire [2:0] address_sel;
wire [6:0] alu_function;
wire [1:0] barrel_shift_function;
wire [1:0] barrel_shift_data_sel;
wire [1:0] barrel_shift_amount_sel;
wire [3:0] rn_sel;
wire [3:0] rds_sel;
wire [3:0] rm_sel;
wire [4:0] imm_shift_amount;
wire [7:0] imm8;
wire [0:0] divrst;
wire [31:0] dividend;
wire [31:0] divider;
wire [0:0] flush;
wire [31:0] a23_crnt;
wire [0:0] read_enable;
wire [0:0] write_enable;
wire [3:0] a23_ccode;
wire [3:0] byte_strobes;
wire [31:0] readwrite_addr;
wire [31:0] write_data;

reg [31:0] crnt;
reg [31:0] prev_readwrite_addr;

wire [7:0] writech;
wire     [2:0]            interrupt_vector_sel = 0;

wire                      system_rdy = !a23_rst;
wire     [31:0]           copro_read_data = 0;

wire     [31:0]           execute_write_data;
wire     [31:0]           execute_write_data_nxt;
wire     [31:0]           execute_copro_write_data;
wire     [31:0]           execute_address;
wire                      execute_address_valid;
wire     [31:0]           execute_address_nxt;  // un-registered version of execute_address to the cache rams

wire     [3:0]            execute_byte_enable;
wire     [3:0]            execute_byte_enable_nxt;
wire                      execute_write_enable;

TOPLEVEL top(
	      .SYSCLK_N(~a23_clk), 
	      .SYSCLK_P(a23_clk), 
	      .CPU_RESET(a23_rst),
	      .CLK_33MHZ_FPGA(a23_clk), 
	      .GPIO_SW_C(1'b0),
	      .GPIO_SW_N(1'b0),
	      .GPIO_SW_S(1'b0),
	      .GPIO_SW_E(1'b0),
	      .GPIO_SW_W(1'b0),
	      .GPIO_LED(writech)
);

always @(negedge a23_clk)
	begin
	if (flush) $fflush(stdout);
	if (writech[7]) $fwrite(stdout, "%c", writech[6:0]);
	if (readstrobe) readch = $fgetc(stdin);
	prev_readwrite_addr = sm_readwrite_addr;
	end

initial
	begin
	if ($test$plusargs("DBG_DUMPVARS") != 0) $dumpvars;
	dbg_x = $test$plusargs("DBG_X") != 0;
	dbg_arg = $test$plusargs("DBG_ARGS") != 0;
	dbg_mem = $test$plusargs("DBG_MEM") != 0;
	dbg_str = $test$plusargs("DBG_STRING") != 0;
	dbg_goto = $test$plusargs("DBG_GOTO") != 0;
	dbg_heap = $test$plusargs("DBG_HEAP") != 0;
	dbg_regs = $test$plusargs("DBG_REGS") != 0;
	dbg_extcall = $test$plusargs("DBG_EXTCALL") != 0;
	dbg_enc = $test$plusargs("DBG_ENCODE") != 0;
	dbg_exn = $test$plusargs("DBG_EXCEPTION") != 0;
	dbg_rom = $test$plusargs("DBG_ROM") != 0;
	dbg_return = $test$plusargs("DBG_RETURN") != 0;
	stdin = $fopen("/dev/stdin", "r");
	stdout = $fopen("/dev/stdout", "w");
	rst = 1'b1;
	a23_rst = 1'b1;
	a23_clk = 1'b0;
	#16 a23_clk = 1'b1;
	#16 a23_clk = 1'b0;
	#16 a23_clk = 1'b1;
	#16 a23_clk = 1'b0;
	#16 a23_clk = 1'b1;
	#16 a23_clk = 1'b0;
	#16 a23_clk = 1'b1;
	#16 a23_clk = 1'b0; rst = 1'b0;
	#16 a23_clk = 1'b1;
	forever
		begin
		#16 a23_clk = 1'b0; a23_rst = 1'b0;
		#16 a23_clk = 1'b1;
		end
	end

endmodule
