module F_DVL_BUF(Y, A);
   parameter width = 1;
   input [width*2:1] A;
   output [width*2:1] Y;
   
   assign Y = A;
endmodule // DVL_BUF

module F_DVL_ARI_MUL(Y, A, B);
   parameter width1 = 1;
   parameter width2 = 1;
   parameter width3 = 1;
   output [width1*2:1] Y;
   input [width2*2:1] A;
   input [width3*2:1] B;
   
   assign Y = A*B;
endmodule // DVL_ARI_MUL

module F_DVL_EQ(Y, A, B);
   parameter width1 = 1;
   parameter width2 = 1;
   output [2:1] Y;
   input [width1*2:1] A;
   input [width2*2:1] B;
   
   assign Y = !(|(A[width1:1]^B[width2:1]));

endmodule // DVL_EQ

module F_DVL_BW_NOT(Y, A);
   parameter width = 1;
   input [width*2:1] A;
   output [width*2:1] Y;
   
   assign Y = ~A;

endmodule // DVL_BW_NOT

module F_DVL_RED_AND(Y, A);
   parameter width = 1;
   output [2:1] Y;
   input [width*2:1] A;
   
   assign Y = & A[width:1];
   
endmodule // DVL_RED_AND

module F_DVL_ARI_ADD(Y, A, B);
   parameter width1 = 1;
   parameter width2 = 1;
   parameter width3 = 1;
   output [width1*2:1] Y;
   input [width2*2:1] A;
   input [width3*2:1] B;
   
   assign Y = A+B;

endmodule // DVL_ARI_ADD

module F_DVL_ARI_SUB(Y, A, B);
   parameter width1 = 1;
   parameter width2 = 1;
   parameter width3 = 1;
   output [width1*2:1] Y;
   input [width2*2:1] A;
   input [width3*2:1] B;
   
   assign Y = A-B;
endmodule // DVL_ARI_SUB

module F_DVL_MUX(Y, A, B, S);
   parameter width = 1;
   output [width*2:1] Y;
   input [width*2:1] A, B;
   input [2:1]	   S;
   
   
   assign Y = S[1] ? B : A;
   
endmodule // DVL_MUX

module F_DVL_LOG_OR(Y, A, B);
   parameter width = 1;
   input [width*2:1] A, B;
   output [width*2:1] Y;
   
   assign Y = A[width:1] | B[width:1];
endmodule // DVL_LOG_OR

module F_DVL_LOG_AND(Y, A, B);
   parameter width = 1;
   input [width*2:1] A, B;
   output [width*2:1] Y;
   
   assign Y = A[width:1] & B[width:1];
endmodule // DVL_LOG_AND

module F_DVL_X_CELL(Y);
   parameter width = 1;
   output [width*2:1] Y;
   
   assign Y = -1;
endmodule // DVL_X_CELL

module F_DVL_SH_L(Y, A, B);
   parameter width1 = 1;
   parameter width2 = 1;
   parameter width3 = 1;
   output [width1*2:1] Y;
   input [width2*2:1] A;
   input [width3*2:1] B;
   
   assign Y = A << B[width3:1];

endmodule // DVL_SH_L

module F_DVL_SH_R(Y, A, B);
   parameter width1 = 1;
   parameter width2 = 1;
   parameter width3 = 1;
   output [width1*2:1] Y;
   input [width2*2:1] A;
   input [width3*2:1] B;
   
   assign Y = A[width2:1] >> B[width3:1];

endmodule // DVL_SH_R

module F_DVL_BW_XOR(Y, A, B);
   parameter width = 1;
   output [width*2:1] Y;
   input [width*2:1] A, B;
   
   assign Y = A^B;
endmodule // DVL_BW_XOR

module F_DVL_BW_AND(Y, A, B);
   parameter width = 1;
   output [width*2:1] Y;
   input [width*2:1] A, B;
   
   assign Y = A&B;
endmodule // DVL_BW_AND

module F_DVL_BW_OR(Y, A, B);
   parameter width = 1;
   input [width*2:1] A, B;
   output [width*2:1] Y;
   
   assign Y = A|B;
endmodule // DVL_BW_OR

module F_DVL_DFF_SYNC(Q, D, CK);
   parameter width = 1;
   output reg [width*2:1] Q;
   input [width*2:1] D;
   input [2:1]	   CK;
   
   always @(posedge CK[1]) Q = D;
endmodule // DVL_DFF_SYNC

module F_DVL_RED_OR(Y, A);
   parameter width = 1;
   output [2:1] Y;
   input [width*2:1] A;
   
   assign Y = | A[width:1];
   
endmodule // DVL_RED_OR

