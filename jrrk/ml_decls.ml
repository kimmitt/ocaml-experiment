open Mylib

type io =
  { char_of_int: Datatypes.nat;
    print_char: Datatypes.nat;
    exn_failwith: Datatypes.nat }

type type_t =
  | TUnit
  | TBool
  | TChar
  | TString
  | TInt
  | TFloat
  | TFun of type_t list * type_t
  | TTuple of type_t list
  | TArray of type_t
  | TVar of type_t option ref
  | TUser of string
  | Unknown

type argdef = (string * type_t) list

type syntax_t =
  | Unit
  | Bool of bool
  | Char of char
  | String of string
  | Int of int
  | Float of Fappli_IEEE.binary_float
  | Not of syntax_t
  | Neg of syntax_t
  | Add of syntax_t * syntax_t
  | Sub of syntax_t * syntax_t
  | Mul of syntax_t * syntax_t
  | Div of syntax_t * syntax_t
  | Mod of syntax_t * syntax_t
  | FNeg of syntax_t
  | FAdd of syntax_t * syntax_t
  | FSub of syntax_t * syntax_t
  | FMul of syntax_t * syntax_t
  | FDiv of syntax_t * syntax_t
  | Eq of syntax_t * syntax_t
  | LE of syntax_t * syntax_t
  | LT of syntax_t * syntax_t
  | GE of syntax_t * syntax_t
  | GT of syntax_t * syntax_t
  | If of syntax_t * syntax_t * syntax_t
  | Let of (string * type_t) * syntax_t * syntax_t
  | Var of string
  | LetRec of fundef * syntax_t
  | App of syntax_t * syntax_t list
  | Tuple of syntax_t list
  | LetTuple of (string * type_t) list * syntax_t * syntax_t
  | Abstraction of syntax_t * syntax_t
  | Compose of syntax_t * syntax_t
  | Declare of bool * syntax_t * syntax_t * syntax_t
  | List of syntax_t list
  | Match of string * syntax_t list
  | TypCons of string * type_t
  | ArgCons of syntax_t * type_t
  | INTEGER of int
  | STRING of string
  | LISTCONS of syntax_t list
  | TYPCONS of string * syntax_t
  | TOP of syntax_t list
  | STR of syntax_t
  | ANY of syntax_t
  | LBL of string * syntax_t
  | TYPE of syntax_t list
  | TYPDECL of syntax_t list
  | REC of syntax_t list
  | NONREC of syntax_t list
  | LET of syntax_t list * syntax_t
  | PATTERN of string * syntax_t
  | MATCH of string * syntax_t list
  | PATEXP of syntax_t * string
  | TUPLE of syntax_t list
  | FORMAL of string list * syntax_t
  | TYPLOC of string * type_t
  | FUNARG of syntax_t list
  | PATCASE of string * syntax_t list * syntax_t
  | CONSTRAINED of string * string
  | APPLY1 of string * syntax_t
  | APPLY2 of string * syntax_t * syntax_t
  | APPLYLST of string * syntax_t list

and fundef = { name : string * type_t; args : argdef; body : syntax_t }

type debug_t = {
    mutable debugrun: bool;
    mutable debugall: bool;
    mutable stamping: bool;
    mutable memflush: bool;
    mutable memosize: int;
  }

type impl =
  { mutable string_of_float: Fappli_IEEE.binary_float -> string;
    mutable float_of_string: string -> Fappli_IEEE.binary_float;
    mutable dump: syntax_t -> string;
    debug: debug_t}

type codes = {code: (string * Datatypes.nat) list ref; impl: impl; io: io }

let gentyp () = TVar(ref None)
let counter = ref 0
let gentmp typ =
  incr counter;
  "T_"^string_of_int (!counter)

type top_t =
  | Type of string * syntax_t list
  | LetTop of (string * type_t) * syntax_t
  | LetRecTop of fundef
  | TopIn of syntax_t
  | Ctype of Ctypes0.ctypes

let addtyp x = (x, gentyp ())

let rec asNat = function
  | 0 -> Datatypes.O
  | n -> Datatypes.S (asNat(n-1))

let rec fromNat = function
  | Datatypes.O -> 0
  | Datatypes.S num -> 1 + fromNat num

let rec natstr = function
  | Datatypes.O -> "O"
  | Datatypes.S num -> "S("^natstr num^")"

(*
let rec present x = function
  | [] -> print_endline (x^" : not present"); false
  | (a, b) :: l -> let rslt = (x = a) in print_endline("present "^x^" "^a^": "^string_of_bool rslt); (rslt || present x l)

let rec find x = function
  | [] -> print_endline (x^" : not present"); raise Not_found
  | (a,b) :: l -> let msg = "find "^x^" "^a^": " in if (x = a) then (print_endline (msg^natstr b); b) else (print_endline (msg^"false"); find x l)
*)

let present = List.mem_assoc' String.eqb
let find = List.assoc' String.eqb

let encode code str = if String.eqb str "" then
          begin
          let len = List.length !code in
	  let str = "T_"^string_of_int len in
	  let itm = asNat len in
	  code := (str, itm) :: !code;
	  itm
	  end
    else
          begin
	  if not (present str !code) then
	    begin
	    let enc = (str, asNat (List.length !code)) in
(*
	    print_endline ("Added: "^str^" with encoding "^natstr (snd enc));
*)
	    code := enc :: !code;
	    end;
	  find str !code
	  end

let init_codes () =
  let code = ref [] in
  let c = encode code "char_of_int" in
  let p = encode code "print_char" in
  let x = encode code "failwith" in
  {code=code;
    impl={string_of_float=(fun _ -> "_");
	   float_of_string=(fun str -> Fappli_IEEE.B754_zero false);
	   dump=(fun syntax -> "(dump not enabled)");
	   debug={debugrun = false; debugall = false; stamping = false; memflush = true; memosize = 8};
	 };
    io=
         { char_of_int = c;
           print_char = p;
           exn_failwith = x; }}

let decode code num =
  let rslt = ref ("anon_"^string_of_int (fromNat num)) in
  List.iter (fun (str, id) -> if EqNat.beq_nat num id then rslt := str) !code;
  !rslt
