open BinInt
open BinNums
open Datatypes
open ZArith_dec

val coq_Zdiv_eucl_exist : coq_Z -> coq_Z -> (coq_Z, coq_Z) prod

val coq_Zmod_POS : positive -> coq_Z -> coq_Z

val coq_Zmod' : coq_Z -> coq_Z -> coq_Z

val coq_Zdiv_eucl_extended : coq_Z -> coq_Z -> (coq_Z, coq_Z) prod

