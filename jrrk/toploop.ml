open Ml_compiling
open Exec

let _ = while true do
  print_endline "Enter a phrase: ";
  let str = read_line() in
  let str1 = compile'' str in
  let str2 = rw str1 in
  let str3 = exec_till str2 in
  print_endline str3
  done
