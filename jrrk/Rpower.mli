open RIneq
open Rdefinitions
open Rsqrt_def
open Rtrigo_def

val ln_exists1 : coq_R -> coq_R

val ln_exists : coq_R -> coq_R

val coq_Rln : posreal -> coq_R

val ln : coq_R -> coq_R

val coq_Rpower : coq_R -> coq_R -> coq_R

