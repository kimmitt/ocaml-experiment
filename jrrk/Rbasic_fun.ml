open RIneq
open Rdefinitions

type __ = Obj.t
let __ = let rec f _ = Obj.repr f in Obj.repr f

(** val coq_Rmin : coq_R -> coq_R -> coq_R **)

let coq_Rmin x y =
  if coq_Rle_dec x y then x else y

(** val coq_Rmin_case : coq_R -> coq_R -> 'a1 -> 'a1 -> 'a1 **)

let coq_Rmin_case r1 r2 h1 h2 =
  if coq_Rle_dec r1 r2 then h1 else h2

(** val coq_Rmin_case_strong :
    coq_R -> coq_R -> (__ -> 'a1) -> (__ -> 'a1) -> 'a1 **)

let coq_Rmin_case_strong r1 r2 h1 h2 =
  let s = coq_Rle_dec r1 r2 in if s then h1 __ else h2 __

(** val coq_Rmax : coq_R -> coq_R -> coq_R **)

let coq_Rmax x y =
  if coq_Rle_dec x y then y else x

(** val coq_Rmax_case : coq_R -> coq_R -> 'a1 -> 'a1 -> 'a1 **)

let coq_Rmax_case r1 r2 h1 h2 =
  if coq_Rle_dec r1 r2 then h2 else h1

(** val coq_Rmax_case_strong :
    coq_R -> coq_R -> (__ -> 'a1) -> (__ -> 'a1) -> 'a1 **)

let coq_Rmax_case_strong r1 r2 h1 h2 =
  if coq_Rle_dec r1 r2 then h2 __ else h1 __

(** val coq_Rcase_abs : coq_R -> bool **)

let coq_Rcase_abs r =
  let x = coq_Rle_dec coq_R0 r in if x then false else true

(** val coq_Rabs : coq_R -> coq_R **)

let coq_Rabs r =
  if coq_Rcase_abs r then coq_Ropp r else r

