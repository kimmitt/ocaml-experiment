open BinNums
open BinPos
open Datatypes
open Peano
open Rbasic_fun
open Rdefinitions
open Rpow_def

val powerRZ : coq_R -> coq_Z -> coq_R

val decimal_exp : coq_R -> coq_Z -> coq_R

val sum_nat_f_O : (nat -> nat) -> nat -> nat

val sum_nat_f : nat -> nat -> (nat -> nat) -> nat

val sum_nat_O : nat -> nat

val sum_nat : nat -> nat -> nat

val sum_f_R0 : (nat -> coq_R) -> nat -> coq_R

val sum_f : nat -> nat -> (nat -> coq_R) -> coq_R

val coq_R_dist : coq_R -> coq_R -> coq_R

