open Datatypes
open RIneq
open Ranalysis1
open Raxioms
open Rdefinitions
open Rpow_def
open SeqProp
open Specif

val coq_Dichotomy_lb : coq_R -> coq_R -> (coq_R -> bool) -> nat -> coq_R

val coq_Dichotomy_ub : coq_R -> coq_R -> (coq_R -> bool) -> nat -> coq_R

val dicho_lb : coq_R -> coq_R -> (coq_R -> bool) -> nat -> coq_R

val dicho_up : coq_R -> coq_R -> (coq_R -> bool) -> nat -> coq_R

val dicho_lb_cv : coq_R -> coq_R -> (coq_R -> bool) -> coq_R

val dicho_up_cv : coq_R -> coq_R -> (coq_R -> bool) -> coq_R

val pow_2_n : nat -> coq_R

val cond_positivity : coq_R -> bool

val coq_IVT : (coq_R -> coq_R) -> coq_R -> coq_R -> coq_R

val coq_IVT_cor : (coq_R -> coq_R) -> coq_R -> coq_R -> coq_R

val coq_Rsqrt_exists : coq_R -> coq_R

val coq_Rsqrt : nonnegreal -> coq_R

