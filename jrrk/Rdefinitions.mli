open BinNums

type coq_R = Axioms.coq_R

val coq_R0 : coq_R

val coq_R1 : coq_R

val coq_Rplus : coq_R -> coq_R -> coq_R

val coq_Rmult : coq_R -> coq_R -> coq_R

val coq_Ropp : coq_R -> coq_R

val coq_Rinv : coq_R -> coq_R

val up : coq_R -> coq_Z

val coq_Rminus : coq_R -> coq_R -> coq_R

val coq_Rdiv : coq_R -> coq_R -> coq_R

