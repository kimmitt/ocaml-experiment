open Mylib
open Short
open BinNums

let rec asNat = function
  | 0 -> Datatypes.O
  | n -> Datatypes.S (asNat(n-1))

let rec asPi = function
  | 0 -> failwith "not positive"
  | 1 -> Coq_xH
  | n -> let asp = asPi(n lsr 1) in
    if n land 1 = 1 then Coq_xI asp else Coq_xO asp

let asZi = function
  | 0 -> Z0
  | 1 -> Zpos Coq_xH
  | n -> if n < 0 then Zneg (asPi(-n)) else Zpos (asPi n)

let rec fromPi = function
  | Coq_xH -> 1
  | Coq_xO num -> (fromPi num) lsl 1
  | Coq_xI num -> ((fromPi num) lsl 1) + 1
      
let fromZi = function
  | Z0 -> 0
  | Zpos num -> fromPi num
  | Zneg num -> - (fromPi num)
      
let rec reduce_all t =
  match t with
  | Coq_tm_nat num -> num
  | oth -> reduce_all (reduce t)

let _ = for i = 0 to 10 do
  print_int i; print_char '!'; print_char ' '; print_char '=';
  let num = reduce_all(fact_calc (asZi i)) in
  print_char ' '; print_int (fromZi num); print_newline()
done
