open BinInt
open BinNums
open Zbool

val coq_Zplus : coq_Z -> coq_Z -> coq_Z

val coq_Zminus : coq_Z -> coq_Z -> coq_Z

val coq_Zsucc : coq_Z -> coq_Z

val coq_Zpred : coq_Z -> coq_Z

val coq_Zmult : coq_Z -> coq_Z -> coq_Z

val coq_Zdiv : coq_Z -> coq_Z -> coq_Z

val coq_Zmod : coq_Z -> coq_Z -> coq_Z

val coq_Zle_bool : coq_Z -> coq_Z -> bool

val coq_Zge_bool : coq_Z -> coq_Z -> bool

val coq_Zlt_bool : coq_Z -> coq_Z -> bool

val coq_Zgt_bool : coq_Z -> coq_Z -> bool

val coq_Zeq_bool : coq_Z -> coq_Z -> bool

val coq_Zneq_bool : coq_Z -> coq_Z -> bool

