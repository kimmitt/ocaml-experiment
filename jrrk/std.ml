type in_channel
type out_channel

external open_descriptor_in : int -> in_channel = "caml_ml_open_descriptor_in"
external open_descriptor_out : int -> out_channel = "caml_ml_open_descriptor_out"
external ( < ) : 'a -> 'a -> bool = "%lessthan"
external ( <= ) : 'a -> 'a -> bool = "%lessequal"
external ( > ) : 'a -> 'a -> bool = "%greaterthan"
external ( >= ) : 'a -> 'a -> bool = "%greaterequal"
external ( + ) : int -> int -> int = "%addint"
external ( - ) : int -> int -> int = "%subint"
external ( ~- ) : int -> int = "%negint"
external ( lsl ) : int -> int -> int = "%lslint"
external ( lsr ) : int -> int -> int = "%lsrint"
external ( && ) : bool -> bool -> bool = "%sequand"
external register_named_value : string -> 'a -> unit = "caml_register_named_value"
external raise : exn -> 'a = "%raise"
external hw_exn : unit -> bool = "caml_hw_exn"

let in_ = open_descriptor_in 0
let out_ = open_descriptor_out 1
let _ = if hw_exn() then raise (Invalid_argument "hw_err")
