open Raxioms
open Rdefinitions
open Specif

type __ = Obj.t

val coq_Rlt_dec : coq_R -> coq_R -> bool

val coq_Rle_dec : coq_R -> coq_R -> bool

val coq_Rgt_dec : coq_R -> coq_R -> bool

val coq_Rge_dec : coq_R -> coq_R -> bool

val coq_Rlt_le_dec : coq_R -> coq_R -> bool

val coq_Rgt_ge_dec : coq_R -> coq_R -> bool

val coq_Rle_lt_dec : coq_R -> coq_R -> bool

val coq_Rge_gt_dec : coq_R -> coq_R -> bool

val coq_Rle_lt_or_eq_dec : coq_R -> coq_R -> bool

val inser_trans_R : coq_R -> coq_R -> coq_R -> coq_R -> bool

val coq_Rsqr : coq_R -> coq_R

type nonnegreal =
  coq_R
  (* singleton inductive, whose constructor was mknonnegreal *)

val nonnegreal_rect : (coq_R -> __ -> 'a1) -> nonnegreal -> 'a1

val nonnegreal_rec : (coq_R -> __ -> 'a1) -> nonnegreal -> 'a1

val nonneg : nonnegreal -> coq_R

type posreal =
  coq_R
  (* singleton inductive, whose constructor was mkposreal *)

val posreal_rect : (coq_R -> __ -> 'a1) -> posreal -> 'a1

val posreal_rec : (coq_R -> __ -> 'a1) -> posreal -> 'a1

val pos : posreal -> coq_R

type nonposreal =
  coq_R
  (* singleton inductive, whose constructor was mknonposreal *)

val nonposreal_rect : (coq_R -> __ -> 'a1) -> nonposreal -> 'a1

val nonposreal_rec : (coq_R -> __ -> 'a1) -> nonposreal -> 'a1

val nonpos : nonposreal -> coq_R

type negreal =
  coq_R
  (* singleton inductive, whose constructor was mknegreal *)

val negreal_rect : (coq_R -> __ -> 'a1) -> negreal -> 'a1

val negreal_rec : (coq_R -> __ -> 'a1) -> negreal -> 'a1

val neg : negreal -> coq_R

type nonzeroreal =
  coq_R
  (* singleton inductive, whose constructor was mknonzeroreal *)

val nonzeroreal_rect : (coq_R -> __ -> 'a1) -> nonzeroreal -> 'a1

val nonzeroreal_rec : (coq_R -> __ -> 'a1) -> nonzeroreal -> 'a1

val nonzero : nonzeroreal -> coq_R

