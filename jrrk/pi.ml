let rec print_int n = if n > 9l then print_int (Int32.div n 10l); print_char (Char.unsafe_chr (Int32.to_int (Int32.add(Int32.rem n 10l) (Int32.of_int(Char.code '0')))))
let div x y = Int32.div x y

let _ = print_int (div (355000000l) (113l)); print_char '\n'

