type in_channel
type out_channel
external open_descriptor_in : int -> in_channel
  = "caml_ml_open_descriptor_in"
external open_descriptor_out : int -> out_channel
  = "caml_ml_open_descriptor_out"
val in_ : in_channel
val out_ : out_channel
