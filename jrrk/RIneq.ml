open Raxioms
open Rdefinitions
open Specif

type __ = Obj.t
let __ = let rec f _ = Obj.repr f in Obj.repr f

(** val coq_Rlt_dec : coq_R -> coq_R -> bool **)

let coq_Rlt_dec r1 r2 =
  let h = total_order_T r1 r2 in
  (match h with
   | Coq_inleft x -> x
   | Coq_inright -> false)

(** val coq_Rle_dec : coq_R -> coq_R -> bool **)

let coq_Rle_dec r1 r2 =
  let h = total_order_T r1 r2 in
  (match h with
   | Coq_inleft x -> true
   | Coq_inright -> false)

(** val coq_Rgt_dec : coq_R -> coq_R -> bool **)

let coq_Rgt_dec r1 r2 =
  coq_Rlt_dec r2 r1

(** val coq_Rge_dec : coq_R -> coq_R -> bool **)

let coq_Rge_dec r1 r2 =
  coq_Rle_dec r2 r1

(** val coq_Rlt_le_dec : coq_R -> coq_R -> bool **)

let coq_Rlt_le_dec r1 r2 =
  let h = total_order_T r1 r2 in
  (match h with
   | Coq_inleft x -> x
   | Coq_inright -> false)

(** val coq_Rgt_ge_dec : coq_R -> coq_R -> bool **)

let coq_Rgt_ge_dec r1 r2 =
  coq_Rlt_le_dec r2 r1

(** val coq_Rle_lt_dec : coq_R -> coq_R -> bool **)

let coq_Rle_lt_dec r1 r2 =
  let h = total_order_T r1 r2 in
  (match h with
   | Coq_inleft x -> true
   | Coq_inright -> false)

(** val coq_Rge_gt_dec : coq_R -> coq_R -> bool **)

let coq_Rge_gt_dec r1 r2 =
  coq_Rle_lt_dec r2 r1

(** val coq_Rle_lt_or_eq_dec : coq_R -> coq_R -> bool **)

let coq_Rle_lt_or_eq_dec r1 r2 =
  let h0 = total_order_T r1 r2 in
  (match h0 with
   | Coq_inleft x -> x
   | Coq_inright -> false)

(** val inser_trans_R : coq_R -> coq_R -> coq_R -> coq_R -> bool **)

let inser_trans_R n m p q =
  coq_Rlt_le_dec m q

(** val coq_Rsqr : coq_R -> coq_R **)

let coq_Rsqr r =
  coq_Rmult r r

type nonnegreal =
  coq_R
  (* singleton inductive, whose constructor was mknonnegreal *)

(** val nonnegreal_rect : (coq_R -> __ -> 'a1) -> nonnegreal -> 'a1 **)

let nonnegreal_rect f n =
  f n __

(** val nonnegreal_rec : (coq_R -> __ -> 'a1) -> nonnegreal -> 'a1 **)

let nonnegreal_rec f n =
  f n __

(** val nonneg : nonnegreal -> coq_R **)

let nonneg n =
  n

type posreal =
  coq_R
  (* singleton inductive, whose constructor was mkposreal *)

(** val posreal_rect : (coq_R -> __ -> 'a1) -> posreal -> 'a1 **)

let posreal_rect f p =
  f p __

(** val posreal_rec : (coq_R -> __ -> 'a1) -> posreal -> 'a1 **)

let posreal_rec f p =
  f p __

(** val pos : posreal -> coq_R **)

let pos p =
  p

type nonposreal =
  coq_R
  (* singleton inductive, whose constructor was mknonposreal *)

(** val nonposreal_rect : (coq_R -> __ -> 'a1) -> nonposreal -> 'a1 **)

let nonposreal_rect f n =
  f n __

(** val nonposreal_rec : (coq_R -> __ -> 'a1) -> nonposreal -> 'a1 **)

let nonposreal_rec f n =
  f n __

(** val nonpos : nonposreal -> coq_R **)

let nonpos n =
  n

type negreal =
  coq_R
  (* singleton inductive, whose constructor was mknegreal *)

(** val negreal_rect : (coq_R -> __ -> 'a1) -> negreal -> 'a1 **)

let negreal_rect f n =
  f n __

(** val negreal_rec : (coq_R -> __ -> 'a1) -> negreal -> 'a1 **)

let negreal_rec f n =
  f n __

(** val neg : negreal -> coq_R **)

let neg n =
  n

type nonzeroreal =
  coq_R
  (* singleton inductive, whose constructor was mknonzeroreal *)

(** val nonzeroreal_rect : (coq_R -> __ -> 'a1) -> nonzeroreal -> 'a1 **)

let nonzeroreal_rect f n =
  f n __

(** val nonzeroreal_rec : (coq_R -> __ -> 'a1) -> nonzeroreal -> 'a1 **)

let nonzeroreal_rec f n =
  f n __

(** val nonzero : nonzeroreal -> coq_R **)

let nonzero n =
  n

