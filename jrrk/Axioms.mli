type coq_R = R0 | R1
val coq_R0 : coq_R
val coq_R1 : coq_R
val coq_Rplus : 'a -> 'b -> 'c
val coq_Rmult : 'a -> 'b -> 'c
val coq_Ropp : 'a -> 'b
val coq_Rinv : 'a -> 'b
val up : 'a -> 'b
val coq_Rminus : 'a -> 'b -> 'c
val coq_Rdiv : 'a -> 'b -> 'c
val total_order_T : 'a -> 'b -> 'c
val completeness : 'a -> 'b
