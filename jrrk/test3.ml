type out_channel

external open_descriptor_out : int -> out_channel = "caml_ml_open_descriptor_out"
external output_char : out_channel -> char -> unit = "caml_ml_output_char"
external unsafe_output : out_channel -> string -> int -> int -> unit = "caml_ml_output"
external flush : out_channel -> unit = "caml_ml_flush"
external string_length : string -> int = "%string_length"
external ( > ) : 'a -> 'a -> bool = "%greaterthan"
external ( < ) : 'a -> 'a -> bool = "%lessthan"
external ( + ) : int -> int -> int = "%addint"
external ( - ) : int -> int -> int = "%subint"
external ( *  ) : int -> int -> int = "%mulint"
external ( / ) : int -> int -> int = "%divint"
external ( mod ) : int -> int -> int = "%modint"

let output_string oc s = unsafe_output oc s 0 (string_length s)

let stdout = open_descriptor_out 1

let print_char c = output_char stdout c
let rec print_int n = if n > 9 then print_int (n/10); print_char (Char.unsafe_chr (n mod 10 + Char.code '0'))
let print_int n = if n < 0 then (print_char '-'; print_int (0-n)) else print_int n

let _ = print_char 'J'; print_char 'K'; print_int 12345; print_char '\n'; flush stdout
