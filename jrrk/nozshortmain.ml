open Mylib
open Nozshort
      
let rec reduce_all t =
  match t with
  | Coq_tm_nat num -> num
  | oth -> reduce_all (reduce t)

let _ = for i = 0 to 10 do
  print_int i; print_char '!'; print_char ' '; print_char '=';
  let num = reduce_all(fact_calc (i)) in
  print_char ' '; print_int (num); print_newline()
done
