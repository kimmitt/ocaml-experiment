open Datatypes
open RIneq
open Ranalysis1
open Raxioms
open Rdefinitions
open Rpow_def
open SeqProp
open Specif

(** val coq_Dichotomy_lb :
    coq_R -> coq_R -> (coq_R -> bool) -> nat -> coq_R **)

let rec coq_Dichotomy_lb x y p = function
| O -> x
| S n0 ->
  let down = coq_Dichotomy_lb x y p n0 in
  let up = coq_Dichotomy_ub x y p n0 in
  let z = coq_Rdiv (coq_Rplus down up) (coq_Rplus coq_R1 coq_R1) in
  if p z then down else z

(** val coq_Dichotomy_ub :
    coq_R -> coq_R -> (coq_R -> bool) -> nat -> coq_R **)

and coq_Dichotomy_ub x y p = function
| O -> y
| S n0 ->
  let down = coq_Dichotomy_lb x y p n0 in
  let up = coq_Dichotomy_ub x y p n0 in
  let z = coq_Rdiv (coq_Rplus down up) (coq_Rplus coq_R1 coq_R1) in
  if p z then z else up

(** val dicho_lb : coq_R -> coq_R -> (coq_R -> bool) -> nat -> coq_R **)

let dicho_lb x y p n =
  coq_Dichotomy_lb x y p n

(** val dicho_up : coq_R -> coq_R -> (coq_R -> bool) -> nat -> coq_R **)

let dicho_up x y p n =
  coq_Dichotomy_ub x y p n

(** val dicho_lb_cv : coq_R -> coq_R -> (coq_R -> bool) -> coq_R **)

let dicho_lb_cv x y p =
  growing_cv (dicho_lb x y p)

(** val dicho_up_cv : coq_R -> coq_R -> (coq_R -> bool) -> coq_R **)

let dicho_up_cv x y p =
  decreasing_cv (dicho_up x y p)

(** val pow_2_n : nat -> coq_R **)

let pow_2_n n =
  pow (coq_Rplus coq_R1 coq_R1) n

(** val cond_positivity : coq_R -> bool **)

let cond_positivity x =
  if coq_Rle_dec coq_R0 x then true else false

(** val coq_IVT : (coq_R -> coq_R) -> coq_R -> coq_R -> coq_R **)

let coq_IVT f x y =
  dicho_up_cv x y (fun z -> cond_positivity (f z))

(** val coq_IVT_cor : (coq_R -> coq_R) -> coq_R -> coq_R -> coq_R **)

let coq_IVT_cor f x y =
  match total_order_T coq_R0 (f x) with
  | Coq_inleft s ->
    (match total_order_T coq_R0 (f y) with
     | Coq_inleft s0 ->
       if s then if s0 then assert false (* absurd case *) else y else x
     | Coq_inright -> if s then coq_IVT (opp_fct f) x y else x)
  | Coq_inright ->
    (match total_order_T coq_R0 (f y) with
     | Coq_inleft s -> if s then coq_IVT f x y else y
     | Coq_inright -> assert false (* absurd case *))

(** val coq_Rsqrt_exists : coq_R -> coq_R **)

let coq_Rsqrt_exists y =
  let f = fun x -> coq_Rminus (coq_Rsqr x) y in
  (match total_order_T y coq_R1 with
   | Coq_inleft s -> if s then coq_IVT_cor f coq_R0 coq_R1 else coq_R1
   | Coq_inright -> coq_IVT_cor f coq_R0 y)

(** val coq_Rsqrt : nonnegreal -> coq_R **)

let coq_Rsqrt y =
  coq_Rsqrt_exists (nonneg y)

