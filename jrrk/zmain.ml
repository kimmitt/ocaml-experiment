open BinNums
open Axiom_io
open BinPos
open BinNat
open BinInt
open Ml_decls
open MoreZStlc
open STLCExtended
open Examples
open Fappli_IEEE_bits
open Ml_compiling
open Exec

(*
let typing arg =
(*
  let e = Ml_types.g Ml_types.M.empty arg in
  if Ml_types.deref_typ e <> TUnit then
     print_endline "warning: final result does not have type unit.";
*)
  Ml_types.deref_term arg
*)

let exec1 prog = exec_till (strcompile prog)

let exec2 prog = exec_till (mycompile prog)

let exec3 fil n0 =
  let codes,cod' = mycompile fil in
  exec_till (codes,Coq_tm_app(cod', Coq_tm_nat (asZ n0)))

let args argv = match Array.length(argv) with
  | 1 -> ()
  | 2 -> let prog = argv.(1) in Printf.printf "%s () = %s\n" prog (exec2 prog)
  | 3 -> let prog = argv.(1) and n = argv.(2) in
	 Printf.printf "%s(%s) = %s\n" prog argv.(2) (exec3 prog n)
  | _ -> let prg = argv.(1) in Printf.printf "Usage: %s prog or %s prog arg\n" prg prg

let _ = args Sys.argv
