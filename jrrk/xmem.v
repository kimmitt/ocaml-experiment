module xmem(
input           a23_clk, a23_rst, dbg_mem, hw_exn,

output          finish, hw_exn_en,
output          readstrobe,
input   [7:0]   readch,
output  [7:0]   writech,
output  [31:0]  pc_nxt_out,

output  [31:0]  r0_out,
output  [31:0]  r1_out,
output  [31:0]  r2_out,
output  [31:0]  r3_out,
output  [31:0]  r4_out,
output  [31:0]  r5_out,
output  [31:0]  r6_out,
output  [31:0]  r7_out,
output  [31:0]  r8_out,
output  [31:0]  r9_out,
output  [31:0]  r10_out,
output  [31:0]  r11_out,
output  [31:0]  r12_out,
output  [31:0]  r13_out,
output  [31:0]  r14_out,
output  [31:0]  r15_out);

parameter siz = 18;

/*
wire [0:0] divrst;
wire [31:0] dividend;
wire [31:0] divider;
*/

wire                      system_rdy = !a23_rst;

wire [31:0] 		  read_data;
   
wire     [31:0]           execute_write_data_nxt;
wire     [31:0]           execute_address_nxt;  // un-registered version of execute_address to the cache rams

wire     [3:0]            execute_byte_enable_nxt;
wire                      execute_read_enable;

wire [75:0] xstate;
wire       a23_execute_read_enable = a23_rst | !finish;
progmem rom1(.clk(a23_clk), .din(xstate), .addr(pc_nxt_out[17:2]), .we(1'b0), .dout(xstate), .en({10{a23_execute_read_enable}}));
standalone top(
		.a23_clk(a23_clk), 
		.a23_rst(a23_rst),
		.dbg_mem(dbg_mem), 
		.read_data(read_data),
		.execute_write_data(),
		.execute_write_data_nxt(execute_write_data_nxt),
		.execute_address(),
		.execute_address_nxt(execute_address_nxt),
		.execute_byte_enable_nxt(execute_byte_enable_nxt),
		.execute_write_enable(),
		.write_data_wen_out(execute_write_data_wen),
		.read_enable_out(execute_read_enable),
		.finish(finish),
		.readstrobe(readstrobe),
		.readch(readch),
		.writech(writech),
		.r0_out(r0_out),
		.r1_out(r1_out),
		.r2_out(r2_out),
		.r3_out(r3_out),
		.r4_out(r4_out),
		.r5_out(r5_out),
		.r6_out(r6_out),
		.r7_out(r7_out),
		.r8_out(r8_out),
		.r9_out(r9_out),
		.r10_out(r10_out),
		.r11_out(r11_out),
		.r12_out(r12_out),
		.r13_out(r13_out),
		.r14_out(r14_out),
		.r15_out(r15_out),
		.pc_nxt_out(pc_nxt_out),
		.xstate(xstate),
		.hw_exn(hw_exn),
		.hw_exn_en(hw_exn_en)
);

mem_block
  a23_mem(.clk(a23_clk),
	.readwrite_addr(execute_address_nxt[siz+1:0]),
	.byte_strobes(execute_byte_enable_nxt),
	.write_enable(execute_write_data_wen),
	.read_enable(execute_read_enable),
	.write_data(execute_write_data_nxt),
	.read_data(read_data),
	.dbg_mem(dbg_mem));

endmodule
