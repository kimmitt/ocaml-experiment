open Mylib
open Dump
open BinNums
open Axiom_io
open BinPos
open BinNat
open BinInt
open Ml_decls
open MoreZStlc
open STLCExtended
open Examples
open Fappli_IEEE
open Fappli_IEEE_bits
open To_from_Z

type reduced =
  | CntInt of int*string
  | Reduced of int*string

let rec ty_equal = function
  | (Coq_ty_arrow (t0, t1), Coq_ty_arrow (t2, t3)) -> ty_equal(t0,t2) && ty_equal(t1,t3)
  | (Coq_ty_prod (t0, t1), Coq_ty_prod (t2, t3)) -> ty_equal(t0,t2) && ty_equal(t1,t3)
  | (Coq_ty_sum (t0, t1), Coq_ty_sum (t2, t3)) -> ty_equal(t0,t2) && ty_equal(t1,t3)
  | (Coq_ty_List t0, Coq_ty_List t1) -> ty_equal(t0,t1)
  | (Coq_ty_Nat, Coq_ty_Nat) -> true
  | (Coq_ty_Flt, Coq_ty_Flt) -> true
  | (_,_) -> false

let sgn_equal = function
  | (true,true) -> true
  | (false,false) -> true
  | (_,_) -> false

let flt_equal = function
  | (B754_finite (a, b, c), B754_finite (x, y, z)) -> sgn_equal(a, x) && Pos.eqb b y && Z.eqb c z
  | (B754_zero arg1, B754_zero arg2) -> sgn_equal(arg1, arg2)
  | (B754_infinity arg1, B754_infinity arg2) -> sgn_equal(arg1, arg2)
  | (B754_nan (arg1, arg2), B754_nan (arg3, arg4)) -> sgn_equal(arg1, arg3) && Pos.eqb arg2 arg4
  | (_,_) -> false

let rec tree_equal = function
  | (Coq_tm_var id1,Coq_tm_var id2) -> EqNat.beq_nat id1 id2
  | (Coq_tm_app (tm1, tm2), Coq_tm_app (tm3, tm4)) -> tree_equal(tm1,tm3) && tree_equal(tm2,tm4)
  | (Coq_tm_abs (id1,ty1,tm1), Coq_tm_abs (id2,ty2,tm2)) ->
    EqNat.beq_nat id1 id2 && ty_equal(ty1, ty2) && tree_equal(tm1,tm2)
  | (Coq_tm_pair (tm1, tm2), Coq_tm_pair (tm3, tm4)) -> tree_equal(tm1,tm3) && tree_equal(tm2,tm4)
  | (Coq_tm_fst tm1, Coq_tm_fst tm2) -> tree_equal(tm1,tm2)
  | (Coq_tm_snd tm1, Coq_tm_snd tm2) -> tree_equal(tm1,tm2)
  | (Coq_tm_inl(ty1, tm1), Coq_tm_inl(ty2, tm2)) -> ty_equal(ty1, ty2) && tree_equal(tm1,tm2)
  | (Coq_tm_inr(ty1, tm1), Coq_tm_inr(ty2, tm2)) -> ty_equal(ty1, ty2) && tree_equal(tm1,tm2)

  | (Coq_tm_case(tm1, id1, tm2, id2, tm3), Coq_tm_case(tm4, id3, tm5, id4, tm6)) ->
      EqNat.beq_nat id1 id3 && EqNat.beq_nat id2 id4 && tree_equal(tm1,tm4) && tree_equal(tm2,tm5) && tree_equal(tm3,tm6)

  | (Coq_tm_nil(ty1), Coq_tm_nil(ty2)) -> ty_equal(ty1, ty2)
  | (Coq_tm_cons (tm1, tm2), Coq_tm_cons (tm3, tm4)) -> tree_equal(tm1,tm3) && tree_equal(tm2,tm4)

  | (Coq_tm_lcase(tm1, tm2, id1, id2, tm3), Coq_tm_lcase(tm4, tm5, id3, id4, tm6)) ->
      EqNat.beq_nat id1 id3 && EqNat.beq_nat id2 id4 && tree_equal(tm1,tm4) && tree_equal(tm2,tm5) && tree_equal(tm3,tm6)

  | (Coq_tm_nat coq_Z1, Coq_tm_nat coq_Z2) -> Z.eqb coq_Z1 coq_Z2
  | (Coq_tm_flt coq_flt1, Coq_tm_flt coq_flt2) -> flt_equal(coq_flt1, coq_flt2)

  | (Coq_tm_succ tm1, Coq_tm_succ tm2) -> tree_equal(tm1,tm2)
  | (Coq_tm_pred tm1, Coq_tm_pred tm2) -> tree_equal(tm1,tm2)
  | (Coq_tm_add (tm1, tm2), Coq_tm_add (tm3, tm4)) -> tree_equal(tm1,tm3) && tree_equal(tm2,tm4)
  | (Coq_tm_sub (tm1, tm2), Coq_tm_sub (tm3, tm4)) -> tree_equal(tm1,tm3) && tree_equal(tm2,tm4)
  | (Coq_tm_mult (tm1, tm2), Coq_tm_mult (tm3, tm4)) -> tree_equal(tm1,tm3) && tree_equal(tm2,tm4)
  | (Coq_tm_div (tm1, tm2), Coq_tm_div (tm3, tm4)) -> tree_equal(tm1,tm3) && tree_equal(tm2,tm4)
  | (Coq_tm_mod (tm1, tm2), Coq_tm_mod (tm3, tm4)) -> tree_equal(tm1,tm3) && tree_equal(tm2,tm4)
  | (Coq_tm_addf (tm1, tm2), Coq_tm_addf (tm3, tm4)) -> tree_equal(tm1,tm3) && tree_equal(tm2,tm4)
  | (Coq_tm_subf (tm1, tm2), Coq_tm_subf (tm3, tm4)) -> tree_equal(tm1,tm3) && tree_equal(tm2,tm4)
  | (Coq_tm_mulf (tm1, tm2), Coq_tm_mulf (tm3, tm4)) -> tree_equal(tm1,tm3) && tree_equal(tm2,tm4)
  | (Coq_tm_divf (tm1, tm2), Coq_tm_divf (tm3, tm4)) -> tree_equal(tm1,tm3) && tree_equal(tm2,tm4)

  | (Coq_tm_if0 (tm1, tm2, tm3), Coq_tm_if0 (tm4, tm5, tm6)) -> tree_equal(tm1,tm4) && tree_equal(tm2,tm5) && tree_equal(tm3,tm6)
  | (Coq_tm_iflt (tm1, tm2, tm3), Coq_tm_iflt (tm4, tm5, tm6)) -> tree_equal(tm1,tm4) && tree_equal(tm2,tm5) && tree_equal(tm3,tm6)

  | (Coq_tm_let(id1, tm1, tm2), Coq_tm_let(id2, tm3, tm4)) -> EqNat.beq_nat id1 id2 && tree_equal(tm1,tm3) && tree_equal(tm2,tm4)
  | (Coq_tm_fix tm1, Coq_tm_fix tm2) -> tree_equal(tm1,tm2)
  | (Coq_tm_io(sel1, tm1), Coq_tm_io(sel2, tm2)) -> EqNat.beq_nat sel1 sel2 && tree_equal(tm1,tm2)
  | (_,_) -> false

let fmtbigint ch arg = output_string ch ( match arg with
    | CntInt(_,str) -> str
    | Reduced(cnt,oth) -> "Not reduced after "^string_of_int cnt^" iterations")

let rec reduce' codes cnt prev oth =
    try if not (tree_equal(prev, oth)) && (cnt < 100) then reduce' codes (cnt+1) oth (reduce oth) else oth with Failure err -> oth

let rec verbose' codes cnt prev = function
    | Coq_tm_nat n -> CntInt (cnt,fromZ n)
    | oth -> print_endline ("** Iter "^string_of_int cnt^" **");
             dumpentry (codes,oth); if not(tree_equal(prev, oth)) && (cnt < 100) then verbose' codes (cnt+1) oth (reduce oth) else Reduced (cnt,dumpentry' codes oth)

let exec_till (codes,compiled) = reduce' codes 0 (Coq_tm_nil Coq_ty_Nat) compiled
let verbose_till (codes,compiled) = verbose' codes 0 (Coq_tm_nil Coq_ty_Nat) compiled
