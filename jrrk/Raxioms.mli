open BinNums
open BinPos
open Datatypes
open Rdefinitions
open Specif

type __ = Obj.t

val total_order_T : coq_R -> coq_R -> bool sumor

val coq_INR : nat -> coq_R

val coq_IZR : coq_Z -> coq_R

val completeness : __ -> coq_R

