open Mylib

type expflt = {sgn:int; expon:int; mant:int;}
type expdbl = {sgn64:int; expon64:int; mant64:int64;}
type spec_flt =
  | Inf of int
  | Nan
  | Max_flt
  | Min_flt
  | Eps_flt
  | Reg_flt of expdbl

let bits64 () =
  let (_,sz,_) = get_config() in
  match sz with
  | 64 -> true
  | 32 -> false
  | _ ->
    print_string "word size unsupported";
    failwith "abort"

let unextract32' sgn expon mant =
  let mant' = Int32.of_int (mant land 0x7FFFFF) in
  let expon' = if expon < -128 then -128 else if expon > 127 then 127 else expon in
  let expon'' = Int32.shift_left (Int32.of_int ((sgn lsl 8) lor (expon' + 128))) 23 in
  Int32.logor expon'' mant'

let extract32' (i:int32) =
  let nonmant = Int32.to_int (Int32.shift_right_logical i 23) in
  let expon = nonmant land 0xFF in
  let sgn = (nonmant lsr 8) land 1 in
  let mant = ((Int32.to_int i) land 0x7FFFFF) in
  {sgn=sgn;expon=expon-128;mant=if expon > 0 then mant lor 0x800000 else mant lsl 1}

let unextract64' sgn expon mant =
  let mant' = Int64.logand mant 0xFFFFFFFFFFFFFL in
  let expon' = Int64.shift_left (Int64.of_int ((sgn lsl 11) lor (expon + 1024))) 52 in
  Int64.logor expon' mant'

let extract64' (i:int64) =
  match i with
  | 0x7F_F0_00_00_00_00_00_00L -> Inf 0
  | 0xFF_F0_00_00_00_00_00_00L -> Inf 1
  | 0x7F_F0_00_00_00_00_00_01L -> Nan
  | 0x7F_EF_FF_FF_FF_FF_FF_FFL -> Max_flt
  | 0x00_10_00_00_00_00_00_00L -> Min_flt
  | 0x3C_B0_00_00_00_00_00_00L -> Eps_flt
  | _ ->
    let nonmant' = Int64.shift_right_logical i 52 in
    let nonmant = Int64.to_int nonmant' in
    let expon = nonmant land 0x7FF in
    let mant = Int64.logand i 0xFFFFFFFFFFFFFL in
    let sgn = (nonmant lsr 11) land 1 in
    Reg_flt {sgn64=sgn;expon64=expon - 1024;mant64=if expon > 0 then Int64.logor mant 0x10000000000000L else Int64.shift_left mant 1}

let untrunc_float' (i:int32) =
  let {sgn=sgn;expon=expon;mant=mant} = extract32' i in
  let mant' = Int64.of_int (mant land 0x7FFFFF) in
  let rnd = Int64.shift_left mant' 29 in
  let rec norm64' sgn expon mant =
    if mant < 0x4000000000000L then norm64' sgn (expon-1) (Int64.shift_left mant 1)
    else unextract64' sgn (-1024) mant' in
  match expon with
  | -128 -> if mant' = 0L then unextract64' sgn (-1024) mant' else norm64' sgn (-128) mant'
  | 127 -> unextract64' sgn 1023 mant'
  | _ -> unextract64' sgn expon rnd

let trunc_float' (i:int64) =
  match extract64' i with
  | Inf sgn -> unextract32' sgn 127 0
  | Nan -> unextract32' 0 127 1 
  | Max_flt -> unextract32' 0 127 (-1)
  | Min_flt -> unextract32' 0 1 (0x800000)
  | Eps_flt -> unextract32' 0 100 (0x800000)
  | Reg_flt {sgn64=sgn;expon64=expon;mant64=mant} ->
    let rnd = Int64.to_int (Int64.shift_right mant 28) in
    unextract32' sgn expon ((rnd lsr 1) + (rnd land 1))

let rec format_int32_hex ostr ptr n len =
  if n > 15l && len > 0 then format_int32_hex ostr ptr (Int32.shift_right_logical n 4) (len-1);
  let mod16 = Int32.to_int (Int32.logand n 15l) in
  let ch = unsafe_char_of_int (if mod16 > 9 then mod16 - 10 + char_code 'A' else mod16 + char_code '0') in
  String.set ostr !ptr ch; incr ptr

let string_of_hexint32 n =
  let ostr = String.create 20 and ptr = ref 0 in
  format_int32_hex ostr ptr n (String.length ostr);
  String.sub ostr 0 !ptr

let unextract32 sgn expon mant =
  let unext = unextract32' sgn expon mant in
  Int32.float_of_bits unext

let extract32 (f:float) =
  let i = Int32.bits_of_float f in
  extract32' i

let unextract64 sgn expon mant =
  let unext = unextract64' sgn expon mant in
  Int64.float_of_bits unext

let extract64 (f:float) =
  let i = Int64.bits_of_float f in
  extract64' i

let trunc_float (f:float) =
  let i = Int64.bits_of_float f in
  let trnc = trunc_float' i in
  Int32.float_of_bits trnc

let rec format_int_hex ostr ptr n len =
  if n > 15 && len > 0 then format_int_hex ostr ptr (n lsr 4) (len-1);
  let mod16 = n land 15 in
  let ch = unsafe_char_of_int (if mod16 > 9 then mod16 - 10 + char_code 'A' else mod16 + char_code '0') in
  String.set ostr !ptr ch; incr ptr

let string_of_hexint n =
  let ostr = String.create 20 and ptr = ref 0 in
  format_int_hex ostr ptr n (String.length ostr);
  String.sub ostr 0 !ptr
  
let dbg sgn expon mant = print_endline(string_of_int sgn^" "^string_of_int expon^" "^string_of_hexint mant^" ("^string_of_int mant^")")

let rec norm rnd lmt (loc:int) {sgn;expon;mant} =
    assert (sgn = 0 || sgn = 1);
    print_string (string_of_int lmt^" -> "^string_of_int loc^" "^string_of_int expon^" : "^string_of_int mant);
    if lmt = 0 then
      begin
      print_string "norm";
      failwith "abort"
      end
    else if mant = 0 then
      unextract32' 0 (-128) 0
    else if mant >= 0x1000000 then
       norm (mant land 1) (lmt-1) loc {sgn;expon=expon+1;mant=mant lsr 1}
    else if mant < 0x800000 then
       norm 0 (lmt-1) loc {sgn;expon=expon-1;mant=mant lsl 1}
    else
      if expon > 126 then
	unextract32' sgn 127 0 (* infinite *)
      else if expon < -127 then
	unextract32' sgn (-128) (mant lsr (-128-expon)) (* sub-normalized *)
      else
	unextract32' sgn expon (mant+rnd)
      
let float_of_int x =
  if x = 0 then
    unextract32' 0 0 0
  else
    norm 0 25 21 {sgn=if x<0 then 1 else 0;expon=22;mant=x}

let plus' norm' {sgn=sgn1;expon=expon1;mant=mant1} {sgn=sgn2;expon=expon2;mant=mant2} =
    assert (sgn1 = 0 || sgn1 = 1);
    assert (sgn2 = 0 || sgn2 = 1);
(*
    debug_int sgn1; debug_int sgn2;
*)
    if sgn1 = sgn2 then
       begin
       if expon1 = expon2 then
       	 let sum = mant1+mant2 in
	 if sum < 0 then
	   norm' 1 {sgn=sgn2; expon=expon1; mant= -sum}
	 else
	   norm' 2 {sgn=sgn1; expon=expon1; mant=sum}
       else if expon1 < expon2 then
	 begin
	 if expon2-expon1 > 64 then
	   norm' 3 {sgn=sgn2; expon=expon2; mant=mant2}	 
	 else
           let sum = (mant1 lsr (expon2-expon1-1)) + (mant2 lsl 1) in
	   norm' 4 {sgn=sgn2; expon=expon2-1; mant=sum}
	 end
       else
	 begin
	   if expon1-expon2 > 64 then
	     norm' 5 {sgn=sgn1; expon=expon1; mant=mant1}	 
	   else
	     let sum = (mant1 lsl 1) + (mant2 lsr (expon1-expon2-1)) in
	     norm' 6 {sgn=sgn1; expon=expon1-1; mant=sum}
	 end
       end
    else if sgn1 < sgn2 then
       begin
       if expon1 = expon2 then
       	 let sum = mant1-mant2 in
	 if sum < 0 then
	   norm' 7 {sgn=sgn2; expon=expon1; mant= -sum}
	 else
	   norm' 8 {sgn=sgn1; expon=expon1; mant=sum}
       else if expon1 < expon2 then
	 begin
	 if expon2-expon1 > 64 then
	   norm' 9 {sgn=sgn2; expon=expon2; mant=mant2}	 
	 else
       	   let sum = (mant2 lsl 1) - (mant1 lsr (expon2-expon1-1)) in
	   norm' 10 {sgn=sgn2; expon=expon2-1; mant=sum}
	 end
       else
	 begin
	   if expon1-expon2 > 64 then
	     norm' 11 {sgn=sgn1; expon=expon1; mant=mant1}	 
	   else
       	     let sum = (mant1 lsl 1) - (mant2 lsr (expon1-expon2-1)) in
	     norm' 12 {sgn=sgn1; expon=expon1-1; mant=sum}
	 end
       end
    else
      begin
	if expon1 = expon2 then
       	  let sum = mant2-mant1 in
	  if sum < 0 then
	    norm' 13 {sgn=sgn2; expon=expon1; mant= -sum}
	  else
	    norm' 14 {sgn=sgn1; expon=expon1; mant=sum}
	else if expon1 < expon2 then
	  begin
	    if expon2-expon1 > 64 then
	      norm' 15 {sgn=sgn2; expon=expon2; mant=mant2}	 
	    else
       	      let sum = (mant2 lsl 1) - (mant1 lsr (expon2-expon1-1)) in
	      norm' 16 {sgn=sgn2; expon=expon2-1; mant=sum}
	  end
	else
	  begin
	    if expon1-expon2 > 64 then
	      norm' 17 {sgn=sgn1; expon=expon1; mant=mant1}	 
	    else
       	      let sum = (mant1 lsl 1) - (mant2 lsr (expon1-expon2-1)) in
	      norm' 18 {sgn=sgn1; expon=expon1-1; mant=sum}
	  end
      end

let expnan = 127
let nan = unextract32' 0 expnan 1

let minus' nan' norm' x y =
  let flt1 = extract32' x in
  let flt2 = extract32' y in
  if flt1.expon = 255 || flt2.expon = 255 then
    nan' ()
  else
    plus' norm' flt1 {flt2 with sgn = 1-flt2.sgn}

let times' nan' norm' {sgn=sgn1;expon=expon1;mant=mant1} {sgn=sgn2;expon=expon2;mant=mant2} =
  let prod = Int64.to_int (Int64.shift_right (Int64.mul (Int64.of_int mant1) (Int64.of_int mant2)) 20) in
  print_string (string_of_int mant1^" "^string_of_int mant2^" "^string_of_int prod);
  norm' 19 {sgn=sgn1 lxor sgn2; expon=expon1+expon2-2; mant=prod}

(*
let div64 x y =
(*
  print_endline (Int64.to_string x);
  print_endline (Int64.to_string y);
*)
  let z = Int64.div x y in
(*
  print_endline (Int64.to_string z);
  print_endline (string_of_int (Int64.to_int x)^"/"^string_of_int (Int64.to_int y)^"->"^string_of_int (Int64.to_int z));
*)
  z

let rem64 x y =
  let z = Int64.rem x y in
(*
  print_endline (string_of_int (Int64.to_int x)^"%"^string_of_int (Int64.to_int y)^"->"^string_of_int (Int64.to_int z));
*)
  z
*)

let divide' nan' norm' {sgn=sgn1;expon=expon1;mant=mant1} {sgn=sgn2;expon=expon2;mant=mant2} =
  let mant1' = Int64.of_int mant1 in
  let mant2' = Int64.of_int mant2 in
  let quot = Int64.div (Int64.shift_left mant1' 24) mant2' in
  norm' 20 {sgn=sgn1 lxor sgn2; expon=expon1-expon2-2; mant=Int64.to_int quot}

let plus x y =
  let flt1 = extract32' x in
  let flt2 = extract32' y in
  if flt1.expon = expnan || flt2.expon = expnan then
    nan
  else
    (plus' (norm 0 25) flt1 flt2)

let minus x y = minus' (fun _ -> nan) (norm 0 25) x y

let times x y =
  let flt1 = extract32' x in
  let flt2 = extract32' y in
  if flt1.expon = expnan || flt2.expon = expnan then
    nan
  else
    (times' nan (norm 0 25) flt1 flt2)

let divide x y =
(*
  print_endline (Int32.to_string x);
*)
  let flt1 = extract32' x in
(*
  dbg flt1.sgn flt1.expon flt1.mant;
  print_endline (Int32.to_string y);
*)
  let flt2 = extract32' y in
(*
  dbg flt2.sgn flt2.expon flt2.mant;
*)
  if flt1.expon = expnan || flt2.expon = expnan || flt2.mant = 0 then
    nan
  else
    (divide' nan (norm 0 25) flt1 flt2)
      
let negate x =
  let flt = extract32' x in
  if flt.expon = expnan then
    nan
  else
    unextract32' (1-flt.sgn) flt.expon flt.mant

let caml_add_float (arg0:float) (arg1:float) : float = Int32.float_of_bits (plus (Int32.bits_of_float arg0) (Int32.bits_of_float arg1))
let caml_sub_float (arg0:float) (arg1:float) : float = Int32.float_of_bits (minus (Int32.bits_of_float arg0) (Int32.bits_of_float arg1))
let caml_mul_float (arg0:float) (arg1:float) : float = Int32.float_of_bits (times (Int32.bits_of_float arg0) (Int32.bits_of_float arg1))
let caml_div_float (arg0:float) (arg1:float) : float = Int32.float_of_bits (divide (Int32.bits_of_float arg0) (Int32.bits_of_float arg1))
let caml_neg_float (arg0:float) : float = Int32.float_of_bits (negate (Int32.bits_of_float arg0))
let caml_float_of_int (arg0:int) : float = Int32.float_of_bits (float_of_int arg0)

let flt_cmp_eq loc {sgn;expon;mant} = mant = 0
let flt_cmp_ne loc {sgn;expon;mant} = mant <> 0
let flt_cmp_ge loc {sgn;expon;mant} = sgn = 0
let flt_cmp_gt loc {sgn;expon;mant} = sgn = 0 && mant <> 0
let flt_cmp_le loc {sgn;expon;mant} = mant = 0 || sgn = 1
let flt_cmp_lt loc {sgn;expon;mant} = sgn = 1
let flt_cmp loc {sgn;expon;mant} = if sgn = 1 then -1 else if expon > 0 then 1 else 0
let cmp_nan _ = false
let cmp_nan' _ = 0

let caml_cmp_float_eq  (arg0:float) (arg1:float) : bool = minus' cmp_nan flt_cmp_eq (Int32.bits_of_float arg0) (Int32.bits_of_float arg1)
let caml_cmp_float_ne  (arg0:float) (arg1:float) : bool = minus' cmp_nan flt_cmp_ne (Int32.bits_of_float arg0) (Int32.bits_of_float arg1)
let caml_cmp_float_ge  (arg0:float) (arg1:float) : bool = minus' cmp_nan flt_cmp_ge (Int32.bits_of_float arg0) (Int32.bits_of_float arg1)
let caml_cmp_float_gt  (arg0:float) (arg1:float) : bool = minus' cmp_nan flt_cmp_gt (Int32.bits_of_float arg0) (Int32.bits_of_float arg1)
let caml_cmp_float_le  (arg0:float) (arg1:float) : bool = minus' cmp_nan flt_cmp_le (Int32.bits_of_float arg0) (Int32.bits_of_float arg1)
let caml_cmp_float_lt  (arg0:float) (arg1:float) : bool = minus' cmp_nan flt_cmp_lt (Int32.bits_of_float arg0) (Int32.bits_of_float arg1)
let caml_float_compare (arg0:float) (arg1:float) : int = minus' cmp_nan' flt_cmp (Int32.bits_of_float arg0) (Int32.bits_of_float arg1)

let caml_float_compare'Y2FtbF9mbG9hdF9jb21wYXJlJw'' (flt1:float) (flt2:float) =
  print_string "caml_float_compare";
  caml_float_compare (flt1:float) (flt2:float)

let ( ~-. ) = caml_neg_float
let ( +. ) = caml_add_float
let ( -. ) = caml_sub_float
let ( *. ) = caml_mul_float
let ( /. ) = caml_div_float

let caml_format_float (arg0: string) (f: float): string =
  match arg0 with
  | "%.12g" ->
    let rec adjust binscale dec mant =
      if dec > 0 then adjust binscale (dec-1) (5 * mant lsr binscale) else mant in
(*
    debug_float f;
*)
    let {sgn;expon;mant} = extract32 f in
    let dec = 6 in
    let binscale = (22-dec)/dec in
    let adj' = adjust binscale dec mant in
    let shft = 22-(binscale+1)*dec-expon in
    let adj = string_of_int(if shft < 0 then adj' lsl (-shft) else adj' lsr shft) in
(*
    print_endline ("adj: "^adj);
*)
    let len' = dec - String.length adj in
    let pad = (if len' > 0 then String.make len' '0' else "")^adj in
(*
    print_endline ("pad: "^pad);
*)
    let len = String.length pad in
(*
    print_endline(string_of_int len^" - "^string_of_int dec);
*)
    let split1 = String.sub pad 0 (len - dec) in
(*
    print_endline ("split1: "^split1);
*)
    let split2 = String.sub pad (len - dec) dec in
(*
    print_endline ("split2: "^split2);
*)
    let split = split1^"."^split2 in
(*
    print_endline ("split: "^split);
*)
    if sgn > 0 then "-"^split else split
  | _ ->
    print_endline ("unknown format: "^arg0);
    arg0

let pi'  = 3.14159265358979323846264338328
let pi'2 = 1.57079632679489661923132169164
let pi'3 = 1.04719755119659774615421446109
let pi'4 = 0.78539816339744830961566084582
let pi'6 = 0.52359877559829887307710723055

let pi_test = caml_format_float "%.12g" pi'

let rec sqrtfun1 cnt tgt xn = 
  let xn' = (xn +. tgt /. xn) /. 2. in
(*
  print_endline (string_of_int cnt ^": "^string_of_float xn');
*)
  if xn' <> xn && cnt < 20 then sqrtfun1 (cnt+1) tgt xn'
  else xn'

let sqrt' n = if n <> 0.0 then sqrtfun1 0 n n else 0.0
let caml_sqrt_float (arg0: float): float = sqrt' arg0

let rec taylsin'' n x2 xn f sum =
  let n' = n+.n+.1. in
  let xn' = -. x2 *. xn in
  let f' = n' *. (n'-.1.) *. f in
(*
  print_endline (string_of_float n^": "^string_of_float f);
*)
  let sum' = sum +. xn' /. f' in
  if sum <> sum' && n < 12. then taylsin'' (n+.1.) x2 xn' f' sum'
  else (n',sum')

let taylsin' x = taylsin'' 1. (x*.x) x 1. x

let caml_sin_float (arg0: float): float = snd (taylsin' arg0)

let rec taylcos'' n x2 xn f sum =
  let n' = n+.n in
  let xn' = -. x2 *. xn in
  let f' = n' *. (n'-.1.) *. f in
  let sum' = sum +. xn' /. f' in
  if sum <> sum' && n < 12. then taylcos'' (n+.1.) x2 xn' f' sum'
  else (n',sum')

let taylcos' x = taylcos'' 1. (x*.x) 1. 1. 1.

let caml_cos_float (arg0: float): float = snd (taylcos' arg0)
let caml_tan_float (arg0: float): float = caml_sin_float arg0 /. caml_cos_float arg0

let rec taylatan'' n x2 xn sum =
  let n' = n+.n+.1. in
  let xn' = -. x2 *. xn in
  let sum' = sum +. xn' /. n' in
  if sum <> sum' && n < 12. then taylatan'' (n+.1.) x2 xn' sum'
  else (n',sum')

let taylatan' x = taylatan'' 1. (x*.x) x x
let taylatan x = snd (taylatan' x)

let bigatan' x = taylatan' ((-1. +. sqrt'(1. +. x *. x)) /. x)

let caml_atan_float (arg0: float): float = 2. *. snd (bigatan' arg0)
let caml_asin_float (x: float): float = if x = -1. || x = 1. then pi'2 *. x else caml_atan_float(x/.sqrt'(1.-.x*.x))
let caml_acos_float (x: float): float = pi'2-.caml_asin_float x

let caml_atan2_float (arg0: float) (arg1: float): float = caml_atan_float (arg0 /. arg1)

let rec taylsinh'' n x2 xn f sum =
  let n' = n+.n+.1. in
  let xn' = x2 *. xn in
  let f' = n' *. (n'-.1.) *. f in
  let sum' = sum +. xn' /. f' in
  if sum <> sum' && n < 12. then taylsinh'' (n+.1.) x2 xn' f' sum'
  else (n',sum')

let taylsinh' x = taylsinh'' 1. (x*.x) x 1. x

let caml_sinh_float (arg0: float): float = snd (taylsinh' arg0)

let rec taylcosh'' n x2 xn f sum =
  let n' = n+.n in
  let xn' = x2 *. xn in
  let f' = n' *. (n'-.1.) *. f in
  let sum' = sum +. xn' /. f' in
  if sum <> sum' && n < 12. then taylcosh'' (n+.1.) x2 xn' f' sum'
  else (n',sum')

let taylcosh' x = taylcosh'' 1. (x*.x) 1. 1. 1.

let caml_cosh_float (arg0: float): float = snd (taylcosh' arg0)
let caml_tanh_float (arg0: float): float = caml_sinh_float arg0 /. caml_cosh_float arg0
  
let rec taylexp'' n x xn f sum =
  let xn' = x *. xn in
  let f' = n *. f in
  let sum' = sum +. xn' /. f' in
  if sum <> sum' && n < 12. then taylexp'' (n+.1.) x xn' f' sum'
  else (n,sum')

let taylexp' x = taylexp'' 1. x 1. 1. 1.
let taylexpm1' x = taylexp'' 1. x 1. 1. 0.

let caml_exp_float (arg0: float): float = snd (taylexp' arg0)
let caml_expm1_float (arg0: float): float = snd (taylexpm1' arg0)

let rec tayllog'' n x2 xn sum =
  let n' = n+.n+.1. in
  let xn' = x2 *. xn in
  let sum' = sum +. xn' /. n' in
  if sum <> sum' && n < 12. then tayllog'' (n+.1.) x2 xn' sum'
  else (n',sum')

let tayllog' x = let a=(x-.1.)/.(x+.1.) in tayllog'' 1. (a*.a) a a 
let tayllog1p' x = let a=x/.(x+.2.) in tayllog'' 1. (a*.a) a a

let rec precond scale fn arg =
  if arg < 0.5 || arg >= 2. then precond (scale *. 2.) fn (caml_sqrt_float arg)
  else scale *. snd (fn arg)

let caml_log_float (arg0: float): float = precond 2. tayllog' arg0
let caml_log1p_float (arg0: float): float = precond 2. tayllog1p' arg0
let caml_log10_float (arg0: float): float = caml_log_float arg0 /. caml_log_float 10.

let caml_power_float x y = caml_exp_float(caml_log_float x *. y)
let caml_hypot_float x y = caml_sqrt_float(x*.x +. y*.y)

let caml_ldexp_float x n =
  let {sgn;expon;mant} = extract32 x in
  unextract32 sgn (expon+n) mant

let ( ** ) = caml_power_float
let exp = caml_exp_float
let expm1 = caml_expm1_float
let acos = caml_acos_float
let asin = caml_asin_float
let atan = caml_atan_float
let atan2 = caml_atan2_float
let hypot = caml_hypot_float
let cos = caml_cos_float
let cosh = caml_cosh_float
let log = caml_log_float
let log10 = caml_log10_float
let log1p = caml_log1p_float
let sin = caml_sin_float
let sinh = caml_sinh_float
let sqrt = caml_sqrt_float
let tan = caml_tan_float
let tanh = caml_tanh_float
(*
let ceil = caml_ceil_float
let floor = caml_floor_float
let abs_float = cam_abs_float
let copysign = caml_copysign_float
let mod_float = caml_fmod_float
let frexp = caml_frexp_float
let modf = caml_modf_float
let truncate = caml_intoffloat
let int_of_float = caml_intoffloat
*)
let float = caml_float_of_int
let ldexp = caml_ldexp_float
let float_of_bits = Int64.float_of_bits
let string_of_float f = caml_format_float "%.12g" f
