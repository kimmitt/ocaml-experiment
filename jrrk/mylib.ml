
type 'a ref = { mutable contents : 'a }

external raise : exn -> 'a = "%raise"
external ignore : 'a -> unit = "%ignore"
external string_length : string -> int = "%string_length"
external input_byte : Std.in_channel -> int = "caml_ml_input_char"
external output_char : Std.out_channel -> char -> unit = "caml_ml_output_char"
external flush : Std.out_channel -> unit = "caml_ml_flush"
external int_of_char : char -> int = "%identity"
external sys_exit : int -> 'a = "caml_sys_exit"
external ( := ) : 'a ref -> 'a -> unit = "%setfield0"
external ( ! ) : 'a ref -> 'a = "%field0"
external incr : int ref -> unit = "%incr"
external ( ~- ) : int -> int = "%negint"
external ( = ) : 'a -> 'a -> bool = "%equal"
external ( > ) : 'a -> 'a -> bool = "%greaterthan"
external ( >= ) : 'a -> 'a -> bool = "%greaterequal"
external ( < ) : 'a -> 'a -> bool = "%lessthan"
external ( <= ) : 'a -> 'a -> bool = "%lessequal"
external ( <> ) : 'a -> 'a -> bool = "%notequal"
external ( == ) : 'a -> 'a -> bool = "%eq"
external ( != ) : 'a -> 'a -> bool = "%noteq"
external compare : 'a -> 'a -> int = "%compare"
external ( + ) : int -> int -> int = "%addint"
external ( - ) : int -> int -> int = "%subint"
external ( *  ) : int -> int -> int = "%mulint"
external ( lsl ) : int -> int -> int = "%lslint"
external ( lsr ) : int -> int -> int = "%lsrint"
external ( asr ) : int -> int -> int = "%asrint"
external ( land ) : int -> int -> int = "%andint"
external ( lor ) : int -> int -> int = "%orint"
external ( lxor ) : int -> int -> int = "%xorint"
external ( && ) : bool -> bool -> bool = "%sequand"
external ( || ) : bool -> bool -> bool = "%sequor"
external not : bool -> bool = "%boolnot"
external ref : 'a -> 'a ref = "%makemutable"
external fst : 'a * 'b -> 'a = "%field0"
external snd : 'a * 'b -> 'b = "%field1"
external succ : int -> int = "%succint"

module Char = struct
  external unsafe_chr : int -> char = "%identity"
  external code : char -> int = "%identity"
  let of_int n = unsafe_chr (n land 255)

  let lowercase c =
    if (c >= 'A' && c <= 'Z')
    || (c >= '\192' && c <= '\214')
    || (c >= '\216' && c <= '\222')
    then unsafe_chr(code c + 32)
    else c

  let uppercase c =
    if (c >= 'a' && c <= 'z')
    || (c >= '\224' && c <= '\246')
    || (c >= '\248' && c <= '\254')
    then unsafe_chr(code c - 32)
    else c

end

module Sys = struct
  external hw_exn : unit -> bool = "caml_hw_exn"

  let get_config () = ("standalone", 32, false)
  let get_argv () = ("a.out", [|"a.out"|])

end

external ( / ) : int -> int -> int = "%divint"
external ( mod ) : int -> int -> int = "%modint"

module Array = struct
  external get: 'a array -> int -> 'a = "%array_safe_get"
  external make: int -> 'a -> 'a array = "caml_make_vect"
  external length : 'a array -> int = "%array_length"
  external get: 'a array -> int -> 'a = "%array_safe_get"
  external unsafe_get: 'a array -> int -> 'a = "%array_unsafe_get"
  external set: 'a array -> int -> 'a -> unit = "%array_safe_set"

  let fold_left f x a =
    let r = ref x in
    for i = 0 to length a - 1 do
      r := f !r (unsafe_get a i)
    done;
    !r

  let iter f a = for i = 0 to length a - 1 do f(unsafe_get a i) done

  let to_list a =
    let rec tolist i res =
      if i < 0 then res else tolist (i - 1) (unsafe_get a i :: res) in
    tolist (length a - 1) []
end

let failwith s = raise(Failure s)
let invalid_arg s = raise(Invalid_argument s)
let string_of_bool b = if b then "true" else "false"
let stdin = Std.in_
let stdout = Std.out_
let print_char c = output_char (stdout) c
let rec print_int n = if n > 9 then
  print_int (n/10); print_char (Char.unsafe_chr (n mod 10 + Char.code '0'))
let print_int n = if n < 0 then (print_char '-'; print_int (0-n)) else print_int n
let print_int_nl n = print_int n; print_char '\n'; flush (stdout)
let print_newline () = output_char (stdout) '\n'; flush (stdout)

module Int32 = struct
  type forbidden
  type t = int32
  external neg : int32 -> int32 = "%int32_neg"
  external add : int32 -> int32 -> int32 = "%int32_add"
  external sub : int32 -> int32 -> int32 = "%int32_sub"
  external mul : int32 -> int32 -> int32 = "%int32_mul"
  external div : int32 -> int32 -> int32 = "%int32_div"
  external rem : int32 -> int32 -> int32 = "%int32_mod"
  external logand : int32 -> int32 -> int32 = "%int32_and"
  external logor : int32 -> int32 -> int32 = "%int32_or"
  external logxor : int32 -> int32 -> int32 = "%int32_xor"
  external shift_left : int32 -> int -> int32 = "%int32_lsl"
  external shift_right : int32 -> int -> int32 = "%int32_asr"
  external shift_right_logical : int32 -> int -> int32 = "%int32_lsr"
  external of_int : int -> int32 = "%int32_of_int"
  external to_int : int32 -> int = "%int32_to_int"
  external compare : t -> t -> int = "%compare"
end

module Int64 = struct
  type forbidden
  type t = int64
  external neg : int64 -> int64 = "%int64_neg"
  external add : int64 -> int64 -> int64 = "%int64_add"
  external sub : int64 -> int64 -> int64 = "%int64_sub"
  external mul : int64 -> int64 -> int64 = "%int64_mul"
  external div : int64 -> int64 -> int64 = "%int64_div"
  external rem : int64 -> int64 -> int64 = "%int64_mod"
  external logand : int64 -> int64 -> int64 = "%int64_and"
  external logor : int64 -> int64 -> int64 = "%int64_or"
  external logxor : int64 -> int64 -> int64 = "%int64_xor"
  external shift_left : int64 -> int -> int64 = "%int64_lsl"
  external shift_right : int64 -> int -> int64 = "%int64_asr"
  external shift_right_logical : int64 -> int -> int64 = "%int64_lsr"
  external of_int : int -> int64 = "%int64_of_int"
  external to_int : int64 -> int = "%int64_to_int"
  external of_int32 : int32 -> int64 = "%int64_of_int32"
  external to_int32 : int64 -> int32 = "%int64_to_int32"
  external of_nativeint : nativeint -> int64 = "%int64_of_nativeint"
  external to_nativeint : int64 -> nativeint = "%int64_to_nativeint"
  external compare : t -> t -> int = "%compare"
end

module List = struct
  let rec length_aux len = function
      [] -> len
    | a::l -> length_aux (len + 1) l

  let length l = length_aux 0 l

  let hd = function
      [] -> failwith "hd"
    | a::l -> a

  let tl = function
      [] -> failwith "tl"
    | a::l -> l

  let nth l n =
    if n < 0 then invalid_arg "List.nth" else
    let rec nth_aux l n =
      match l with
      | [] -> failwith "nth"
      | a::l -> if n = 0 then a else nth_aux l (n-1)
    in nth_aux l n

  let rec rev_append l1 l2 =
    match l1 with
      [] -> l2
    | a :: l -> rev_append l (a :: l2)

  let rev l = rev_append l []

  let rec iter f = function
      [] -> ()
    | a::l -> f a; iter f l

  let rec map f = function
      [] -> []
    | a::l -> let r = f a in r :: map f l

  let rec fold_left f accu l =
    match l with
      [] -> accu
    | a::l -> fold_left f (f accu a) l

  let rec fold_right f l accu =
    match l with
      [] -> accu
    | a::l -> f a (fold_right f l accu)

  let rec mem' cmp x = function
      [] -> false
    | a::l -> cmp a x || mem' cmp x l

  let rec assoc' cmp x = function
      [] -> raise Not_found
    | (a,b)::l -> if cmp a x then b else assoc' cmp x l

  let rec mem_assoc' cmp x = function
    | [] -> false
    | (a, b) :: l -> cmp a x || mem_assoc' cmp x l

  let rec remove_assoc' cmp x = function
    | [] -> []
    | (a, b as pair) :: l ->
	if cmp a x then l else pair :: remove_assoc' cmp x l
end

module String = struct
  external length : string -> int = "%string_length"
  external create : int -> string = "caml_create_string"
  external get : string -> int -> char = "%string_safe_get"
  external set : string -> int -> char -> unit = "%string_safe_set"
  external unsafe_get : string -> int -> char = "%string_unsafe_get"
  external unsafe_set : string -> int -> char -> unit = "%string_unsafe_set"

  let blit (str1: string) (int1: int) (str2: string) (int2: int) (int3: int): unit =
   for i = 0 to int3-1 do
     set str2 (int2+i) (get str1 (int1+i))
   done

  let unsafe_blit (str1: string) (int1: int) (str2: string) (int2: int) (int3: int): unit =
   for i = 0 to int3-1 do
     unsafe_set str2 (int2+i) (unsafe_get str1 (int1+i))
   done

  let unsafe_fill (str1: string) (int1: int) (int2: int) (char1: char): unit =
    assert(int1+int2 <= length str1);
    for i = int1 to int1+int2-1 do
      unsafe_set str1 i char1
    done

  let eqb (a:string) (b:string) =
    let len1 = length a and len2 = length b in
    if len1 = len2 then
      (try
	for i = 0 to len1 - 1 do 
	  if unsafe_get a i <> unsafe_get b i then raise (Failure "eqb");
	done;
	true
      with
	Failure _ -> false)
    else 
      false

  let output oc s = for i = 0 to (string_length s) - 1 do output_char oc (unsafe_get s i) done
  let print s = output (stdout) s
  let endline s = output (stdout) s; print_newline()

  let iter f a =
    for i = 0 to length a - 1 do f(unsafe_get a i) done

  let make n c =
    let s = create n in
    unsafe_fill s 0 n c;
    s

  let sub s ofs len =
    if ofs < 0 || len < 0 || ofs > length s - len
    then invalid_arg "String.sub"
    else begin
      let r = create len in
      unsafe_blit s ofs r 0 len;
      r
    end

  let concat sep l =
    match l with
      [] -> ""
    | hd :: tl ->
	let num = ref 0 and len = ref 0 in
	List.iter (fun s -> incr num; len := !len + length s) l;
	let r = create (!len + length sep * (!num - 1)) in
	unsafe_blit hd 0 r 0 (length hd);
	let pos = ref(length hd) in
	List.iter
	  (fun s ->
	    unsafe_blit sep 0 r !pos (length sep);
	    pos := !pos + length sep;
	    unsafe_blit s 0 r !pos (length s);
	    pos := !pos + length s)
	  tl;
	r

end

let output_string = String.output
let print_string = String.print
let print_endline = String.endline

let string_of_int (arg1: int): string =
  let rec format_int_d ostr ptr n len =
    if n > 9 && len > 0 then
      format_int_d ostr ptr (n/10) (len-1);
    let ch = Char.unsafe_chr (n mod 10 + Char.code '0') in
    ostr.[!ptr] <- ch;
    incr ptr in
  let ostr = String.create 20 and ptr = ref 0 in
  if arg1 < 0 then
    (ostr.[!ptr] <- '-'; incr ptr; format_int_d ostr ptr (0-arg1) (String.length ostr))
  else
    format_int_d ostr ptr arg1 (String.length ostr);
  String.sub ostr 0 !ptr

let rec ( @ ) l1 l2 =
  match l1 with
    [] -> l2
  | hd :: tl -> hd :: (tl @ l2)

let input_char in_channel = match input_byte in_channel with
| (-1) -> raise End_of_file
| oth -> Char.of_int oth

let ( ^ ) s1 s2 =
  let l1 = String.length s1 and l2 = String.length s2 in
  if l1 < 0 || l2 < 0 then
    begin
      print_endline "string create with negative length";
      raise(Failure "concat")
    end;
  let s = String.create (l1 + l2) in
  String.blit s1 0 s 0 l1;
  String.blit s2 0 s l1 l2;
  s

let int_of_string str =
  let rslt = ref 0 in
  let sign = str.[0] = '-' in
  let len = String.length str in
  for i = if sign then 1 else 0 to len - 1 do
    match str.[i] with
    | '0'..'9' as dig -> let dig' = Char.code dig - Char.code '0' in
                         rslt := !rslt * 10 + (if sign then -dig' else dig')
    | _ -> invalid_arg "int_of_string"
  done;
  !rslt

let char_of_int = Char.of_int
