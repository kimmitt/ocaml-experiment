type token =
  | BOOL of (bool)
  | INT of (int)
  | CHAR of (char)
  | STRING of (string)
  | FLOAT of (float)
  | NOT
  | PLUS
  | MINUS
  | AST
  | SLASH
  | MODULO
  | PLUS_DOT
  | MINUS_DOT
  | AST_DOT
  | SLASH_DOT
  | EQUAL
  | LESS_GREATER
  | LESS_EQUAL
  | GREATER_EQUAL
  | LESS
  | GREATER
  | IF
  | THEN
  | ELSE
  | IDENT of (string)
  | TCONS of (string)
  | LET
  | IN
  | REC
  | COMMA
  | ARRAY_CREATE
  | DOT
  | LESS_MINUS
  | SEMICOLON
  | DBLCOLON
  | LPAREN
  | RPAREN
  | LBRACK
  | RBRACK
  | MINUS_GREATER
  | FUNCTION
  | MATCH
  | WITH
  | VBAR
  | TYPE
  | OF
  | TUNIT
  | TBOOL
  | TCHAR
  | TINT
  | TSTRING
  | TOPTION
  | SOME
  | NONE
  | DBLSEMI
  | EOF

val toplst :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> Ml_decls.top_t list
