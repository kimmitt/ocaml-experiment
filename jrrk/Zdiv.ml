open BinInt
open BinNums
open Datatypes
open ZArith_dec

(** val coq_Zdiv_eucl_exist : coq_Z -> coq_Z -> (coq_Z, coq_Z) prod **)

let coq_Zdiv_eucl_exist b a =
  Z.div_eucl a b

(** val coq_Zmod_POS : positive -> coq_Z -> coq_Z **)

let rec coq_Zmod_POS a b =
  match a with
  | Coq_xI a' ->
    let r = coq_Zmod_POS a' b in
    let r' = Z.add (Z.mul (Zpos (Coq_xO Coq_xH)) r) (Zpos Coq_xH) in
    if Z.ltb r' b then r' else Z.sub r' b
  | Coq_xO a' ->
    let r = coq_Zmod_POS a' b in
    let r' = Z.mul (Zpos (Coq_xO Coq_xH)) r in
    if Z.ltb r' b then r' else Z.sub r' b
  | Coq_xH -> if Z.leb (Zpos (Coq_xO Coq_xH)) b then Zpos Coq_xH else Z0

(** val coq_Zmod' : coq_Z -> coq_Z -> coq_Z **)

let coq_Zmod' a b =
  match a with
  | Z0 -> Z0
  | Zpos a' ->
    (match b with
     | Z0 -> Z0
     | Zpos p -> coq_Zmod_POS a' b
     | Zneg b' ->
       let r = coq_Zmod_POS a' (Zpos b') in
       (match r with
        | Z0 -> Z0
        | _ -> Z.add b r))
  | Zneg a' ->
    (match b with
     | Z0 -> Z0
     | Zpos p ->
       let r = coq_Zmod_POS a' b in
       (match r with
        | Z0 -> Z0
        | _ -> Z.sub b r)
     | Zneg b' -> Z.opp (coq_Zmod_POS a' (Zpos b')))

(** val coq_Zdiv_eucl_extended : coq_Z -> coq_Z -> (coq_Z, coq_Z) prod **)

let coq_Zdiv_eucl_extended b a =
  if coq_Z_le_gt_dec Z0 b
  then coq_Zdiv_eucl_exist b a
  else let Coq_pair (x, x0) = coq_Zdiv_eucl_exist (Z.opp b) a in
       Coq_pair ((Z.opp x), x0)

