open BinInt
open BinNums
open Datatypes

(** val coq_Z_of_nat' : nat -> coq_Z **)

let coq_Z_of_nat' =
  Z.of_nat

(** val coq_Zpos' : positive -> coq_Z **)

let coq_Zpos' x =
  Zpos x

(** val coq_Zneg' : positive -> coq_Z **)

let coq_Zneg' x =
  Zneg x

(** val coq_Z_of_N' : coq_N -> coq_Z **)

let coq_Z_of_N' =
  Z.of_N

