open Mylib
open BinNums
open BinPos
open BinNat
open BinInt
open Ml_decls
open MoreZStlc
open STLCExtended
open Examples
open Fappli_IEEE_bits
open Dump
open To_from_Z

(*
let compile' prgfil =
  let chan = open_in prgfil in
  (Ml_parse.toplst Ml_lex.token (Lexing.from_channel chan))

let compile'' str = Ml_parse.toplst Ml_lex.token (Lexing.from_string str)
*)
type fold = Ident of string | Pairing of fold * fold

let rec unpack codes depth = function
	| Ident arg' ->
	let rec unpack' = function
	    | Coq_tm_fst (Coq_tm_var Datatypes.O) :: tl -> Coq_tm_fst (unpack' tl)
	    | Coq_tm_snd (Coq_tm_var Datatypes.O) :: tl -> Coq_tm_snd (unpack' tl)
	    | Coq_tm_var arg :: [] -> Coq_tm_var arg
	    | hd :: tl -> failwith ("Unrecognised argument format: "^dumpentry' codes hd)
	    | [] -> Coq_tm_var Datatypes.O in [arg', unpack' depth]
	    | Pairing (lft, rght) -> unpack codes (Coq_tm_fst (Coq_tm_var Datatypes.O) :: depth) lft @ unpack codes (Coq_tm_snd (Coq_tm_var (Datatypes.O)) :: depth) rght

let unpack' arg = List.fold_right (fun (a,t) b -> Pairing(Ident a,b)) (List.tl arg) (Ident(fst(List.hd arg)))

let rec cnv_typ = function
  | TUnit -> Coq_ty_Nat
  | TBool -> Coq_ty_Nat
  | TChar -> Coq_ty_Nat
  | TString -> Coq_ty_Nat
  | TUser _ -> Coq_ty_Nat
  | TFloat -> Coq_ty_Flt
  | TFun (_, _) ->  failwith "Unable to convert type Fun"
  | TTuple arg -> Coq_ty_List (cnv_typ (List.hd arg))
  | TArray _ -> failwith "Unable to convert type Array"
  | TVar v -> (match !v with Some ty -> cnv_typ ty | None -> Coq_ty_Nat)
  | TInt -> Coq_ty_Nat
  | Unknown ->  failwith "Unable to convert type Unknown"

let rec composelst = function
  | Compose(pat, v) -> composelst pat @ [v]
  | oth -> [oth]

let rec rewrite (codes:codes) = function
  | Match (l,
    [List [List []; exp1];
     List [List [Var hd; Var tl]; exp2]]) ->
       Coq_tm_lcase ( Coq_tm_var (encode codes.code l), rewrite codes exp1, encode codes.code hd, encode codes.code tl, rewrite codes exp2)
  | Match (x,
     [List [ArgCons (Var l, TUser inl); exp1];
      List [ArgCons (Var r, TUser inr); exp2]]) when String.eqb inl "Inl" && String.eqb inr "Inr" -> 
       Coq_tm_case ( Coq_tm_var (encode codes.code x), encode codes.code l, rewrite codes exp1, encode codes.code r, rewrite codes exp2)
  | LetRec ({name=(nam,typ); args=arg'; body=body}, nxt) -> letrec codes nam arg' body (rewrite codes nxt)
  | List lst -> List.fold_right (fun arg nxt -> Coq_tm_cons (rewrite codes arg, nxt)) lst (Coq_tm_nil(Coq_ty_Nat))
  | Int n -> Coq_tm_nat (asZi n)
  | Char n -> Coq_tm_nat (asZi (int_of_char n))
  | String str -> Coq_tm_nat (encstr str)
  | Bool bool -> Coq_tm_nat (if bool then Zpos Coq_xH else Z0)
  | Var t -> Coq_tm_var (encode codes.code t)
  | App (Var fst, [p4exp]) when String.eqb fst "fst" -> Coq_tm_fst(rewrite codes p4exp)
  | App (Var snd, [p4exp]) when String.eqb snd "snd" -> Coq_tm_snd(rewrite codes p4exp)
  | App (Var succ, [p4exp]) when String.eqb succ "succ" -> Coq_tm_succ(rewrite codes p4exp)
  | App (Var pred, [p4exp]) when String.eqb pred "pred" -> Coq_tm_pred(rewrite codes p4exp)
  | App (Var fn, [p4exp]) when String.eqb fn "char_of_int" || String.eqb fn "print_char" || String.eqb fn "failwith" ->
      Coq_tm_io (encode codes.code fn, rewrite codes p4exp)
  | App (p4exp1, p4exp2) -> List.fold_left (fun a b -> Coq_tm_app(a,b)) (rewrite codes p4exp1) (List.map (rewrite codes) p4exp2)
  | If (Eq(p4exp1, Int 0), p4exp2, p4exp3) -> Coq_tm_if0 (rewrite codes p4exp1, rewrite codes p4exp2, rewrite codes p4exp3)
  | If (LT(p4exp0, p4exp1), p4exp2, p4exp3) -> Coq_tm_iflt (Coq_tm_sub(rewrite codes p4exp0, rewrite codes p4exp1), rewrite codes p4exp2, rewrite codes p4exp3)
  | If (GT(p4exp0, p4exp1), p4exp2, p4exp3) -> Coq_tm_iflt (Coq_tm_sub(rewrite codes p4exp1, rewrite codes p4exp0), rewrite codes p4exp2, rewrite codes p4exp3)
  | If (LE(p4exp0, Int n), p4exp2, p4exp3) -> Coq_tm_iflt (Coq_tm_sub(rewrite codes p4exp0, Coq_tm_nat(asZi (n+1))), rewrite codes p4exp2, rewrite codes p4exp3)
  | If (LE(p4exp0, p4exp1), p4exp2, p4exp3) -> Coq_tm_iflt (Coq_tm_sub(rewrite codes p4exp0, Coq_tm_succ(rewrite codes p4exp1)), rewrite codes p4exp2, rewrite codes p4exp3)
  | If (GE(p4exp0, Int n), p4exp2, p4exp3) -> Coq_tm_iflt (Coq_tm_sub(Coq_tm_succ(Coq_tm_nat(asZi (n+1))), rewrite codes p4exp0), rewrite codes p4exp2, rewrite codes p4exp3)
  | If (GE(p4exp0, p4exp1), p4exp2, p4exp3) -> Coq_tm_iflt (Coq_tm_sub(rewrite codes p4exp1, Coq_tm_pred(rewrite codes p4exp0)), rewrite codes p4exp2, rewrite codes p4exp3)
  | Tuple (p4exp :: p4explst) -> List.fold_right (fun a b -> Coq_tm_pair(a,b)) (List.map (rewrite codes) p4explst) (rewrite codes p4exp)
  | Unit -> Coq_tm_nat Z0
  | Float f -> Coq_tm_flt f
  | Add (p, Int 1) -> Coq_tm_succ(rewrite codes p)
  | Sub (p, Int 1) -> Coq_tm_pred(rewrite codes p)
  | Neg p -> Coq_tm_sub(Coq_tm_nat Z0, rewrite codes p)
  | Add (p, q) -> Coq_tm_add(rewrite codes p, rewrite codes q)
  | Sub (p, q) -> Coq_tm_sub(rewrite codes p, rewrite codes q)
  | Mul (p, q) -> Coq_tm_mult(rewrite codes p, rewrite codes q)
  | Div (p, q) -> Coq_tm_div(rewrite codes p, rewrite codes q)
  | Mod (p, q) -> Coq_tm_mod(rewrite codes p, rewrite codes q)
  | FNeg p -> Coq_tm_subf(Coq_tm_nat Z0, rewrite codes p)
  | FAdd (p, q) -> Coq_tm_addf(rewrite codes p, rewrite codes q)
  | FSub (p, q) -> Coq_tm_subf(rewrite codes p, rewrite codes q)
  | FMul (p, q) -> Coq_tm_mulf(rewrite codes p, rewrite codes q)
  | FDiv (p, q) -> Coq_tm_divf(rewrite codes p, rewrite codes q)
  | Let ((nam,typ), body, nxt) -> Coq_tm_let(encode codes.code nam, rewrite codes body, rewrite codes nxt)
  | LetTuple (tupl, Var arg, body) ->
      let lst = unpack codes [Coq_tm_var (encode codes.code arg)] (unpack' tupl) in
      List.fold_right (fun (nam,cod) nxt -> Coq_tm_let (encode codes.code nam, cod, nxt)) (List.rev lst) (rewrite codes body)
  | Abstraction (_, _) -> failwith "Abstraction (_, _)"
  | Compose (_, _) -> failwith "Compose (_, _)"
  | Declare (_, _, _, _) -> failwith "Declare (_, _, _, _)"
  | Tuple _ -> failwith "Tuple _"
  | Not _ -> failwith "Not _"
  | Eq (_, _) -> failwith "Eq _"
  | LE (_, _) -> failwith "LE _"
  | LT (_, _) -> failwith "LT _"
  | GE (_, _) -> failwith "GE _"
  | GT (_, _) -> failwith "GT _"
  | TypCons (_, _) -> failwith "TypCons _"
  | ArgCons (exp, TUser inl) when String.eqb inl "Inl" -> Coq_tm_inl(Coq_ty_Nat, rewrite codes exp)
  | ArgCons (exp, TUser inr) when String.eqb inr "Inr" -> Coq_tm_inr(Coq_ty_Nat, rewrite codes exp)
  | ArgCons (_, _) as arg -> failwith (dump4exp codes.impl arg)
  | Match (_, _) -> failwith "Match (_,_)"
  | If (_, _, _) -> failwith "If (_,_,_)"
  | LetTuple (_,_,_) -> failwith "LetTuple (_,_,_)"
  | INTEGER n -> Coq_tm_nat (asZi n)
  | STRING _ -> failwith "STRING _"
  | LISTCONS _ -> failwith "LISTCONS _"
  | TYPCONS (_, _) -> failwith "TYPCONS (_, _)"
  | TOP arg -> failwith ("TOP ("^dump4lst codes.impl arg^")")
  | STR arg -> failwith ("STR ("^dump4exp codes.impl arg^")")
  | ANY _ -> failwith "ANY _"
  | LBL _ -> failwith "LBL _"
  | TYPE _ -> failwith "TYPE _"
  | TYPDECL _ -> failwith "TYPDECL _"
  | REC _ -> failwith "REC _"
  | NONREC _ -> failwith "NONREC _"
  | LET(decl', nxt') ->
      List.fold_right (fun a nxt ->
        match a with
         | Declare(recurse,Var nam, List [], body) -> 
             Coq_tm_let(encode codes.code nam, rewrite codes body, nxt)
         | Declare(recurse,Var nam, List args, body) -> 
             let args' = List.map (function Var x -> addtyp x | oth -> failwith (codes.impl.dump oth)) args in
             letrec codes nam args' body nxt
         | oth -> failwith (codes.impl.dump oth)) decl' (rewrite codes nxt')
  | LET (dst, src) -> failwith ("LET ("^dump4lst codes.impl dst^", "^dump4exp codes.impl src^")")
  | PATTERN (fn, _) -> failwith ("PATTERN ("^fn^", _)")
  | MATCH (_, _) -> failwith "MATCH (_, _)"
  | PATEXP (_, _) -> failwith "PATEXP (_, _)"
  | FORMAL (_, _) -> failwith "FORMAL (_, _)"
  | TUPLE (tupl) -> failwith ("TUPLE ("^dump4lst codes.impl tupl^")")
  | TYPLOC (_, _) -> failwith "TYPLOC (_, _)"
  | FUNARG arg -> failwith ("FUNARG "^dump4lst codes.impl arg)
  | PATCASE (_, _, _) -> failwith "PATCASE (_, _, _)"
  | CONSTRAINED (_, _) -> failwith "CONSTRAINED (_, _)"
  | APPLY1 (fn, arg) -> rewrite codes (App(Var fn,[arg]))
  | APPLY2 (fn, arg1, arg2) -> rewrite codes (App(Var fn,[arg1;arg2]))
  | APPLYLST (p4exp1, p4exp2) -> List.fold_left (fun a b -> Coq_tm_app(a,b)) (Coq_tm_var (encode codes.code p4exp1)) (List.map (rewrite codes) p4exp2)

and letrec codes nam arg' body nxt = 
  let arg,rewritten = (match body with
      | List funcases -> let l = addtyp "_" in (arg'@[l]),rewrite codes (Match(fst l, funcases))
      | oth -> arg',rewrite codes oth) in
  let folded = List.fold_right (fun (nam,ty) nxt -> Coq_tm_abs (encode codes.code nam, cnv_typ ty, nxt)) arg rewritten in
  let fix = Coq_tm_fix(Coq_tm_abs(encode codes.code nam, Coq_ty_arrow (Coq_ty_Nat, Coq_ty_Nat), folded)) in
  Coq_tm_let (encode codes.code nam, fix, nxt)

let rw p =
  let codes = init_codes() in
  let rev = List.rev p in
  let last,rest = (match List.hd rev with
    | TopIn exp -> rewrite codes exp, List.rev (List.tl rev)
    | oth -> (Coq_tm_nil Coq_ty_Nat, p)) in
  let rewritten = List.fold_right (fun arg nxt ->
(*
    print_endline("------");
    print_endline(dump4top arg);
    print_endline("---");
    print_endline (dumpentry' codes nxt);
    print_endline("---");
*)
    let m = (match arg with
    | Type (_, _) -> nxt
    | LetTop ((nam,typ), body) -> Coq_tm_let(encode codes.code nam, rewrite codes body, nxt)
    | LetRecTop {name=(nam,typ); args=arg'; body=body} -> letrec codes nam arg' body nxt
    | TopIn exp -> failwith "Top-level in expression not last"
    | Ctype exp -> failwith "Ctype not allowed in this version of parser") in
(*
    print_endline (dumpentry' codes m);
    print_endline("------");
*)
    m) rest last in
  codes,rewritten
(*
let mycompile prog = rw (compile' prog)
let strcompile prog = rw (compile'' prog)
*)
