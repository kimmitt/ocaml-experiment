open BinInt
open BinNums
open Datatypes
open Fcalc_bracket
open Fcore_Zaux
open Fcore_digits

(** val cond_incr : bool -> coq_Z -> coq_Z **)

let cond_incr b m =
  if b then Z.add m (Zpos Coq_xH) else m

(** val round_sign_DN : bool -> location -> bool **)

let round_sign_DN s = function
| Coq_loc_Exact -> false
| Coq_loc_Inexact c -> s

(** val round_UP : location -> bool **)

let round_UP = function
| Coq_loc_Exact -> false
| Coq_loc_Inexact c -> true

(** val round_sign_UP : bool -> location -> bool **)

let round_sign_UP s = function
| Coq_loc_Exact -> false
| Coq_loc_Inexact c -> negb s

(** val round_ZR : bool -> location -> bool **)

let round_ZR s = function
| Coq_loc_Exact -> false
| Coq_loc_Inexact c -> s

(** val round_N : bool -> location -> bool **)

let round_N p = function
| Coq_loc_Exact -> false
| Coq_loc_Inexact c ->
  (match c with
   | Eq -> p
   | Lt -> false
   | Gt -> true)

(** val truncate_aux :
    radix -> ((coq_Z, coq_Z) prod, location) prod -> coq_Z -> ((coq_Z, coq_Z)
    prod, location) prod **)

let truncate_aux beta t k =
  let Coq_pair (y, l) = t in
  let Coq_pair (m, e) = y in
  let p = Z.pow (radix_val beta) k in
  Coq_pair ((Coq_pair ((Z.div m p), (Z.add e k))),
  (new_location p (Z.modulo m p) l))

(** val truncate :
    radix -> (coq_Z -> coq_Z) -> ((coq_Z, coq_Z) prod, location) prod ->
    ((coq_Z, coq_Z) prod, location) prod **)

let truncate beta fexp t = match t with
| Coq_pair (y, l) ->
  let Coq_pair (m, e) = y in
  let k = Z.sub (fexp (Z.add (coq_Zdigits beta m) e)) e in
  if Z.ltb Z0 k then truncate_aux beta t k else t

(** val truncate_FIX :
    radix -> coq_Z -> ((coq_Z, coq_Z) prod, location) prod -> ((coq_Z, coq_Z)
    prod, location) prod **)

let truncate_FIX beta emin t = match t with
| Coq_pair (y, l) ->
  let Coq_pair (m, e) = y in
  let k = Z.sub emin e in
  if Z.ltb Z0 k
  then let p = Z.pow (radix_val beta) k in
       Coq_pair ((Coq_pair ((Z.div m p), (Z.add e k))),
       (new_location p (Z.modulo m p) l))
  else t

