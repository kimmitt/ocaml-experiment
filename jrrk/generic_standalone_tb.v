`timescale 10ns/10ns

module tb;

parameter siz = 18;
wire [31:0] nxtpc;

wire execute_readstrobe;
reg [7:0] readch;
reg [31:0] stdin, stdout;
reg a23_clk, a23_rst, rst, dbg_mem;

wire [31:0] read_data;

wire [0:0] divrst;
wire [31:0] dividend;
wire [31:0] divider;
wire [31:0] a23_crnt;
wire [0:0] write_enable;
wire [3:0] byte_strobes;
wire [31:0] readwrite_addr;
wire [31:0] write_data;

reg [31:0] crnt;

wire [7:0] writech;

wire                      system_rdy = !a23_rst;
wire     [31:0]           copro_read_data = 0;

wire     [31:0]           execute_write_data;
wire     [31:0]           execute_write_data_nxt;
wire     [31:0]           execute_copro_write_data;
wire     [31:0]           execute_address;
wire                      execute_address_valid;
wire     [31:0]           execute_address_nxt;  // un-registered version of execute_address to the cache rams

wire     [3:0]            execute_byte_enable;
wire     [3:0]            execute_byte_enable_nxt;
wire                      execute_read_enable;
wire                      execute_write_enable;

wire  	 [31:0] 	  r0_out;
wire  	 [31:0] 	  r1_out;
wire  	 [31:0] 	  r2_out;
wire  	 [31:0] 	  r3_out;
wire  	 [31:0] 	  r4_out;
wire  	 [31:0] 	  r5_out;
wire  	 [31:0] 	  r6_out;
wire  	 [31:0] 	  r7_out;
wire  	 [31:0] 	  r8_out;
wire  	 [31:0] 	  r9_out;
wire  	 [31:0] 	  r10_out;
wire  	 [31:0] 	  r11_out;
wire  	 [31:0] 	  r12_out;
wire  	 [31:0] 	  r13_out;
wire  	 [31:0] 	  r14_out;
wire  	 [31:0] 	  r15_out;
wire  	 [31:0] 	  pc_nxt_out;

wire hw_exn_en;
reg hw_exn;

xmem top(
		.a23_clk(a23_clk), 
		.a23_rst(a23_rst),
		.dbg_mem(dbg_mem), 
		.finish(execute_finish),
		.readstrobe(execute_readstrobe),
		.readch(readch),
		.writech(writech),
		.r0_out(r0_out),
		.r1_out(r1_out),
		.r2_out(r2_out),
		.r3_out(r3_out),
		.r4_out(r4_out),
		.r5_out(r5_out),
		.r6_out(r6_out),
		.r7_out(r7_out),
		.r8_out(r8_out),
		.r9_out(r9_out),
		.r10_out(r10_out),
		.r11_out(r11_out),
		.r12_out(r12_out),
		.r13_out(r13_out),
		.r14_out(r14_out),
		.r15_out(r15_out),
		.pc_nxt_out(pc_nxt_out),
		.hw_exn(hw_exn),
		.hw_exn_en(hw_exn_en)
);

initial
  begin
     hw_exn = 1'b0;
`ifdef HW_EXN
     @(posedge hw_exn_en)
       #1000000 hw_exn = 1'b1;
`endif
  end

always @(negedge a23_clk)
        begin
        if (writech[7]) 
           begin
           if (writech[6:0] == 10)
                $fdisplay(stdout);
           else
                $fwrite(stdout, "%c", writech & 127);
           end
        if (execute_readstrobe)
     	   readch = $fgetc(stdin);
	if (execute_finish)
           begin
                $fflush(stdout);
                $finish(0);
           end
	end

initial
	begin
	if ($test$plusargs("DBG_DUMPVARS") != 0) $dumpvars;
	dbg_mem = $test$plusargs("DBG_MEM") != 0;
	stdin = $fopen("/dev/stdin", "r");
	stdout = $fopen("/dev/stdout", "w");
	readch = 0;
	rst = 1'b1;
	a23_rst = 1'b1;
	a23_clk = 1'b0;
	#16 a23_clk = 1'b1;
	#16 a23_clk = 1'b0;
	#16 a23_clk = 1'b1;
	#16 a23_clk = 1'b0;
	#16 a23_clk = 1'b1;
	#16 a23_clk = 1'b0;
	#16 a23_clk = 1'b1;
	#16 a23_clk = 1'b0; rst = 1'b0;
	#16 a23_clk = 1'b1;
	forever
		begin
		#16 a23_clk = 1'b0; a23_rst = 1'b0;
		#16 a23_clk = 1'b1;
		end
	end

endmodule
