open BinNums
open Fcore_Raux
open Fcore_Zaux
open Rdefinitions

type float = { coq_Fnum : coq_Z; coq_Fexp : coq_Z }

(** val float_rect : radix -> (coq_Z -> coq_Z -> 'a1) -> float -> 'a1 **)

let float_rect beta f f0 =
  let { coq_Fnum = x; coq_Fexp = x0 } = f0 in f x x0

(** val float_rec : radix -> (coq_Z -> coq_Z -> 'a1) -> float -> 'a1 **)

let float_rec beta f f0 =
  let { coq_Fnum = x; coq_Fexp = x0 } = f0 in f x x0

(** val coq_Fnum : radix -> float -> coq_Z **)

let coq_Fnum _ x = x.coq_Fnum

(** val coq_Fexp : radix -> float -> coq_Z **)

let coq_Fexp _ x = x.coq_Fexp

(** val coq_F2R : radix -> float -> coq_R **)

let coq_F2R beta f =
  coq_Rmult (coq_Z2R f.coq_Fnum) (bpow beta f.coq_Fexp)

