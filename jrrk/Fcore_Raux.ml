open BinInt
open BinNums
open Datatypes
open Fcore_Zaux
open Raxioms
open Rbasic_fun
open Rdefinitions
open Rpower
open Specif

type __ = Obj.t
let __ = let rec f _ = Obj.repr f in Obj.repr f

(** val coq_P2R : positive -> coq_R **)

let rec coq_P2R = function
| Coq_xI t ->
  (match t with
   | Coq_xH -> coq_Rplus coq_R1 (coq_Rplus coq_R1 coq_R1)
   | _ -> coq_Rplus coq_R1 (coq_Rmult (coq_Rplus coq_R1 coq_R1) (coq_P2R t)))
| Coq_xO t ->
  (match t with
   | Coq_xH -> coq_Rplus coq_R1 coq_R1
   | _ -> coq_Rmult (coq_Rplus coq_R1 coq_R1) (coq_P2R t))
| Coq_xH -> coq_R1

(** val coq_Z2R : coq_Z -> coq_R **)

let coq_Z2R = function
| Z0 -> coq_R0
| Zpos p -> coq_P2R p
| Zneg p -> coq_Ropp (coq_P2R p)

(** val coq_Rcompare : coq_R -> coq_R -> comparison **)

let coq_Rcompare x y =
  match total_order_T x y with
  | Coq_inleft s -> if s then Lt else Eq
  | Coq_inright -> Gt

(** val coq_Rle_bool : coq_R -> coq_R -> bool **)

let coq_Rle_bool x y =
  match coq_Rcompare x y with
  | Gt -> false
  | _ -> true

(** val coq_Rlt_bool : coq_R -> coq_R -> bool **)

let coq_Rlt_bool x y =
  match coq_Rcompare x y with
  | Lt -> true
  | _ -> false

(** val coq_Req_bool : coq_R -> coq_R -> bool **)

let coq_Req_bool x y =
  match coq_Rcompare x y with
  | Eq -> true
  | _ -> false

(** val coq_Zfloor : coq_R -> coq_Z **)

let coq_Zfloor x =
  Z.sub (up x) (Zpos Coq_xH)

(** val coq_Zceil : coq_R -> coq_Z **)

let coq_Zceil x =
  Z.opp (coq_Zfloor (coq_Ropp x))

(** val coq_Ztrunc : coq_R -> coq_Z **)

let coq_Ztrunc x =
  if coq_Rlt_bool x coq_R0 then coq_Zceil x else coq_Zfloor x

(** val coq_Zaway : coq_R -> coq_Z **)

let coq_Zaway x =
  if coq_Rlt_bool x coq_R0 then coq_Zfloor x else coq_Zceil x

(** val bpow : radix -> coq_Z -> coq_R **)

let bpow r = function
| Z0 -> coq_R1
| Zpos p -> coq_Z2R (Z.pow_pos (radix_val r) p)
| Zneg p -> coq_Rinv (coq_Z2R (Z.pow_pos (radix_val r) p))

type ln_beta_prop =
  coq_Z
  (* singleton inductive, whose constructor was Build_ln_beta_prop *)

(** val ln_beta_prop_rect :
    radix -> coq_R -> (coq_Z -> __ -> 'a1) -> ln_beta_prop -> 'a1 **)

let ln_beta_prop_rect r x f l =
  f l __

(** val ln_beta_prop_rec :
    radix -> coq_R -> (coq_Z -> __ -> 'a1) -> ln_beta_prop -> 'a1 **)

let ln_beta_prop_rec r x f l =
  f l __

(** val ln_beta_val : radix -> coq_R -> ln_beta_prop -> coq_Z **)

let ln_beta_val r x l =
  l

(** val ln_beta : radix -> coq_R -> ln_beta_prop **)

let ln_beta r x =
  let fact = ln (coq_Z2R (radix_val r)) in
  Z.add (coq_Zfloor (coq_Rdiv (ln (coq_Rabs x)) fact)) (Zpos Coq_xH)

(** val cond_Ropp : bool -> coq_R -> coq_R **)

let cond_Ropp b m =
  if b then coq_Ropp m else m

