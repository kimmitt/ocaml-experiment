open RIneq
open Rdefinitions
open Rsqrt_def
open Rtrigo_def

(** val ln_exists1 : coq_R -> coq_R **)

let ln_exists1 y =
  let f = fun x -> coq_Rminus (exp x) y in coq_IVT_cor f coq_R0 y

(** val ln_exists : coq_R -> coq_R **)

let ln_exists y =
  if coq_Rle_dec coq_R1 y
  then ln_exists1 y
  else let s = ln_exists1 (coq_Rinv y) in coq_Ropp s

(** val coq_Rln : posreal -> coq_R **)

let coq_Rln y =
  ln_exists (pos y)

(** val ln : coq_R -> coq_R **)

let ln x =
  if coq_Rlt_dec coq_R0 x then coq_Rln x else coq_R0

(** val coq_Rpower : coq_R -> coq_R -> coq_R **)

let coq_Rpower x y =
  exp (coq_Rmult y (ln x))

