type out_channel
exception Dummy of string
external sys_exit : int -> 'a = "caml_sys_exit"
external raise : exn -> 'a = "%raise"
external flush : out_channel -> unit = "caml_ml_flush"
external string_length : string -> int = "%string_length"
external open_descriptor_out : int -> out_channel = "caml_ml_open_descriptor_out"
external out_channels_list : unit -> out_channel list = "caml_ml_out_channels_list"
external unsafe_output : out_channel -> string -> int -> int -> unit = "caml_ml_output"

let output_string oc s = unsafe_output oc s 0 (string_length s)
let stdout = open_descriptor_out 1
let print_string s = output_string stdout s; flush stdout

(*
let out_channels_list () : out_channel list = []
*)

let try_with () =
  (try
     print_string (Sys.getenv "Term")
   with _ ->
     print_string "\nenvironment not found\n")

let begin_end () = print_string "begin\n"; try_with(); print_string "end\n"

let _ = 
  print_string "start\n";
  begin_end();
(*
  let rec iter = function
      [] -> ()
    | a :: l -> (try flush a with _ -> ()); iter l
  in iter (out_channels_list ());
*)
  print_string "finish\n";
  if false then sys_exit 0
