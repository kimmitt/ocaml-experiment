open Mylib
open Dump
open Ml_decls
open To_from_Z

let args str =
  let enc = encstr str in
  print_endline (fromZ enc);
  let dec = decstr enc in
  print_endline dec

let fact = LET ([Declare (Var "f", Var "x",
    If (Eq (Var "x", Int 0), Int 1,
     Mul (Var "x", App (Var "f", [Sub (Var "x", Int 1)]))))],
 App (Var "f", [Int 6]))

let _ = List.iter args (List.tl (Array.to_list Sys.argv));
  print_endline "p4exp";
  try
  let codes = init_codes() in
  print_endline (Dump.dump4exp codes.impl fact);
  let rw = Ml_compiling.rewrite codes fact in
  print_endline "entry";
  print_endline (dumpentry' codes rw)
  with _ -> print_endline "raised exception"
