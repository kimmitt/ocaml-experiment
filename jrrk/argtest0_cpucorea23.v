   
module tb;

wire [0:0] finish;
wire [0:0] flush;
wire [0:0] readstrobe;
wire [7:0] writech;

wire     [31:0]           execute_address_nxt;  // un-registered version of execute_address to the ram
wire     [3:0]            execute_byte_enable_nxt;
wire                      execute_write_enable;
wire                      execute_write_enable_nxt;
wire     [31:0]           execute_write_data;
wire     [31:0]           execute_write_data_nxt;
wire     [31:0]           read_data;
wire     [0:0] 		  read_enable;
   
reg [7:0] readch;
reg [31:0] stdin, stdout;
reg a23_clk, a23_rst, dbg_mem;

mem_behav a23_mem(.clk(a23_clk),
	.write_addr(execute_address_nxt[23:0]),
	.read_addr(execute_address_nxt[23:0]),
	.byte_strobes(execute_byte_enable_nxt),
	.write_enable(execute_write_enable_nxt),
	.read_enable(read_enable),
	.write_data(execute_write_data_nxt),
	.read_data(read_data),
	.dbg_mem(dbg_mem));

   wire [7:0]  dinb;
   wire [10:0] addrb;
   wire [0:0]  web;
   wire [0:0]  enb;
   wire [7:0]  doutb;
   
CORE0_TOP dut(
              .clk(a23_clk),
              .reset(!CPU_RESET),
              .failed(failed),
              .leds8(),
              .dil8_sw_data(8'b0),
              .rx_serial_uart(FPGA_SERIAL1_RX),
              .tx_serial_uart(FPGA_SERIAL1_TX),
              .dinb(dinb), .addrb(addrb), .web(web), .doutb(doutb), .enb(enb),
              .logging(0),
              .PS2_K_CLK_IO(PS2_K_CLK_IO),
              .PS2_K_DATA_IO(PS2_K_DATA_IO),
              .PS2_M_CLK_IO(PS2_M_CLK_IO),
              .PS2_M_DATA_IO(PS2_M_DATA_IO),
	      .read_data(read_data),
	      .execute_write_data(execute_write_data),
	      .execute_write_data_nxt(execute_write_data_nxt),
	      .execute_address_nxt(execute_address_nxt),
	      .execute_byte_enable_nxt(execute_byte_enable_nxt),
	      .execute_write_enable(execute_write_enable),
	      .execute_write_enable_nxt(execute_write_enable_nxt),
	      .read_enable(read_enable),
	      .finish(finish),
	      .readstrobe(readstrobe),
	      .flush(flush),
	      .writech(writech));
   
always @(negedge a23_clk)
	begin
	if (finish)
	  begin
	     $fflush(stdout);
	     $finish;
	  end
	if (flush) $fflush(stdout);
	if (writech[7]) $fwrite(stdout, "%c", writech[6:0]);
	if (readstrobe) readch = $fgetc(stdin);
	end

initial
	begin
	if ($test$plusargs("DBG_DUMPVARS") != 0) $dumpvars;
	dbg_mem = $test$plusargs("DBG_MEM") != 0;
	stdin = $fopen("/dev/stdin", "r");
	stdout = $fopen("/dev/stdout", "w");
	a23_rst = 1'b1;
	a23_clk = 1'b0;
	#1000 a23_clk = 1'b1;
	#1000 a23_clk = 1'b0;
	#1000 a23_clk = 1'b1;
	#1000 a23_clk = 1'b0;
	#1000 a23_clk = 1'b1;
	#1000 a23_clk = 1'b0;
	#1000 a23_clk = 1'b1;
	#1000 a23_clk = 1'b0;
	#1000 a23_clk = 1'b1;
	forever
		begin
		#1000 a23_clk = 1'b0; a23_rst = 1'b0;
		#1000 a23_clk = 1'b1;
		end
	end

endmodule