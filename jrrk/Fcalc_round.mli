open BinInt
open BinNums
open Datatypes
open Fcalc_bracket
open Fcore_Zaux
open Fcore_digits

val cond_incr : bool -> coq_Z -> coq_Z

val round_sign_DN : bool -> location -> bool

val round_UP : location -> bool

val round_sign_UP : bool -> location -> bool

val round_ZR : bool -> location -> bool

val round_N : bool -> location -> bool

val truncate_aux :
  radix -> ((coq_Z, coq_Z) prod, location) prod -> coq_Z -> ((coq_Z, coq_Z)
  prod, location) prod

val truncate :
  radix -> (coq_Z -> coq_Z) -> ((coq_Z, coq_Z) prod, location) prod ->
  ((coq_Z, coq_Z) prod, location) prod

val truncate_FIX :
  radix -> coq_Z -> ((coq_Z, coq_Z) prod, location) prod -> ((coq_Z, coq_Z)
  prod, location) prod

