open BinNums
open Fcore_Raux
open Fcore_Zaux
open Rdefinitions

type float = { coq_Fnum : coq_Z; coq_Fexp : coq_Z }

val float_rect : radix -> (coq_Z -> coq_Z -> 'a1) -> float -> 'a1

val float_rec : radix -> (coq_Z -> coq_Z -> 'a1) -> float -> 'a1

val coq_Fnum : radix -> float -> coq_Z

val coq_Fexp : radix -> float -> coq_Z

val coq_F2R : radix -> float -> coq_R

