open BinInt
open BinNums
open Datatypes

val coq_Z_of_nat' : nat -> coq_Z

val coq_Zpos' : positive -> coq_Z

val coq_Zneg' : positive -> coq_Z

val coq_Z_of_N' : coq_N -> coq_Z

