open BinNums
open Datatypes
open Fappli_IEEE
open Fappli_IEEE_bits
open SfLib
open Zops

module STLCExtended = 
 struct 
  type ty =
  | Coq_ty_arrow of ty * ty
  | Coq_ty_prod of ty * ty
  | Coq_ty_sum of ty * ty
  | Coq_ty_List of ty
  | Coq_ty_Nat
  | Coq_ty_Flt
  
  (** val ty_rect :
      (ty -> 'a1 -> ty -> 'a1 -> 'a1) -> (ty -> 'a1 -> ty -> 'a1 -> 'a1) ->
      (ty -> 'a1 -> ty -> 'a1 -> 'a1) -> (ty -> 'a1 -> 'a1) -> 'a1 -> 'a1 ->
      ty -> 'a1 **)
  
  let rec ty_rect f f0 f1 f2 f3 f4 = function
  | Coq_ty_arrow (t0, t1) ->
    f t0 (ty_rect f f0 f1 f2 f3 f4 t0) t1 (ty_rect f f0 f1 f2 f3 f4 t1)
  | Coq_ty_prod (t0, t1) ->
    f0 t0 (ty_rect f f0 f1 f2 f3 f4 t0) t1 (ty_rect f f0 f1 f2 f3 f4 t1)
  | Coq_ty_sum (t0, t1) ->
    f1 t0 (ty_rect f f0 f1 f2 f3 f4 t0) t1 (ty_rect f f0 f1 f2 f3 f4 t1)
  | Coq_ty_List t0 -> f2 t0 (ty_rect f f0 f1 f2 f3 f4 t0)
  | Coq_ty_Nat -> f3
  | Coq_ty_Flt -> f4
  
  (** val ty_rec :
      (ty -> 'a1 -> ty -> 'a1 -> 'a1) -> (ty -> 'a1 -> ty -> 'a1 -> 'a1) ->
      (ty -> 'a1 -> ty -> 'a1 -> 'a1) -> (ty -> 'a1 -> 'a1) -> 'a1 -> 'a1 ->
      ty -> 'a1 **)
  
  let rec ty_rec f f0 f1 f2 f3 f4 = function
  | Coq_ty_arrow (t0, t1) ->
    f t0 (ty_rec f f0 f1 f2 f3 f4 t0) t1 (ty_rec f f0 f1 f2 f3 f4 t1)
  | Coq_ty_prod (t0, t1) ->
    f0 t0 (ty_rec f f0 f1 f2 f3 f4 t0) t1 (ty_rec f f0 f1 f2 f3 f4 t1)
  | Coq_ty_sum (t0, t1) ->
    f1 t0 (ty_rec f f0 f1 f2 f3 f4 t0) t1 (ty_rec f f0 f1 f2 f3 f4 t1)
  | Coq_ty_List t0 -> f2 t0 (ty_rec f f0 f1 f2 f3 f4 t0)
  | Coq_ty_Nat -> f3
  | Coq_ty_Flt -> f4
  
  type tm =
  | Coq_tm_var of id
  | Coq_tm_app of tm * tm
  | Coq_tm_abs of id * ty * tm
  | Coq_tm_pair of tm * tm
  | Coq_tm_fst of tm
  | Coq_tm_snd of tm
  | Coq_tm_inl of ty * tm
  | Coq_tm_inr of ty * tm
  | Coq_tm_case of tm * id * tm * id * tm
  | Coq_tm_nil of ty
  | Coq_tm_cons of tm * tm
  | Coq_tm_lcase of tm * tm * id * id * tm
  | Coq_tm_nat of coq_Z
  | Coq_tm_flt of binary64
  | Coq_tm_succ of tm
  | Coq_tm_pred of tm
  | Coq_tm_add of tm * tm
  | Coq_tm_sub of tm * tm
  | Coq_tm_mult of tm * tm
  | Coq_tm_div of tm * tm
  | Coq_tm_mod of tm * tm
  | Coq_tm_addf of tm * tm
  | Coq_tm_subf of tm * tm
  | Coq_tm_mulf of tm * tm
  | Coq_tm_divf of tm * tm
  | Coq_tm_if0 of tm * tm * tm
  | Coq_tm_iflt of tm * tm * tm
  | Coq_tm_let of id * tm * tm
  | Coq_tm_fix of tm
  | Coq_tm_io of nat * tm
  
  (** val tm_rect :
      (id -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (id -> ty -> tm ->
      'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> 'a1) ->
      (tm -> 'a1 -> 'a1) -> (ty -> tm -> 'a1 -> 'a1) -> (ty -> tm -> 'a1 ->
      'a1) -> (tm -> 'a1 -> id -> tm -> 'a1 -> id -> tm -> 'a1 -> 'a1) -> (ty
      -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1
      -> id -> id -> tm -> 'a1 -> 'a1) -> (coq_Z -> 'a1) -> (binary64 -> 'a1)
      -> (tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1
      -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1
      -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1
      -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1
      -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1
      -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1
      -> tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (id -> tm -> 'a1 -> tm -> 'a1 ->
      'a1) -> (tm -> 'a1 -> 'a1) -> (nat -> tm -> 'a1 -> 'a1) -> tm -> 'a1 **)
  
  let rec tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 = function
  | Coq_tm_var i -> f i
  | Coq_tm_app (t0, t1) ->
    f0 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_abs (i, t0, t1) ->
    f1 i t0 t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_pair (t0, t1) ->
    f2 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_fst t0 ->
    f3 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0)
  | Coq_tm_snd t0 ->
    f4 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0)
  | Coq_tm_inl (t0, t1) ->
    f5 t0 t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_inr (t0, t1) ->
    f6 t0 t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_case (t0, i, t1, i0, t2) ->
    f7 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) i t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1) i0 t2
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t2)
  | Coq_tm_nil t0 -> f8 t0
  | Coq_tm_cons (t0, t1) ->
    f9 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_lcase (t0, t1, i, i0, t2) ->
    f10 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1) i i0 t2
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t2)
  | Coq_tm_nat z -> f11 z
  | Coq_tm_flt b -> f12 b
  | Coq_tm_succ t0 ->
    f13 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0)
  | Coq_tm_pred t0 ->
    f14 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0)
  | Coq_tm_add (t0, t1) ->
    f15 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_sub (t0, t1) ->
    f16 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_mult (t0, t1) ->
    f17 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_div (t0, t1) ->
    f18 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_mod (t0, t1) ->
    f19 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_addf (t0, t1) ->
    f20 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_subf (t0, t1) ->
    f21 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_mulf (t0, t1) ->
    f22 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_divf (t0, t1) ->
    f23 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_if0 (t0, t1, t2) ->
    f24 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1) t2
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t2)
  | Coq_tm_iflt (t0, t1, t2) ->
    f25 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1) t2
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t2)
  | Coq_tm_let (i, t0, t1) ->
    f26 i t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_fix t0 ->
    f27 t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0)
  | Coq_tm_io (n, t0) ->
    f28 n t0
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
        f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0)
  
  (** val tm_rec :
      (id -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (id -> ty -> tm ->
      'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> 'a1) ->
      (tm -> 'a1 -> 'a1) -> (ty -> tm -> 'a1 -> 'a1) -> (ty -> tm -> 'a1 ->
      'a1) -> (tm -> 'a1 -> id -> tm -> 'a1 -> id -> tm -> 'a1 -> 'a1) -> (ty
      -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1
      -> id -> id -> tm -> 'a1 -> 'a1) -> (coq_Z -> 'a1) -> (binary64 -> 'a1)
      -> (tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1
      -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1
      -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1
      -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1
      -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1 -> tm -> 'a1
      -> 'a1) -> (tm -> 'a1 -> tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (tm -> 'a1
      -> tm -> 'a1 -> tm -> 'a1 -> 'a1) -> (id -> tm -> 'a1 -> tm -> 'a1 ->
      'a1) -> (tm -> 'a1 -> 'a1) -> (nat -> tm -> 'a1 -> 'a1) -> tm -> 'a1 **)
  
  let rec tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 = function
  | Coq_tm_var i -> f i
  | Coq_tm_app (t0, t1) ->
    f0 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_abs (i, t0, t1) ->
    f1 i t0 t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_pair (t0, t1) ->
    f2 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_fst t0 ->
    f3 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0)
  | Coq_tm_snd t0 ->
    f4 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0)
  | Coq_tm_inl (t0, t1) ->
    f5 t0 t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_inr (t0, t1) ->
    f6 t0 t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_case (t0, i, t1, i0, t2) ->
    f7 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) i t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1) i0 t2
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t2)
  | Coq_tm_nil t0 -> f8 t0
  | Coq_tm_cons (t0, t1) ->
    f9 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_lcase (t0, t1, i, i0, t2) ->
    f10 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1) i i0 t2
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t2)
  | Coq_tm_nat z -> f11 z
  | Coq_tm_flt b -> f12 b
  | Coq_tm_succ t0 ->
    f13 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0)
  | Coq_tm_pred t0 ->
    f14 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0)
  | Coq_tm_add (t0, t1) ->
    f15 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_sub (t0, t1) ->
    f16 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_mult (t0, t1) ->
    f17 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_div (t0, t1) ->
    f18 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_mod (t0, t1) ->
    f19 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_addf (t0, t1) ->
    f20 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_subf (t0, t1) ->
    f21 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_mulf (t0, t1) ->
    f22 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_divf (t0, t1) ->
    f23 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_if0 (t0, t1, t2) ->
    f24 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1) t2
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t2)
  | Coq_tm_iflt (t0, t1, t2) ->
    f25 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1) t2
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t2)
  | Coq_tm_let (i, t0, t1) ->
    f26 i t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0) t1
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t1)
  | Coq_tm_fix t0 ->
    f27 t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0)
  | Coq_tm_io (n, t0) ->
    f28 n t0
      (tm_rec f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17
        f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 t0)
  
  (** val subst : id -> tm -> tm -> tm **)
  
  let rec subst x s t = match t with
  | Coq_tm_var y -> if beq_id x y then s else t
  | Coq_tm_app (t1, t2) -> Coq_tm_app ((subst x s t1), (subst x s t2))
  | Coq_tm_abs (y, t0, t1) ->
    Coq_tm_abs (y, t0, (if beq_id x y then t1 else subst x s t1))
  | Coq_tm_pair (t1, t2) -> Coq_tm_pair ((subst x s t1), (subst x s t2))
  | Coq_tm_fst t0 -> Coq_tm_fst (subst x s t0)
  | Coq_tm_snd t0 -> Coq_tm_snd (subst x s t0)
  | Coq_tm_inl (t0, t1) -> Coq_tm_inl (t0, (subst x s t1))
  | Coq_tm_inr (t0, t1) -> Coq_tm_inr (t0, (subst x s t1))
  | Coq_tm_case (t0, y1, t1, y2, t2) ->
    Coq_tm_case ((subst x s t0), y1,
      (if beq_id x y1 then t1 else subst x s t1), y2,
      (if beq_id x y2 then t2 else subst x s t2))
  | Coq_tm_cons (t1, t2) -> Coq_tm_cons ((subst x s t1), (subst x s t2))
  | Coq_tm_lcase (t1, t2, y, z, t3) ->
    Coq_tm_lcase ((subst x s t1), (subst x s t2), y, z,
      (if if beq_id x y then true else beq_id x z then t3 else subst x s t3))
  | Coq_tm_nat n -> Coq_tm_nat n
  | Coq_tm_succ t0 -> Coq_tm_succ (subst x s t0)
  | Coq_tm_pred t0 -> Coq_tm_pred (subst x s t0)
  | Coq_tm_add (t1, t2) -> Coq_tm_add ((subst x s t1), (subst x s t2))
  | Coq_tm_sub (t1, t2) -> Coq_tm_sub ((subst x s t1), (subst x s t2))
  | Coq_tm_mult (t1, t2) -> Coq_tm_mult ((subst x s t1), (subst x s t2))
  | Coq_tm_div (t1, t2) -> Coq_tm_div ((subst x s t1), (subst x s t2))
  | Coq_tm_mod (t1, t2) -> Coq_tm_mod ((subst x s t1), (subst x s t2))
  | Coq_tm_addf (t1, t2) -> Coq_tm_addf ((subst x s t1), (subst x s t2))
  | Coq_tm_subf (t1, t2) -> Coq_tm_subf ((subst x s t1), (subst x s t2))
  | Coq_tm_mulf (t1, t2) -> Coq_tm_mulf ((subst x s t1), (subst x s t2))
  | Coq_tm_divf (t1, t2) -> Coq_tm_divf ((subst x s t1), (subst x s t2))
  | Coq_tm_if0 (t1, t2, t3) ->
    Coq_tm_if0 ((subst x s t1), (subst x s t2), (subst x s t3))
  | Coq_tm_iflt (t1, t2, t3) ->
    Coq_tm_iflt ((subst x s t1), (subst x s t2), (subst x s t3))
  | Coq_tm_let (y, t1, t2) ->
    Coq_tm_let (y, (subst x s t1), (if beq_id x y then t2 else subst x s t2))
  | Coq_tm_fix t0 -> Coq_tm_fix (subst x s t0)
  | Coq_tm_io (y, t0) -> Coq_tm_io (y, (subst x s t0))
  | _ -> t
  
  type context = ty partial_map
  
  (** val redvalue : tm -> bool **)
  
  let rec redvalue = function
  | Coq_tm_abs (x, t11, t12) -> true
  | Coq_tm_pair (v1, v2) -> if redvalue v1 then redvalue v2 else false
  | Coq_tm_inl (t0, v1) -> redvalue v1
  | Coq_tm_inr (t0, v2) -> redvalue v2
  | Coq_tm_nil t0 -> true
  | Coq_tm_cons (v1, v2) -> if redvalue v1 then redvalue v2 else false
  | Coq_tm_nat n -> true
  | Coq_tm_flt n -> true
  | _ -> false
  
  (** val axiom_io : nat -> coq_Z -> coq_Z **)
  
  let axiom_io = Axiom_io.axiom_io
  
  (** val coq_Fplus : binary_float -> binary_float -> binary_float **)
  
  let coq_Fplus x y =
    b64_plus Coq_mode_NE x y
  
  (** val coq_Fminus : binary_float -> binary_float -> binary_float **)
  
  let coq_Fminus x y =
    b64_minus Coq_mode_NE x y
  
  (** val coq_Fmult : binary_float -> binary_float -> binary_float **)
  
  let coq_Fmult x y =
    b64_mult Coq_mode_NE x y
  
  (** val coq_Fdiv : binary_float -> binary_float -> binary_float **)
  
  let coq_Fdiv x y =
    b64_div Coq_mode_NE x y
  
  (** val coq_FplusZ : coq_Z -> coq_Z -> coq_Z **)
  
  let coq_FplusZ x y =
    bits_of_b64 (b64_plus Coq_mode_NE (b64_of_bits x) (b64_of_bits y))
  
  (** val coq_FminusZ : coq_Z -> coq_Z -> coq_Z **)
  
  let coq_FminusZ x y =
    bits_of_b64 (b64_minus Coq_mode_NE (b64_of_bits x) (b64_of_bits y))
  
  (** val coq_FmultZ : coq_Z -> coq_Z -> coq_Z **)
  
  let coq_FmultZ x y =
    bits_of_b64 (b64_mult Coq_mode_NE (b64_of_bits x) (b64_of_bits y))
  
  (** val coq_FdivZ : coq_Z -> coq_Z -> coq_Z **)
  
  let coq_FdivZ x y =
    bits_of_b64 (b64_div Coq_mode_NE (b64_of_bits x) (b64_of_bits y))
  
  (** val reduce : tm -> tm **)
  
  let rec reduce = function
  | Coq_tm_app (a, b) ->
    (match a with
     | Coq_tm_abs (x, ty0, t12) ->
       if redvalue b
       then subst x b t12
       else Coq_tm_app ((reduce a), (reduce b))
     | _ -> Coq_tm_app ((reduce a), (reduce b)))
  | Coq_tm_abs (a, b, c) -> Coq_tm_abs (a, b, (reduce c))
  | Coq_tm_pair (v1, t2) ->
    if redvalue v1
    then Coq_tm_pair (v1, (reduce t2))
    else Coq_tm_pair ((reduce v1), t2)
  | Coq_tm_fst v ->
    (match v with
     | Coq_tm_pair (v1, v2) ->
       if if redvalue v1 then redvalue v2 else false
       then v1
       else Coq_tm_fst (Coq_tm_pair (v1, v2))
     | _ -> Coq_tm_fst (reduce v))
  | Coq_tm_snd v ->
    (match v with
     | Coq_tm_pair (v1, v2) ->
       if if redvalue v1 then redvalue v2 else false
       then v2
       else Coq_tm_snd (Coq_tm_pair (v1, v2))
     | _ -> Coq_tm_snd (reduce v))
  | Coq_tm_inl (a, b) -> Coq_tm_inl (a, (reduce b))
  | Coq_tm_inr (a, b) -> Coq_tm_inr (a, (reduce b))
  | Coq_tm_case (a, b, c, d, e) ->
    (match a with
     | Coq_tm_inl (t2, v0) ->
       if redvalue v0
       then subst b v0 c
       else Coq_tm_case ((reduce a), b, c, d, e)
     | Coq_tm_inr (t1, v0) ->
       if redvalue v0
       then subst d v0 e
       else Coq_tm_case ((reduce a), b, c, d, e)
     | _ -> Coq_tm_case ((reduce a), b, c, d, e))
  | Coq_tm_cons (v1, t2) ->
    if redvalue v1
    then Coq_tm_cons (v1, (reduce t2))
    else Coq_tm_cons ((reduce v1), t2)
  | Coq_tm_lcase (a, b, c, d, e) ->
    (match a with
     | Coq_tm_nil t0 -> b
     | Coq_tm_cons (vh, vt) ->
       if if redvalue vh then redvalue vt else false
       then subst c vh (subst d vt e)
       else Coq_tm_lcase ((reduce a), b, c, d, e)
     | _ -> Coq_tm_lcase ((reduce a), b, c, d, e))
  | Coq_tm_succ v ->
    (match v with
     | Coq_tm_nat n -> Coq_tm_nat (coq_Zsucc n)
     | _ -> Coq_tm_succ (reduce v))
  | Coq_tm_pred v ->
    (match v with
     | Coq_tm_nat n -> Coq_tm_nat (coq_Zpred n)
     | _ -> Coq_tm_pred (reduce v))
  | Coq_tm_add (a, b) ->
    (match a with
     | Coq_tm_nat n1 ->
       (match b with
        | Coq_tm_nat n2 -> Coq_tm_nat (coq_Zplus n1 n2)
        | _ -> Coq_tm_add ((reduce a), (reduce b)))
     | _ -> Coq_tm_add ((reduce a), (reduce b)))
  | Coq_tm_sub (a, b) ->
    (match a with
     | Coq_tm_nat n1 ->
       (match b with
        | Coq_tm_nat n2 -> Coq_tm_nat (coq_Zminus n1 n2)
        | _ -> Coq_tm_sub ((reduce a), (reduce b)))
     | _ -> Coq_tm_sub ((reduce a), (reduce b)))
  | Coq_tm_mult (a, b) ->
    (match a with
     | Coq_tm_nat n1 ->
       (match b with
        | Coq_tm_nat n2 -> Coq_tm_nat (coq_Zmult n1 n2)
        | _ -> Coq_tm_mult ((reduce a), (reduce b)))
     | _ -> Coq_tm_mult ((reduce a), (reduce b)))
  | Coq_tm_div (a, b) ->
    (match a with
     | Coq_tm_nat n1 ->
       (match b with
        | Coq_tm_nat n2 -> Coq_tm_nat (coq_Zdiv n1 n2)
        | _ -> Coq_tm_div ((reduce a), (reduce b)))
     | _ -> Coq_tm_div ((reduce a), (reduce b)))
  | Coq_tm_mod (a, b) ->
    (match a with
     | Coq_tm_nat n1 ->
       (match b with
        | Coq_tm_nat n2 -> Coq_tm_nat (coq_Zmod n1 n2)
        | _ -> Coq_tm_mod ((reduce a), (reduce b)))
     | _ -> Coq_tm_mod ((reduce a), (reduce b)))
  | Coq_tm_addf (a, b) ->
    (match a with
     | Coq_tm_flt n1 ->
       (match b with
        | Coq_tm_flt n2 -> Coq_tm_flt (coq_Fplus n1 n2)
        | _ -> Coq_tm_addf ((reduce a), (reduce b)))
     | _ -> Coq_tm_addf ((reduce a), (reduce b)))
  | Coq_tm_subf (a, b) ->
    (match a with
     | Coq_tm_flt n1 ->
       (match b with
        | Coq_tm_flt n2 -> Coq_tm_flt (coq_Fminus n1 n2)
        | _ -> Coq_tm_subf ((reduce a), (reduce b)))
     | _ -> Coq_tm_subf ((reduce a), (reduce b)))
  | Coq_tm_mulf (a, b) ->
    (match a with
     | Coq_tm_flt n1 ->
       (match b with
        | Coq_tm_flt n2 -> Coq_tm_flt (coq_Fmult n1 n2)
        | _ -> Coq_tm_mulf ((reduce a), (reduce b)))
     | _ -> Coq_tm_mulf ((reduce a), (reduce b)))
  | Coq_tm_divf (a, b) ->
    (match a with
     | Coq_tm_flt n1 ->
       (match b with
        | Coq_tm_flt n2 -> Coq_tm_flt (coq_Fdiv n1 n2)
        | _ -> Coq_tm_divf ((reduce a), (reduce b)))
     | _ -> Coq_tm_divf ((reduce a), (reduce b)))
  | Coq_tm_if0 (a, b, c) ->
    (match a with
     | Coq_tm_nat z ->
       (match z with
        | Z0 -> b
        | _ -> c)
     | _ -> Coq_tm_if0 ((reduce a), b, c))
  | Coq_tm_iflt (a, b, c) ->
    (match a with
     | Coq_tm_nat z ->
       (match z with
        | Zneg p -> b
        | _ -> c)
     | _ -> Coq_tm_iflt ((reduce a), b, c))
  | Coq_tm_let (x, v1, t2) ->
    if redvalue v1 then subst x v1 t2 else Coq_tm_let (x, (reduce v1), t2)
  | Coq_tm_fix v ->
    (match v with
     | Coq_tm_abs (x, t1, t2) ->
       subst x (Coq_tm_fix (Coq_tm_abs (x, t1, t2))) t2
     | _ -> Coq_tm_fix (reduce v))
  | Coq_tm_io (id0, v) ->
    (match v with
     | Coq_tm_nat n -> Coq_tm_nat (axiom_io id0 n)
     | _ -> Coq_tm_io (id0, (reduce v)))
  | x -> x
  
  module Examples = 
   struct 
    module NumTest = 
     struct 
      (** val test : tm **)
      
      let test =
        Coq_tm_if0 ((Coq_tm_pred (Coq_tm_succ (Coq_tm_pred (Coq_tm_mult
          ((Coq_tm_nat (Zpos (Coq_xO Coq_xH))), (Coq_tm_nat Z0)))))),
          (Coq_tm_nat (Zpos (Coq_xI (Coq_xO Coq_xH)))), (Coq_tm_nat (Zpos
          (Coq_xO (Coq_xI Coq_xH)))))
     end
    
    module ProdTest = 
     struct 
      (** val test : tm **)
      
      let test =
        Coq_tm_snd (Coq_tm_fst (Coq_tm_pair ((Coq_tm_pair ((Coq_tm_nat (Zpos
          (Coq_xI (Coq_xO Coq_xH)))), (Coq_tm_nat (Zpos (Coq_xO (Coq_xI
          Coq_xH)))))), (Coq_tm_nat (Zpos (Coq_xI (Coq_xI Coq_xH)))))))
     end
    
    module LetTest = 
     struct 
      (** val test : tm **)
      
      let test =
        Coq_tm_let ((S (S (S (S (S (S (S (S (S O))))))))), (Coq_tm_pred
          (Coq_tm_nat (Zpos (Coq_xO (Coq_xI Coq_xH))))), (Coq_tm_succ
          (Coq_tm_var (S (S (S (S (S (S (S (S (S O))))))))))))
     end
    
    module SumTest1 = 
     struct 
      (** val test : tm **)
      
      let test =
        Coq_tm_case ((Coq_tm_inl (Coq_ty_Nat, (Coq_tm_nat (Zpos (Coq_xI
          (Coq_xO Coq_xH)))))), (S (S (S (S (S (S (S (S (S O))))))))),
          (Coq_tm_var (S (S (S (S (S (S (S (S (S O)))))))))), (S (S (S (S (S
          (S (S (S (S (S O)))))))))), (Coq_tm_var (S (S (S (S (S (S (S (S (S
          (S O))))))))))))
     end
    
    module SumTest2 = 
     struct 
      (** val test : tm **)
      
      let test =
        Coq_tm_let ((S (S (S (S (S (S (S (S (S (S (S O))))))))))),
          (Coq_tm_abs ((S (S (S (S (S (S (S (S (S O))))))))), (Coq_ty_sum
          (Coq_ty_Nat, Coq_ty_Nat)), (Coq_tm_case ((Coq_tm_var (S (S (S (S (S
          (S (S (S (S O)))))))))), (S (S (S (S (S (S (S (S (S (S (S (S
          O)))))))))))), (Coq_tm_var (S (S (S (S (S (S (S (S (S (S (S (S
          O))))))))))))), (S (S (S (S (S (S (S (S (S (S (S (S O)))))))))))),
          (Coq_tm_if0 ((Coq_tm_var (S (S (S (S (S (S (S (S (S (S (S (S
          O))))))))))))), (Coq_tm_nat (Zpos Coq_xH)), (Coq_tm_nat Z0))))))),
          (Coq_tm_pair ((Coq_tm_app ((Coq_tm_var (S (S (S (S (S (S (S (S (S
          (S (S O)))))))))))), (Coq_tm_inl (Coq_ty_Nat, (Coq_tm_nat (Zpos
          (Coq_xI (Coq_xO Coq_xH)))))))), (Coq_tm_app ((Coq_tm_var (S (S (S
          (S (S (S (S (S (S (S (S O)))))))))))), (Coq_tm_inr (Coq_ty_Nat,
          (Coq_tm_nat (Zpos (Coq_xI (Coq_xO Coq_xH)))))))))))
     end
    
    module ListTest = 
     struct 
      (** val test : tm **)
      
      let test =
        Coq_tm_let ((S (S (S O))), (Coq_tm_cons ((Coq_tm_nat (Zpos (Coq_xI
          (Coq_xO Coq_xH)))), (Coq_tm_cons ((Coq_tm_nat (Zpos (Coq_xO (Coq_xI
          Coq_xH)))), (Coq_tm_nil Coq_ty_Nat))))), (Coq_tm_lcase ((Coq_tm_var
          (S (S (S O)))), (Coq_tm_nat Z0), (S (S (S (S (S (S (S (S (S
          O))))))))), (S (S (S (S (S (S (S (S (S (S O)))))))))), (Coq_tm_mult
          ((Coq_tm_var (S (S (S (S (S (S (S (S (S O)))))))))), (Coq_tm_var (S
          (S (S (S (S (S (S (S (S O)))))))))))))))
     end
    
    module FixTest1 = 
     struct 
      (** val fact : tm **)
      
      let fact =
        Coq_tm_fix (Coq_tm_abs ((S O), (Coq_ty_arrow (Coq_ty_Nat,
          Coq_ty_Nat)), (Coq_tm_abs (O, Coq_ty_Nat, (Coq_tm_if0 ((Coq_tm_var
          O), (Coq_tm_nat (Zpos Coq_xH)), (Coq_tm_mult ((Coq_tm_var O),
          (Coq_tm_app ((Coq_tm_var (S O)), (Coq_tm_pred (Coq_tm_var
          O))))))))))))
      
      (** val fact_calc : coq_Z -> tm **)
      
      let fact_calc n =
        Coq_tm_app (fact, (Coq_tm_nat n))
     end
    
    module FixTest2 = 
     struct 
      (** val map : tm **)
      
      let map =
        Coq_tm_abs ((S (S O)), (Coq_ty_arrow (Coq_ty_Nat, Coq_ty_Nat)),
          (Coq_tm_fix (Coq_tm_abs ((S O), (Coq_ty_arrow ((Coq_ty_List
          Coq_ty_Nat), (Coq_ty_List Coq_ty_Nat))), (Coq_tm_abs ((S (S (S
          O))), (Coq_ty_List Coq_ty_Nat), (Coq_tm_lcase ((Coq_tm_var (S (S (S
          O)))), (Coq_tm_nil Coq_ty_Nat), O, (S (S (S O))), (Coq_tm_cons
          ((Coq_tm_app ((Coq_tm_var (S (S O))), (Coq_tm_var O))), (Coq_tm_app
          ((Coq_tm_var (S O)), (Coq_tm_var (S (S (S O))))))))))))))))
      
      (** val map_func : tm **)
      
      let map_func =
        Coq_tm_app (map, (Coq_tm_abs (O, Coq_ty_Nat, (Coq_tm_succ (Coq_tm_var
          O)))))
      
      (** val map_arg : tm **)
      
      let map_arg =
        Coq_tm_cons ((Coq_tm_nat (Zpos Coq_xH)), (Coq_tm_cons ((Coq_tm_nat
          (Zpos (Coq_xO Coq_xH))), (Coq_tm_nil Coq_ty_Nat))))
      
      (** val map_rslt : tm **)
      
      let map_rslt =
        Coq_tm_cons ((Coq_tm_nat (Zpos (Coq_xO Coq_xH))), (Coq_tm_cons
          ((Coq_tm_nat (Zpos (Coq_xI Coq_xH))), (Coq_tm_nil Coq_ty_Nat))))
     end
    
    module FixTest3 = 
     struct 
      (** val equal : tm **)
      
      let equal =
        Coq_tm_fix (Coq_tm_abs ((S (S (S (S (S (S (S (S (S (S (S (S (S
          O))))))))))))), (Coq_ty_arrow (Coq_ty_Nat, (Coq_ty_arrow
          (Coq_ty_Nat, Coq_ty_Nat)))), (Coq_tm_abs ((S (S (S (S (S (S (S (S
          (S (S (S (S (S (S O)))))))))))))), Coq_ty_Nat, (Coq_tm_abs ((S (S
          (S (S (S (S (S (S (S (S (S (S O)))))))))))), Coq_ty_Nat,
          (Coq_tm_if0 ((Coq_tm_var (S (S (S (S (S (S (S (S (S (S (S (S (S (S
          O))))))))))))))), (Coq_tm_if0 ((Coq_tm_var (S (S (S (S (S (S (S (S
          (S (S (S (S O))))))))))))), (Coq_tm_nat (Zpos Coq_xH)), (Coq_tm_nat
          Z0))), (Coq_tm_if0 ((Coq_tm_var (S (S (S (S (S (S (S (S (S (S (S (S
          O))))))))))))), (Coq_tm_nat Z0), (Coq_tm_app ((Coq_tm_app
          ((Coq_tm_var (S (S (S (S (S (S (S (S (S (S (S (S (S
          O)))))))))))))), (Coq_tm_pred (Coq_tm_var (S (S (S (S (S (S (S (S
          (S (S (S (S (S (S O)))))))))))))))))), (Coq_tm_pred (Coq_tm_var (S
          (S (S (S (S (S (S (S (S (S (S (S O))))))))))))))))))))))))))
      
      (** val equal1 : tm **)
      
      let equal1 =
        Coq_tm_app ((Coq_tm_app (equal, (Coq_tm_nat (Zpos (Coq_xO (Coq_xO
          Coq_xH)))))), (Coq_tm_nat (Zpos (Coq_xO (Coq_xO Coq_xH)))))
      
      (** val equal2 : tm **)
      
      let equal2 =
        Coq_tm_app ((Coq_tm_app (equal, (Coq_tm_nat (Zpos (Coq_xO (Coq_xO
          Coq_xH)))))), (Coq_tm_nat (Zpos (Coq_xI (Coq_xO Coq_xH)))))
     end
    
    module FixTest4 = 
     struct 
      (** val eotest : tm **)
      
      let eotest =
        Coq_tm_let ((S (S (S (S (S (S (S (S (S (S (S (S (S (S (S
          O))))))))))))))), (Coq_tm_fix (Coq_tm_abs ((S (S (S (S (S (S (S (S
          (S (S (S (S (S (S (S (S (S (S O)))))))))))))))))), (Coq_ty_prod
          ((Coq_ty_arrow (Coq_ty_Nat, Coq_ty_Nat)), (Coq_ty_arrow
          (Coq_ty_Nat, Coq_ty_Nat)))), (Coq_tm_pair ((Coq_tm_abs ((S (S (S (S
          (S (S (S (S (S (S (S (S O)))))))))))), Coq_ty_Nat, (Coq_tm_if0
          ((Coq_tm_var (S (S (S (S (S (S (S (S (S (S (S (S O))))))))))))),
          (Coq_tm_nat (Zpos Coq_xH)), (Coq_tm_app ((Coq_tm_snd (Coq_tm_var (S
          (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S
          O)))))))))))))))))))), (Coq_tm_pred (Coq_tm_var (S (S (S (S (S (S
          (S (S (S (S (S (S O)))))))))))))))))))), (Coq_tm_abs ((S (S (S (S
          (S (S (S (S (S (S (S (S O)))))))))))), Coq_ty_Nat, (Coq_tm_if0
          ((Coq_tm_var (S (S (S (S (S (S (S (S (S (S (S (S O))))))))))))),
          (Coq_tm_nat Z0), (Coq_tm_app ((Coq_tm_fst (Coq_tm_var (S (S (S (S
          (S (S (S (S (S (S (S (S (S (S (S (S (S (S O)))))))))))))))))))),
          (Coq_tm_pred (Coq_tm_var (S (S (S (S (S (S (S (S (S (S (S (S
          O))))))))))))))))))))))))), (Coq_tm_let ((S (S (S (S (S (S (S (S (S
          (S (S (S (S (S (S (S O)))))))))))))))), (Coq_tm_fst (Coq_tm_var (S
          (S (S (S (S (S (S (S (S (S (S (S (S (S (S O))))))))))))))))),
          (Coq_tm_let ((S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S
          O))))))))))))))))), (Coq_tm_snd (Coq_tm_var (S (S (S (S (S (S (S (S
          (S (S (S (S (S (S (S O))))))))))))))))), (Coq_tm_pair ((Coq_tm_app
          ((Coq_tm_var (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S
          O))))))))))))))))), (Coq_tm_nat (Zpos (Coq_xI Coq_xH))))),
          (Coq_tm_app ((Coq_tm_var (S (S (S (S (S (S (S (S (S (S (S (S (S (S
          (S (S O))))))))))))))))), (Coq_tm_nat (Zpos (Coq_xO (Coq_xO
          Coq_xH)))))))))))))
      
      (** val eotest_rslt : tm **)
      
      let eotest_rslt =
        Coq_tm_pair ((Coq_tm_nat Z0), (Coq_tm_nat (Zpos Coq_xH)))
     end
   end
 end

