open Mylib


let print_char c = output_char stdout c
let rec print_int n = if n > 9 then print_int (n/10); print_char (Char.unsafe_chr (n mod 10 + Char.code '0'))
let print_int n = if n < 0 then (print_char '-'; print_int (0-n)) else print_int n
let print_int_nl n = print_int n; print_char '\n'; flush stdout

let str = "Hello, World"
let str' = String.create 12
let mystrlen str = String.length str
let myget str ix = Char.code (String.unsafe_get str ix)

let _ = print_char 'J'; print_char 'K';
  print_int_nl (2 * Obj.magic str);
  print_int_nl (mystrlen str);
  print_int_nl (mystrlen str'); 
  for i = -4 to 16 do
    print_int i; print_char ' ';
    print_int (myget str i); print_char ' ';
    print_int_nl (myget str' i);
  done
