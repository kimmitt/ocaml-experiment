open Datatypes
open Rcomplete
open Rdefinitions
open Rfunctions

(** val cv_cauchy_2 : (nat -> coq_R) -> coq_R **)

let cv_cauchy_2 an =
  coq_R_complete (fun n -> sum_f_R0 an n)

(** val coq_SP : (nat -> coq_R -> coq_R) -> nat -> coq_R -> coq_R **)

let coq_SP fn n x =
  sum_f_R0 (fun k -> fn k x) n

