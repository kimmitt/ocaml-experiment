type in_channel
type out_channel

external open_descriptor_out : int -> out_channel = "caml_ml_open_descriptor_out"
external open_descriptor_in : int -> in_channel = "caml_ml_open_descriptor_in"
external output_char : out_channel -> char -> unit = "caml_ml_output_char"
external flush : out_channel -> unit = "caml_ml_flush"
external out_channels_list : unit -> out_channel list = "caml_ml_out_channels_list"
external format_int : string -> int -> string = "caml_format_int"
external unsafe_output : out_channel -> string -> int -> int -> unit = "caml_ml_output"
external string_length : string -> int = "%string_length"
external ( > ) : 'a -> 'a -> bool = "%greaterthan"
external ( < ) : 'a -> 'a -> bool = "%lessthan"
external ( + ) : int -> int -> int = "%addint"
external ( - ) : int -> int -> int = "%subint"
external ( *  ) : int -> int -> int = "%mulint"
external ( / ) : int -> int -> int = "%divint"
external ( mod ) : int -> int -> int = "%modint"

let stdin = open_descriptor_in 0
let stdout = open_descriptor_out 1
let stderr = open_descriptor_out 2

let base = 0 + 2 * Obj.magic (Obj.field (Obj.repr stdin) 1) 

let string_of_int n = format_int "%d" n
let output_string oc s = unsafe_output oc s 0 (string_length s)
let print_string s = output_string stdout s

let print_char c = output_char stdout c
let print_char c = output_char stdout c
let rec print_int n = if n > 9 then print_int (n/10); print_char (Char.unsafe_chr (n mod 10 + Char.code '0'))
let print_int n = if n < 0 then (print_char '-'; print_int (0-n)) else print_int n
let print_int_nl n = print_int n; print_char '\n'; flush stdout

let str = "Hello, World"
let mystrlen str = string_length str

let print_obj ch obj = print_char ch; print_char ' '; print_char 'O'; print_char 'b'; print_char 'j';
  let r = Obj.repr obj in
  let tag = Obj.tag r in
  print_int tag; print_char ' ';
  (match tag with
  | 255 -> print_string "custom, len "; let siz = Obj.size r in
    print_int siz; print_char ' '; print_int_nl (2 * Obj.magic (Obj.field r (siz-1)) - base)
  | 252 -> print_string "string "; print_string (Obj.obj r); print_char '\n'
  | _ -> print_string "default ";   print_int_nl (2 * Obj.magic (Obj.field r 0)))

let _ = print_char 'J'; print_char 'K'; print_int 12345; print_char '\n'

let myflush a = 
  print_obj 'f' a

let flush_all () =
  let rec iter = function
      [] -> ()
    | a :: l -> (try myflush a with _ -> ()); iter l
  in iter (out_channels_list ())

let _ = print_obj 's' str; print_obj 'i' stdin; print_obj 'o' stdout; print_obj 'e' stderr; flush_all()
