type 'a ref = { mutable contents : 'a; }
external raise : exn -> 'a = "%raise"
external ignore : 'a -> unit = "%ignore"
external string_length : string -> int = "%string_length"
external input_byte : Std.in_channel -> int = "caml_ml_input_char"
external output_char : Std.out_channel -> char -> unit
  = "caml_ml_output_char"
external flush : Std.out_channel -> unit = "caml_ml_flush"
external int_of_char : char -> int = "%identity"
external sys_exit : int -> 'a = "caml_sys_exit"
external ( := ) : 'a ref -> 'a -> unit = "%setfield0"
external ( ! ) : 'a ref -> 'a = "%field0"
external incr : int ref -> unit = "%incr"
external ( ~- ) : int -> int = "%negint"
external ( = ) : 'a -> 'a -> bool = "%equal"
external ( > ) : 'a -> 'a -> bool = "%greaterthan"
external ( >= ) : 'a -> 'a -> bool = "%greaterequal"
external ( < ) : 'a -> 'a -> bool = "%lessthan"
external ( <= ) : 'a -> 'a -> bool = "%lessequal"
external ( <> ) : 'a -> 'a -> bool = "%notequal"
external ( == ) : 'a -> 'a -> bool = "%eq"
external ( != ) : 'a -> 'a -> bool = "%noteq"
external compare : 'a -> 'a -> int = "%compare"
external ( + ) : int -> int -> int = "%addint"
external ( - ) : int -> int -> int = "%subint"
external ( * ) : int -> int -> int = "%mulint"
external ( lsl ) : int -> int -> int = "%lslint"
external ( lsr ) : int -> int -> int = "%lsrint"
external ( asr ) : int -> int -> int = "%asrint"
external ( land ) : int -> int -> int = "%andint"
external ( lor ) : int -> int -> int = "%orint"
external ( lxor ) : int -> int -> int = "%xorint"
external ( && ) : bool -> bool -> bool = "%sequand"
external ( || ) : bool -> bool -> bool = "%sequor"
external not : bool -> bool = "%boolnot"
external ref : 'a -> 'a ref = "%makemutable"
external fst : 'a * 'b -> 'a = "%field0"
external snd : 'a * 'b -> 'b = "%field1"
external succ : int -> int = "%succint"

module Char :
  sig
    external unsafe_chr : int -> char = "%identity"
    external code : char -> int = "%identity"
    val lowercase : char -> char
    val uppercase : char -> char
  end
module Sys :
  sig
    external hw_exn : unit -> bool = "caml_hw_exn"
    val get_config : unit -> string * int * bool
    val get_argv : unit -> string * string array
  end

external ( / ) : int -> int -> int = "%divint"
external ( mod ) : int -> int -> int = "%modint"

module Array :
  sig
    external make : int -> 'a -> 'a array = "caml_make_vect"
    external length : 'a array -> int = "%array_length"
    external get : 'a array -> int -> 'a = "%array_safe_get"
    external unsafe_get : 'a array -> int -> 'a = "%array_unsafe_get"
    external set : 'a array -> int -> 'a -> unit = "%array_safe_set"
    val fold_left : ('a -> 'b -> 'a) -> 'a -> 'b array -> 'a
    val iter : ('a -> 'b) -> 'a array -> unit
    val to_list : 'a array -> 'a list
  end
val failwith : string -> 'a
val invalid_arg : string -> 'a
val string_of_bool : bool -> string
val stdin : Std.in_channel
val stdout : Std.out_channel
val print_char : char -> unit
val print_int : int -> unit
val print_int_nl : int -> unit
val print_newline : unit -> unit
module Int32 :
  sig
    type forbidden
    type t = int32
    external neg : int32 -> int32 = "%int32_neg"
    external add : int32 -> int32 -> int32 = "%int32_add"
    external sub : int32 -> int32 -> int32 = "%int32_sub"
    external mul : int32 -> int32 -> int32 = "%int32_mul"
    external div : int32 -> int32 -> int32 = "%int32_div"
    external rem : int32 -> int32 -> int32 = "%int32_mod"
    external logand : int32 -> int32 -> int32 = "%int32_and"
    external logor : int32 -> int32 -> int32 = "%int32_or"
    external logxor : int32 -> int32 -> int32 = "%int32_xor"
    external shift_left : int32 -> int -> int32 = "%int32_lsl"
    external shift_right : int32 -> int -> int32 = "%int32_asr"
    external shift_right_logical : int32 -> int -> int32 = "%int32_lsr"
    external of_int : int -> int32 = "%int32_of_int"
    external to_int : int32 -> int = "%int32_to_int"
    external compare : t -> t -> int = "%compare"
  end
module Int64 :
  sig
    type forbidden
    type t = int64
    external neg : int64 -> int64 = "%int64_neg"
    external add : int64 -> int64 -> int64 = "%int64_add"
    external sub : int64 -> int64 -> int64 = "%int64_sub"
    external mul : int64 -> int64 -> int64 = "%int64_mul"
    external div : int64 -> int64 -> int64 = "%int64_div"
    external rem : int64 -> int64 -> int64 = "%int64_mod"
    external logand : int64 -> int64 -> int64 = "%int64_and"
    external logor : int64 -> int64 -> int64 = "%int64_or"
    external logxor : int64 -> int64 -> int64 = "%int64_xor"
    external shift_left : int64 -> int -> int64 = "%int64_lsl"
    external shift_right : int64 -> int -> int64 = "%int64_asr"
    external shift_right_logical : int64 -> int -> int64 = "%int64_lsr"
    external of_int : int -> int64 = "%int64_of_int"
    external to_int : int64 -> int = "%int64_to_int"
    external of_int32 : int32 -> int64 = "%int64_of_int32"
    external to_int32 : int64 -> int32 = "%int64_to_int32"
    external of_nativeint : nativeint -> int64 = "%int64_of_nativeint"
    external to_nativeint : int64 -> nativeint = "%int64_to_nativeint"
    external compare : t -> t -> int = "%compare"
  end
module List :
  sig
    val length_aux : int -> 'a list -> int
    val length : 'a list -> int
    val hd : 'a list -> 'a
    val tl : 'a list -> 'a list
    val nth : 'a list -> int -> 'a
    val rev_append : 'a list -> 'a list -> 'a list
    val rev : 'a list -> 'a list
    val iter : ('a -> 'b) -> 'a list -> unit
    val map : ('a -> 'b) -> 'a list -> 'b list
    val fold_left : ('a -> 'b -> 'a) -> 'a -> 'b list -> 'a
    val fold_right : ('a -> 'b -> 'b) -> 'a list -> 'b -> 'b
    val mem' : ('a -> 'b -> bool) -> 'b -> 'a list -> bool
    val assoc' : ('a -> 'b -> bool) -> 'b -> ('a * 'c) list -> 'c
    val mem_assoc' : ('a -> 'b -> bool) -> 'b -> ('a * 'c) list -> bool
    val remove_assoc' :
      ('a -> 'b -> bool) -> 'b -> ('a * 'c) list -> ('a * 'c) list
  end
module String :
  sig
    external length : string -> int = "%string_length"
    external create : int -> string = "caml_create_string"
    external get : string -> int -> char = "%string_safe_get"
    external set : string -> int -> char -> unit = "%string_safe_set"
    external unsafe_get : string -> int -> char = "%string_unsafe_get"
    external unsafe_set : string -> int -> char -> unit
      = "%string_unsafe_set"
    val blit : string -> int -> string -> int -> int -> unit
    val unsafe_blit : string -> int -> string -> int -> int -> unit
    val unsafe_fill : string -> int -> int -> char -> unit
    val eqb : string -> string -> bool
    val output : Std.out_channel -> string -> unit
    val print : string -> unit
    val endline : string -> unit
    val iter : (char -> 'a) -> string -> unit
    val make : int -> char -> string
    val sub : string -> int -> int -> string
    val concat : string -> string list -> string
  end
val output_string : Std.out_channel -> string -> unit
val print_string : string -> unit
val print_endline : string -> unit
val string_of_int : int -> string
val ( @ ) : 'a list -> 'a list -> 'a list
val char_of_int : int -> char
val input_char : Std.in_channel -> char
val ( ^ ) : string -> string -> string
val int_of_string : string -> int
