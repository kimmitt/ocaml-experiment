open Datatypes
open Peano
open Raxioms
open Rdefinitions
open Rpow_def

(** val plus_fct : (coq_R -> coq_R) -> (coq_R -> coq_R) -> coq_R -> coq_R **)

let plus_fct f1 f2 x =
  coq_Rplus (f1 x) (f2 x)

(** val opp_fct : (coq_R -> coq_R) -> coq_R -> coq_R **)

let opp_fct f x =
  coq_Ropp (f x)

(** val mult_fct : (coq_R -> coq_R) -> (coq_R -> coq_R) -> coq_R -> coq_R **)

let mult_fct f1 f2 x =
  coq_Rmult (f1 x) (f2 x)

(** val mult_real_fct : coq_R -> (coq_R -> coq_R) -> coq_R -> coq_R **)

let mult_real_fct a f x =
  coq_Rmult a (f x)

(** val minus_fct :
    (coq_R -> coq_R) -> (coq_R -> coq_R) -> coq_R -> coq_R **)

let minus_fct f1 f2 x =
  coq_Rminus (f1 x) (f2 x)

(** val div_fct : (coq_R -> coq_R) -> (coq_R -> coq_R) -> coq_R -> coq_R **)

let div_fct f1 f2 x =
  coq_Rdiv (f1 x) (f2 x)

(** val div_real_fct : coq_R -> (coq_R -> coq_R) -> coq_R -> coq_R **)

let div_real_fct a f x =
  coq_Rdiv a (f x)

(** val comp : (coq_R -> coq_R) -> (coq_R -> coq_R) -> coq_R -> coq_R **)

let comp f1 f2 x =
  f1 (f2 x)

(** val inv_fct : (coq_R -> coq_R) -> coq_R -> coq_R **)

let inv_fct f x =
  coq_Rinv (f x)

(** val fct_cte : coq_R -> coq_R -> coq_R **)

let fct_cte a x =
  a

(** val id : coq_R -> coq_R **)

let id x =
  x

type derivable_pt = coq_R

type derivable = coq_R -> derivable_pt

(** val derive_pt : (coq_R -> coq_R) -> coq_R -> derivable_pt -> coq_R **)

let derive_pt f x pr =
  pr

(** val derive : (coq_R -> coq_R) -> derivable -> coq_R -> coq_R **)

let derive f pr x =
  derive_pt f x (pr x)

type coq_Differential = { d1 : (coq_R -> coq_R); cond_diff : derivable }

(** val coq_Differential_rect :
    ((coq_R -> coq_R) -> derivable -> 'a1) -> coq_Differential -> 'a1 **)

let coq_Differential_rect f d =
  let { d1 = x; cond_diff = x0 } = d in f x x0

(** val coq_Differential_rec :
    ((coq_R -> coq_R) -> derivable -> 'a1) -> coq_Differential -> 'a1 **)

let coq_Differential_rec f d =
  let { d1 = x; cond_diff = x0 } = d in f x x0

(** val d1 : coq_Differential -> coq_R -> coq_R **)

let d1 x = x.d1

(** val cond_diff : coq_Differential -> derivable **)

let cond_diff x = x.cond_diff

type coq_Differential_D2 = { d2 : (coq_R -> coq_R); cond_D1 : derivable;
                             cond_D2 : derivable }

(** val coq_Differential_D2_rect :
    ((coq_R -> coq_R) -> derivable -> derivable -> 'a1) ->
    coq_Differential_D2 -> 'a1 **)

let coq_Differential_D2_rect f d =
  let { d2 = x; cond_D1 = x0; cond_D2 = x1 } = d in f x x0 x1

(** val coq_Differential_D2_rec :
    ((coq_R -> coq_R) -> derivable -> derivable -> 'a1) ->
    coq_Differential_D2 -> 'a1 **)

let coq_Differential_D2_rec f d =
  let { d2 = x; cond_D1 = x0; cond_D2 = x1 } = d in f x x0 x1

(** val d2 : coq_Differential_D2 -> coq_R -> coq_R **)

let d2 x = x.d2

(** val cond_D1 : coq_Differential_D2 -> derivable **)

let cond_D1 x = x.cond_D1

(** val cond_D2 : coq_Differential_D2 -> derivable **)

let cond_D2 x = x.cond_D2

(** val derivable_pt_plus :
    (coq_R -> coq_R) -> (coq_R -> coq_R) -> coq_R -> derivable_pt ->
    derivable_pt -> derivable_pt **)

let derivable_pt_plus f1 f2 x x0 x1 =
  coq_Rplus x0 x1

(** val derivable_pt_opp :
    (coq_R -> coq_R) -> coq_R -> derivable_pt -> derivable_pt **)

let derivable_pt_opp f x x0 =
  coq_Ropp x0

(** val derivable_pt_minus :
    (coq_R -> coq_R) -> (coq_R -> coq_R) -> coq_R -> derivable_pt ->
    derivable_pt -> derivable_pt **)

let derivable_pt_minus f1 f2 x x0 x1 =
  coq_Rminus x0 x1

(** val derivable_pt_mult :
    (coq_R -> coq_R) -> (coq_R -> coq_R) -> coq_R -> derivable_pt ->
    derivable_pt -> derivable_pt **)

let derivable_pt_mult f1 f2 x x0 x1 =
  coq_Rplus (coq_Rmult x0 (f2 x)) (coq_Rmult (f1 x) x1)

(** val derivable_pt_const : coq_R -> coq_R -> derivable_pt **)

let derivable_pt_const a x =
  coq_R0

(** val derivable_pt_scal :
    (coq_R -> coq_R) -> coq_R -> coq_R -> derivable_pt -> derivable_pt **)

let derivable_pt_scal f1 a x x0 =
  coq_Rmult a x0

(** val derivable_pt_id : coq_R -> derivable_pt **)

let derivable_pt_id x =
  coq_R1

(** val derivable_pt_Rsqr : coq_R -> derivable_pt **)

let derivable_pt_Rsqr x =
  coq_Rmult (coq_Rplus coq_R1 coq_R1) x

(** val derivable_pt_comp :
    (coq_R -> coq_R) -> (coq_R -> coq_R) -> coq_R -> derivable_pt ->
    derivable_pt -> derivable_pt **)

let derivable_pt_comp f1 f2 x x0 x1 =
  coq_Rmult x1 x0

(** val derivable_plus :
    (coq_R -> coq_R) -> (coq_R -> coq_R) -> derivable -> derivable ->
    derivable **)

let derivable_plus f1 f2 x x0 x1 =
  derivable_pt_plus f1 f2 x1 (x x1) (x0 x1)

(** val derivable_opp : (coq_R -> coq_R) -> derivable -> derivable **)

let derivable_opp f x x0 =
  derivable_pt_opp f x0 (x x0)

(** val derivable_minus :
    (coq_R -> coq_R) -> (coq_R -> coq_R) -> derivable -> derivable ->
    derivable **)

let derivable_minus f1 f2 x x0 x1 =
  derivable_pt_minus f1 f2 x1 (x x1) (x0 x1)

(** val derivable_mult :
    (coq_R -> coq_R) -> (coq_R -> coq_R) -> derivable -> derivable ->
    derivable **)

let derivable_mult f1 f2 x x0 x1 =
  derivable_pt_mult f1 f2 x1 (x x1) (x0 x1)

(** val derivable_const : coq_R -> derivable **)

let derivable_const a x =
  derivable_pt_const a x

(** val derivable_scal :
    (coq_R -> coq_R) -> coq_R -> derivable -> derivable **)

let derivable_scal f a x x0 =
  derivable_pt_scal f a x0 (x x0)

(** val derivable_id : derivable **)

let derivable_id x =
  derivable_pt_id x

(** val derivable_Rsqr : derivable **)

let derivable_Rsqr x =
  derivable_pt_Rsqr x

(** val derivable_comp :
    (coq_R -> coq_R) -> (coq_R -> coq_R) -> derivable -> derivable ->
    derivable **)

let derivable_comp f1 f2 x x0 x1 =
  derivable_pt_comp f1 f2 x1 (x x1) (x0 (f1 x1))

(** val pow_fct : nat -> coq_R -> coq_R **)

let pow_fct n y =
  pow y n

(** val derivable_pt_pow : nat -> coq_R -> derivable_pt **)

let derivable_pt_pow n x =
  coq_Rmult (coq_INR n) (pow x (pred n))

(** val derivable_pow : nat -> derivable **)

let derivable_pow n x =
  derivable_pt_pow n x

