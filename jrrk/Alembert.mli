open Datatypes
open PartSum
open Raxioms
open Rbasic_fun
open Rdefinitions
open Rpow_def
open Specif

val coq_Alembert_C1 : (nat -> coq_R) -> coq_R

val coq_Alembert_C2 : (nat -> coq_R) -> coq_R

val coq_AlembertC3_step1 : (nat -> coq_R) -> coq_R -> coq_R

val coq_AlembertC3_step2 : (nat -> coq_R) -> coq_R -> coq_R

val coq_Alembert_C3 : (nat -> coq_R) -> coq_R -> coq_R

val coq_Alembert_C4 : (nat -> coq_R) -> coq_R -> coq_R

val coq_Alembert_C5 : (nat -> coq_R) -> coq_R -> coq_R

val coq_Alembert_C6 : (nat -> coq_R) -> coq_R -> coq_R -> coq_R

