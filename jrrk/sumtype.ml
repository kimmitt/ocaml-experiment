
type sumtype =
    Inl of int
  | Inr of int

let rec processSum x = (match x with
    Inl n -> n
  | Inr n -> if n = 0 then 1 else 0)

let pair = (processSum (Inl 5), processSum (Inr 5))
;;
pair