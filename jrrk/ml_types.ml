(* type inference/reconstruction *)

open Ml_decls

exception Unify of type_t * type_t
exception Error of syntax_t * type_t * type_t

module M =
  Map.Make
    (struct
      type t = string
      let compare = Mylib.compare
    end)
include M

let add_list xys env = List.fold_left (fun env (x, y) -> add x y env) env xys
let add_list2 xs ys env = List.fold_left2 (fun env x y -> add x y env) env xs ys

let extenv = ref M.empty

(* for pretty printing (and type normalization) *)
let rec deref_typ = function
  | TFun(t1s, t2) -> TFun(List.map deref_typ t1s, deref_typ t2)
  | TTuple(ts) -> TTuple(List.map deref_typ ts)
  | TArray(t) -> TArray(deref_typ t)
  | TVar({ contents = None } as r) ->
      Format.eprintf "uninstantiated type variable detected; assuming int@.";
      r := Some(TInt);
      TInt
  | TVar({ contents = Some(t) } as r) ->
      let t' = deref_typ t in
      r := Some(t');
      t'
  | t -> t
let rec deref_id_typ (x, t) = (x, deref_typ t)
let rec deref_term = function
  | Not(e) -> Not(deref_term e)
  | Neg(e) -> Neg(deref_term e)
  | Add(e1, e2) -> Add(deref_term e1, deref_term e2)
  | Sub(e1, e2) -> Sub(deref_term e1, deref_term e2)
  | Mul(e1, e2) -> Mul(deref_term e1, deref_term e2)
  | Div(e1, e2) -> Div(deref_term e1, deref_term e2)
  | Mod(e1, e2) -> Mod(deref_term e1, deref_term e2)
  | Eq(e1, e2) -> Eq(deref_term e1, deref_term e2)
  | LE(e1, e2) -> LE(deref_term e1, deref_term e2)
  | FNeg(e) -> FNeg(deref_term e)
  | FAdd(e1, e2) -> FAdd(deref_term e1, deref_term e2)
  | FSub(e1, e2) -> FSub(deref_term e1, deref_term e2)
  | FMul(e1, e2) -> FMul(deref_term e1, deref_term e2)
  | FDiv(e1, e2) -> FDiv(deref_term e1, deref_term e2)
  | If(e1, e2, e3) -> If(deref_term e1, deref_term e2, deref_term e3)
  | Let(xt, e1, e2) -> Let(deref_id_typ xt, deref_term e1, deref_term e2)
  | LetRec({ name = xt; args = yts; body = e1 }, e2) ->
      LetRec({ name = deref_id_typ xt;
	       args = List.map deref_id_typ yts;
	       body = deref_term e1 },
	     deref_term e2)
  | App(e, es) -> App(deref_term e, List.map deref_term es)
  | Tuple(es) -> Tuple(List.map deref_term es)
  | LetTuple(xts, e1, e2) -> LetTuple(List.map deref_id_typ xts, deref_term e1, deref_term e2)
  | Abstraction(e1, e2) -> Abstraction(deref_term e1, deref_term e2)
  | Compose(e1, e2) -> Compose(deref_term e1, deref_term e2)
  | Declare(e1, e2, e3) -> Declare(deref_term e1, deref_term e2, deref_term e3)
  | e -> e

let rec occur r1 = function (* occur check (caml2html: typing_occur) *)
  | TFun(t2s, t2) -> List.exists (occur r1) t2s || occur r1 t2
  | TTuple(t2s) -> List.exists (occur r1) t2s
  | TArray(t2) -> occur r1 t2
  | TVar(r2) when r1 == r2 -> true
  | TVar({ contents = None }) -> false
  | TVar({ contents = Some(t2) }) -> occur r1 t2
  | _ -> false

let rec unify t1 t2 = (* 型が合うように、型変数への代入をする (caml2html: typing_unify) *)
  match t1, t2 with
  | TUnit, TUnit | TBool, TBool | TInt, TInt | TFloat, TFloat -> ()
  | TFun(t1s, t1'), TFun(t2s, t2') ->
      (try List.iter2 unify t1s t2s
      with Invalid_argument("List.iter2") -> raise (Unify(t1, t2)));
      unify t1' t2'
  | TTuple(t1s), TTuple(t2s) ->
      (try List.iter2 unify t1s t2s
      with Invalid_argument("List.iter2") -> raise (Unify(t1, t2)))
  | TArray(t1), TArray(t2) -> unify t1 t2
  | TVar(r1), TVar(r2) when r1 == r2 -> ()
  | TVar({ contents = Some(t1') }), _ -> unify t1' t2
  | _, TVar({ contents = Some(t2') }) -> unify t1 t2'
  | TVar({ contents = None } as r1), _ -> (* 一方が未定義の型変数の場合 (caml2html: typing_undef) *)
      if occur r1 t2 then raise (Unify(t1, t2));
      r1 := Some(t2)
  | _, TVar({ contents = None } as r2) ->
      if occur r2 t1 then raise (Unify(t1, t2));
      r2 := Some(t1)
  | _, _ -> raise (Unify(t1, t2))

let rec g env e = (* 型推論ルーチン (caml2html: typing_g) *)
  try
    match e with
    | Unit -> TUnit
    | Bool(_) -> TBool
    | Char(_) -> TChar
    | String(_) -> TString
    | Int(_) -> TInt
    | Float(_) -> TFloat
    | Not(e) ->
	unify TBool (g env e);
	TBool
    | Neg(e) ->
	unify TInt (g env e);
	TInt
    | Add(e1, e2) | Sub(e1, e2) | Mul(e1, e2) | Div(e1, e2) | Mod(e1, e2) -> (* typing_add *)
	unify TInt (g env e1);
	unify TInt (g env e2);
	TInt
    | FNeg(e) ->
	unify TFloat (g env e);
	TFloat
    | FAdd(e1, e2) | FSub(e1, e2) | FMul(e1, e2) | FDiv(e1, e2) ->
	unify TFloat (g env e1);
	unify TFloat (g env e2);
	TFloat
    | Eq(e1, e2) | LE(e1, e2) | LT(e1, e2) | GE(e1, e2) | GT(e1, e2) ->
	unify (g env e1) (g env e2);
	TBool
    | If(e1, e2, e3) ->
	unify (g env e1) TBool;
	let t2 = g env e2 in
	let t3 = g env e3 in
	unify t2 t3;
	t2
    | Let((x, t), e1, e2) -> (* letの型推論 (caml2html: typing_let) *)
	unify t (g env e1);
	g (add x t env) e2
    | Var(x) when M.mem x env -> M.find x env (* 変数の型推論 (caml2html: typing_var) *)
    | Var(x) when M.mem x !extenv -> M.find x !extenv
    | Var(x) -> (* 外部変数の型推論 (caml2html: typing_extvar) *)
	Format.eprintf "free variable %s assumed as external@." x;
	let t = gentyp () in
	extenv := add x t !extenv;
	t
    | LetRec({ name = (x, t); args = yts; body = e1 }, e2) -> (* let recの型推論 (caml2html: typing_letrec) *)
	let env = add x t env in
	unify t (TFun(List.map snd yts, g (add_list yts env) e1));
	g env e2
    | App(e, es) -> (* 関数適用の型推論 (caml2html: typing_app) *)
	let t = gentyp () in
	unify (g env e) (TFun(List.map (g env) es, t));
	t
    | Tuple(es) -> TTuple(List.map (g env) es)
    | LetTuple(xts, e1, e2) ->
	unify (TTuple(List.map snd xts)) (g env e1);
	g (add_list xts env) e2
    | Abstraction(e1, e2) -> (* must be a primitive for "polymorphic" typing *)
	unify (g env e1) TInt;
	TArray(g env e2)
    | Compose(e1, e2) ->
	let t = gentyp () in
	unify (TArray(t)) (g env e1);
	unify TInt (g env e2);
	t
    | Declare(e1, e2, e3) ->
	let t = g env e3 in
	unify (TArray(t)) (g env e1);
	unify TInt (g env e2);
	TUnit
    | Match _ -> TUnit
    | List _ -> TUnit
    | TypCons _ -> TUnit
    | ArgCons _ -> TUnit
    | INTEGER _ -> TUnit
    | STRING _ -> TUnit
    | LISTCONS _ -> TUnit
    | TYPCONS (_, _) -> TUnit
    | TOP _ -> TUnit
    | STR _ -> TUnit
    | ANY _ -> TUnit
    | LBL (_, _) -> TUnit
    | TYPE _ -> TUnit
    | TYPDECL _ -> TUnit
    | REC _ -> TUnit
    | NONREC _ -> TUnit
    | LET (_, _) -> TUnit
    | PATTERN (_, _) -> TUnit
    | MATCH (_, _) -> TUnit
    | PATEXP (_, _) -> TUnit
    | TUPLE _ -> TUnit
    | FORMAL (_, _) -> TUnit
    | TYPLOC (_, _) -> TUnit
    | FUNARG _ -> TUnit
    | PATCASE (_, _, _) -> TUnit
    | CONSTRAINED (_, _) -> TUnit
    | APPLY1 (_, _) -> TUnit
    | APPLY2 (_, _, _) -> TUnit
    | APPLYLST (_, _) -> TUnit
  with Unify(t1, t2) -> raise (Error(deref_term e, deref_typ t1, deref_typ t2))

let f e =
  extenv := M.empty;
(*
  (match deref_typ (g M.empty e) with
  | TUnit -> ()
  | _ -> Format.eprintf "warning: final result does not have type unit@.");
*)
  (try unify TUnit (g M.empty e)
  with Unify _ -> failwith "top level does not have type unit");
  extenv := M.map deref_typ !extenv;
  deref_term e
