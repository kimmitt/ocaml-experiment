open BinInt
open BinNums
open Datatypes
open Fcore_Raux
open Fcore_Zaux
open Fcore_defs
open Rdefinitions

type __ = Obj.t

val canonic_exp : radix -> (coq_Z -> coq_Z) -> coq_R -> coq_Z

val scaled_mantissa : radix -> (coq_Z -> coq_Z) -> coq_R -> coq_R

type coq_Valid_rnd =
| Build_Valid_rnd

val coq_Valid_rnd_rect : (coq_R -> coq_Z) -> (__ -> __ -> 'a1) -> 'a1

val coq_Valid_rnd_rec : (coq_R -> coq_Z) -> (__ -> __ -> 'a1) -> 'a1

val round : radix -> (coq_Z -> coq_Z) -> (coq_R -> coq_Z) -> coq_R -> coq_R

val coq_Zrnd_opp : (coq_R -> coq_Z) -> coq_R -> coq_Z

val coq_Znearest : (coq_Z -> bool) -> coq_R -> coq_Z

