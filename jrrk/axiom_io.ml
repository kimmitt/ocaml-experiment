open BinNums
open Datatypes
open BinNat
open BinPos
open BinInt
open BinNums
open Mylib
open Ml_decls
open Fappli_IEEE
open To_from_Z

let asNi = function
  | 0 -> N0
  | n -> Npos (asPi(n))

(** val axiom_io : nat -> coq_Z -> coq_Z **)
  
let axiom_io sel arg =
  let iosel = (init_codes()).io in
  if EqNat.beq_nat sel iosel.char_of_int then arg
  else if EqNat.beq_nat sel iosel.print_char then (print_char (char_of_int (fromZi arg)); arg)
  else if EqNat.beq_nat sel iosel.exn_failwith then failwith "failwith _"
  else failwith ("axiom_io: "^string_of_int (fromNat sel)^" - AXIOM TO BE REALIZED")

(** val big_endian : bool **)

let big_endian = false

type coq_float = float (* coq_float: AXIOM TO BE REALIZED *)

  (** val zero : float **)
  
  let zero = B754_zero false (* failwith "zero: AXIOM TO BE REALIZED" *)
  
  (** val eq_dec : float -> float -> bool **)
  
  let eq_dec _ _ = failwith "eq_dec: AXIOM TO BE REALIZED"
  
  (** val neg : float -> float **)
  
  let neg _ = failwith "neg: AXIOM TO BE REALIZED"
  
  (** val abs : float -> float **)
  
  let abs _ =
    failwith "AXIOM TO BE REALIZED"
  
  (** val singleoffloat : float -> float **)
  
  let singleoffloat _ =
    failwith "AXIOM TO BE REALIZED"
  
  (** val intoffloat : float -> Int.int option **)
  
  let intoffloat _ =
    failwith "AXIOM TO BE REALIZED"
  
  (** val intuoffloat : float -> Int.int option **)
  
  let intuoffloat _ =
    failwith "AXIOM TO BE REALIZED"
  
  (** val floatofint : Int.int -> float **)
  
  let floatofint _ =
    failwith "AXIOM TO BE REALIZED"
  
  (** val floatofintu : Int.int -> float **)
  
  let floatofintu _ =
    failwith "AXIOM TO BE REALIZED"
  
  (** val add : float -> float -> float **)
  
  let add _ _ =
    failwith "AXIOM TO BE REALIZED"
  
  (** val sub : float -> float -> float **)
  
  let sub _ _ =
    failwith "AXIOM TO BE REALIZED"
  
  (** val mul : float -> float -> float **)
  
  let mul _ _ =
    failwith "AXIOM TO BE REALIZED"
  
  (** val div : float -> float -> float **)
  
  let div _ _ =
    failwith "AXIOM TO BE REALIZED"
  
  (** val cmp : comparison -> float -> float -> bool **)
  
  let cmp _ _ =
    failwith "AXIOM TO BE REALIZED"
  
  (** val bits_of_double : float -> Int64.int **)
  
  let bits_of_double _ =
    failwith "AXIOM TO BE REALIZED"
  
  (** val double_of_bits : Int64.int -> float **)
  
  let double_of_bits _ =
    failwith "AXIOM TO BE REALIZED"
  
  (** val bits_of_single : float -> Int.int **)
  
  let bits_of_single _ =
    failwith "AXIOM TO BE REALIZED"
  
  (** val single_of_bits : Int.int -> float **)
  
  let single_of_bits _ =
    failwith "AXIOM TO BE REALIZED"
  
