open BinNums
open BinPos
open Mylib
open Fappli_IEEE
open Fappli_IEEE_bits
(*
open Axiom_io
*)
open BinPos
open Ml_decls
open To_from_Z

let string_of_float_b754' dec top = function
  | Fappli_IEEE.B754_finite (sgn,mant,expon) ->
    let five = asPi 5 in
    let rec adjust binscale dec mant =
      if dec > 0 then adjust binscale (dec-1) (Pos.shiftr_nat (Pos.mul five mant) binscale) else mant in
    let binscale = (top-dec)/dec in
    let adj' = adjust (asNat binscale) dec mant in
    let shft = -(binscale+1)*dec-(fromZi expon) in
    let adj = fromZ (Zpos (if shft < 0 then Pos.shiftl_nat adj' (asNat(-shft)) else Pos.shiftr_nat adj' (asNat shft))) in
    let len' = dec - String.length adj in
    let pad = (if len' > 0 then String.make len' '0' else "")^adj in
    let len = String.length pad in
    let split1 = String.sub pad 0 (len - dec) in
    let split2 = String.sub pad (len - dec) dec in
    let split = split1^"."^split2 in
    if sgn then "-"^split else split
  | B754_zero _ -> "0.0"
  | B754_infinity _ -> "inf"
  | B754_nan (_, _) -> "nan"

let string_of_float_b754 mode = if mode then string_of_float_b754' 20 20 else string_of_float_b754' 6 22

let cnv mode sgn x =
  let rec shft n = if n < 2 then 0 else 1 + shft (n lsr 1) in
  if x = 0 then Fappli_IEEE.B754_zero sgn
  else let shft' = (if mode then 52 else 23) - shft x in Fappli_IEEE.B754_finite (sgn, Pos.shiftl_nat (asPi x) (asNat shft'), Zneg ( asPi shft'))

let float_of_string_b754 mode str =
  let one = cnv mode false 1 in
  let ten = cnv mode false 10 in
  let nan = Fappli_IEEE.B754_nan(false,Coq_xH) in
  let rslt = ref (Fappli_IEEE.B754_zero false) and dotseen = ref false and frac = ref one in
  let sign = str.[0] = '-' in
  let (plus,minus,mult,div) = if mode then (b64_plus,b64_minus,b64_mult,b64_div) else (b32_plus,b32_minus,b32_mult,b32_div) in
  for i = if sign then 1 else 0 to String.length str - 1 do
    match str.[i] with
    | '0' ..'9' as dig -> let dig' = cnv mode sign (Char.code dig - Char.code '0') in
			 if !dotseen then
                           (frac := div Coq_mode_ZR !frac ten; rslt := plus Coq_mode_ZR !rslt (mult Coq_mode_ZR dig' !frac))
			 else
                           rslt := plus Coq_mode_ZR (mult Coq_mode_ZR !rslt ten) dig'
    | '.' -> dotseen := true
    | _ -> invalid_arg "float_of_string"
  done;
  !rslt

let float_of_int_b754 mode n = if n < 0 then
  cnv mode true (-n)
    else 
  cnv mode false n
