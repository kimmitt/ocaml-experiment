let _ = output stdout "Hello, world\n" 0 13

let rec caml_print_int n = if n > 9 then caml_print_int (n/10); print_char (Char.unsafe_chr (n mod 10 + Char.code '0'))
let caml_print_int n = if n < 0 then (print_char '-'; caml_print_int (0-n)) else caml_print_int n

let fact n =
  let rec factorial m n =
  if (n) < 1 then m
  else factorial (m*n) (n-1) in
  factorial 1 n

let _ = for i = 0 to 10 do caml_print_int i; print_char '\t';caml_print_int (fact i); print_char '\n'; done
