open BinNums
open Mylib
open Fappli_IEEE
open Fappli_IEEE_bits
open Axiom_io
open String_of_float

module Obj = struct
   type t
   external repr : 'a -> t = "%identity"
   external obj : t -> 'a = "%identity"
   external magic : 'a -> 'b = "%identity"
   external tag : t -> int = "caml_obj_tag"
   external set_tag : t -> int -> unit = "caml_obj_set_tag"
   external new_block : int -> int -> t = "caml_obj_block"
   external size : t -> int = "%obj_size"
   external field : t -> int -> t = "%obj_field"
   external set_field : t -> int -> t -> unit = "%obj_set_field"

   let lazy_tag = 246
   let closure_tag = 247
   let object_tag = 248
   let infix_tag = 249
   let forward_tag = 250

   let no_scan_tag = 251

   let abstract_tag = 251
   let string_tag = 252
   let double_tag = 253
   let double_array_tag = 254
   let custom_tag = 255
   let final_tag = custom_tag
end

external native_int64_bits_of_float : float -> int64 = "caml_int64_bits_of_float"
external native_int64_float_of_bits : int64 -> float = "caml_int64_float_of_bits"
external native_int32_bits_of_float : float -> int32 = "caml_int32_bits_of_float"
external native_int32_float_of_bits : int32 -> float = "caml_int32_float_of_bits"
external unsafe_string : int -> string = "%string_create"

let set_double_field x i v = Array.set (Obj.obj x : float array) i v

let caml_int64_bits_of_float (f:float) : int64 =
  let int64_object = 0L in
  let rslt = Obj.new_block Obj.custom_tag 2 in
  Obj.set_field (Obj.repr rslt) 0 (Obj.field (Obj.repr int64_object) 0);
  set_double_field (Obj.repr rslt) 1 f;
  let (int64:int64) = Obj.magic rslt in
  int64

let caml_int64_float_of_bits (n:int64) : float =
  let rslt = Obj.new_block Obj.double_tag 1 in
  Obj.set_field (Obj.repr rslt) 0 (Obj.field (Obj.repr n) 1);
  (Obj.obj rslt)

type expflt = {sgn:int; expon:int; mant:int;}
type expdbl = {sgn64:int; expon64:int; mant64:int64;}
type spec_flt =
  | Inf of int
  | Nan
  | Max_flt
  | Min_flt
  | Eps_flt
  | Reg_flt of expdbl

let unextract32' sgn expon mant =
  let mant' = Int32.of_int (mant land 0x7FFFFF) in
  let expon' = if expon < -128 then -128 else if expon > 127 then 127 else expon in
  let expon'' = Int32.shift_left (Int32.of_int ((sgn lsl 8) lor (expon' + 128))) 23 in
  Int32.logor expon'' mant'

let extract32' (i:int32) =
  let nonmant = Int32.to_int (Int32.shift_right_logical i 23) in
  let expon = nonmant land 0xFF in
  let sgn = (nonmant lsr 8) land 1 in
  let mant = ((Int32.to_int i) land 0x7FFFFF) in
  {sgn=sgn;expon=expon-128;mant=if expon > 0 then mant lor 0x800000 else mant lsl 1}

let unextract64' sgn expon mant =
  let mant' = Int64.logand mant 0xFFFFFFFFFFFFFL in
  let expon' = Int64.shift_left (Int64.of_int ((sgn lsl 11) lor (expon + 1024))) 52 in
  Int64.logor expon' mant'

let extract64' (i:int64) =
  match i with
  | 0x7F_F0_00_00_00_00_00_00L -> Inf 0
  | 0xFF_F0_00_00_00_00_00_00L -> Inf 1
  | 0x7F_F0_00_00_00_00_00_01L -> Nan
  | 0x7F_EF_FF_FF_FF_FF_FF_FFL -> Max_flt
  | 0x00_10_00_00_00_00_00_00L -> Min_flt
  | 0x3C_B0_00_00_00_00_00_00L -> Eps_flt
  | _ ->
    let nonmant' = Int64.shift_right_logical i 52 in
    let nonmant = Int64.to_int nonmant' in
    let expon = nonmant land 0x7FF in
    let mant = Int64.logand i 0xFFFFFFFFFFFFFL in
    let sgn = (nonmant lsr 11) land 1 in
    Reg_flt {sgn64=sgn;expon64=expon - 1024;mant64=if expon > 0 then Int64.logor mant 0x10000000000000L else Int64.shift_left mant 1}

let untrunc_float' (i:int32) =
  let {sgn=sgn;expon=expon;mant=mant} = extract32' i in
  let mant' = Int64.of_int (mant land 0x7FFFFF) in
  let rnd = Int64.shift_left mant' 29 in
  let rec norm64' sgn expon mant =
    if mant < 0x4000000000000L then norm64' sgn (expon-1) (Int64.shift_left mant 1)
    else unextract64' sgn (-1024) mant' in
  match expon with
  | -128 -> if mant' = 0L then unextract64' sgn (-1024) mant' else norm64' sgn (-128) mant'
  | 127 -> unextract64' sgn 1023 mant'
  | _ -> unextract64' sgn expon rnd

let trunc_float' (i:int64) =
  match extract64' i with
  | Inf sgn -> unextract32' sgn 127 0
  | Nan -> unextract32' 0 127 1 
  | Max_flt -> unextract32' 0 127 (-1)
  | Min_flt -> unextract32' 0 1 (0x800000)
  | Eps_flt -> unextract32' 0 100 (0x800000)
  | Reg_flt {sgn64=sgn;expon64=expon;mant64=mant} ->
    let rnd = Int64.to_int (Int64.shift_right mant 28) in
    unextract32' sgn expon ((rnd lsr 1) + (rnd land 1))

let caml_int32_bits_of_float (f:float) : int32 = trunc_float' (caml_int64_bits_of_float f)

let caml_int32_float_of_bits (n:int32) : float = caml_int64_float_of_bits (untrunc_float' n)

let rec asPL = function
  | 0L -> failwith "not positive"
  | 1L -> Coq_xH
  | n -> let asp = asPL(Int64.shift_right_logical n 1) in if (Int64.compare (Int64.logand n 1L) 0L) > 0 then Coq_xI asp else Coq_xO asp

let asZL = function
  | 0L -> Z0
  | 1L -> Zpos Coq_xH
  | n -> if (Int64.compare n 0L) < 0 then Zneg (asPL(Int64.neg n)) else Zpos (asPL n)

let rec fromPL = function
  | Coq_xH -> 1L
  | Coq_xO num -> Int64.shift_left (fromPL num) 1
  | Coq_xI num -> Int64.logor (Int64.shift_left (fromPL num) 1) 1L

let fromZL = function
  | Z0 -> 0L
  | Zpos num -> fromPL num
  | Zneg num -> Int64.neg (fromPL num)

let rec asPl = function
  | 0l -> failwith "not positive"
  | 1l -> Coq_xH
  | n -> let asp = asPl(Int32.shift_right_logical n 1) in if (Int32.compare (Int32.logand n 1l) 0l) > 0 then Coq_xI asp else Coq_xO asp

let asZl = function
  | 0l -> Z0
  | 1l -> Zpos Coq_xH
  | n -> if (Int32.compare n 0l) < 0 then Zneg (asPl(Int32.neg n)) else Zpos (asPl n)

let rec fromPl = function
  | Coq_xH -> 1l
  | Coq_xO num -> Int32.shift_left (fromPl num) 1
  | Coq_xI num -> Int32.logor (Int32.shift_left (fromPl num) 1) 1l

let fromZl = function
  | Z0 -> 0l
  | Zpos num -> fromPl num
  | Zneg num -> Int32.neg (fromPl num)

let as_float32 x = (caml_int32_float_of_bits (fromZl (bits_of_b32 x)));;
let from_float32 x = b32_of_bits (asZl (caml_int32_bits_of_float x));;

let ( +. ) x y = as_float32 (b32_plus Coq_mode_ZR (from_float32 x) (from_float32 y));;
let ( -. ) x y = as_float32 (b32_minus Coq_mode_ZR (from_float32 x) (from_float32 y));;
let ( *. ) x y = as_float32 (b32_mult Coq_mode_ZR (from_float32 x) (from_float32 y));;
let ( /. ) x y = as_float32 (b32_div Coq_mode_ZR (from_float32 x) (from_float32 y));;

let as_float64 x = (caml_int64_float_of_bits (fromZL (bits_of_b64 x)));;
let from_float64 x = b64_of_bits (asZL (caml_int64_bits_of_float x));;
let tiny = as_float64 (B754_finite(false,Coq_xH,Zpos(Coq_xH)));;
let plus64 x y = as_float64 (b64_plus Coq_mode_ZR (from_float64 x) (from_float64 y));;
let minus64 x y = as_float64 (b64_minus Coq_mode_ZR (from_float64 x) (from_float64 y));;
let times64 x y = as_float64 (b64_mult Coq_mode_ZR (from_float64 x) (from_float64 y));;
let divide64 x y = as_float64 (b64_div Coq_mode_ZR (from_float64 x) (from_float64 y));;

let string_of_float f = string_of_float_b754 (from_float32 f)

let float_of_string str = as_float32 (float_of_string_b754 false str)
