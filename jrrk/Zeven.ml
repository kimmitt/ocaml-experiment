open BinInt
open BinNums
open Datatypes

(** val coq_Zeven_odd_dec : coq_Z -> bool **)

let coq_Zeven_odd_dec = function
| Z0 -> true
| Zpos p ->
  (match p with
   | Coq_xO p0 -> true
   | _ -> false)
| Zneg p ->
  (match p with
   | Coq_xO p0 -> true
   | _ -> false)

(** val coq_Zeven_dec : coq_Z -> bool **)

let coq_Zeven_dec = function
| Z0 -> true
| Zpos p ->
  (match p with
   | Coq_xO p0 -> true
   | _ -> false)
| Zneg p ->
  (match p with
   | Coq_xO p0 -> true
   | _ -> false)

(** val coq_Zodd_dec : coq_Z -> bool **)

let coq_Zodd_dec = function
| Z0 -> false
| Zpos p ->
  (match p with
   | Coq_xO p0 -> false
   | _ -> true)
| Zneg p ->
  (match p with
   | Coq_xO p0 -> false
   | _ -> true)

(** val coq_Z_modulo_2 : coq_Z -> (coq_Z, coq_Z) sum **)

let coq_Z_modulo_2 n =
  let s = coq_Zeven_odd_dec n in
  if s then Coq_inl (Z.div2 n) else Coq_inr (Z.div2 n)

(** val coq_Zsplit2 : coq_Z -> (coq_Z, coq_Z) prod **)

let coq_Zsplit2 n =
  let s = coq_Z_modulo_2 n in
  (match s with
   | Coq_inl s0 -> Coq_pair (s0, s0)
   | Coq_inr s0 -> Coq_pair (s0, (Z.add s0 (Zpos Coq_xH))))

