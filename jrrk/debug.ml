open Mylib
open Ml_decls
open To_from_Z
open String_of_float
open BinNums
open BinInt
open Fappli_IEEE
open Fappli_IEEE_bits

let rec dumpP = function
  | Coq_xH -> "xH"
  | Coq_xO num -> dumpP num^"_xO"
  | Coq_xI num -> dumpP num^"_xI"

let dumpZ = function
  | Z0 -> "zero"
  | Zpos num -> dumpP num
  | Zneg num -> "neg("^dumpP num^")"

let extb754 = function
  | B754_finite (a, b, c) -> string_of_bool a^" : "^dumpP b^" : "^dumpZ c
  | B754_zero arg1 -> "B754_zero "^string_of_bool arg1
  | B754_infinity arg1 -> "B754_infinity "^string_of_bool arg1
  | B754_nan (arg1, arg2) -> "B754_nan("^string_of_bool arg1^", "^dumpP arg2^")"

let debug_float f = string_of_float_b754 true f^" = "^extb754 f

let one = float_of_int_b754 true 1

let ( +! ) = Z.add
let ( -! ) = Z.sub
let ( *! ) = Z.mul
let ( /! ) = Z.div
let ( ~-! ) = Z.sub Z0
let ( +. ) = Fappli_IEEE_bits.b64_plus Fappli_IEEE.Coq_mode_ZR
let ( -. ) = Fappli_IEEE_bits.b64_minus Fappli_IEEE.Coq_mode_ZR
let ( *. ) = Fappli_IEEE_bits.b64_mult Fappli_IEEE.Coq_mode_ZR
let ( /. ) = Fappli_IEEE_bits.b64_div Fappli_IEEE.Coq_mode_ZR
let ( ~-. ) = Fappli_IEEE_bits.b64_minus Fappli_IEEE.Coq_mode_ZR (B754_zero false)

let rec fact n = if n > 0 then (float_of_int_b754 true n) *. fact(n-1) else one
let fact3 n = let f3 = fact n in f3 *. f3 *. f3

let rec oneoverpi cnt sgn cube k = 
  (if cnt > 0 then oneoverpi (cnt-1) (-.sgn /. cube) cube (k+1) else B754_zero false) +.
      sgn *. (fact (6*k)) *. float_of_int_b754 true (163 * 3344418 * k + 13591409) /. ((fact (3*k)) *. fact3 k)

let ramanujan = float_of_int_b754 true 640320
let cube = ramanujan *. ramanujan *. ramanujan
let _ = print_endline (string_of_float_b754 true cube)
let frac = float_of_string_b754 true "12." /. (b64_sqrt Fappli_IEEE.Coq_mode_ZR cube)
let _ = for i = 0 to 2 do
  let iter = one /. oneoverpi i frac cube 0 in
  print_endline (string_of_float_b754 true iter)
done
