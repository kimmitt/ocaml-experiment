open Datatypes
open Specif

val coq_O_or_S : nat -> nat sumor

val eq_nat_dec : nat -> nat -> bool

